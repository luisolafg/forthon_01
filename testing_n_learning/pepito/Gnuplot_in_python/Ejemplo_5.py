# Ejemplo_5 
# Crea una grafica de sin(1/x)
# Con posibilidad para exportar a png

import numpy as np
import Gnuplot

g = Gnuplot.Gnuplot(persist=1)
#g = Gnuplot.Gnuplot()
g.title('My Favorite Plot')
g.xlabel('x')
g.ylabel('sin(1/x)')

ax = np.arange(10001)
x=[]
y=[]

for i in ax:
    if i!=5000:
        i=i+0.0
        i=(i/50000)-0.1
        x.append(i)
    else:
        pass

for j in x:
    j =np-sin(1/j)
    y.append(j)

#Empaquetando para graficar
d = Gnuplot.Data(x, y, with_="l", title= "sin (1/x)")

#Mandando a graficar
g.plot(d)

#Para generar un archivo png
#g('set term pngcairo')
#g('set out "GnuplotFromPython.png"')
#g('plot sin(1/x)')