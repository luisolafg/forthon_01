MODULE DATA
    IMPLICIT NONE
    REAL(KIND = 8), PARAMETER :: g  = 9.81
    REAL(KIND = 8), PARAMETER :: PI = 3.1415926
    REAL(kind = 8)  :: Angle
    REAL(kind = 8)  :: Time
    REAL(kind = 8)  :: Theta
    REAL(kind = 8)  :: Vi
    REAL(kind = 8)  :: V
    REAL(kind = 8)  :: Vx
    REAL(kind = 8)  :: Vy
    REAL(kind = 8)  :: X
    REAL(kind = 8)  :: Y
    
END MODULE DATA

PROGRAM MAIN
CALL Projectile

END PROGRAM MAIN

SUBROUTINE  Projectile
    USE DATA
    IMPLICIT    NONE

    WRITE(*,*) "GIVE ME LUNCHING ANGLE"
    READ(*,*)  Angle
    !WRITE(*,*) "GIVE ME FLIGHT TIME"
    !READ(*,*)  Time
    WRITE(*,*) "GIVE ME LUNCHING VELOCITY"
    READ(*,*)  Vi


    Angle = Angle * PI / 180.0
    Time = ((Vi * SIN(Angle)) / g) * 2
    X     = Vi * COS(Angle) * Time
    Y     = ((Vi * SIN(Angle)) ** 2) / (2 * g)
    Vx    = Vi * COS(Angle)
    Vy    = Vi * SIN(Angle) - g * Time
    V     = SQRT(Vx*Vx + Vy*Vy)
    Theta = ATAN(Vy/Vx) * 180.0 / PI

    !WRITE(*,*) Angle, Time

    WRITE(*,*)  'Horizontal displacement : ', X
    WRITE(*,*)  'Vertical displacement   : ', Y
    WRITE(*,*)  'Resultant velocity      : ', V
    WRITE(*,*)  'Direction (in degree)   : ', Theta

END SUBROUTINE  Projectile
