import projectile_f2py

print " Calculation of a projectile displacement given initial parameters"
print projectile_f2py.outside.projectile.__doc__
angle = float(raw_input(" Give me the initial angle in degrees : "))
vi    = float(raw_input(" Give me the total initial velocity of the projectile : "))
projectile_f2py.outside.projectile(angle, vi)

x     = projectile_f2py.outside.x
y     = projectile_f2py.outside.y
v     = projectile_f2py.outside.v
theta = projectile_f2py.outside.theta
time  = projectile_f2py.outside.time

print
print ' Horizontal displacement : ', x
print ' Vertical displacement   : ', y
print ' Resultant velocity      : ', v
print ' Direction (in degrees)  : ', theta
print ' Flight time             : ', time
print
