module common
implicit none
real :: x,y=3.
end module

program test
implicit none
call sub1
call sub2
call sub3
end

subroutine sub1
use common, only: x
implicit none
real :: y
x=4.
y=2.
write(*,*) x,y
end

subroutine sub2
use common, only: x
implicit none
write(*,*) x
x=9.
end

subroutine sub3
use common
implicit none
write(*,*) x, y
end
