echo "removing previous .so builds"
rm *.so *.pyc
echo "testing projectile_f2py"
f2py -c -m projectile_f2py projectile_f2py.f90
echo "running projectile_f2py_call.py"
python projectile_f2py_call.py
rm *.so *.pyc
