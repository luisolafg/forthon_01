MODULE outside

    IMPLICIT NONE
    REAL(KIND = 8), PARAMETER :: g  = 9.81
    REAL(KIND = 8), PARAMETER :: pi = 3.1415926
    REAL(kind = 8)            :: time
    REAL(kind = 8)            :: theta
    REAL(kind = 8)            :: V
    REAL(kind = 8)            :: vx
    REAL(kind = 8)            :: vy
    REAL(kind = 8)            :: x
    REAL(kind = 8)            :: y
    CONTAINS

    SUBROUTINE projectile(angle, vi)

        REAL(kind = 8)         :: angle
        REAL(kind = 8)         :: vi

        angle = angle * pi / 180.0
        time  = ((vi * SIN(angle)) / g) * 2
        x     = vi * COS(angle) * time
        y     = ((vi * SIN(angle)) ** 2) / (2 * g)
        vx    = vi * COS(angle)
        vy    = vi * SIN(angle) - g * time
        v     = SQRT(vx*vx+ vy*vy)
        theta = ATAN(vy/vx) * 180.0 / pi

    END SUBROUTINE  projectile

END MODULE outside
