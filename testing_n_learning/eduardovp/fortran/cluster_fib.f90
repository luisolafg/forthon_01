PROGRAM PIEZOFIB

CHARACTER(LEN=100) :: arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9, arg10, arg11
CHARACTER(LEN=100) :: arg12, arg13, arg14, arg15, arg16, arg17, arg18, arg19, arg20, arg21, arg22 
REAL (KIND=8), DIMENSION (3, 6)  :: dmat
REAL (KIND=8), DIMENSION (3,3,3) :: dtens
REAL (KIND=8), DIMENSION (3,3)   :: Q
REAL (KIND=8), DIMENSION (3,3,3) :: drotten, dtexten
REAL (KIND=8), DIMENSION (3,6)   :: dtexmat
REAL (KIND=8)    :: phi1, phi, phi2, pi, beta, R
REAL (KIND=8)    :: a, b, c, d, dphi, dbeta, omeg, su, mh, mk, ml
INTEGER (KIND=8) :: i, j, k, l, m, n, np, nphi, nb, nbeta 

!R(phi,beta)= (4.d0 * pi/su) * exp(-(180.d0*phi/(pi*omeg))**2) * sin(phi)
R(phi,beta)= (1.d0/su) * exp(-(180.d0*phi/(pi*omeg))**2) * sin(phi)

nphi = 360; nbeta = 720
pi = 3.141592653589793D+00

a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi
dphi= (b-a)/dble(nphi); dbeta=(d-c)/dble(nbeta)

!call system('cls')
write(*,'(/A)')' Piezoelectricity for axial textures'


call GET_COMMAND_ARGUMENT(1,arg1)
call GET_COMMAND_ARGUMENT(2,arg2)
call GET_COMMAND_ARGUMENT(3,arg3)
call GET_COMMAND_ARGUMENT(4,arg4)
call GET_COMMAND_ARGUMENT(5,arg5)
call GET_COMMAND_ARGUMENT(6,arg6)
call GET_COMMAND_ARGUMENT(7,arg7)
call GET_COMMAND_ARGUMENT(8,arg8)
call GET_COMMAND_ARGUMENT(9,arg9)
call GET_COMMAND_ARGUMENT(10,arg10)
call GET_COMMAND_ARGUMENT(11,arg11)
call GET_COMMAND_ARGUMENT(12,arg12)
call GET_COMMAND_ARGUMENT(13,arg13)
call GET_COMMAND_ARGUMENT(14,arg14)
call GET_COMMAND_ARGUMENT(15,arg15)
call GET_COMMAND_ARGUMENT(16,arg16)
call GET_COMMAND_ARGUMENT(17,arg17)
call GET_COMMAND_ARGUMENT(18,arg18)
call GET_COMMAND_ARGUMENT(19,arg19)
call GET_COMMAND_ARGUMENT(20,arg20)
call GET_COMMAND_ARGUMENT(21,arg21)
call GET_COMMAND_ARGUMENT(22,arg22)

read( arg1, '(f10.0)' ) mh
read( arg2, '(f10.0)' ) mk
read( arg3, '(f10.0)' ) ml
read( arg4, '(f10.0)' ) omeg
read( arg5, '(f10.0)' ) dmat(1,1)
read( arg6, '(f10.0)' ) dmat(1,2)
read( arg7, '(f10.0)' ) dmat(1,3)
read( arg8, '(f10.0)' ) dmat(1,4)
read( arg9, '(f10.0)' ) dmat(1,5)
read( arg10, '(f10.0)' ) dmat(1,6)
read( arg11, '(f10.0)' ) dmat(2,1)
read( arg12, '(f10.0)' ) dmat(2,2)
read( arg13, '(f10.0)' ) dmat(2,3)
read( arg14, '(f10.0)' ) dmat(2,4)
read( arg15, '(f10.0)' ) dmat(2,5)
read( arg16, '(f10.0)' ) dmat(2,6)
read( arg17, '(f10.0)' ) dmat(3,1)
read( arg18, '(f10.0)' ) dmat(3,2)
read( arg19, '(f10.0)' ) dmat(3,3)
read( arg20, '(f10.0)' ) dmat(3,4)
read( arg21, '(f10.0)' ) dmat(3,5)
read( arg22, '(f10.0)' ) dmat(3,6)

call inte2D(omeg, su)

! Tensor diagonal elements
Do i=1,3
    Do j=1,3
        dtens(i,j,j)=dmat(i,j)
    End Do
End Do

! Off-diagonal tensor elements
Do i=1,3
    dtens(i,2,3)=0.5*dmat(i,4)
    dtens(i,3,2)=dtens(i,2,3)
    dtens(i,1,3)=0.5*dmat(i,5)
    dtens(i,3,1)=dtens(i,1,3)
    dtens(i,1,2)=0.5*dmat(i,6)
    dtens(i,2,1)=dtens(i,1,2)
End Do

phi1=0.d0   !Fibre texture, Bunge convention

! START THE GREAT LOOP FOR TENSOR COMPONENTS
Do i = 1, 3
   Do j = 1, 3
      Do k = 1,3
        dtexten(i,j,k) =0.d0  ! Setting to "0" the textured polycrystal piezotensor component:
           Do np =1, nphi  ! loop on polar angle
                phi = a + dphi/2.d0 + dble(np - 1)*dphi
                Do nb = 1, nbeta   ! loop on azimuth
                        beta = c + dbeta/2.d0 + dble(nb - 1)*dbeta
                        phi2= (pi/2.d0) - beta    ! Bunge convention
                        Q(1,1) =  cos(phi2)* cos(phi1) - cos(phi)*sin(phi1)*sin(phi2)
                        Q(1,2) =  cos(phi2)* sin(phi1) + cos(phi)*cos(phi1)*sin(phi2)
                        Q(1,3) =  sin(phi2)* sin(phi)
                        Q(2,1) = -sin(phi2)* cos(phi1) - cos(phi)*sin(phi1)*cos(phi2)
                        Q(2,2) = -sin(phi2)* sin(phi1) + cos(phi)*cos(phi1)*cos(phi2)
                        Q(2,3) =  cos(phi2)* sin(phi)
                        Q(3,1) =  sin(phi) * sin(phi1)
                        Q(3,2) = -sin(phi) * cos(phi1)
                        Q(3,3) =  cos(phi)
                        Do l=1,3
                           Do m = 1, 3
                              Do n =1, 3
                                 drotten(i,j,k) =0.d0  ! Setting to "0" a rotated crystal piezotensor component:
                                 End Do
                           End Do
                        End Do
                        Do l=1,3
                           Do m = 1, 3
                              Do n =1, 3
                                 drotten(i,j,k) = drotten(i,j,k) + Q(i,l)*Q(j,m)*Q(k,n)*dtens(l,m,n)
                                 !write(*,'(A)')' np,nb, l, m, n,    phi,     beta, drotten(i,j,k)'
                              !write(*,'(5I3,3F10.3)') np,nb, l,m,n, 180*phi/pi,180*beta/pi, drotten(i,j,k)
                              End Do
                           End Do
                        End Do
                 !write(*,*)'i,k,j,drotten(i,j,k)', i,k,j,drotten(i,j,k)
                 dtexten(i,j,k) = dtexten(i,j,k)+R(phi, beta)*drotten(i,j,k)*dphi*dbeta
                 End Do
           End Do
      End Do
   End Do
End Do

Do i = 1, 3
        Do j = 1, 3
            dtexmat(i,j)= dtexten(i,j,j)
        End Do
        dtexmat(i,4)=2.000*dtexten(i,2,3)
        dtexmat(i,5)=2.000*dtexten(i,1,3)
        dtexmat(i,6)=2.000*dtexten(i,1,2)
End Do
write(*,'(/A)') 'Textured polycrystal matrix:'
Do i = 1,3
         write (*,'(6F10.2)') (dtexmat(i,j), j =1,6)
End Do

End

subroutine inte2D(omega, suma)
  implicit none
  real (kind = 8):: a, b, c, d, pi, omega
  real (kind = 8):: x, y, hx, hy, f, suma
  integer (kind = 4) :: ii, jj, nx, ny

  f(x,y)= exp(-(180.d0*x/(pi*omega))**2) * sin(x)

  pi = 3.141592653589793D+00
  nx = 1024; ny = 2048
  a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi

  hx = (b-a)/dble(nx); hy = (d-c)/dble(ny)
  suma = 0.0D+00
  Do ii = 1, nx
     x = a + hx/2.d0 + dble(ii-1)*hx
     Do jj = 1, ny
     y = c + hy/2.d0 + dble(jj-1)*hy
     suma = suma + hx * hy * f (x, y)
     End Do
  End Do
Return
End
