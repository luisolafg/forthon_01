module commonvar

    implicit none
    real, allocatable :: hklnew(:, :)
    real :: h, k, l, pg
    real, dimension(3) :: hkl
    integer :: counting_dimension, counting_hklnew, multi
    integer :: m, n, o, i, j
    logical :: test(3), found
    character(len=5) :: Miller
    
end module commonvar

program pointgroups

    use commonvar
    implicit none
    
    write(*,'(/A$)')' Enter hkl: '
    read(*,*) h, k, l
    
    write(*,'(/A$)')' Enter point group as a number; from 1 to 32: '
    read(*,*) pg
    
    hkl = (/h,k,l/)
    
    write(*,*) " Resultant vectors are: "
    
    IF ( pg == 1 ) THEN 
        call C1_1
    ELSE IF ( pg == 2 ) THEN
        call Ci_2
    ELSE IF ( pg == 3 ) THEN
        call C2_3
    ELSE IF ( pg == 4 ) THEN
        call Cs_4
    ELSE IF ( pg == 5 ) THEN
        call C2h_5
    ELSE IF ( pg == 6 ) THEN
        call D2_6
    ELSE IF ( pg == 7 ) THEN
        call C2v_7
    ELSE IF ( pg == 8 ) THEN
        call D2h_8
    ELSE IF ( pg == 9 ) THEN
        call C4_9
    ELSE IF ( pg == 10 ) THEN
        call S4_10
    ELSE IF ( pg == 11 ) THEN
        call C4h_11
    ELSE IF ( pg == 12 ) THEN
        call D4_12
    ELSE IF ( pg == 13 ) THEN
        call C4v_13
    ELSE IF ( pg == 14 ) THEN
        call D2d_14
    ELSE IF ( pg == 15 ) THEN
        call D4h_15
    ELSE IF ( pg == 16 ) THEN
        call C3_16
    ELSE IF ( pg == 17 ) THEN
        call S6_17
    ELSE IF ( pg == 18 ) THEN
        call D3_18
    ELSE IF ( pg == 19 ) THEN
        call C3v_19
    ELSE IF ( pg == 20 ) THEN
        call D3d_20
    ELSE IF ( pg == 21 ) THEN
        call C6_21
    ELSE IF ( pg == 22 ) THEN
        call C3h_22
    ELSE IF ( pg == 23 ) THEN
        call C6h_23
    ELSE IF ( pg == 24 ) THEN
        call D6_24
    ELSE IF ( pg == 25 ) THEN
        call C6v_25
    ELSE IF ( pg == 26 ) THEN
        call D3h_26
    ELSE IF ( pg == 27 ) THEN
        call D6h_27
    ELSE IF ( pg == 28 ) THEN
        call T_28
    ELSE IF ( pg == 29 ) THEN
        call Th_29
    ELSE IF ( pg == 30 ) THEN
        call O_30
    ELSE IF ( pg == 31 ) THEN
        call Td_31
    ELSE IF ( pg == 32 ) THEN
        call Oh_32
    ELSE
        Write(*,*) " Write a number between 1 and 32"
    End IF
    
    write(*,*) " Multiplicity is: ", counting_dimension
    
deallocate (hklnew)    
end program pointgroups

subroutine C1_1
    use commonvar
    implicit none
    real, dimension(3,3,1) :: a
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    allocate ( hklnew(3,1) )
    
    hklnew(:,1) = matmul(hkl,a(:,:,1))
    counting_dimension = 1
    
end subroutine C1_1

subroutine Ci_2
    use commonvar
    implicit none
    real, dimension(3,3,2) :: a
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !i
    a(1,1,2) = -1
    a(1,2,2) = 0
    a(1,3,2) = 0
    a(2,1,2) = 0
    a(2,2,2) = -1
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = -1
    
    allocate ( hklnew(3,2) )
    
    Do m = 1, 2
        hklnew(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do
    
    counting_dimension = 2
    
end subroutine Ci_2

subroutine C2_3
    use commonvar
    implicit none
    real, dimension(3,3,2) :: a
    real, dimension(3,2) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C2sub001
    a(1,1,2) = -1
    a(1,2,2) = 0
    a(1,3,2) = 0
    a(2,1,2) = 0
    a(2,2,2) = -1
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
   Do m = 1, 2
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 2
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 2
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C2_3

subroutine Cs_4
    use commonvar
    implicit none
    real, dimension(3,3,2) :: a
    real, dimension(3,2) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !msub001
    a(1,1,2) = 1
    a(1,2,2) = 0
    a(1,3,2) = 0
    a(2,1,2) = 0
    a(2,2,2) = 1
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = -1
    
    Do m = 1, 2
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 2
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 2
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do

end subroutine Cs_4

subroutine C2h_5
    use commonvar
    implicit none
    real, dimension(3,3,4) :: a
    real, dimension(3,4) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C2sub001
    a(1,1,2) = -1
    a(1,2,2) = 0
    a(1,3,2) = 0
    a(2,1,2) = 0
    a(2,2,2) = -1
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !i
    a(1,1,3) = -1
    a(1,2,3) = 0
    a(1,3,3) = 0
    a(2,1,3) = 0
    a(2,2,3) = -1
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = -1
    
    !m001
    a(1,1,4) = 1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = 1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = -1
    
    Do m = 1, 4
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C2h_5

subroutine D2_6
    use commonvar
    implicit none
    real, dimension(3,3,4) :: a
    real, dimension(3,4) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C2sub001
    a(1,1,2) = -1
    a(1,2,2) = 0
    a(1,3,2) = 0
    a(2,1,2) = 0
    a(2,2,2) = -1
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C2sub010
    a(1,1,3) = -1
    a(1,2,3) = 0
    a(1,3,3) = 0
    a(2,1,3) = 0
    a(2,2,3) = 1
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = -1
    
    !C2sub100
    a(1,1,4) = 1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = -1
    
    Do m = 1, 4
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D2_6

subroutine C2v_7
    use commonvar
    implicit none
    real, dimension(3,3,4) :: a
    real, dimension(3,4) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C2sub001
    a(1,1,2) = -1
    a(1,2,2) = 0
    a(1,3,2) = 0
    a(2,1,2) = 0
    a(2,2,2) = -1
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !msub010
    a(1,1,3) = 1
    a(1,2,3) = 0
    a(1,3,3) = 0
    a(2,1,3) = 0
    a(2,2,3) = -1
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !msub100
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = 1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    Do m = 1, 4
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C2v_7

subroutine D2h_8
    use commonvar
    implicit none
    real, dimension(3,3,8) :: a
    real, dimension(3,8) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C2sub001
    a(1,1,2) = -1
    a(1,2,2) = 0
    a(1,3,2) = 0
    a(2,1,2) = 0
    a(2,2,2) = -1
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C2sub010
    a(1,1,3) = -1
    a(1,2,3) = 0
    a(1,3,3) = 0
    a(2,1,3) = 0
    a(2,2,3) = 1
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = -1
    
    !C2sub100
    a(1,1,4) = 1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = -1
    
    !i
    a(1,1,5) = -1
    a(1,2,5) = 0
    a(1,3,5) = 0
    a(2,1,5) = 0
    a(2,2,5) = -1
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = -1
    
    !msub001
    a(1,1,6) = 1
    a(1,2,6) = 0
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = 1
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = -1
    
    !msub010
    a(1,1,7) = 1
    a(1,2,7) = 0
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = -1
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = 1
    
    !msub100
    a(1,1,8) = -1
    a(1,2,8) = 0
    a(1,3,8) = 0
    a(2,1,8) = 0
    a(2,2,8) = 1
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = 1
    
    Do m = 1, 8
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D2h_8

subroutine C4_9
    use commonvar
    implicit none
    real, dimension(3,3,4) :: a
    real, dimension(3,4) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !4+sub001
    a(1,1,2) = 0
    a(1,2,2) = -1
    a(1,3,2) = 0
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C2sub001
    a(1,1,3) = -1
    a(1,2,3) = 0
    a(1,3,3) = 0
    a(2,1,3) = 0
    a(2,2,3) = -1
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !4-sub001
    a(1,1,4) = 0
    a(1,2,4) = 1
    a(1,3,4) = 0
    a(2,1,4) = -1
    a(2,2,4) = 0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    Do m = 1, 4
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C4_9

subroutine S4_10
    use commonvar
    implicit none
    real, dimension(3,3,4) :: a
    real, dimension(3,4) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !-4-sub001
    a(1,1,2) = 0
    a(1,2,2) = -1
    a(1,3,2) = 0
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = -1
    
    !C2sub001
    a(1,1,3) = -1
    a(1,2,3) = 0
    a(1,3,3) = 0
    a(2,1,3) = 0
    a(2,2,3) = -1
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !-4+sub001
    a(1,1,4) = 0
    a(1,2,4) = 1
    a(1,3,4) = 0
    a(2,1,4) = -1
    a(2,2,4) = 0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = -1
    
    Do m = 1, 4
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 4
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine S4_10

subroutine C4h_11
    use commonvar
    implicit none
    real, dimension(3,3,8) :: a
    real, dimension(3,8) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !4+sub001
    a(1,1,2) = 0
    a(1,2,2) = -1
    a(1,3,2) = 0
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C2sub001
    a(1,1,3) = -1
    a(1,2,3) = 0
    a(1,3,3) = 0
    a(2,1,3) = 0
    a(2,2,3) = -1
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !4-sub001
    a(1,1,4) = 0
    a(1,2,4) = 1
    a(1,3,4) = 0
    a(2,1,4) = -1
    a(2,2,4) = 0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    !i
    a(1,1,5) = -1      
    a(1,2,5) = 0
    a(1,3,5) = 0
    a(2,1,5) = 0
    a(2,2,5) = -1
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = -1
    
    !-4+sub001
    a(1,1,6) = 0 
    a(1,2,6) = 1 
    a(1,3,6) = 0 
    a(2,1,6) = -1
    a(2,2,6) = 0 
    a(2,3,6) = 0 
    a(3,1,6) = 0 
    a(3,2,6) = 0 
    a(3,3,6) = -1
    
    !msub001
    a(1,1,7) = 1
    a(1,2,7) = 0
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = 1
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = -1
    
    !-4-sub001
    a(1,1,8) = 0
    a(1,2,8) = -1
    a(1,3,8) = 0
    a(2,1,8) = 1
    a(2,2,8) = 0
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = -1
    
    Do m = 1, 8
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C4h_11

subroutine D4_12
    use commonvar
    implicit none
    real, dimension(3,3,8) :: a
    real, dimension(3,8) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !4+sub001
    a(1,1,2) = 0
    a(1,2,2) = -1
    a(1,3,2) = 0
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !4-sub001
    a(1,1,3) = 0 
    a(1,2,3) = 1 
    a(1,3,3) = 0 
    a(2,1,3) = -1
    a(2,2,3) = 0 
    a(2,3,3) = 0 
    a(3,1,3) = 0 
    a(3,2,3) = 0 
    a(3,3,3) = 1 
    
    !C2sub001
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    !C2sub010
    a(1,1,5) = -1
    a(1,2,5) = 0
    a(1,3,5) = 0
    a(2,1,5) = 0
    a(2,2,5) = 1
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = -1
    
    !C2sub100
    a(1,1,6) = 1
    a(1,2,6) = 0
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = -1
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = -1
    
    !C2sub110
    a(1,1,7) = 0
    a(1,2,7) = 1
    a(1,3,7) = 0
    a(2,1,7) = 1
    a(2,2,7) = 0
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = -1
    
    !C2sub1-10
    a(1,1,8) = 0
    a(1,2,8) = -1
    a(1,3,8) = 0
    a(2,1,8) = -1
    a(2,2,8) = 0
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = -1
    
    Do m = 1, 8
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D4_12

subroutine C4v_13
    use commonvar
    implicit none
    real, dimension(3,3,8) :: a
    real, dimension(3,8) :: hklold
    
    !E
    a(1,1,1) = 1           
    a(1,2,1) = 0           
    a(1,3,1) = 0           
    a(2,1,1) = 0           
    a(2,2,1) = 1           
    a(2,3,1) = 0           
    a(3,1,1) = 0           
    a(3,2,1) = 0           
    a(3,3,1) = 1    
    
    !4+sub001              
    a(1,1,2) = 0
    a(1,2,2) = -1          
    a(1,3,2) = 0          
    a(2,1,2) = 1          
    a(2,2,2) = 0          
    a(2,3,2) = 0          
    a(3,1,2) = 0          
    a(3,2,2) = 0          
    a(3,3,2) = 1          
    
    !4-sub001
    a(1,1,3) = 0          
    a(1,2,3) = 1  
    a(1,3,3) = 0           
    a(2,1,3) = -1         
    a(2,2,3) = 0          
    a(2,3,3) = 0          
    a(3,1,3) = 0          
    a(3,2,3) = 0          
    a(3,3,3) = 1
    
    !C2sub001                      
    a(1,1,4) = -1         
    a(1,2,4) = 0          
    a(1,3,4) = 0
    a(2,1,4) = 0           
    a(2,2,4) = -1          
    a(2,3,4) = 0           
    a(3,1,4) = 0           
    a(3,2,4) = 0           
    a(3,3,4) = 1           
    
    !msub010
    a(1,1,5) = 1           
    a(1,2,5) = 0           
    a(1,3,5) = 0           
    a(2,1,5) = 0
    a(2,2,5) = -1
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = 1
    
    !msub100
    a(1,1,6) = -1
    a(1,2,6) = 0
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = 1
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = 1
    
    !msub110
    a(1,1,7) = 0
    a(1,2,7) = -1
    a(1,3,7) = 0
    a(2,1,7) = -1
    a(2,2,7) = 0
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = 1
    
    !msub1-10
    a(1,1,8) = 0
    a(1,2,8) = 1
    a(1,3,8) = 0
    a(2,1,8) = 1
    a(2,2,8) = 0
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = 1
    
    Do m = 1, 8
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C4v_13

subroutine D2d_14
    use commonvar
    implicit none
    real, dimension(3,3,8) :: a
    real, dimension(3,8) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !-4-sub001
    a(1,1,2) = 0 
    a(1,2,2) = -1
    a(1,3,2) = 0 
    a(2,1,2) = 1 
    a(2,2,2) = 0 
    a(2,3,2) = 0 
    a(3,1,2) = 0 
    a(3,2,2) = 0 
    a(3,3,2) = -1
    
    !-4+sub001
    a(1,1,3) = 0
    a(1,2,3) = 1
    a(1,3,3) = 0
    a(2,1,3) = -1
    a(2,2,3) = 0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = -1
    
    !C2sub001
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    !C2sub010
    a(1,1,5) = -1
    a(1,2,5) = 0
    a(1,3,5) = 0
    a(2,1,5) = 0
    a(2,2,5) = 1
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = -1
    
    !C2sub100
    a(1,1,6) = 1
    a(1,2,6) = 0
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = -1
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = -1
    
    !msub110
    a(1,1,7) = 0
    a(1,2,7) = -1
    a(1,3,7) = 0
    a(2,1,7) = -1
    a(2,2,7) = 0
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = 1
    
    !msub1-10
    a(1,1,8) = 0
    a(1,2,8) = 1
    a(1,3,8) = 0
    a(2,1,8) = 1
    a(2,2,8) = 0
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = 1
    
    Do m = 1, 8
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 8
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D2d_14

subroutine D4h_15
    use commonvar
    implicit none
    real, dimension(3,3,16) :: a
    real, dimension(3,16) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
     
    !4+sub001 
    a(1,1,2) = 0
    a(1,2,2) = -1
    a(1,3,2) = 0
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !4-sub001 
    a(1,1,3) = 0 
    a(1,2,3) = 1 
    a(1,3,3) = 0 
    a(2,1,3) = -1
    a(2,2,3) = 0 
    a(2,3,3) = 0 
    a(3,1,3) = 0 
    a(3,2,3) = 0 
    a(3,3,3) = 1 
    
    !C2sub001 
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    !C2sub010 
    a(1,1,5) = -1
    a(1,2,5) = 0
    a(1,3,5) = 0
    a(2,1,5) = 0
    a(2,2,5) = 1
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = -1
    
    !C2sub100
    a(1,1,6) = 1
    a(1,2,6) = 0
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = -1
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = -1
    
    !C2sub110
    a(1,1,7) = 0
    a(1,2,7) = 1
    a(1,3,7) = 0
    a(2,1,7) = 1
    a(2,2,7) = 0
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = -1
    
    !C2sub1-10
    a(1,1,8) = 0
    a(1,2,8) = -1
    a(1,3,8) = 0
    a(2,1,8) = -1
    a(2,2,8) = 0
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = -1
    
    !i
    a(1,1,9) = -1
    a(1,2,9) = 0
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = -1
    a(2,3,9) = 0
    a(3,1,9) = 0
    a(3,2,9) = 0
    a(3,3,9) = -1
    
    !-4-sub001
    a(1,1,10) = 0 
    a(1,2,10) = -1
    a(1,3,10) = 0 
    a(2,1,10) = 1 
    a(2,2,10) = 0 
    a(2,3,10) = 0 
    a(3,1,10) = 0 
    a(3,2,10) = 0 
    a(3,3,10) = -1
    
    !-4+sub001
    a(1,1,11) = 0
    a(1,2,11) = 1
    a(1,3,11) = 0
    a(2,1,11) = -1
    a(2,2,11) = 0
    a(2,3,11) = 0
    a(3,1,11) = 0
    a(3,2,11) = 0
    a(3,3,11) = -1
    
    !msub001
    a(1,1,12) = 1
    a(1,2,12) = 0
    a(1,3,12) = 0
    a(2,1,12) = 0
    a(2,2,12) = 1
    a(2,3,12) = 0
    a(3,1,12) = 0
    a(3,2,12) = 0
    a(3,3,12) = -1
    
    !msub010
    a(1,1,13) = 1
    a(1,2,13) = 0
    a(1,3,13) = 0
    a(2,1,13) = 0
    a(2,2,13) = -1
    a(2,3,13) = 0
    a(3,1,13) = 0
    a(3,2,13) = 0
    a(3,3,13) = 1
    
    !msub100
    a(1,1,14) = -1
    a(1,2,14) = 0
    a(1,3,14) = 0
    a(2,1,14) = 0
    a(2,2,14) = 1
    a(2,3,14) = 0
    a(3,1,14) = 0
    a(3,2,14) = 0
    a(3,3,14) = 1
    
    !msub110
    a(1,1,15) = 0
    a(1,2,15) = -1
    a(1,3,15) = 0
    a(2,1,15) = -1
    a(2,2,15) = 0
    a(2,3,15) = 0
    a(3,1,15) = 0
    a(3,2,15) = 0
    a(3,3,15) = 1
    
    !msub1-10
    a(1,1,16) = 0
    a(1,2,16) = 1
    a(1,3,16) = 0
    a(2,1,16) = 1
    a(2,2,16) = 0
    a(2,3,16) = 0
    a(3,1,16) = 0
    a(3,2,16) = 0
    a(3,3,16) = 1
    
    Do m = 1, 16
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 16
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 16
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D4h_15

subroutine C3_16
    use commonvar
    implicit none
    real, dimension(3,3,3) :: a
    real, dimension(3,3) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    Do m = 1, 3
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 3
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 3
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C3_16

subroutine S6_17
    use commonvar
    implicit none
    real, dimension(3,3,6) :: a
    real, dimension(3,6) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !i
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = -1
    
    !-3+sub001                        
    a(1,1,5) = 1/2.0                 
    a(1,2,5) = sqrt(3.0)/2           
    a(1,3,5) = 0                     
    a(2,1,5) = -sqrt(3.0)/2          
    a(2,2,5) = 1/2.0                 
    a(2,3,5) = 0                     
    a(3,1,5) = 0                     
    a(3,2,5) = 0                     
    a(3,3,5) = -1                    
                                      
    !-3-sub001                        
    a(1,1,6) = 1/2.0                 
    a(1,2,6) = -sqrt(3.0)/2.0        
    a(1,3,6) = 0                     
    a(2,1,6) = sqrt(3.0)/2.0         
    a(2,2,6) = 1/2.0                 
    a(2,3,6) = 0                     
    a(3,1,6) = 0                     
    a(3,2,6) = 0                     
    a(3,3,6) = -1
    
    Do m = 1, 6
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine S6_17

subroutine D3_18
    use commonvar
    implicit none
    real, dimension(3,3,6) :: a
    real, dimension(3,6) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !2sub1-10
    a(1,1,4) = 1/2.0
    a(1,2,4) = -sqrt(3.0)/2
    a(1,3,4) = 0
    a(2,1,4) = -sqrt(3.0)/2
    a(2,2,4) = -1/2.0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = -1
    
    !2sub120     
    a(1,1,5) = -11/14.0
    a(1,2,5) = 5.0*sqrt(3.0)/14.0
    a(1,3,5) = 0
    a(2,1,5) = 5.0*sqrt(3.0)/14.0
    a(2,2,5) = 11/14.0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = -1.0
    
    !2sub210
    a(1,1,6) = -1/7.0
    a(1,2,6) = 4.0*sqrt(3.0)/7.0
    a(1,3,6) = 0
    a(2,1,6) = 4.0*sqrt(3.0)/7.0
    a(2,2,6) = 1/7.0
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = -1
    
    Do m = 1, 6
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D3_18

subroutine C3v_19
    use commonvar
    implicit none
    real, dimension(3,3,6) :: a
    real, dimension(3,6) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !msub1-10              
    a(1,1,4) = -1/2.0     
    a(1,2,4) = sqrt(3.0)/2
    a(1,3,4) = 0         
    a(2,1,4) = sqrt(3.0)/2
    a(2,2,4) = 1/2.0      
    a(2,3,4) = 0        
    a(3,1,4) = 0        
    a(3,2,4) = 0        
    a(3,3,4) = 1        
    
    !msub120
    a(1,1,5) = 11/14.0                     
    a(1,2,5) = -5.0*sqrt(3.0)/14.0         
    a(1,3,5) = 0                           
    a(2,1,5) = -5.0*sqrt(3.0)/14.0         
    a(2,2,5) = -11/14.0                    
    a(2,3,5) = 0                           
    a(3,1,5) = 0                           
    a(3,2,5) = 0                           
    a(3,3,5) = 1.0                         
                                            
    !msub120
    a(1,1,6) = 1/7.0                       
    a(1,2,6) = -4.0*sqrt(3.0)/7.0          
    a(1,3,6) = 0                           
    a(2,1,6) = -4.0*sqrt(3.0)/7.0          
    a(2,2,6) = -1/7.0                      
    a(2,3,6) = 0                           
    a(3,1,6) = 0                           
    a(3,2,6) = 0                           
    a(3,3,6) = 1
    
    Do m = 1, 6
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C3v_19

subroutine D3d_20
    use commonvar
    implicit none
    real, dimension(3,3,12) :: a
    real, dimension(3,12) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !2sub1-10
    a(1,1,4) = 1/2.0
    a(1,2,4) = -sqrt(3.0)/2
    a(1,3,4) = 0
    a(2,1,4) = -sqrt(3.0)/2
    a(2,2,4) = -1/2.0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = -1
    
    !2sub120     
    a(1,1,5) = -11/14.0
    a(1,2,5) = 5.0*sqrt(3.0)/14.0
    a(1,3,5) = 0
    a(2,1,5) = 5.0*sqrt(3.0)/14.0
    a(2,2,5) = 11/14.0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = -1.0
    
    !2sub210
    a(1,1,6) = -1/7.0
    a(1,2,6) = 4.0*sqrt(3.0)/7.0
    a(1,3,6) = 0
    a(2,1,6) = 4.0*sqrt(3.0)/7.0
    a(2,2,6) = 1/7.0
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = -1
    
    !i
    a(1,1,7) = -1
    a(1,2,7) = 0
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = -1
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = -1
    
     !-3+sub001                        
    a(1,1,8) = 1/2.0                 
    a(1,2,8) = sqrt(3.0)/2           
    a(1,3,8) = 0                     
    a(2,1,8) = -sqrt(3.0)/2          
    a(2,2,8) = 1/2.0                 
    a(2,3,8) = 0                     
    a(3,1,8) = 0                     
    a(3,2,8) = 0                     
    a(3,3,8) = -1                    
                                      
    !-3-sub001                        
    a(1,1,9) = 1/2.0                 
    a(1,2,9) = -sqrt(3.0)/2.0        
    a(1,3,9) = 0                     
    a(2,1,9) = sqrt(3.0)/2.0         
    a(2,2,9) = 1/2.0                 
    a(2,3,9) = 0                     
    a(3,1,9) = 0                     
    a(3,2,9) = 0                     
    a(3,3,9) = -1             
               
    !msub1-10              
    a(1,1,10) = -1/2.0     
    a(1,2,10) = sqrt(3.0)/2
    a(1,3,10) = 0         
    a(2,1,10) = sqrt(3.0)/2
    a(2,2,10) = 1/2.0      
    a(2,3,10) = 0        
    a(3,1,10) = 0        
    a(3,2,10) = 0        
    a(3,3,10) = 1        
    
    !msub120
    a(1,1,11) = 11/14.0                     
    a(1,2,11) = -5.0*sqrt(3.0)/14.0         
    a(1,3,11) = 0                           
    a(2,1,11) = -5.0*sqrt(3.0)/14.0         
    a(2,2,11) = -11/14.0                    
    a(2,3,11) = 0                           
    a(3,1,11) = 0                           
    a(3,2,11) = 0                           
    a(3,3,11) = 1.0                         
                                            
    !msub120
    a(1,1,12) = 1/7.0                       
    a(1,2,12) = -4.0*sqrt(3.0)/7.0          
    a(1,3,12) = 0                           
    a(2,1,12) = -4.0*sqrt(3.0)/7.0          
    a(2,2,12) = -1/7.0                      
    a(2,3,12) = 0                           
    a(3,1,12) = 0                           
    a(3,2,12) = 0                           
    a(3,3,12) = 1
    
    Do m = 1, 12
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D3d_20

subroutine C6_21
    use commonvar
    implicit none
    real, dimension(3,3,6) :: a
    real, dimension(3,6) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !C2
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
        
    !C6+
    a(1,1,5) = 1/2.0
    a(1,2,5) = -sqrt(3.0)/2.0
    a(1,3,5) = 0
    a(2,1,5) = sqrt(3.0)/2.0
    a(2,2,5) = 1.0/2.0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = 1
    
    !C6-
    a(1,1,6) = 1/2.0
    a(1,2,6) = sqrt(3.0)/2.0
    a(1,3,6) = 0
    a(2,1,6) = -sqrt(3.0)/2.0
    a(2,2,6) = 1/2.0
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = 1
    
    Do m = 1, 6
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C6_21

subroutine C3h_22
    use commonvar
    implicit none
    real, dimension(3,3,6) :: a
    real, dimension(3,6) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !msub001
    a(1,1,4) = 1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = 1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = -1
     
    !-6+sub001
    a(1,1,5) = -1/2.0                  
    a(1,2,5) = sqrt(3.0)/2             
    a(1,3,5) = 0                       
    a(2,1,5) = -sqrt(3.0)/2            
    a(2,2,5) = -1/2.0                  
    a(2,3,5) = 0                       
    a(3,1,5) = 0                       
    a(3,2,5) = 0                       
    a(3,3,5) = -1                      
                                        
    !-6-sub001                          
    a(1,1,6) = -1/2.0                  
    a(1,2,6) = -sqrt(3.0)/2            
    a(1,3,6) = 0                       
    a(2,1,6) = sqrt(3.0)/2             
    a(2,2,6) = -1/2.0                  
    a(2,3,6) = 0                       
    a(3,1,6) = 0                       
    a(3,2,6) = 0                       
    a(3,3,6) = -1
    
    Do m = 1, 6
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 6
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C3h_22

subroutine C6h_23
    use commonvar
    implicit none
    real, dimension(3,3,12) :: a
    real, dimension(3,12) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !C2
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
        
    !C6+
    a(1,1,5) = 1/2.0
    a(1,2,5) = -sqrt(3.0)/2.0
    a(1,3,5) = 0
    a(2,1,5) = sqrt(3.0)/2.0
    a(2,2,5) = 1.0/2.0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = 1
    
    !C6-
    a(1,1,6) = 1/2.0
    a(1,2,6) = sqrt(3.0)/2.0
    a(1,3,6) = 0
    a(2,1,6) = -sqrt(3.0)/2.0
    a(2,2,6) = 1/2.0
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = 1
    
    !i
    a(1,1,7) = -1
    a(1,2,7) = 0
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = -1
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = -1
      
    !-3+sub001                        
    a(1,1,8) = 1/2.0                 
    a(1,2,8) = sqrt(3.0)/2           
    a(1,3,8) = 0                     
    a(2,1,8) = -sqrt(3.0)/2          
    a(2,2,8) = 1/2.0                 
    a(2,3,8) = 0                     
    a(3,1,8) = 0                     
    a(3,2,8) = 0                     
    a(3,3,8) = -1                    
                                      
    !-3-sub001                        
    a(1,1,9) = 1/2.0                 
    a(1,2,9) = -sqrt(3.0)/2.0        
    a(1,3,9) = 0                     
    a(2,1,9) = sqrt(3.0)/2.0         
    a(2,2,9) = 1/2.0                 
    a(2,3,9) = 0                     
    a(3,1,9) = 0                     
    a(3,2,9) = 0                     
    a(3,3,9) = -1  
    
    !msub001             
    a(1,1,10) = 1        
    a(1,2,10) = 0        
    a(1,3,10) = 0        
    a(2,1,10) = 0        
    a(2,2,10) = 1        
    a(2,3,10) = 0        
    a(3,1,10) = 0        
    a(3,2,10) = 0        
    a(3,3,10) = -1   
                                      
    !-6+sub001
    a(1,1,11) = -1/2.0                  
    a(1,2,11) = sqrt(3.0)/2             
    a(1,3,11) = 0                       
    a(2,1,11) = -sqrt(3.0)/2            
    a(2,2,11) = -1/2.0                  
    a(2,3,11) = 0                       
    a(3,1,11) = 0                       
    a(3,2,11) = 0                       
    a(3,3,11) = -1                      
                                        
    !-6-sub001                          
    a(1,1,12) = -1/2.0                  
    a(1,2,12) = -sqrt(3.0)/2            
    a(1,3,12) = 0                       
    a(2,1,12) = sqrt(3.0)/2             
    a(2,2,12) = -1/2.0                  
    a(2,3,12) = 0                       
    a(3,1,12) = 0                       
    a(3,2,12) = 0                       
    a(3,3,12) = -1
    
    Do m = 1, 12
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
                                        
end subroutine C6h_23

subroutine D6_24
    use commonvar
    implicit none
    real, dimension(3,3,12) :: a
    real, dimension(3,12) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !C2
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    !C6+
    a(1,1,5) = 1/2.0
    a(1,2,5) = -sqrt(3.0)/2.0
    a(1,3,5) = 0
    a(2,1,5) = sqrt(3.0)/2.0
    a(2,2,5) = 1.0/2.0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = 1
    
    !C6-
    a(1,1,6) = 1/2.0
    a(1,2,6) = sqrt(3.0)/2.0
    a(1,3,6) = 0
    a(2,1,6) = -sqrt(3.0)/2.0
    a(2,2,6) = 1/2.0
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = 1
    
    !2sub110
    a(1,1,7) = -1/2.0
    a(1,2,7) = sqrt(3.0)/2
    a(1,3,7) = 0
    a(2,1,7) = sqrt(3.0)/2
    a(2,2,7) = 1/2.0
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = -1
    
    !2sub100
    a(1,1,8) = 1/2.0
    a(1,2,8) = sqrt(3.0)/2
    a(1,3,8) = 0
    a(2,1,8) = sqrt(3.0)/2
    a(2,2,8) = -1/2.0
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = -1
    
    !2sub010
    a(1,1,9) = -1
    a(1,2,9) = 0
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = 1
    a(2,3,9) = 0
    a(3,1,9) = 0
    a(3,2,9) = 0
    a(3,3,9) = -1
    
    !2sub1-10
    a(1,1,10) = 1/2.0
    a(1,2,10) = -sqrt(3.0)/2
    a(1,3,10) = 0
    a(2,1,10) = -sqrt(3.0)/2
    a(2,2,10) = -1/2.0
    a(2,3,10) = 0
    a(3,1,10) = 0
    a(3,2,10) = 0
    a(3,3,10) = -1
    
    !2sub120     
    a(1,1,11) = -11/14.0
    a(1,2,11) = 5.0*sqrt(3.0)/14.0
    a(1,3,11) = 0
    a(2,1,11) = 5.0*sqrt(3.0)/14.0
    a(2,2,11) = 11/14.0
    a(2,3,11) = 0
    a(3,1,11) = 0
    a(3,2,11) = 0
    a(3,3,11) = -1.0
    
    !2sub210
    a(1,1,12) = -1/7.0
    a(1,2,12) = 4.0*sqrt(3.0)/7.0
    a(1,3,12) = 0
    a(2,1,12) = 4.0*sqrt(3.0)/7.0
    a(2,2,12) = 1/7.0
    a(2,3,12) = 0
    a(3,1,12) = 0
    a(3,2,12) = 0
    a(3,3,12) = -1
    
    Do m = 1, 12
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D6_24

subroutine C6v_25
    use commonvar
    implicit none
    real, dimension(3,3,12) :: a
    real, dimension(3,12) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !C2
    a(1,1,4) = -1
    a(1,2,4) = 0
    a(1,3,4) = 0
    a(2,1,4) = 0
    a(2,2,4) = -1
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    !C6+
    a(1,1,5) = 1/2.0
    a(1,2,5) = -sqrt(3.0)/2.0
    a(1,3,5) = 0
    a(2,1,5) = sqrt(3.0)/2.0
    a(2,2,5) = 1.0/2.0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = 1
    
    !C6-
    a(1,1,6) = 1/2.0
    a(1,2,6) = sqrt(3.0)/2.0
    a(1,3,6) = 0
    a(2,1,6) = -sqrt(3.0)/2.0
    a(2,2,6) = 1/2.0
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = 1
    
    !msub110                    
    a(1,1,7) = 1/2.0           
    a(1,2,7) = -sqrt(3.0)/2    
    a(1,3,7) = 0               
    a(2,1,7) = -sqrt(3.0)/2    
    a(2,2,7) = -1/2.0          
    a(2,3,7) = 0               
    a(3,1,7) = 0               
    a(3,2,7) = 0               
    a(3,3,7) = 1               
    
    !msub100                 
    a(1,1,8) = -1/2.0       
    a(1,2,8) = -sqrt(3.0)/2 
    a(1,3,8) = 0            
    a(2,1,8) = -sqrt(3.0)/2 
    a(2,2,8) = 1/2.0        
    a(2,3,8) = 0            
    a(3,1,8) = 0            
    a(3,2,8) = 0            
    a(3,3,8) = 1            
    
    !msub010              
    a(1,1,9) = 1         
    a(1,2,9) = 0         
    a(1,3,9) = 0         
    a(2,1,9) = 0         
    a(2,2,9) = -1        
    a(2,3,9) = 0         
    a(3,1,9) = 0         
    a(3,2,9) = 0         
    a(3,3,9) = 1         
    
    !msub1-10              
    a(1,1,10) = -1/2.0     
    a(1,2,10) = sqrt(3.0)/2
    a(1,3,10) = 0         
    a(2,1,10) = sqrt(3.0)/2
    a(2,2,10) = 1/2.0      
    a(2,3,10) = 0        
    a(3,1,10) = 0        
    a(3,2,10) = 0        
    a(3,3,10) = 1        
    
    !msub120
    a(1,1,11) = 11/14.0                     
    a(1,2,11) = -5.0*sqrt(3.0)/14.0         
    a(1,3,11) = 0                           
    a(2,1,11) = -5.0*sqrt(3.0)/14.0         
    a(2,2,11) = -11/14.0                    
    a(2,3,11) = 0                           
    a(3,1,11) = 0                           
    a(3,2,11) = 0                           
    a(3,3,11) = 1.0                         
                                            
    !msub210
    a(1,1,12) = 1/7.0                       
    a(1,2,12) = -4.0*sqrt(3.0)/7.0          
    a(1,3,12) = 0                           
    a(2,1,12) = -4.0*sqrt(3.0)/7.0          
    a(2,2,12) = -1/7.0                      
    a(2,3,12) = 0                           
    a(3,1,12) = 0                           
    a(3,2,12) = 0                           
    a(3,3,12) = 1
    
    Do m = 1, 12
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine C6v_25

subroutine D3h_26
    use commonvar
    implicit none
    real, dimension(3,3,12) :: a
    real, dimension(3,12) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C3+
    a(1,1,2) = -1/2.0
    a(1,2,2) = -sqrt(3.0)/2
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2
    a(2,2,2) = -1/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C3-
    a(1,1,3) = -1/2.0
    a(1,2,3) = sqrt(3.0)/2
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2
    a(2,2,3) = -1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !msub001             
    a(1,1,4) = 1        
    a(1,2,4) = 0        
    a(1,3,4) = 0        
    a(2,1,4) = 0        
    a(2,2,4) = 1        
    a(2,3,4) = 0        
    a(3,1,4) = 0        
    a(3,2,4) = 0        
    a(3,3,4) = -1     
     
   !-6+sub001
    a(1,1,5) = -1/2.0                  
    a(1,2,5) = sqrt(3.0)/2             
    a(1,3,5) = 0                       
    a(2,1,5) = -sqrt(3.0)/2            
    a(2,2,5) = -1/2.0                  
    a(2,3,5) = 0                       
    a(3,1,5) = 0                       
    a(3,2,5) = 0                       
    a(3,3,5) = -1                      
                                        
    !-6-sub001                          
    a(1,1,6) = -1/2.0                  
    a(1,2,6) = -sqrt(3.0)/2            
    a(1,3,6) = 0                       
    a(2,1,6) = sqrt(3.0)/2             
    a(2,2,6) = -1/2.0                  
    a(2,3,6) = 0                       
    a(3,1,6) = 0                       
    a(3,2,6) = 0                       
    a(3,3,6) = -1        
    
    !2sub110
    a(1,1,7) = -1/2.0
    a(1,2,7) = sqrt(3.0)/2
    a(1,3,7) = 0
    a(2,1,7) = sqrt(3.0)/2
    a(2,2,7) = 1/2.0
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = -1
    
    !2sub100
    a(1,1,8) = 1/2.0
    a(1,2,8) = sqrt(3.0)/2
    a(1,3,8) = 0
    a(2,1,8) = sqrt(3.0)/2
    a(2,2,8) = -1/2.0
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = -1
    
    !2sub010
    a(1,1,9) = -1
    a(1,2,9) = 0
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = 1
    a(2,3,9) = 0
    a(3,1,9) = 0
    a(3,2,9) = 0
    a(3,3,9) = -1
     
    !msub1-10              
    a(1,1,10) = -1/2.0     
    a(1,2,10) = sqrt(3.0)/2
    a(1,3,10) = 0         
    a(2,1,10) = sqrt(3.0)/2
    a(2,2,10) = 1/2.0      
    a(2,3,10) = 0        
    a(3,1,10) = 0        
    a(3,2,10) = 0        
    a(3,3,10) = 1        
    
    !msub120
    a(1,1,11) = 11/14.0                     
    a(1,2,11) = -5.0*sqrt(3.0)/14.0         
    a(1,3,11) = 0                           
    a(2,1,11) = -5.0*sqrt(3.0)/14.0         
    a(2,2,11) = -11/14.0                    
    a(2,3,11) = 0                           
    a(3,1,11) = 0                           
    a(3,2,11) = 0                           
    a(3,3,11) = 1.0                         
                                            
    !msub210
    a(1,1,12) = 1/7.0                       
    a(1,2,12) = -4.0*sqrt(3.0)/7.0          
    a(1,3,12) = 0                           
    a(2,1,12) = -4.0*sqrt(3.0)/7.0          
    a(2,2,12) = -1/7.0                      
    a(2,3,12) = 0                           
    a(3,1,12) = 0                           
    a(3,2,12) = 0                           
    a(3,3,12) = 1
    
    Do m = 1, 12
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D3h_26

subroutine D6h_27
    use commonvar
    implicit none
    real, dimension(3,3,24) :: a
    real, dimension(3,24) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !C6+
    a(1,1,2) = 1/2.0
    a(1,2,2) = -sqrt(3.0)/2.0
    a(1,3,2) = 0
    a(2,1,2) = sqrt(3.0)/2.0
    a(2,2,2) = 1.0/2.0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 0
    a(3,3,2) = 1
    
    !C6-
    a(1,1,3) = 1/2.0
    a(1,2,3) = sqrt(3.0)/2.0
    a(1,3,3) = 0
    a(2,1,3) = -sqrt(3.0)/2.0
    a(2,2,3) = 1/2.0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = 0
    a(3,3,3) = 1
    
    !C3+
    a(1,1,4) = -1/2.0
    a(1,2,4) = -sqrt(3.0)/2
    a(1,3,4) = 0
    a(2,1,4) = sqrt(3.0)/2
    a(2,2,4) = -1/2.0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 0
    a(3,3,4) = 1
    
    !C3-
    a(1,1,5) = -1/2.0
    a(1,2,5) = sqrt(3.0)/2
    a(1,3,5) = 0
    a(2,1,5) = -sqrt(3.0)/2
    a(2,2,5) = -1/2.0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = 0
    a(3,3,5) = 1
    
    !C2
    a(1,1,6) = -1
    a(1,2,6) = 0
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = -1
    a(2,3,6) = 0
    a(3,1,6) = 0
    a(3,2,6) = 0
    a(3,3,6) = 1
    
    !2sub110
    a(1,1,7) = -1/2.0
    a(1,2,7) = sqrt(3.0)/2
    a(1,3,7) = 0
    a(2,1,7) = sqrt(3.0)/2
    a(2,2,7) = 1/2.0
    a(2,3,7) = 0
    a(3,1,7) = 0
    a(3,2,7) = 0
    a(3,3,7) = -1
    
    !2sub100
    a(1,1,8) = 1/2.0
    a(1,2,8) = sqrt(3.0)/2
    a(1,3,8) = 0
    a(2,1,8) = sqrt(3.0)/2
    a(2,2,8) = -1/2.0
    a(2,3,8) = 0
    a(3,1,8) = 0
    a(3,2,8) = 0
    a(3,3,8) = -1
    
    !2sub010
    a(1,1,9) = -1
    a(1,2,9) = 0
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = 1
    a(2,3,9) = 0
    a(3,1,9) = 0
    a(3,2,9) = 0
    a(3,3,9) = -1
    
    !2sub1-10
    a(1,1,10) = 1/2.0
    a(1,2,10) = -sqrt(3.0)/2
    a(1,3,10) = 0
    a(2,1,10) = -sqrt(3.0)/2
    a(2,2,10) = -1/2.0
    a(2,3,10) = 0
    a(3,1,10) = 0
    a(3,2,10) = 0
    a(3,3,10) = -1
    
    !2sub120     
    a(1,1,11) = -11/14.0
    a(1,2,11) = 5.0*sqrt(3.0)/14.0
    a(1,3,11) = 0
    a(2,1,11) = 5.0*sqrt(3.0)/14.0
    a(2,2,11) = 11/14.0
    a(2,3,11) = 0
    a(3,1,11) = 0
    a(3,2,11) = 0
    a(3,3,11) = -1.0
    
    !2sub210
    a(1,1,12) = -1/7.0
    a(1,2,12) = 4.0*sqrt(3.0)/7.0
    a(1,3,12) = 0
    a(2,1,12) = 4.0*sqrt(3.0)/7.0
    a(2,2,12) = 1/7.0
    a(2,3,12) = 0
    a(3,1,12) = 0
    a(3,2,12) = 0
    a(3,3,12) = -1
    
    !-1
    a(1,1,13) = -1
    a(1,2,13) = 0
    a(1,3,13) = 0
    a(2,1,13) = 0
    a(2,2,13) = -1
    a(2,3,13) = 0
    a(3,1,13) = 0
    a(3,2,13) = 0
    a(3,3,13) = -1
    
    !-3+sub001                        
    a(1,1,14) = 1/2.0                 
    a(1,2,14) = sqrt(3.0)/2           
    a(1,3,14) = 0                     
    a(2,1,14) = -sqrt(3.0)/2          
    a(2,2,14) = 1/2.0                 
    a(2,3,14) = 0                     
    a(3,1,14) = 0                     
    a(3,2,14) = 0                     
    a(3,3,14) = -1                    
                                      
    !-3-sub001                        
    a(1,1,15) = 1/2.0                 
    a(1,2,15) = -sqrt(3.0)/2.0        
    a(1,3,15) = 0                     
    a(2,1,15) = sqrt(3.0)/2.0         
    a(2,2,15) = 1/2.0                 
    a(2,3,15) = 0                     
    a(3,1,15) = 0                     
    a(3,2,15) = 0                     
    a(3,3,15) = -1                    
                                      
    !-6+sub001
    a(1,1,16) = -1/2.0                  
    a(1,2,16) = sqrt(3.0)/2             
    a(1,3,16) = 0                       
    a(2,1,16) = -sqrt(3.0)/2            
    a(2,2,16) = -1/2.0                  
    a(2,3,16) = 0                       
    a(3,1,16) = 0                       
    a(3,2,16) = 0                       
    a(3,3,16) = -1                      
                                        
    !-6-sub001                          
    a(1,1,17) = -1/2.0                  
    a(1,2,17) = -sqrt(3.0)/2            
    a(1,3,17) = 0                       
    a(2,1,17) = sqrt(3.0)/2             
    a(2,2,17) = -1/2.0                  
    a(2,3,17) = 0                       
    a(3,1,17) = 0                       
    a(3,2,17) = 0                       
    a(3,3,17) = -1                      
                                        
    !msub001             
    a(1,1,18) = 1        
    a(1,2,18) = 0        
    a(1,3,18) = 0        
    a(2,1,18) = 0        
    a(2,2,18) = 1        
    a(2,3,18) = 0        
    a(3,1,18) = 0        
    a(3,2,18) = 0        
    a(3,3,18) = -1       
    
    !msub110                    
    a(1,1,19) = 1/2.0           
    a(1,2,19) = -sqrt(3.0)/2    
    a(1,3,19) = 0               
    a(2,1,19) = -sqrt(3.0)/2    
    a(2,2,19) = -1/2.0          
    a(2,3,19) = 0               
    a(3,1,19) = 0               
    a(3,2,19) = 0               
    a(3,3,19) = 1               
    
    !msub100                 
    a(1,1,20) = -1/2.0       
    a(1,2,20) = -sqrt(3.0)/2 
    a(1,3,20) = 0            
    a(2,1,20) = -sqrt(3.0)/2 
    a(2,2,20) = 1/2.0        
    a(2,3,20) = 0            
    a(3,1,20) = 0            
    a(3,2,20) = 0            
    a(3,3,20) = 1            
    
    !msub010              
    a(1,1,21) = 1         
    a(1,2,21) = 0         
    a(1,3,21) = 0         
    a(2,1,21) = 0         
    a(2,2,21) = -1        
    a(2,3,21) = 0         
    a(3,1,21) = 0         
    a(3,2,21) = 0         
    a(3,3,21) = 1         
    
    !msub1-10              
    a(1,1,22) = -1/2.0     
    a(1,2,22) = sqrt(3.0)/2
    a(1,3,22) = 0         
    a(2,1,22) = sqrt(3.0)/2
    a(2,2,22) = 1/2.0      
    a(2,3,22) = 0        
    a(3,1,22) = 0        
    a(3,2,22) = 0        
    a(3,3,22) = 1        
    
    !msub120
    a(1,1,23) = 11/14.0                     
    a(1,2,23) = -5.0*sqrt(3.0)/14.0         
    a(1,3,23) = 0                           
    a(2,1,23) = -5.0*sqrt(3.0)/14.0         
    a(2,2,23) = -11/14.0                    
    a(2,3,23) = 0                           
    a(3,1,23) = 0                           
    a(3,2,23) = 0                           
    a(3,3,23) = 1.0                         
                                            
    !msub210
    a(1,1,24) = 1/7.0                       
    a(1,2,24) = -4.0*sqrt(3.0)/7.0          
    a(1,3,24) = 0                           
    a(2,1,24) = -4.0*sqrt(3.0)/7.0          
    a(2,2,24) = -1/7.0                      
    a(2,3,24) = 0                           
    a(3,1,24) = 0                           
    a(3,2,24) = 0                           
    a(3,3,24) = 1                           
                                            
    Do m = 1, 24
        hklold(:,m) = matmul(a(:,:,m),hkl)
        !write(*,*) hklold(:,m)
    End Do
    
    counting_dimension = 1 
    Do n = 2, 24
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 24
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine D6h_27

subroutine T_28
    use commonvar
    implicit none
    real, dimension(3,3,12) :: a
    real, dimension(3,12) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
     !3+sub111
    a(1,1,2) = 0
    a(1,2,2) = 0
    a(1,3,2) = 1
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 1
    a(3,3,2) = 0
    
    !3+sub-11-1
    a(1,1,3) = 0
    a(1,2,3) = 0
    a(1,3,3) = 1
    a(2,1,3) = -1
    a(2,2,3) = 0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = -1
    a(3,3,3) = 0
    
    !3+sub1-1-1
    a(1,1,4) = 0
    a(1,2,4) = 0
    a(1,3,4) = -1
    a(2,1,4) = -1
    a(2,2,4) = 0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 1
    a(3,3,4) = 0
    
    !3+sub-1-11
    a(1,1,5) = 0
    a(1,2,5) = 0
    a(1,3,5) = -1
    a(2,1,5) = 1
    a(2,2,5) = 0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = -1
    a(3,3,5) = 0
    
    !3-sub111
    a(1,1,6) = 0
    a(1,2,6) = 1
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = 0
    a(2,3,6) = 1
    a(3,1,6) = 1
    a(3,2,6) = 0
    a(3,3,6) = 0

    !3-sub1-1-1
    a(1,1,7) = 0
    a(1,2,7) = -1
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = 0
    a(2,3,7) = 1
    a(3,1,7) = -1
    a(3,2,7) = 0
    a(3,3,7) = 0

    !3-sub-1-11
    a(1,1,8) = 0
    a(1,2,8) = 1
    a(1,3,8) = 0
    a(2,1,8) = 0
    a(2,2,8) = 0
    a(2,3,8) = -1
    a(3,1,8) = -1
    a(3,2,8) = 0
    a(3,3,8) = 0

    !3sub-11-1
    a(1,1,9) = 0
    a(1,2,9) = -1
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = 0
    a(2,3,9) = -1
    a(3,1,9) = 1
    a(3,2,9) = 0
    a(3,3,9) = 0
    
    !2sub001
    a(1,1,10) = -1
    a(1,2,10) = 0
    a(1,3,10) = 0
    a(2,1,10) = 0
    a(2,2,10) = -1
    a(2,3,10) = 0
    a(3,1,10) = 0
    a(3,2,10) = 0
    a(3,3,10) = 1

    !2sub010
    a(1,1,11) = -1
    a(1,2,11) = 0
    a(1,3,11) = 0
    a(2,1,11) = 0
    a(2,2,11) = 1
    a(2,3,11) = 0
    a(3,1,11) = 0
    a(3,2,11) = 0
    a(3,3,11) = -1

    !2sub100
    a(1,1,12) = 1
    a(1,2,12) = 0
    a(1,3,12) = 0
    a(2,1,12) = 0
    a(2,2,12) = -1
    a(2,3,12) = 0
    a(3,1,12) = 0
    a(3,2,12) = 0
    a(3,3,12) = -1
    
    Do m = 1, 12
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 12
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine T_28
    
subroutine Th_29
    use commonvar
    implicit none
    real, dimension(3,3,24) :: a
    real, dimension(3,24) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
     !3+sub111
    a(1,1,2) = 0
    a(1,2,2) = 0
    a(1,3,2) = 1
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 1
    a(3,3,2) = 0
    
    !3+sub-11-1
    a(1,1,3) = 0
    a(1,2,3) = 0
    a(1,3,3) = 1
    a(2,1,3) = -1
    a(2,2,3) = 0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = -1
    a(3,3,3) = 0
    
    !3+sub1-1-1
    a(1,1,4) = 0
    a(1,2,4) = 0
    a(1,3,4) = -1
    a(2,1,4) = -1
    a(2,2,4) = 0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 1
    a(3,3,4) = 0
    
    !3+sub-1-11
    a(1,1,5) = 0
    a(1,2,5) = 0
    a(1,3,5) = -1
    a(2,1,5) = 1
    a(2,2,5) = 0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = -1
    a(3,3,5) = 0
    
    !3-sub111
    a(1,1,6) = 0
    a(1,2,6) = 1
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = 0
    a(2,3,6) = 1
    a(3,1,6) = 1
    a(3,2,6) = 0
    a(3,3,6) = 0

    !3-sub1-1-1
    a(1,1,7) = 0
    a(1,2,7) = -1
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = 0
    a(2,3,7) = 1
    a(3,1,7) = -1
    a(3,2,7) = 0
    a(3,3,7) = 0

    !3-sub-1-11
    a(1,1,8) = 0
    a(1,2,8) = 1
    a(1,3,8) = 0
    a(2,1,8) = 0
    a(2,2,8) = 0
    a(2,3,8) = -1
    a(3,1,8) = -1
    a(3,2,8) = 0
    a(3,3,8) = 0

    !3sub-11-1
    a(1,1,9) = 0
    a(1,2,9) = -1
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = 0
    a(2,3,9) = -1
    a(3,1,9) = 1
    a(3,2,9) = 0
    a(3,3,9) = 0
    
    !2sub001
    a(1,1,10) = -1
    a(1,2,10) = 0
    a(1,3,10) = 0
    a(2,1,10) = 0
    a(2,2,10) = -1
    a(2,3,10) = 0
    a(3,1,10) = 0
    a(3,2,10) = 0
    a(3,3,10) = 1

    !2sub010
    a(1,1,11) = -1
    a(1,2,11) = 0
    a(1,3,11) = 0
    a(2,1,11) = 0
    a(2,2,11) = 1
    a(2,3,11) = 0
    a(3,1,11) = 0
    a(3,2,11) = 0
    a(3,3,11) = -1

    !2sub100
    a(1,1,12) = 1
    a(1,2,12) = 0
    a(1,3,12) = 0
    a(2,1,12) = 0
    a(2,2,12) = -1
    a(2,3,12) = 0
    a(3,1,12) = 0
    a(3,2,12) = 0
    a(3,3,12) = -1
    
    !i
    a(1,1,13) = -1
    a(1,2,13) = 0
    a(1,3,13) = 0
    a(2,1,13) = 0
    a(2,2,13) = -1
    a(2,3,13) = 0
    a(3,1,13) = 0
    a(3,2,13) = 0
    a(3,3,13) = -1
    
     !-3+sub111
    a(1,1,14) = 0
    a(1,2,14) = 0
    a(1,3,14) = -1
    a(2,1,14) = -1
    a(2,2,14) = 0
    a(2,3,14) = 0
    a(3,1,14) = 0
    a(3,2,14) = -1
    a(3,3,14) = 0

    !-3+sub-11-1
    a(1,1,15) = 0
    a(1,2,15) = 0
    a(1,3,15) = -1
    a(2,1,15) = 1
    a(2,2,15) = 0
    a(2,3,15) = 0
    a(3,1,15) = 0
    a(3,2,15) = 1
    a(3,3,15) = 0

    !-3+sub1-1-1
    a(1,1,16) = 0
    a(1,2,16) = 0
    a(1,3,16) = 1
    a(2,1,16) = 1
    a(2,2,16) = 0
    a(2,3,16) = 0
    a(3,1,16) = 0
    a(3,2,16) = -1
    a(3,3,16) = 0
    
    !-3+sub-1-11
    a(1,1,17) = 0
    a(1,2,17) = 0
    a(1,3,17) = 1
    a(2,1,17) = -1
    a(2,2,17) = 0
    a(2,3,17) = 0
    a(3,1,17) = 0
    a(3,2,17) = 1
    a(3,3,17) = 0
    
    !-3-sub111
    a(1,1,18) = 0
    a(1,2,18) = -1
    a(1,3,18) = 0
    a(2,1,18) = 0
    a(2,2,18) = 0
    a(2,3,18) = -1
    a(3,1,18) = -1
    a(3,2,18) = 0
    a(3,3,18) = 0

    !-3-sub1-1-1
    a(1,1,19) = 0
    a(1,2,19) = 1
    a(1,3,19) = 0
    a(2,1,19) = 0
    a(2,2,19) = 0
    a(2,3,19) = -1
    a(3,1,19) = 1
    a(3,2,19) = 0
    a(3,3,19) = 0

    !-3-sub-1-11
    a(1,1,20) = 0
    a(1,2,20) = -1
    a(1,3,20) = 0
    a(2,1,20) = 0
    a(2,2,20) = 0
    a(2,3,20) = 1
    a(3,1,20) = 1
    a(3,2,20) = 0
    a(3,3,20) = 0

    !-3-sub-11-1
    a(1,1,21) = 0
    a(1,2,21) = 1
    a(1,3,21) = 0
    a(2,1,21) = 0
    a(2,2,21) = 0
    a(2,3,21) = 1
    a(3,1,21) = -1
    a(3,2,21) = 0
    a(3,3,21) = 0

    !msub001
    a(1,1,22) = 1
    a(1,2,22) = 0
    a(1,3,22) = 0
    a(2,1,22) = 0
    a(2,2,22) = 1
    a(2,3,22) = 0
    a(3,1,22) = 0
    a(3,2,22) = 0
    a(3,3,22) = -1

    !msub010
    a(1,1,23) = 1
    a(1,2,23) = 0
    a(1,3,23) = 0
    a(2,1,23) = 0
    a(2,2,23) = -1
    a(2,3,23) = 0
    a(3,1,23) = 0
    a(3,2,23) = 0
    a(3,3,23) = 1

    !msub100
    a(1,1,24) = -1
    a(1,2,24) = 0
    a(1,3,24) = 0
    a(2,1,24) = 0
    a(2,2,24) = 1
    a(2,3,24) = 0
    a(3,1,24) = 0
    a(3,2,24) = 0
    a(3,3,24) = 1
    
    Do m = 1, 24
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 24
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 24
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do

end subroutine Th_29

subroutine O_30
    use commonvar
    implicit none
    real, dimension(3,3,24) :: a
    real, dimension(3,24) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !3+sub111
    a(1,1,2) = 0
    a(1,2,2) = 0
    a(1,3,2) = 1
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 1
    a(3,3,2) = 0
    
    !3+sub-11-1
    a(1,1,3) = 0
    a(1,2,3) = 0
    a(1,3,3) = 1
    a(2,1,3) = -1
    a(2,2,3) = 0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = -1
    a(3,3,3) = 0
    
    !3+sub1-1-1
    a(1,1,4) = 0
    a(1,2,4) = 0
    a(1,3,4) = -1
    a(2,1,4) = -1
    a(2,2,4) = 0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 1
    a(3,3,4) = 0
    
    !3+sub-1-11
    a(1,1,5) = 0
    a(1,2,5) = 0
    a(1,3,5) = -1
    a(2,1,5) = 1
    a(2,2,5) = 0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = -1
    a(3,3,5) = 0
    
    !3-sub111
    a(1,1,6) = 0
    a(1,2,6) = 1
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = 0
    a(2,3,6) = 1
    a(3,1,6) = 1
    a(3,2,6) = 0
    a(3,3,6) = 0

    !3-sub1-1-1
    a(1,1,7) = 0
    a(1,2,7) = -1
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = 0
    a(2,3,7) = 1
    a(3,1,7) = -1
    a(3,2,7) = 0
    a(3,3,7) = 0

    !3-sub-1-11
    a(1,1,8) = 0
    a(1,2,8) = 1
    a(1,3,8) = 0
    a(2,1,8) = 0
    a(2,2,8) = 0
    a(2,3,8) = -1
    a(3,1,8) = -1
    a(3,2,8) = 0
    a(3,3,8) = 0

    !3sub-11-1
    a(1,1,9) = 0
    a(1,2,9) = -1
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = 0
    a(2,3,9) = -1
    a(3,1,9) = 1
    a(3,2,9) = 0
    a(3,3,9) = 0

    !2sub110
    a(1,1,10) = 0
    a(1,2,10) = 1
    a(1,3,10) = 0
    a(2,1,10) = 1
    a(2,2,10) = 0
    a(2,3,10) = 0
    a(3,1,10) = 0
    a(3,2,10) = 0
    a(3,3,10) = -1
    
    !2sub1-10
    a(1,1,11) = 0
    a(1,2,11) = -1
    a(1,3,11) = 0
    a(2,1,11) = -1
    a(2,2,11) = 0
    a(2,3,11) = 0
    a(3,1,11) = 0
    a(3,2,11) = 0
    a(3,3,11) = -1
    
    !2sub011
    a(1,1,12) = -1
    a(1,2,12) = 0
    a(1,3,12) = 0
    a(2,1,12) = 0
    a(2,2,12) = 0
    a(2,3,12) = 1
    a(3,1,12) = 0
    a(3,2,12) = 1
    a(3,3,12) = 0

    !2sub01-1
    a(1,1,13) = -1
    a(1,2,13) = 0
    a(1,3,13) = 0
    a(2,1,13) = 0
    a(2,2,13) = 0
    a(2,3,13) = -1
    a(3,1,13) = 0
    a(3,2,13) = -1
    a(3,3,13) = 0

    !2sub101
    a(1,1,14) = 0
    a(1,2,14) = 0
    a(1,3,14) = 1
    a(2,1,14) = 0
    a(2,2,14) = -1
    a(2,3,14) = 0
    a(3,1,14) = 1
    a(3,2,14) = 0
    a(3,3,14) = 0

    !2sub-101
    a(1,1,15) = 0
    a(1,2,15) = 0
    a(1,3,15) = -1
    a(2,1,15) = 0
    a(2,2,15) = -1
    a(2,3,15) = 0
    a(3,1,15) = -1
    a(3,2,15) = 0
    a(3,3,15) = 0

    !4-sub001
    a(1,1,16) = 0
    a(1,2,16) = 1
    a(1,3,16) = 0
    a(2,1,16) = -1
    a(2,2,16) = 0
    a(2,3,16) = 0
    a(3,1,16) = 0
    a(3,2,16) = 0
    a(3,3,16) = 1

    !4+sub001
    a(1,1,17) = 0
    a(1,2,17) = -1
    a(1,3,17) = 0
    a(2,1,17) = 1
    a(2,2,17) = 0
    a(2,3,17) = 0
    a(3,1,17) = 0
    a(3,2,17) = 0
    a(3,3,17) = 1

    !4-sub100
    a(1,1,18) = 1
    a(1,2,18) = 0
    a(1,3,18) = 0
    a(2,1,18) = 0
    a(2,2,18) = 0
    a(2,3,18) = 1
    a(3,1,18) = 0
    a(3,2,18) = -1
    a(3,3,18) = 0

    !4+sub100
    a(1,1,19) = 1
    a(1,2,19) = 0
    a(1,3,19) = 0
    a(2,1,19) = 0
    a(2,2,19) = 0
    a(2,3,19) = -1
    a(3,1,19) = 0
    a(3,2,19) = 1
    a(3,3,19) = 0

    !4+sub010
    a(1,1,20) = 0
    a(1,2,20) = 0
    a(1,3,20) = 1
    a(2,1,20) = 0
    a(2,2,20) = 1
    a(2,3,20) = 0
    a(3,1,20) = -1
    a(3,2,20) = 0
    a(3,3,20) = 0

    !4-sub010
    a(1,1,21) = 0
    a(1,2,21) = 0
    a(1,3,21) = -1
    a(2,1,21) = 0
    a(2,2,21) = 1
    a(2,3,21) = 0
    a(3,1,21) = 1
    a(3,2,21) = 0
    a(3,3,21) = 0

    !2sub001
    a(1,1,22) = -1
    a(1,2,22) = 0
    a(1,3,22) = 0
    a(2,1,22) = 0
    a(2,2,22) = -1
    a(2,3,22) = 0
    a(3,1,22) = 0
    a(3,2,22) = 0
    a(3,3,22) = 1

    !2sub010
    a(1,1,23) = -1
    a(1,2,23) = 0
    a(1,3,23) = 0
    a(2,1,23) = 0
    a(2,2,23) = 1
    a(2,3,23) = 0
    a(3,1,23) = 0
    a(3,2,23) = 0
    a(3,3,23) = -1

    !2sub100
    a(1,1,24) = 1
    a(1,2,24) = 0
    a(1,3,24) = 0
    a(2,1,24) = 0
    a(2,2,24) = -1
    a(2,3,24) = 0
    a(3,1,24) = 0
    a(3,2,24) = 0
    a(3,3,24) = -1
    
    Do m = 1, 24
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 24
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 24
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine O_30

subroutine Td_31
    use commonvar
    implicit none
    real, dimension(3,3,24) :: a
    real, dimension(3,24) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
     !3+sub111
    a(1,1,2) = 0
    a(1,2,2) = 0
    a(1,3,2) = 1
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 1
    a(3,3,2) = 0
    
    !3+sub-11-1
    a(1,1,3) = 0
    a(1,2,3) = 0
    a(1,3,3) = 1
    a(2,1,3) = -1
    a(2,2,3) = 0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = -1
    a(3,3,3) = 0
    
    !3+sub1-1-1
    a(1,1,4) = 0
    a(1,2,4) = 0
    a(1,3,4) = -1
    a(2,1,4) = -1
    a(2,2,4) = 0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 1
    a(3,3,4) = 0
    
    !3+sub-1-11
    a(1,1,5) = 0
    a(1,2,5) = 0
    a(1,3,5) = -1
    a(2,1,5) = 1
    a(2,2,5) = 0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = -1
    a(3,3,5) = 0
    
    !3-sub111
    a(1,1,6) = 0
    a(1,2,6) = 1
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = 0
    a(2,3,6) = 1
    a(3,1,6) = 1
    a(3,2,6) = 0
    a(3,3,6) = 0

    !3-sub1-1-1
    a(1,1,7) = 0
    a(1,2,7) = -1
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = 0
    a(2,3,7) = 1
    a(3,1,7) = -1
    a(3,2,7) = 0
    a(3,3,7) = 0

    !3-sub-1-11
    a(1,1,8) = 0
    a(1,2,8) = 1
    a(1,3,8) = 0
    a(2,1,8) = 0
    a(2,2,8) = 0
    a(2,3,8) = -1
    a(3,1,8) = -1
    a(3,2,8) = 0
    a(3,3,8) = 0

    !3sub-11-1
    a(1,1,9) = 0
    a(1,2,9) = -1
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = 0
    a(2,3,9) = -1
    a(3,1,9) = 1
    a(3,2,9) = 0
    a(3,3,9) = 0
    
    !2sub001
    a(1,1,10) = -1
    a(1,2,10) = 0
    a(1,3,10) = 0
    a(2,1,10) = 0
    a(2,2,10) = -1
    a(2,3,10) = 0
    a(3,1,10) = 0
    a(3,2,10) = 0
    a(3,3,10) = 1

    !2sub010
    a(1,1,11) = -1
    a(1,2,11) = 0
    a(1,3,11) = 0
    a(2,1,11) = 0
    a(2,2,11) = 1
    a(2,3,11) = 0
    a(3,1,11) = 0
    a(3,2,11) = 0
    a(3,3,11) = -1

    !2sub100
    a(1,1,12) = 1
    a(1,2,12) = 0
    a(1,3,12) = 0
    a(2,1,12) = 0
    a(2,2,12) = -1
    a(2,3,12) = 0
    a(3,1,12) = 0
    a(3,2,12) = 0
    a(3,3,12) = -1

    !-4+sub001
    a(1,1,13) = 0
    a(1,2,13) = 1
    a(1,3,13) = 0
    a(2,1,13) = -1
    a(2,2,13) = 0
    a(2,3,13) = 0
    a(3,1,13) = 0
    a(3,2,13) = 0
    a(3,3,13) = -1
    
    !-4-sub001
    a(1,1,14) = 0
    a(1,2,14) = -1
    a(1,3,14) = 0
    a(2,1,14) = 1
    a(2,2,14) = 0
    a(2,3,14) = 0
    a(3,1,14) = 0
    a(3,2,14) = 0
    a(3,3,14) = -1

    !-4+sub100
    a(1,1,15) = -1
    a(1,2,15) = 0
    a(1,3,15) = 0
    a(2,1,15) = 0
    a(2,2,15) = 0
    a(2,3,15) = 1
    a(3,1,15) = 0
    a(3,2,15) = -1
    a(3,3,15) = 0

    !-4-sub100
    a(1,1,16) = -1
    a(1,2,16) = 0
    a(1,3,16) = 0
    a(2,1,16) = 0
    a(2,2,16) = 0
    a(2,3,16) = -1
    a(3,1,16) = 0
    a(3,2,16) = 1
    a(3,3,16) = 0

    !-4-sub010
    a(1,1,17) = 0
    a(1,2,17) = 0
    a(1,3,17) = 1
    a(2,1,17) = 0
    a(2,2,17) = -1
    a(2,3,17) = 0
    a(3,1,17) = -1
    a(3,2,17) = 0
    a(3,3,17) = 0

    !-4+sub010
    a(1,1,18) = 0
    a(1,2,18) = 0
    a(1,3,18) = -1
    a(2,1,18) = 0
    a(2,2,18) = -1
    a(2,3,18) = 0
    a(3,1,18) = 1
    a(3,2,18) = 0
    a(3,3,18) = 0           
    
    !msub1-10
    a(1,1,19) = 0
    a(1,2,19) = 1
    a(1,3,19) = 0
    a(2,1,19) = 1
    a(2,2,19) = 0
    a(2,3,19) = 0
    a(3,1,19) = 0
    a(3,2,19) = 0
    a(3,3,19) = 1

    !msub110
    a(1,1,20) = 0
    a(1,2,20) = -1
    a(1,3,20) = 0
    a(2,1,20) = -1
    a(2,2,20) = 0
    a(2,3,20) = 0
    a(3,1,20) = 0
    a(3,2,20) = 0
    a(3,3,20) = 1

    !msub01-1
    a(1,1,21) = 1
    a(1,2,21) = 0
    a(1,3,21) = 0
    a(2,1,21) = 0
    a(2,2,21) = 0
    a(2,3,21) = 1
    a(3,1,21) = 0
    a(3,2,21) = 1
    a(3,3,21) = 0

    !msub011
    a(1,1,22) = 1
    a(1,2,22) = 0
    a(1,3,22) = 0
    a(2,1,22) = 0
    a(2,2,22) = 0
    a(2,3,22) = -1
    a(3,1,22) = 0
    a(3,2,22) = -1
    a(3,3,22) = 0

    !msub-101
    a(1,1,23) = 0
    a(1,2,23) = 0
    a(1,3,23) = 1
    a(2,1,23) = 0
    a(2,2,23) = 1
    a(2,3,23) = 0
    a(3,1,23) = 1
    a(3,2,23) = 0
    a(3,3,23) = 0

    !msub101
    a(1,1,24) = 0
    a(1,2,24) = 0
    a(1,3,24) = -1
    a(2,1,24) = 0
    a(2,2,24) = 1
    a(2,3,24) = 0
    a(3,1,24) = -1
    a(3,2,24) = 0
    a(3,3,24) = 0
    
    Do m = 1, 24
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 24
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 24
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
    
end subroutine Td_31
    
subroutine Oh_32
    use commonvar
    implicit none
    real, dimension(3,3,48) :: a
    real, dimension(3,48) :: hklold
    
    !E
    a(1,1,1) = 1
    a(1,2,1) = 0
    a(1,3,1) = 0
    a(2,1,1) = 0
    a(2,2,1) = 1
    a(2,3,1) = 0
    a(3,1,1) = 0
    a(3,2,1) = 0
    a(3,3,1) = 1
    
    !3+sub111
    a(1,1,2) = 0
    a(1,2,2) = 0
    a(1,3,2) = 1
    a(2,1,2) = 1
    a(2,2,2) = 0
    a(2,3,2) = 0
    a(3,1,2) = 0
    a(3,2,2) = 1
    a(3,3,2) = 0
    
    !3+sub-11-1
    a(1,1,3) = 0
    a(1,2,3) = 0
    a(1,3,3) = 1
    a(2,1,3) = -1
    a(2,2,3) = 0
    a(2,3,3) = 0
    a(3,1,3) = 0
    a(3,2,3) = -1
    a(3,3,3) = 0
    
    !3+sub1-1-1
    a(1,1,4) = 0
    a(1,2,4) = 0
    a(1,3,4) = -1
    a(2,1,4) = -1
    a(2,2,4) = 0
    a(2,3,4) = 0
    a(3,1,4) = 0
    a(3,2,4) = 1
    a(3,3,4) = 0
    
    !3+sub-1-11
    a(1,1,5) = 0
    a(1,2,5) = 0
    a(1,3,5) = -1
    a(2,1,5) = 1
    a(2,2,5) = 0
    a(2,3,5) = 0
    a(3,1,5) = 0
    a(3,2,5) = -1
    a(3,3,5) = 0
    
    !3-sub111
    a(1,1,6) = 0
    a(1,2,6) = 1
    a(1,3,6) = 0
    a(2,1,6) = 0
    a(2,2,6) = 0
    a(2,3,6) = 1
    a(3,1,6) = 1
    a(3,2,6) = 0
    a(3,3,6) = 0

    !3-sub1-1-1
    a(1,1,7) = 0
    a(1,2,7) = -1
    a(1,3,7) = 0
    a(2,1,7) = 0
    a(2,2,7) = 0
    a(2,3,7) = 1
    a(3,1,7) = -1
    a(3,2,7) = 0
    a(3,3,7) = 0

    !3-sub-1-11
    a(1,1,8) = 0
    a(1,2,8) = 1
    a(1,3,8) = 0
    a(2,1,8) = 0
    a(2,2,8) = 0
    a(2,3,8) = -1
    a(3,1,8) = -1
    a(3,2,8) = 0
    a(3,3,8) = 0

    !3sub-11-1
    a(1,1,9) = 0
    a(1,2,9) = -1
    a(1,3,9) = 0
    a(2,1,9) = 0
    a(2,2,9) = 0
    a(2,3,9) = -1
    a(3,1,9) = 1
    a(3,2,9) = 0
    a(3,3,9) = 0

    !2sub110
    a(1,1,10) = 0
    a(1,2,10) = 1
    a(1,3,10) = 0
    a(2,1,10) = 1
    a(2,2,10) = 0
    a(2,3,10) = 0
    a(3,1,10) = 0
    a(3,2,10) = 0
    a(3,3,10) = -1
    
    !2sub1-10
    a(1,1,11) = 0
    a(1,2,11) = -1
    a(1,3,11) = 0
    a(2,1,11) = -1
    a(2,2,11) = 0
    a(2,3,11) = 0
    a(3,1,11) = 0
    a(3,2,11) = 0
    a(3,3,11) = -1
    
    !2sub011
    a(1,1,12) = -1
    a(1,2,12) = 0
    a(1,3,12) = 0
    a(2,1,12) = 0
    a(2,2,12) = 0
    a(2,3,12) = 1
    a(3,1,12) = 0
    a(3,2,12) = 1
    a(3,3,12) = 0

    !2sub01-1
    a(1,1,13) = -1
    a(1,2,13) = 0
    a(1,3,13) = 0
    a(2,1,13) = 0
    a(2,2,13) = 0
    a(2,3,13) = -1
    a(3,1,13) = 0
    a(3,2,13) = -1
    a(3,3,13) = 0

    !2sub101
    a(1,1,14) = 0
    a(1,2,14) = 0
    a(1,3,14) = 1
    a(2,1,14) = 0
    a(2,2,14) = -1
    a(2,3,14) = 0
    a(3,1,14) = 1
    a(3,2,14) = 0
    a(3,3,14) = 0

    !2sub-101
    a(1,1,15) = 0
    a(1,2,15) = 0
    a(1,3,15) = -1
    a(2,1,15) = 0
    a(2,2,15) = -1
    a(2,3,15) = 0
    a(3,1,15) = -1
    a(3,2,15) = 0
    a(3,3,15) = 0

    !4-sub001
    a(1,1,16) = 0
    a(1,2,16) = 1
    a(1,3,16) = 0
    a(2,1,16) = -1
    a(2,2,16) = 0
    a(2,3,16) = 0
    a(3,1,16) = 0
    a(3,2,16) = 0
    a(3,3,16) = 1

    !4+sub001
    a(1,1,17) = 0
    a(1,2,17) = -1
    a(1,3,17) = 0
    a(2,1,17) = 1
    a(2,2,17) = 0
    a(2,3,17) = 0
    a(3,1,17) = 0
    a(3,2,17) = 0
    a(3,3,17) = 1

    !4-sub100
    a(1,1,18) = 1
    a(1,2,18) = 0
    a(1,3,18) = 0
    a(2,1,18) = 0
    a(2,2,18) = 0
    a(2,3,18) = 1
    a(3,1,18) = 0
    a(3,2,18) = -1
    a(3,3,18) = 0

    !4+sub100
    a(1,1,19) = 1
    a(1,2,19) = 0
    a(1,3,19) = 0
    a(2,1,19) = 0
    a(2,2,19) = 0
    a(2,3,19) = -1
    a(3,1,19) = 0
    a(3,2,19) = 1
    a(3,3,19) = 0

    !4+sub010
    a(1,1,20) = 0
    a(1,2,20) = 0
    a(1,3,20) = 1
    a(2,1,20) = 0
    a(2,2,20) = 1
    a(2,3,20) = 0
    a(3,1,20) = -1
    a(3,2,20) = 0
    a(3,3,20) = 0

    !4-sub010
    a(1,1,21) = 0
    a(1,2,21) = 0
    a(1,3,21) = -1
    a(2,1,21) = 0
    a(2,2,21) = 1
    a(2,3,21) = 0
    a(3,1,21) = 1
    a(3,2,21) = 0
    a(3,3,21) = 0

    !2sub001
    a(1,1,22) = -1
    a(1,2,22) = 0
    a(1,3,22) = 0
    a(2,1,22) = 0
    a(2,2,22) = -1
    a(2,3,22) = 0
    a(3,1,22) = 0
    a(3,2,22) = 0
    a(3,3,22) = 1

    !2sub010
    a(1,1,23) = -1
    a(1,2,23) = 0
    a(1,3,23) = 0
    a(2,1,23) = 0
    a(2,2,23) = 1
    a(2,3,23) = 0
    a(3,1,23) = 0
    a(3,2,23) = 0
    a(3,3,23) = -1

    !2sub100
    a(1,1,24) = 1
    a(1,2,24) = 0
    a(1,3,24) = 0
    a(2,1,24) = 0
    a(2,2,24) = -1
    a(2,3,24) = 0
    a(3,1,24) = 0
    a(3,2,24) = 0
    a(3,3,24) = -1

    !i
    a(1,1,25) = -1
    a(1,2,25) = 0
    a(1,3,25) = 0
    a(2,1,25) = 0
    a(2,2,25) = -1
    a(2,3,25) = 0
    a(3,1,25) = 0
    a(3,2,25) = 0
    a(3,3,25) = -1

    !-4+sub001
    a(1,1,26) = 0
    a(1,2,26) = 1
    a(1,3,26) = 0
    a(2,1,26) = -1
    a(2,2,26) = 0
    a(2,3,26) = 0
    a(3,1,26) = 0
    a(3,2,26) = 0
    a(3,3,26) = -1
    
    !-4-sub001
    a(1,1,27) = 0
    a(1,2,27) = -1
    a(1,3,27) = 0
    a(2,1,27) = 1
    a(2,2,27) = 0
    a(2,3,27) = 0
    a(3,1,27) = 0
    a(3,2,27) = 0
    a(3,3,27) = -1

    !-4+sub100
    a(1,1,28) = -1
    a(1,2,28) = 0
    a(1,3,28) = 0
    a(2,1,28) = 0
    a(2,2,28) = 0
    a(2,3,28) = 1
    a(3,1,28) = 0
    a(3,2,28) = -1
    a(3,3,28) = 0

    !-4-sub100
    a(1,1,29) = -1
    a(1,2,29) = 0
    a(1,3,29) = 0
    a(2,1,29) = 0
    a(2,2,29) = 0
    a(2,3,29) = -1
    a(3,1,29) = 0
    a(3,2,29) = 1
    a(3,3,29) = 0

    !-4-sub010
    a(1,1,30) = 0
    a(1,2,30) = 0
    a(1,3,30) = 1
    a(2,1,30) = 0
    a(2,2,30) = -1
    a(2,3,30) = 0
    a(3,1,30) = -1
    a(3,2,30) = 0
    a(3,3,30) = 0

    !-4+sub010
    a(1,1,31) = 0
    a(1,2,31) = 0
    a(1,3,31) = -1
    a(2,1,31) = 0
    a(2,2,31) = -1
    a(2,3,31) = 0
    a(3,1,31) = 1
    a(3,2,31) = 0
    a(3,3,31) = 0

    !-3+sub111
    a(1,1,32) = 0
    a(1,2,32) = 0
    a(1,3,32) = -1
    a(2,1,32) = -1
    a(2,2,32) = 0
    a(2,3,32) = 0
    a(3,1,32) = 0
    a(3,2,32) = -1
    a(3,3,32) = 0

    !-3+sub-11-1
    a(1,1,33) = 0
    a(1,2,33) = 0
    a(1,3,33) = -1
    a(2,1,33) = 1
    a(2,2,33) = 0
    a(2,3,33) = 0
    a(3,1,33) = 0
    a(3,2,33) = 1
    a(3,3,33) = 0

    !-3+sub1-1-1
    a(1,1,34) = 0
    a(1,2,34) = 0
    a(1,3,34) = 1
    a(2,1,34) = 1
    a(2,2,34) = 0
    a(2,3,34) = 0
    a(3,1,34) = 0
    a(3,2,34) = -1
    a(3,3,34) = 0
    
    !-3+sub-1-11
    a(1,1,35) = 0
    a(1,2,35) = 0
    a(1,3,35) = 1
    a(2,1,35) = -1
    a(2,2,35) = 0
    a(2,3,35) = 0
    a(3,1,35) = 0
    a(3,2,35) = 1
    a(3,3,35) = 0
    
    !-3-sub111
    a(1,1,36) = 0
    a(1,2,36) = -1
    a(1,3,36) = 0
    a(2,1,36) = 0
    a(2,2,36) = 0
    a(2,3,36) = -1
    a(3,1,36) = -1
    a(3,2,36) = 0
    a(3,3,36) = 0

    !-3-sub1-1-1
    a(1,1,37) = 0
    a(1,2,37) = 1
    a(1,3,37) = 0
    a(2,1,37) = 0
    a(2,2,37) = 0
    a(2,3,37) = -1
    a(3,1,37) = 1
    a(3,2,37) = 0
    a(3,3,37) = 0

    !-3-sub-1-11
    a(1,1,38) = 0
    a(1,2,38) = -1
    a(1,3,38) = 0
    a(2,1,38) = 0
    a(2,2,38) = 0
    a(2,3,38) = 1
    a(3,1,38) = 1
    a(3,2,38) = 0
    a(3,3,38) = 0

    !-3-sub-11-1
    a(1,1,39) = 0
    a(1,2,39) = 1
    a(1,3,39) = 0
    a(2,1,39) = 0
    a(2,2,39) = 0
    a(2,3,39) = 1
    a(3,1,39) = -1
    a(3,2,39) = 0
    a(3,3,39) = 0

    !msub001
    a(1,1,40) = 1
    a(1,2,40) = 0
    a(1,3,40) = 0
    a(2,1,40) = 0
    a(2,2,40) = 1
    a(2,3,40) = 0
    a(3,1,40) = 0
    a(3,2,40) = 0
    a(3,3,40) = -1

    !msub010
    a(1,1,41) = 1
    a(1,2,41) = 0
    a(1,3,41) = 0
    a(2,1,41) = 0
    a(2,2,41) = -1
    a(2,3,41) = 0
    a(3,1,41) = 0
    a(3,2,41) = 0
    a(3,3,41) = 1

    !msub100
    a(1,1,42) = -1
    a(1,2,42) = 0
    a(1,3,42) = 0
    a(2,1,42) = 0
    a(2,2,42) = 1
    a(2,3,42) = 0
    a(3,1,42) = 0
    a(3,2,42) = 0
    a(3,3,42) = 1

    !msub1-10
    a(1,1,43) = 0
    a(1,2,43) = 1
    a(1,3,43) = 0
    a(2,1,43) = 1
    a(2,2,43) = 0
    a(2,3,43) = 0
    a(3,1,43) = 0
    a(3,2,43) = 0
    a(3,3,43) = 1

    !msub110
    a(1,1,44) = 0
    a(1,2,44) = -1
    a(1,3,44) = 0
    a(2,1,44) = -1
    a(2,2,44) = 0
    a(2,3,44) = 0
    a(3,1,44) = 0
    a(3,2,44) = 0
    a(3,3,44) = 1

    !msub01-1
    a(1,1,45) = 1
    a(1,2,45) = 0
    a(1,3,45) = 0
    a(2,1,45) = 0
    a(2,2,45) = 0
    a(2,3,45) = 1
    a(3,1,45) = 0
    a(3,2,45) = 1
    a(3,3,45) = 0

    !msub011
    a(1,1,46) = 1
    a(1,2,46) = 0
    a(1,3,46) = 0
    a(2,1,46) = 0
    a(2,2,46) = 0
    a(2,3,46) = -1
    a(3,1,46) = 0
    a(3,2,46) = -1
    a(3,3,46) = 0

    !msub-101
    a(1,1,47) = 0
    a(1,2,47) = 0
    a(1,3,47) = 1
    a(2,1,47) = 0
    a(2,2,47) = 1
    a(2,3,47) = 0
    a(3,1,47) = 1
    a(3,2,47) = 0
    a(3,3,47) = 0

    !msub101
    a(1,1,48) = 0
    a(1,2,48) = 0
    a(1,3,48) = -1
    a(2,1,48) = 0
    a(2,2,48) = 1
    a(2,3,48) = 0
    a(3,1,48) = -1
    a(3,2,48) = 0
    a(3,3,48) = 0
    
    Do m = 1, 48
        hklold(:,m) = matmul(hkl,a(:,:,m))
        !write(*,*) hklold(:,m)
    End Do

    counting_dimension = 1 
    Do n = 2, 48
        found = .false.
        Do o = 1, n -1
        
        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                !write(*,*) " At least one is equal to an old one"
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_dimension = counting_dimension + 1
        End IF
    End Do  

    allocate ( hklnew(3,counting_dimension) ) 
    hklnew(:,1) = hklold(:,1) 

    counting_hklnew = 1
    Do n = 2, 48
        found = .false.
        Do o = 1, n -1

        test = ( hklold(:,o) == hklold(:,n))
            
            IF ( all(test) ) THEN 
                found = .true.
            End IF
        End Do

        IF ( found .eqv. .false. ) THEN 
            counting_hklnew = counting_hklnew + 1
            hklnew(:,counting_hklnew) = hklold(:,n)
        End IF
    End Do  

    Do i = 1, counting_hklnew
        write(*,*) hklnew(:,i)
    End Do
        
end subroutine Oh_32
    
