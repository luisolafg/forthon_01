program output1

   implicit none
   real :: a,b,c

   a = 1.0
   b = 2.0
   c = 3.0
   write(*,*) 'a',a
   call prompt()
   write(*,*) 'b',b
   call prompt()
   write(*,*) 'c',c
   call prompt()
   write(*,*) 'a*b*c',a * b * c

end program output1

subroutine prompt()

  implicit none
  character :: answer
  print *, 'type y to continue or any other key to finish'
  read *, answer
  if (answer /= 'y') stop

end subroutine prompt
