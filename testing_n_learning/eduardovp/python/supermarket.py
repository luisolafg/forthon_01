prices = {
    "banana" : 4,
    "apple"  : 2,
    "orange" : 1.5,
    "pear"   : 3,
}
stock = {
    "banana" : 6,
    "apple"  : 0,
    "orange" : 32,
    "pear"   : 15,
}

for key in prices:
    print '\n',str.upper(key) + ':'
    print "Price: %s" % prices[key]
    print "Stock: %s" % stock[key]
    print "Total value in stock = $%s\n" % (prices[key] * stock[key])

total = 0

for key in prices:
    total = total + float(prices.get(key) * stock.get(key))
print "Grand Total = $%s" % total, '\n'
