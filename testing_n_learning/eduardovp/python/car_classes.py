class Car(object):
    condition = "new"
    def __init__(self, model, color, mpg):
        self.model = model
        self.color = color
        self.mpg   = mpg
    def display_car(self):
         return 'This is a %s %s with %d MPG.' %(self.color,self.model,self.mpg)
    def drive_car(self):     
        self.condition = "used"
        
class ElectricCar(Car):

    def __init__(self, model, color, mpg, battery_type):
        self.model = model
        self.color = color
        self.mpg = mpg
        self.battery_type = battery_type
    def drive_car(self):     
        self.condition = "like new"
        

    
my_car = ElectricCar("model", "colour", "mpg", "molten salt")
my_car = Car("DeLorean", "silver", 88)
my_car = ElectricCar("fiesta", "black", 100, "molten salt")
    
#if __name__ == "__name__":  "no se porque no fuciona con esta parte"
        
print my_car.condition
print my_car.model
print my_car.color
print my_car.mpg
print my_car.condition
print my_car.drive_car()