def resolver_ecuacion():
	
    print
    
    a = float(raw_input(" a= "))
    b = float(raw_input(" b= "))
    c = float(raw_input(" c= "))

    print
    
    # los print solos son para dejar espacios antes y desues
    # la D es para definir el discriminante

    D = (b ** 2 - 4 * a * c)

    # lo primero es para evitar que de error con valores de "cero"

    if (a == 0 and b != 0):
      
      print " Esta es una ecuacion lineal\n"
      
      x= (-c / b)
      
      print " x= %s\n" %x
      
    elif (a == 0 and b == 0):
      
      print " Esta es una funcion constante\n\n", " c= %s\n" %c

    # de aqui en delante es para resolver los 3 casos de la ecuacion cuadratica

    elif (D > 0):
      
    # import math as mt R = mt.sqrt(D)
    # usar **0.5 es igual a usar math.sqrt

	
      R = D ** 0.5
      
      print ' Tiene dos soluciones \n'

      x1 = (-b + R) / (2 * a)
      x2 = (-b - R) / (2 * a)

      print " x1= %s" %x1 , "  x2= %s \n" %x2
      
    elif (D == 0): 
    
      R = 0
      
      print ' Solo tiene una solucion \n'
      
      x1 = (-b + R) / (2 * a)

      print " x1= %s \n" %x1
      
    elif (D < 0):
      
      import math as mt
      
      R = mt.sqrt(-D)
      
      print  " Resultados complejos \n"
      
      ''' lo siguiente es un acomodo para separar la parte real de la parte imaginaria
      ya que no supe como usar numeros complejos '''
      
      x1 = ((-b) / (2 * a))
      xuno = (+R / 2 * a)
      
      x2 = ((-b) / (2 * a))
      xdos = (-R / 2 * a)
      
      # las i son para que aparezca en la segunda parte del numero como complejo
      
      
      print " x1= %s" %x1,"+", xuno,"i", "  x2=" , x2, "+", xdos, "i", "\n"
      

def resolver_nueva():
    print " Quieres resolver otra ecuacion?"
    print " si, presiona 'y'"
    print " no, presiona cualquier tecla "
    return raw_input(" ").lower() == "y"

if(__name__ == "__main__"):
    while True:
	try:
	    resolver_ecuacion()
	    if not resolver_nueva():
		print "\n OK hasta pronto... \n"
		break
	
	except ValueError:
	  print "\n Esto no es un numero real prueba de nuevo"	
	  resolver_nueva()
	  
    #from mi_ruta.mi_fichero,py import resolver_nueva, resolver_ecuacion
