precios = {'manzana': 2.50, 'naranja': 1.50}
mi_compra = {
    'manzana': 4,
    'naranja': 2}
costo_total = sum(precios[fruta] * mi_compra[fruta]
                   for fruta in mi_compra)
print 'le debo al cajero $%.2f' % costo_total

# puedes usar el simbolo % o simplemente poner una coma y luego el costo_total