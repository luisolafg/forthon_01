

def play_game():
    if int(raw_input("Guess a number between 0 and 9:"))!= 5:
          print "You Lose!"
    else:
          print "You Win!"

def play_again():
    return raw_input("Play Again?").lower() == "y"

while True:
    play_game()
    if not play_again(): break

print "OK Goodbye..."

