from random import randint


def game():
    # Generates a number from 1 through 10 inclusive
    random_number = randint(1, 10)

    guesses_left = 3

    while guesses_left > 0:
        print "\n you have %i chances\n" %(guesses_left) 
        guess = int(raw_input(" guess a number from 1 to 10 "))
        if random_number == guess:
            print "\n You win!\n"
            break
        guesses_left -= 1
    else:
        print "\n You lose!\n"
        
game()
