import numpy as np
import h5py


def h5man():
    with h5py.File('TPI_M2_poi4888_4749.h5', "r") as h5file:
        #print(h5file.keys())
        dataset1 = h5file['1.1/instrument/avg_Al_Ka1/data']
        dataset2 = h5file['2.1/measurement/enetraj']
        print(dataset1)
        print(dataset2)
        a = dataset1[:]
        b = dataset2[...]
       
        oper = a / b
        #print(oper)
        
        combined = np.vstack((a, b)).T
        combinedoper = np.vstack((b, oper)).T
        #print(a)
        #print(b)
        #print(combined.shape)
        #print(combined)
        
        np.savetxt("athena2.txt",combined,fmt='%10.5f', header= 'pruebas de Eduardo \n')
        np.savetxt("athena3.txt",combinedoper,fmt='%10.5f', header= 'pruebas de Eduardo oper \nprueba')


if __name__ == "__main__":
    h5man()
