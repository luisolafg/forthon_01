
import numpy as np
from numpy import sin,cos,pi,abs
from mayavi import mlab

#ctexmat = np.loadtxt("BaTiO3_sijE.dat", dtype='i', delimiter=',')
ctexmat = np.loadtxt("BaTiO3_sijE_result.dat")

print(ctexmat)

c11=ctexmat[0,0];   c12=ctexmat[0,1];  c13=ctexmat[0,2];   c14=ctexmat[0,3];    c15=ctexmat[0,4];     c16=ctexmat[0,5]
c22=ctexmat[1,1];   c23=ctexmat[1,2];  c24=ctexmat[1,3];   c25=ctexmat[1,4];    c26=ctexmat[1,5]       
c33=ctexmat[2,2];   c34=ctexmat[2,3];  c35=ctexmat[2,4];   c36=ctexmat[2,5]       
c44=ctexmat[3,3];   c45=ctexmat[3,4];  c46=ctexmat[3,5]
c55=ctexmat[4,4];   c56=ctexmat[4,5]     
c66=ctexmat[5,5]

phi, beta = np.mgrid[0:np.pi:180j,0:2*np.pi:360j]
x = sin(phi)*cos(beta)
y = sin(phi)*sin(beta)
z = cos(phi)

c1 = c11*(x**4) + 2*c12*(x**2)*(y**2) + 2*c13*(x**2)*(z**2) + 2*c14*(x**2)*y*z + 2*c15*(x**3)*z + 2*c16*(x**3)*y
c2 = c22*(y**4) + 2*c23*(y**2)*(z**2) + 2*c24*(y**3)*z + 2*c25*x*(y**2)*z + 2*c26*x*(y**3)
c3 = c33*(z**4) + 2*c34*y*(z**3) + 2*c35*x*(z**3) + 2*c36*x*y*(z**2)
c4 = c44*(y**2)*(z**2) + 2*c45*x*y*(z**2) + 2*c46*x*(y**2)*z
c5 = c55*(x**2)*(z**2) + 2*c56*(x**2)*y*z + c66*(x**2)*(y**2)

C = c1+c2+c3+c4+c5

x1 = C*sin(phi)*cos(beta)
y1 = C*sin(phi)*sin(beta)
z1 = C*cos(phi)

mlab.figure()
mlab.mesh(x1,y1,z1,scalars=np.abs(C))
mlab.scalarbar(orientation="vertical")
mlab.show()
