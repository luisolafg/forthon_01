Program ELASFIB
    implicit none
    REAL (KIND=8), DIMENSION (6, 6) :: smat
    REAL (KIND=8), DIMENSION (3,3,3,3) :: sten
    REAL (KIND=8), DIMENSION (3,3) :: Q
    REAL (KIND=8), DIMENSION (3,3,3,3) :: srotten, stexten
    REAL (KIND=8), DIMENSION (6,6) :: stexmat
    REAL (KIND=8) :: phi1, phi, phi2, pi, beta, R
    REAL (KIND=8) :: a, b, c, d, dphi, dbeta, omeg, su, total
    INTEGER :: i, j, k, l, m, n, o, p, ll, nn, npas, np, nphi, nb, nbeta, nt
    CHARACTER (LEN=50) :: entra
    INTEGER, PARAMETER :: out_unit = 300

    !dV/V = (1/4*pi)*R(phi,beta)*sin(phi)*dphi*dbeta [Bunge, eq(5.13)]
    R(phi,beta)= (1.d0/su) * exp(-(180.d0*phi/(pi*omeg))**2) * sin(phi)

    nphi = 128; nbeta = 256

    pi = 3.141592653589793D+00

    a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi
    dphi= (b-a)/dble(nphi); dbeta=(d-c)/dble(nbeta)

    !call system('cls')
    write(*,'(/A)')' Elasticity for axial textures'
    !write(*,'(/A$)')' Enter data file: '
    !read (*, '(A)') entra
    write(*,'(/A)')' Single crystal matrix in data file:'

    OPEN(unit=1, status='old',file = 'BaTiO3_sijE.dat', action='read')
    !OPEN(unit=1, status='old',file = entra, action='read')

    Do m=1,6
        read(1,*) (smat(m,i), i=1,6)
        write (*,'(6F10.2)')(smat(m,i), i=1,6)
    End Do
    CLOSE(1)

    ! Convert matrix to tensor:
    sten(1,1,1,1)=smat(1,1);    sten(1,1,2,2)=smat(1,2);    sten(1,1,3,3)=smat(1,3)
    sten(1,1,1,2)=0.5*smat(1,6);sten(1,1,1,3)=0.5*smat(1,5);sten(1,1,2,3)=0.5*smat(1,4)
    sten(1,1,2,1)=0.5*smat(1,6);sten(1,1,3,1)=0.5*smat(1,5);sten(1,1,3,2)=0.5*smat(1,4)
    sten(2,2,1,1)=smat(2,1);    sten(2,2,2,2)=smat(2,2);    sten(2,2,3,3)=smat(2,3)
    sten(2,2,1,2)=0.5*smat(2,6);sten(2,2,1,3)=0.5*smat(2,5);sten(2,2,2,3)=0.5*smat(2,4)
    sten(2,2,2,1)=0.5*smat(2,6);sten(2,2,3,1)=0.5*smat(2,5);sten(2,2,3,2)=0.5*smat(2,4)
    sten(3,3,1,1)=smat(3,1);    sten(3,3,2,2)=smat(3,2);    sten(3,3,3,3)=smat(3,3)
    sten(3,3,1,2)=0.5*smat(3,6);sten(3,3,1,3)=0.5*smat(3,5);sten(3,3,2,3)=0.5*smat(3,4)
    sten(3,3,2,1)=0.5*smat(3,6);sten(3,3,3,1)=0.5*smat(3,5);sten(3,3,3,2)=0.5*smat(3,4)

    sten(2,3,1,1)=0.5*smat(4,1); sten(2,3,2,2)=0.5*smat(4,2); sten(2,3,3,3)=0.5*smat(4,3)
    sten(2,3,1,2)=0.25*smat(4,6);sten(2,3,1,3)=0.25*smat(4,5);sten(2,3,2,3)=0.25*smat(4,4)
    sten(2,3,2,1)=0.25*smat(4,6);sten(2,3,3,1)=0.25*smat(4,5);sten(2,3,3,2)=0.25*smat(4,4)
    sten(3,1,1,1)=0.5*smat(5,1); sten(3,1,2,2)=0.5*smat(5,2); sten(3,1,3,3)=0.5*smat(5,3)
    sten(3,1,1,2)=0.25*smat(5,6);sten(3,1,1,3)=0.25*smat(5,5);sten(3,1,2,3)=0.25*smat(5,4)
    sten(3,1,2,1)=0.25*smat(5,6);sten(3,1,3,1)=0.25*smat(5,5);sten(3,1,3,2)=0.25*smat(5,4)
    sten(1,2,1,1)=0.5*smat(6,1); sten(1,2,2,2)=0.5*smat(6,2);sten(1,2,3,3)=0.5*smat(6,3)
    sten(1,2,1,2)=0.25*smat(6,6);sten(1,2,1,3)=0.25*smat(6,5);sten(1,2,2,3)=0.25*smat(6,4)
    sten(1,2,2,1)=0.25*smat(6,6);sten(1,2,3,1)=0.25*smat(6,5);sten(1,2,3,2)=0.25*smat(6,4)

    sten(3,2,1,1)=0.5*smat(4,1); sten(3,2,2,2)=0.5*smat(4,2); sten(3,2,3,3)=0.5*smat(4,3)
    sten(3,2,1,2)=0.25*smat(4,6);sten(3,2,1,3)=0.25*smat(4,5);sten(3,2,2,3)=0.25*smat(4,4)
    sten(3,2,2,1)=0.25*smat(4,6);sten(3,2,3,1)=0.25*smat(4,5);sten(3,2,3,2)=0.25*smat(4,4)
    sten(1,3,1,1)=0.5*smat(5,1); sten(1,3,2,2)=0.5*smat(5,2); sten(1,3,3,3)=0.5*smat(5,3)
    sten(1,3,1,2)=0.25*smat(5,6);sten(1,3,1,3)=0.25*smat(5,5);sten(1,3,2,3)=0.25*smat(5,4)
    sten(1,3,2,1)=0.25*smat(5,6);sten(1,3,3,1)=0.25*smat(5,5);sten(1,3,3,2)=0.25*smat(5,4)
    sten(2,1,1,1)=0.5*smat(6,1); sten(2,1,2,2)=0.5*smat(6,2); sten(2,1,3,3)=0.5*smat(6,3)
    sten(2,1,1,2)=0.25*smat(6,6);sten(2,1,1,3)=0.25*smat(6,5);sten(2,1,2,3)=0.25*smat(6,4)
    sten(2,1,2,1)=0.25*smat(6,6);sten(2,1,3,1)=0.25*smat(6,5);sten(2,1,3,2)=0.25*smat(6,4)

    write(*,'(/A$)')' Enter Omega (degrees): '
    read(*,*) omeg
    call inte2D(omeg, su)

    phi1 = 0.d0   !Fibre texture, Bunge convention

    ! START THE GREAT LOOP FOR POLYCRYSTAL TENSOR COMPONENTS
    Do i = 1, 3
        Do j = 1, 3
            Do k = 1,3
                Do l = 1,3
                    stexten(i,j,k,l) =0.d0  ! Setting to "0" the textured polycrystal elast tensor component:
                    Do np = 1, nphi  ! loop on polar angle
                        phi = a + dphi/2.d0 + dble(np - 1)*dphi
                        Do nb = 1, nbeta   ! loop on azimuth
                            beta = c + dbeta/2.d0 + dble(nb - 1)*dbeta
                            phi2 = (pi/2.d0) - beta    ! Bunge convention
                            Q(1,1) =  cos(phi2)* cos(phi1) - cos(phi)*sin(phi1)*sin(phi2)
                            Q(1,2) =  cos(phi2)* sin(phi1) + cos(phi)*cos(phi1)*sin(phi2)
                            Q(1,3) =  sin(phi2)* sin(phi)
                            Q(2,1) = -sin(phi2)* cos(phi1) - cos(phi)*sin(phi1)*cos(phi2)
                            Q(2,2) = -sin(phi2)* sin(phi1) + cos(phi)*cos(phi1)*cos(phi2)
                            Q(2,3) =  cos(phi2)* sin(phi)
                            Q(3,1) =  sin(phi) * sin(phi1)
                            Q(3,2) = -sin(phi) * cos(phi1)
                            Q(3,3) =  cos(phi)
                            ! Calculating a rotated crystal elastic tensor component:
                            Do ll = 1, 3
                                Do m = 1, 3
                                    Do n = 1, 3
                                        Do o = 1, 3
                                            srotten(ll,m,n,o) = 0.d0
                                        End Do
                                    End Do
                                End Do
                            End Do
                            Do ll = 1, 3
                                Do m = 1, 3
                                    Do n = 1, 3
                                        Do o = 1, 3
                                        srotten(i,j,k,l) = srotten(i,j,k,l) + Q(i,ll)*Q(j,m)*Q(k,n)*Q(l,o)*sten(ll,m,n,o)
                                        End Do
                                    End Do
                                End Do
                            End Do
                            !Integrating textured polycrystal piezoelectric tensor components
                            stexten(i,j,k,l) = stexten(i,j,k,l)+R(phi, beta)*srotten(i,j,k,l)*dphi*dbeta
                        End Do
                    End Do
                End Do
            End Do
        End Do
    End Do

    ! Convert polycrystal tensor to matrix:
    stexmat(1,1)=stexten(1,1,1,1);    stexmat(1,2)=stexten(1,1,2,2);    stexmat(1,3)=stexten(1,1,3,3)
    stexmat(1,4)=2.0*stexten(1,1,2,3);stexmat(1,5)=2.0*stexten(1,1,1,3);stexmat(1,6)=2.0*stexten(1,1,1,2)
    stexmat(2,1)=stexten(2,2,1,1);    stexmat(2,2)=stexten(2,2,2,2);    stexmat(2,3)=stexten(2,2,3,3)
    stexmat(2,4)=2.0*stexten(2,2,2,3);stexmat(2,5)=2.0*stexten(2,2,1,3);stexmat(2,6)=2.0*stexten(2,2,1,2)
    stexmat(3,1)=stexten(3,3,1,1);    stexmat(3,2)=stexten(3,3,2,2);    stexmat(3,3)=stexten(3,3,3,3)
    stexmat(3,4)=2.0*stexten(3,3,2,3);stexmat(3,5)=2.0*stexten(3,3,1,3);stexmat(3,6)=2.0*stexten(3,3,1,2)
    stexmat(4,1)=2.0*stexten(2,3,1,1);stexmat(4,2)=2.0*stexten(2,3,2,2);stexmat(4,3)=2.0*stexten(2,3,3,3)
    stexmat(4,4)=4.0*stexten(2,3,2,3);stexmat(4,5)=4.0*stexten(2,3,1,3);stexmat(4,6)=4.0*stexten(2,3,1,2)
    stexmat(5,1)=2.0*stexten(3,1,1,1);stexmat(5,2)=2.0*stexten(3,1,2,2);stexmat(5,3)=2.0*stexten(3,1,3,3)
    stexmat(5,4)=4.0*stexten(3,1,2,3);stexmat(5,5)=4.0*stexten(3,1,1,3);stexmat(5,6)=4.0*stexten(3,1,1,2)
    stexmat(6,1)=2.0*stexten(1,2,1,1);stexmat(6,2)=2.0*stexten(1,2,2,2);stexmat(6,3)=2.0*stexten(1,2,3,3)
    stexmat(6,4)=4.0*stexten(1,2,2,3);stexmat(6,5)=4.0*stexten(1,2,1,3);stexmat(6,6)=4.0*stexten(1,2,1,2)

    write(*,'(/A)') ' Textured polycrystal matrix:'
    
    Do i = 1,6
        write (*,'(6F10.2)') (stexmat(i,j), j = 1,6)
    End Do
    
    open (unit=out_unit,file="BaTiO3_sijE_result.dat",action="write",status="replace")
     Do i = 1,6
        write (out_unit,'(6F10.2)') (stexmat(i,j), j = 1,6)
    End Do
    close (out_unit)
    
End Program ELASFIB
    
    
Subroutine inte2D(omega, suma)
    ! Calculating the surface integral of a Gaussian distribution
    implicit none
    real (kind = 8):: a, b, c, d, pi, omega
    real (kind = 8):: x, y, hx, hy, f, suma
    integer (kind = 4) :: ii, jj, nx, ny

    f(x,y) = exp(-(180.d0*x/(pi*omega))**2) * sin(x)

    pi = 3.141592653589793D+00
    nx = 1024; ny = 2048
    a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi

    hx = (b-a)/dble(nx); hy = (d-c)/dble(ny)
    suma = 0.0D+00
    Do ii = 1, nx
        x = a + hx/2.d0 + dble(ii-1)*hx
        Do jj = 1, ny
            y = c + hy/2.d0 + dble(jj-1)*hy
            suma = suma + hx * hy * f (x, y)
        End Do
    End Do
    return
End Subroutine inte2D
