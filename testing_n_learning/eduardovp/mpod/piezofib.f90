Program PIEZOFIB
    implicit none
    REAL (KIND=8), DIMENSION (3, 6)  :: dmat
    REAL (KIND=8), DIMENSION (3,3,3) :: dtens
    REAL (KIND=8), DIMENSION (3,3)   :: Q
    REAL (KIND=8), DIMENSION (3,3,3) :: drotten, dtexten
    REAL (KIND=8), DIMENSION (3,6)   :: dtexmat
    REAL (KIND=8)    :: phi1, phi, phi2, pi, beta, R
    REAL (KIND=8)    :: a, b, c, d, dphi, dbeta, omeg, su, mh, mk, ml
    INTEGER (KIND=8) :: i, j, k, l, m, n, np, nphi, nb, nbeta 
    INTEGER, PARAMETER :: out_unit = 300

    !R(phi,beta)= (4.d0 * pi/su) * exp(-(180.d0*phi/(pi*omeg))**2) * sin(phi)
    R(phi,beta)= (1.d0/su) * exp(-(180.d0*phi/(pi*omeg))**2) * sin(phi)

    nphi = 360; nbeta = 720
    pi = 3.141592653589793D+00

    a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi
    dphi= (b-a)/dble(nphi); dbeta=(d-c)/dble(nbeta)

    !call system('cls')
    write(*,'(/A)')' Piezoelectricity for axial textures:'

    OPEN(unit=1, status='old',file = 'BaTiO3.dat', action='read')

        Do m=1,3
            read(1,*) (dmat(m,i), i=1,6)
            write (*,'(6F10.2)')(dmat(m,i), i=1,6)
        End Do
        CLOSE(1)

        write(*,'(/A$)')' Enter Omega: '
        read(*,*) omeg

    call inte2D(omeg, su)

    ! Tensor diagonal elements
    Do i=1,3
        Do j=1,3
            dtens(i,j,j)=dmat(i,j)
        End Do
    End Do

    ! Off-diagonal tensor elements
    Do i=1,3
        dtens(i,2,3)=0.5*dmat(i,4)
        dtens(i,3,2)=dtens(i,2,3)
        dtens(i,1,3)=0.5*dmat(i,5)
        dtens(i,3,1)=dtens(i,1,3)
        dtens(i,1,2)=0.5*dmat(i,6)
        dtens(i,2,1)=dtens(i,1,2)
    End Do

    phi1=0.d0   !Fibre texture, Bunge convention

    ! START THE GREAT LOOP FOR TENSOR COMPONENTS
    Do i = 1, 3
        Do j = 1, 3
            Do k = 1,3
                dtexten(i,j,k) =0.d0  ! Setting to "0" the textured polycrystal piezotensor component:
                Do np =1, nphi  ! loop on polar angle
                        phi = a + dphi/2.d0 + dble(np - 1)*dphi
                        Do nb = 1, nbeta   ! loop on azimuth
                                beta = c + dbeta/2.d0 + dble(nb - 1)*dbeta
                                phi2= (pi/2.d0) - beta    ! Bunge convention
                                Q(1,1) =  cos(phi2)* cos(phi1) - cos(phi)*sin(phi1)*sin(phi2)
                                Q(1,2) =  cos(phi2)* sin(phi1) + cos(phi)*cos(phi1)*sin(phi2)
                                Q(1,3) =  sin(phi2)* sin(phi)
                                Q(2,1) = -sin(phi2)* cos(phi1) - cos(phi)*sin(phi1)*cos(phi2)
                                Q(2,2) = -sin(phi2)* sin(phi1) + cos(phi)*cos(phi1)*cos(phi2)
                                Q(2,3) =  cos(phi2)* sin(phi)
                                Q(3,1) =  sin(phi) * sin(phi1)
                                Q(3,2) = -sin(phi) * cos(phi1)
                                Q(3,3) =  cos(phi)
                                Do l=1,3
                                    Do m = 1, 3
                                        Do n =1, 3
                                            drotten(i,j,k) =0.d0  ! Setting to "0" a rotated crystal piezotensor component:
                                        End Do
                                    End Do
                                End Do
                                Do l=1,3
                                    Do m = 1, 3
                                        Do n =1, 3
                                            drotten(i,j,k) = drotten(i,j,k) + Q(i,l)*Q(j,m)*Q(k,n)*dtens(l,m,n)
                                            !write(*,'(A)')' np,nb, l, m, n,    phi,     beta, drotten(i,j,k)'
                                        !write(*,'(5I3,3F10.3)') np,nb, l,m,n, 180*phi/pi,180*beta/pi, drotten(i,j,k)
                                        End Do
                                    End Do
                                End Do
                        !write(*,*)'i,k,j,drotten(i,j,k)', i,k,j,drotten(i,j,k)
                        dtexten(i,j,k) = dtexten(i,j,k)+R(phi, beta)*drotten(i,j,k)*dphi*dbeta
                        End Do
                End Do
            End Do
        End Do
    End Do

    Do i = 1, 3
            Do j = 1, 3
                dtexmat(i,j)= dtexten(i,j,j)
            End Do
            dtexmat(i,4)=2.000*dtexten(i,2,3)
            dtexmat(i,5)=2.000*dtexten(i,1,3)
            dtexmat(i,6)=2.000*dtexten(i,1,2)
    End Do
    
    write(*,'(/A)') ' Textured polycrystal matrix:'
    Do i = 1,3
            write (*,'(6F10.2)') (dtexmat(i,j), j =1,6)
    End Do

    open (unit=out_unit,file="BaTiO3_result.dat",action="write",status="replace")
    
    Do i = 1,3
            write (out_unit,'(6F10.2)') (dtexmat(i,j), j =1,6)
    End Do

    close (out_unit)
        
End PROGRAM PIEZOFIB

Subroutine inte2D(omega, suma)
  implicit none
  real (kind = 8):: a, b, c, d, pi, omega
  real (kind = 8):: x, y, hx, hy, f, suma
  integer (kind = 4) :: ii, jj, nx, ny

  f(x,y)= exp(-(180.d0*x/(pi*omega))**2) * sin(x)

  pi = 3.141592653589793D+00
  nx = 1024; ny = 2048
  a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi

  hx = (b-a)/dble(nx); hy = (d-c)/dble(ny)
  suma = 0.0D+00
  Do ii = 1, nx
     x = a + hx/2.d0 + dble(ii-1)*hx
     Do jj = 1, ny
        y = c + hy/2.d0 + dble(jj-1)*hy
        suma = suma + hx * hy * f (x, y)
     End Do
  End Do
Return
End Subroutine
