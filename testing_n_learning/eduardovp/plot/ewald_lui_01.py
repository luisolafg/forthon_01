# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, combinations
from mpl_toolkits.mplot3d import proj3d

import wx
import numpy
import matplotlib


fig = plt.figure()
ax = fig.gca(projection='3d')
'''
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.set_xlim([-8,8])
ax.set_ylim([-8,8])
ax.set_zlim([-8,8])
'''

#ax.set_aspect("equal")


class DirectToReciprocal:
    def __init__(self, a, b, c, alpha, beta, gamma):


        self.a = float(a)
        self.b = float(b)
        self.c = float(c)
        self.alpha = float(alpha)
        self.beta  = float(beta)
        self.gamma = float(gamma)

        self.a1 = np.array([a, 0.0, 0.0])
        self.a2 = np.array([b * np.cos(gamma * (np.pi/180.0)), b * np.sin(gamma * (np.pi/180.0)), 0.0])
        self.a3 = np.array([c * np.cos(beta * (np.pi/180.0)), c * ( np.cos(alpha * (np.pi/180.0)) * np.sin(gamma * (np.pi/180.0))), c * np.sin(alpha * (np.pi/180.0))])

        self.b1 = ((2.0 * np.pi) * ((np.cross(self.a2, self.a3)) / (np.dot(self.a1, np.cross(self.a2, self.a3)))))
        self.b2 = ((2.0 * np.pi) * ((np.cross(self.a3, self.a1)) / (np.dot(self.a2, np.cross(self.a3, self.a1)))))
        self.b3 = ((2.0 * np.pi) * ((np.cross(self.a1, self.a2)) / (np.dot(self.a3, np.cross(self.a1, self.a2)))))


        print "a1, a2, a3 =", self.a1, self.a2, self.a3
        print "b1, b2, b3 =", self.b1, self.b2, self.b3


data = DirectToReciprocal(1.0, 1.0, 1.0, 90, 90, 90)

lmb = 1.0
r = 5.0
h = 0.0
k = 0.0
l = 0.0
c = 1 / lmb

min_lattice_range = -20
max_lattice_range = +20


for h_mil in range(min_lattice_range, max_lattice_range):
    for k_mil in range(min_lattice_range, max_lattice_range):
        for l_mil in range(min_lattice_range, max_lattice_range):

            h_fl = float(h_mil)
            k_fl = float(k_mil)
            l_fl = float(l_mil)
            '''
            x = h_fl * data.b1[0] + k_fl * data.b1[1] + l_fl * data.b1[2]
            y = h_fl * data.b2[0] + k_fl * data.b2[1] + l_fl * data.b2[2]
            z = h_fl * data.b3[0] + k_fl * data.b3[1] + l_fl * data.b3[2]
            '''
            x = h_fl * data.a1[0] + k_fl * data.a1[1] + l_fl * data.a1[2]
            y = h_fl * data.a2[0] + k_fl * data.a2[1] + l_fl * data.a2[2]
            z = h_fl * data.a3[0] + k_fl * data.a3[1] + l_fl * data.a3[2]
            #'''

            # the sphere should NOT have the center in the origin
            # the center SHOULD be the origin (+) or (-) the radius
            sph = ( (x-h)**2 + (y-k)**2 + (z-l)**2 )
            ra = r**2
            dif = sph - ra
            if dif >= -c and  dif <= c:
                ax.scatter(x, y, z, color="r", s=10)



plt.show()

