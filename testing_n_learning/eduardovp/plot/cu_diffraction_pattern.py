import numpy as np
import matplotlib.pyplot as plt
import math as mt

def selecting(a,b):
    if angle >= ((a * 2 - (10 * b)) * 57.3) and angle <= ((a * 2 + (10 * b)) * 57.3):
        return True
        
def plotting(a,b,c):
            
    x[angle_pos] = angle
    y[angle_pos] = c * (mt.exp((-1.0 * ((angle - (2.0 * a * 57.3)) ** 2.0)/ (2.0 * (b) * 57.3) ** 2.0)))
            

# Angles selection
print "\n Angles\n"

lmd = 1.542
a = 3.615
t= 500
pi = mt.sqrt(2 * mt.pi)
    
miller111 = int((mt.asin(((lmd * (mt.sqrt( 1.0 ** 2 + 1.0 ** 2 + 1.0 ** 2))) / (2.0 * a)))) * 57.3)

print miller111

miller200 = int((mt.asin(((lmd * (mt.sqrt( 2.0 ** 2 + 0.0 ** 2 + 0.0 ** 2))) / (2.0 * a)))) * 57.3)

print miller200

miller220 = int((mt.asin(((lmd * (mt.sqrt( 2.0 ** 2+ 2.0 ** 2 + 0.0 ** 2))) / (2.0 * a)))) * 57.3)

print miller220

miller311 = int((mt.asin(((lmd * (mt.sqrt( 3.0 ** 2 + 1.0 ** 2 + 1.0 ** 2))) / (2.0 * a)))) * 57.3)

print miller311

miller222 = int((mt.asin(((lmd * (mt.sqrt( 2.0 ** 2 + 2.0 ** 2 + 2.0 ** 2))) / (2.0 * a)))) * 57.3)

print miller222

miller400 = int((mt.asin(((lmd * (mt.sqrt( 4.0 ** 2 + 0.0 ** 2 + 0.0 ** 2))) / (2.0 * a)))) * 57.3)

print miller400

miller331 = int((mt.asin(((lmd * (mt.sqrt( 3.0 ** 2 + 3.0 ** 2 + 1.0 ** 2))) / (2.0 * a)))) * 57.3)

print miller331

miller420 = int((mt.asin(((lmd * (mt.sqrt( 4.0 ** 2 + 2.0 ** 2 + 0.0 ** 2))) / (2.0 * a)))) * 57.3)

print miller420


#Scattering factor
print "\n Scattering factor\n"

miller111 /= 57.3
miller200 /= 57.3
miller220 /= 57.3
miller311 /= 57.3
miller222 /= 57.3
miller400 /= 57.3
miller331 /= 57.3
miller420 /= 57.3

f111= 13.338 * mt.e ** ( - 3.583 * (miller111) ** 2)  + 7.168 * mt.e ** ( - 0.247 * (miller111) ** 2) + 5.616 * mt.e ** ( - 11.397 * (miller111) ** 2) + 1.673 * mt.e ** ( - 64.813 * (miller111) ** 2) + 5.71
print f111

f200= 13.338 * mt.e ** ( - 3.583 * (miller200) ** 2)  + 7.168 * mt.e ** ( - 0.247 * (miller200) ** 2) + 5.616 * mt.e ** ( - 11.397 * (miller200) ** 2) + 1.673 * mt.e ** ( - 64.813 * (miller200) ** 2) + 6.71
print f200 

f220= 13.338 * mt.e ** ( - 3.583 * (miller220) ** 2)  + 7.168 * mt.e ** ( - 0.247 * (miller220) ** 2) + 5.616 * mt.e ** ( - 11.397 * (miller220) ** 2) + 1.673 * mt.e ** ( - 64.813 * (miller220) ** 2) + 7.31
print f220 

f311= 13.338 * mt.e ** ( - 3.583 * (miller311) ** 2)  + 7.168 * mt.e ** ( - 0.247 * (miller311) ** 2) + 5.616 * mt.e ** ( - 11.397 * (miller311) ** 2) + 1.673 * mt.e ** ( - 64.813 * (miller311) ** 2) + 7.21
print f311 

f222= 13.338 * mt.e ** ( - 3.583 * (miller222) ** 2)  + 7.168 * mt.e ** ( - 0.247 * (miller222) ** 2) + 5.616 * mt.e ** ( - 11.397 * (miller222) ** 2) + 1.673 * mt.e ** ( - 64.813 * (miller222) ** 2) + 7.01
print f222 

f400= 13.338 * mt.e ** ( - 3.583 * (miller400) ** 2)  + 7.168 * mt.e ** ( - 0.247 * (miller400) ** 2) + 5.616 * mt.e ** ( - 11.397 * (miller400) ** 2) + 1.673 * mt.e ** ( - 64.813 * (miller400) ** 2) + 6.61
print f400 

f331= 13.338 * mt.e ** ( - 3.583 * (miller331) ** 2)  + 7.168 * mt.e ** ( - 0.247 * (miller331) ** 2) + 5.616 * mt.e ** ( - 11.397 * (miller331) ** 2) + 1.673 * mt.e ** ( - 64.813 * (miller331) ** 2) + 6.41
print f331 

f420= 13.338 * mt.e ** ( - 3.583 * (miller420) ** 2)  + 7.168 * mt.e ** ( - 0.247 * (miller420) ** 2) + 5.616 * mt.e ** ( - 11.397 * (miller420) ** 2) + 1.673 * mt.e ** ( - 64.813 * (miller420) ** 2) + 6.21
print f420 
 
#Structure factor
print "\n F\n"

F111 = 16 * (f111) ** 2
print F111

F200 = 16 * (f200) ** 2
print F200

F220 = 16 * (f220) ** 2
print F220

F311 = 16 * (f311) ** 2
print F311

F222 = 16 * (f222) ** 2
print F222

F400 = 16 * (f400) ** 2
print F400

F331 = 16 * (f331) ** 2
print F331

F420 = 16 * (f420) ** 2
print F420

#Lorentz polarization
print "\n Lorentz polarization factor\n"

p111 = ((1 + ((mt.cos(2 * miller111)) ** 2)) / (((mt.sin(miller111)) ** 2) * (mt.cos(miller111))))
print p111

p200 = ((1 + ((mt.cos(2 * miller200)) ** 2)) / (((mt.sin(miller200)) ** 2) * (mt.cos(miller200))))
print p200

p220 = ((1 + ((mt.cos(2 * miller220)) ** 2)) / (((mt.sin(miller220)) ** 2) * (mt.cos(miller220))))
print p220

p311 = ((1 + ((mt.cos(2 * miller311)) ** 2)) / (((mt.sin(miller311)) ** 2) * (mt.cos(miller311))))
print p311

p222 = ((1 + ((mt.cos(2 * miller222)) ** 2)) / (((mt.sin(miller222)) ** 2) * (mt.cos(miller222))))
print p222

p400 = ((1 + ((mt.cos(2 * miller400)) ** 2)) / (((mt.sin(miller400)) ** 2) * (mt.cos(miller400))))
print p400

p331 = ((1 + ((mt.cos(2 * miller331)) ** 2)) / (((mt.sin(miller331)) ** 2) * (mt.cos(miller331))))
print p331

p420 = ((1 + ((mt.cos(2 * miller420)) ** 2)) / (((mt.sin(miller420)) ** 2) * (mt.cos(miller420))))
print p420


#Multiplicity
print "\n Multiplicity\n"

m111 = 8
print m111

m200 = 6
print m200

m220 = 12
print m220

m311 = 24
print m311

m222 = 8
print m222

m400 = 6
print m400

m331 = 24
print m331

m420 =  24
print m420

#Temperature factor
print "\n Temperature factor\n"

t111= mt.exp( 1 * (((mt.sin(miller111)) ** 2) / (lmd ** 2)))
print t111

t200= mt.exp( 1 * (((mt.sin(miller200)) ** 2) / (lmd ** 2)))
print t200

t220= mt.exp( 1 * (((mt.sin(miller220)) ** 2) / (lmd ** 2)))
print t220

t311= mt.exp( 1 * (((mt.sin(miller311)) ** 2) / (lmd ** 2)))
print t311

t222= mt.exp( 1 * (((mt.sin(miller222)) ** 2) / (lmd ** 2)))
print t222

t400= mt.exp( 1 * (((mt.sin(miller400)) ** 2) / (lmd ** 2)))
print t400

t331= mt.exp( 1 * (((mt.sin(miller331)) ** 2) / (lmd ** 2)))
print t331

t420= mt.exp( 1 * (((mt.sin(miller420)) ** 2) / (lmd ** 2)))
print t420

#Total Intensity
print "\n Total area\n"

T111 = (F111 * p111 * m111 * t111)
print T111

T200 = (F200 * p200 * m200 * t200)
print T200

T220 = (F220 * p220 * m220 * t220)
print T220

T311 = (F311 * p311 * m311 * t311)
print T311

T222 = (F222 * p222 * m222 * t222)
print T222

T400 = (F400 * p400 * m400 * t400)
print T400

T331 = (F331 * p331 * m331 * t331)
print T331

T420 = (F420 * p420 * m420 * t420)
print T420

#peak width
print "\n peak width\n"

W111 = lmd / ( t * (mt.cos(miller111)))
print W111

W200 = lmd / ( t * (mt.cos(miller200)))
print W200

W220 = lmd / ( t * (mt.cos(miller220)))
print W220

W311 = lmd / ( t * (mt.cos(miller311)))
print W311

W222 = lmd / ( t * (mt.cos(miller222)))
print W222

W400 = lmd / ( t * (mt.cos(miller400)))
print W400

W331 = lmd / ( t * (mt.cos(miller331)))
print W331

W420 = lmd / ( t * (mt.cos(miller420)))
print W420


#Height peak
print "\n Height peak\n"

H111 = ((T111) / (W111 * pi))
print H111

H200 = ((T200) / (W200 * pi))
print H200
 
H220 = ((T220) / (W220 * pi))
print H220

H311 = ((T311) / (W311 * pi))
print H311

H222 = ((T222) / (W222 * pi))
print H222

H400 = ((T400) / (W400 * pi))
print H400

H331 = ((T331) / (W331 * pi))
print H331

H420 = ((T420) / (W420 * pi))
print H420

# Plotting

x = np.zeros(181000)
y = np.zeros(181000)


for angle_pos in range(181000):
    
    angle = angle_pos * 0.001
    
    if selecting(miller111,W111) == True:
        
        plotting(miller111,W111,H111)
    
    elif selecting(miller200,W200) == True:
        
        plotting(miller200,W200,H200) 
        
    elif selecting(miller220,W220) == True:
        
        plotting(miller220,W220,H220)
        
    elif selecting(miller311,W311) == True:
        
        plotting(miller311,W311,H311)
        
    elif selecting(miller222,W222) == True:
        
        plotting(miller222,W222,H222)
    
    elif selecting(miller400,W400) == True:
        
        plotting(miller400,W400,H400)
    
    elif selecting(miller331,W331) == True:
        
        plotting(miller331,W331,H331)
    
    elif selecting(miller420,W420) == True:
        
        plotting(miller420,W420,H420)
    
    else:
        
        y[angle_pos] = 0
        
            
            
plt.plot(x, y, 'g')
#plt.axis([0, 1, 0, 30])
plt.title("Cu diffraction pattern", fontsize = 20)
plt.ylabel("Intensity",fontsize = 15)
plt.xlabel(r'$2\theta$', fontsize = 20)

plt.text(miller111*57.3*2, H111, '111')
plt.text(miller200*57.3*2, H200, '200')
plt.text(miller220*57.3*2, H220, '220')
plt.text(miller311*57.3*2, H311, '311')
plt.text(miller222*57.3*2, H222, '222')
plt.text(miller400*57.3*2, H400, '400')
plt.text(miller331*57.3*2 - 2, H331, '331')
plt.text(miller420*57.3*2, H420 , '420')

plt.show()
