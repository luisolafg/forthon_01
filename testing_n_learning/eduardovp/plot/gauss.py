import numpy as np
import matplotlib.pyplot as plt
import math as mt

x = np.zeros(18100)
y = np.zeros(18100)

for angle_pos in range(18100):
    angle = angle_pos * .01

    if angle > 0 and angle < 180:

        x[angle_pos] = angle
        y[angle_pos] = 1.0 * mt.exp((-1.0 * ((angle - 90.0) ** 2.0)/ (2.0 * (1.0) ** 2.0)))

  # el numero 5 entre los dos numeros 2 es el ancho del pico si le pones un 1 ya no se ve gaussiana
plt.plot(x, y, 'r')
#plt.axis([0, 180, 0, 5])
plt.title("Atomic Scattering Factor of mu", fontsize = 20)
plt.ylabel("f Cu",fontsize = 15)
plt.xlabel(r'$\frac{sin(\theta)}{\lambda}$', fontsize = 23)

plt.show()
