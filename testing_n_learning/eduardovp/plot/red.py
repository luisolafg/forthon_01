# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, combinations
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.set_xlim([-4,4])
ax.set_ylim([-4,4])
ax.set_zlim([-4,4])
#ax.set_aspect("equal")


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)



a = float(2)
b = float(2)
c = float(4)
alpha = float(90)
beta = float(90)
gamma = float(120)



a1 = np.array([a, 0, 0])
a2 = np.array([b * np.cos(gamma * (np.pi/180)), b * np.sin(gamma * (np.pi/180)), 0])
a3 = np.array([c * np.cos(beta * (np.pi/180)), c * ( np.cos(alpha * (np.pi/180)) * np.sin(gamma * (np.pi/180))), c * np.sin(alpha * (np.pi/180))])

b1 = ((2 * np.pi) * ((np.cross(a2, a3)) / (np.dot(a1, np.cross(a2, a3)))))
b2 = ((2 * np.pi) * ((np.cross(a3, a1)) / (np.dot(a2, np.cross(a3, a1)))))
b3 = ((2 * np.pi) * ((np.cross(a1, a2)) / (np.dot(a3, np.cross(a1, a2)))))


ox = 0
oy = 0
oz = 0


v1 = Arrow3D([ox,a1[0]],[oy,a1[1]],[oz,a1[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="b")
v2 = Arrow3D([ox,a2[0]],[oy,a2[1]],[oz,a2[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="g")
v3 = Arrow3D([ox,a3[0]],[oy,a3[1]],[oz,a3[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="r")

vr1 = Arrow3D([ox,b1[0]],[oy,b1[1]],[oz,b1[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="m")
vr2 = Arrow3D([ox,b2[0]],[oy,b2[1]],[oz,b2[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="y")
vr3 = Arrow3D([ox,b3[0]],[oy,b3[1]],[oz,b3[2]], mutation_scale=15, lw=2, arrowstyle="-|>", color="k")


ax.add_artist(v1)
ax.add_artist(v2)
ax.add_artist(v3)

ax.add_artist(vr1)
ax.add_artist(vr2)
ax.add_artist(vr3)


plt.show()
