import matplotlib.pyplot as plt
plt.plot([0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.1,1.2], [29,25.9,21.6,17.9,15.2,13.3,11.7,10.2,9.1,8.1,7.3,6.6,6.0],'g')
plt.axis([0, 1.2, 0, 30])
plt.title("Atomic Scattering Factor of Cu", fontsize = 20)
plt.ylabel("f Cu",fontsize = 15)
plt.xlabel(r'$\frac{sin(\theta)}{\lambda}$', fontsize = 25)
plt.show()