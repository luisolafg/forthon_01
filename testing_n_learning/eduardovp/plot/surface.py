#Piezoelectricity

import numpy as np
from numpy import sin,cos,pi,abs # makes the code more readable
from mayavi import mlab

# Quartz: MPOD 55  Structure Point group D3= 321
#d11=0.151; d12=-0.151; d13=0;    d14=-0.061;   d15=0;     d16=0
#d21=0;     d22=0;      d23=0;    d24=0;        d25=0.061; d26=-0.302
#d31=0;     d32=0;      d33=0;    d34=0;        d35=0;     d36=0

# BaTiO3: MPOD 304  Structure Point group C4v= 4mm
#d11=0;     d12=0;      d13=0;    d14=0;        d15=392;   d16=0
#d21=0;     d22=0;      d23=0;    d24=392;      d25=0;     d26=0
#d31=-34.5; d32=-34.5;  d33=85.6; d34=0;        d35=0;     d36=0



# Virtual crystal infinite symmetry group
d11=0;      d12=0;        d13=0;      d14=0000;       d15=292;     d16=0
d21=0;      d22=0;        d23=0;      d24=292;        d25=000;   d26=0
d31=-53.5;  d32=-53.5;    d33=12.6;   d34=0;          d35=0;       d36=0

phi, beta = np.mgrid[0:1*np.pi:181j,0:2*np.pi:361j]
x = sin(phi)*cos(beta)
y = sin(phi)*sin(beta)
z = cos(phi)

D1=d11*x*x*x + (d12+d26)*x*y*y + (d13+d35)*x*z*z + (d14+d25+d36)*x*y*z + (d15+d31)*x*x*z + (d16+d21)*x*x*y
D2=d22*y*y*y + (d23+d34)*y*z*z + (d24+d32)*y*y*z
D3=d33*z*z*z

D=D1+D2+D3

x1 = D*sin(phi)*cos(beta)
y1 = D*sin(phi)*sin(beta)
z1 = D*cos(phi)
mlab.figure()
mlab.mesh(x1,y1,z1,scalars=np.abs(D))
#mlab.mesh(x1,y1,z1,scalars=D)
#mlab.mesh(x1,y1,z1)
mlab.scalarbar(orientation="vertical")
mlab.show()
