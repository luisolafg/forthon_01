import numpy as np
from mayavi import mlab
from math import radians, cos, sin, asin, sqrt

def two_pts_d(lon1, lat1, lon2, lat2):
    
    # convert decimal degrees to radians 
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])
    
    # sphere ec.
    phi, theta = np.mgrid[0:1*np.pi:181j,0:2*np.pi:361j]
    x = np.sin(phi) * np.cos(theta)
    y = np.sin(phi) * np.sin(theta)
    z = np.cos(phi)

    # converting angles in coordinates
    x1 = np.cos(lon1) * np.cos(lat1)
    y1 = np.cos(lon1) * np.sin(lat1)
    z1 = np.sin(lon1)
    
    x2 = np.cos(lon2) * np.cos(lat2)
    y2 = np.cos(lon2) * np.sin(lat2)
    z2 = np.sin(lon2)
    
    mlab.figure()
    mlab.mesh(x,y,z, color=(0.67, 0.77, 0.93), resolution=50, opacity=0.7, name='Earth')
    mlab.points3d(x1,y1,z1, color = (0,0,0),scale_factor=0.3)
    mlab.points3d(x2,y2,z2, color = (0,0,1),scale_factor=0.3)
    mlab.plot3d([x1, x2], [y1, y2], [z1, z2])
    mlab.scalarbar(orientation="vertical")
    mlab.show()
    
    # haversine formula 
    dlon = lon2 - lon1 
    dlat = lat2 - lat1 
    a = sin(dlat/2)**2 + cos(lat1) * cos(lat2) * sin(dlon/2)**2
    c = 2 * asin(sqrt(a)) 
    # Radius of earth in kilometers is 6371
    #km = 6371 * c
    km = c
    print "the distance is", km
    return km
 
two_pts_d(0, 90, 0, -90)
two_pts_d(90, 45, -90, 135)
two_pts_d(0, 140, 35, -60)

