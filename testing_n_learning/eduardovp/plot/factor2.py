import numpy as np
import matplotlib.pyplot as plt
import math as mt
   
x = np.zeros(91)
y = np.zeros(91)

for angle in range(91):
  
    print "\n angle = %i \n" %angle
    x[angle]= (mt.sin(mt.radians(angle)))
    print " x = %f" %x[angle]
    y[angle] = 13.338 * mt.e ** ( - 3.583 * ( x[angle]) ** 2)  + 7.168 * mt.e ** ( - 0.247 * ( x[angle]) ** 2) + 5.616 * mt.e ** ( - 11.397 * ( x[angle]) ** 2) + 1.673 * mt.e ** ( - 64.813 * ( x[angle]) ** 2) + 1.91
    print " y = %f" %y[angle]
    
plt.plot(x, y, 'r')
plt.axis([0, 1, 0, 30])
plt.title("Atomic Scattering Factor of Cu", fontsize = 20)
plt.ylabel("f Cu",fontsize = 15)
plt.xlabel(r'$\frac{sin(\theta)}{\lambda}$', fontsize = 23)

plt.show()