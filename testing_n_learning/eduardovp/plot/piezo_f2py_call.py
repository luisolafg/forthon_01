import routine_piezo
import numpy as np
from numpy import sin,cos,pi,abs
from mayavi import mlab

print " Calculation of a tensor rotation"
print routine_piezo.piezo.rotation.__doc__

phi1  = float(raw_input(" Give me the phi1 angle in degrees : "))
phi   = float(raw_input(" Give me the phi angle in degrees : "))
phi2  = float(raw_input(" Give me the phi2 angle in degrees : "))
routine_piezo.piezo.rotation(phi1, phi,phi2)

drotmat = routine_piezo.piezo.drotmat

print

# Virtual crystal infinite symmetry group
d11=drotmat[0,0]; d12=drotmat[0,1]; d13=drotmat[0,2]; d14=drotmat[0,3]; d15=drotmat[0,4]; d16=drotmat[0,5]
d21=drotmat[1,0]; d22=drotmat[1,1]; d23=drotmat[1,2]; d24=drotmat[1,3]; d25=drotmat[1,4]; d26=drotmat[1,5]
d31=drotmat[2,0]; d32=drotmat[2,1]; d33=drotmat[2,2]; d34=drotmat[2,3]; d35=drotmat[2,4]; d36=drotmat[2,5]

phi, beta = np.mgrid[0:1*np.pi:181j, 0:2*np.pi:361j]
x = sin(phi)*cos(beta)
y = sin(phi)*sin(beta)
z = cos(phi)

D1=d11*x*x*x + (d12+d26)*x*y*y + (d13+d35)*x*z*z + (d14+d25+d36)*x*y*z + (d15+d31)*x*x*z + (d16+d21)*x*x*y
D2=d22*y*y*y + (d23+d34)*y*z*z + (d24+d32)*y*y*z
D3=d33*z*z*z

D=D1+D2+D3

x1 = D*sin(phi)*cos(beta)
y1 = D*sin(phi)*sin(beta)
z1 = D*cos(phi)

mlab.figure()
mlab.mesh(x1,y1,z1,scalars=np.abs(D))
#mlab.mesh(x1,y1,z1,scalars=D)
#mlab.mesh(x1,y1,z1)
mlab.scalarbar(orientation="vertical")
mlab.show()
