# -*- coding: utf-8 -*-
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
from itertools import product, combinations
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
ax.set_xlim([-3,3])
ax.set_ylim([-3,3])
ax.set_zlim([-3,3])
#ax.set_aspect("equal")


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)

a = float(2)
b = float(2)
c = float(2)

ar = float(1/a)
br = float(1/b)
cr = float(1/c)

ox = 0
oy = 0
oz = 0


v = Arrow3D([ox,a],[oy,b],[oz,c], mutation_scale=15, lw=2, arrowstyle="-|>", color="k")
vr = Arrow3D([ox,ar],[oy,br],[oz,cr], mutation_scale=15, lw=2, arrowstyle="-|>", color="r")

ax.add_artist(v)
ax.add_artist(vr)

plt.show()
