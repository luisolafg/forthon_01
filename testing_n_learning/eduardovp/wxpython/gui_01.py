#!/usr/bin/env python

"""working on my gui."""

import wx
import wx.lib.scrolledpanel as scroll_pan
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np



def GetBitmap_from_np_array(data2d):

    lc_fig = plt.figure()
    plt.imshow(np.transpose(data2d), interpolation = "nearest", cmap = 'hot')
    lc_fig.canvas.draw()
    width, height = lc_fig.canvas.get_width_height()
    np_buf = np.fromstring (lc_fig.canvas.tostring_rgb(), dtype=np.uint8)
    np_buf.shape = (width, height, 3)
    np_buf = np.roll(np_buf, 3, axis = 2)
    wx_image = wx.EmptyImage(width, height)
    wx_image.SetData(np_buf )
    wxBitmap = wx_image.ConvertToBitmap()

    plt.close(lc_fig)

    return wxBitmap
    
    
class ScrolledImgMatplotlib(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(ScrolledImgMatplotlib, self).__init__(parent)
        self.set_scroll_content()
        
    def set_scroll_content(self):
        arr = np.arange(30 * 20).reshape(30, 20)

        bmp = GetBitmap_from_np_array(arr)

        st_bmp = wx.StaticBitmap(self, bitmap=bmp)

        img_sizer = wx.BoxSizer(wx.VERTICAL)
        img_sizer.Add(st_bmp)

        self.SetSizer(img_sizer)
        self.SetupScrolling()
        self.Layout()


class ScrolledImg(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(ScrolledImg, self).__init__(parent)
        self.set_scroll_content()
        
    def set_scroll_content(self):    

        img = wx.Image('pattern.jpg', wx.BITMAP_TYPE_JPEG)
        bmp = img.ConvertToBitmap()
        size = bmp.GetWidth(), bmp.GetHeight()
        my_bmp = wx.StaticBitmap(self, bitmap=bmp)
        
        img2 = wx.Image('mars.jpg', wx.BITMAP_TYPE_JPEG)
        bmp2 = img2.ConvertToBitmap()
        size2 = bmp2.GetWidth(), bmp2.GetHeight()
        my_bmp2 = wx.StaticBitmap(self, bitmap=bmp2)
        
        img_sizer = wx.BoxSizer(wx.VERTICAL)
        img_sizer.Add(my_bmp)
        img_sizer.Add(my_bmp2)
        
        self.SetSizer(img_sizer)
        self.SetupScrolling()
        self.Layout()
        
        
class ScrolledImg2(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(ScrolledImg2, self).__init__(parent)
        self.set_scroll_content()
        
    def set_scroll_content(self):    

        img = wx.Image('mars.jpg', wx.BITMAP_TYPE_JPEG)
        bmp = img.ConvertToBitmap()
        size = bmp.GetWidth(), bmp.GetHeight()
        my_bmp = wx.StaticBitmap(self, bitmap=bmp)
        
        img_sizer = wx.BoxSizer()
        img_sizer.Add(my_bmp)
        
        self.SetSizer(img_sizer)
        self.SetupScrolling()
        self.Layout()
        
class ScrolledImg3(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(ScrolledImg3, self).__init__(parent)
        self.set_scroll_content()
        
    def set_scroll_content(self):    
        
        img = wx.Image('xray.jpg', wx.BITMAP_TYPE_JPEG)
        bmp = img.ConvertToBitmap()
        size = bmp.GetWidth(), bmp.GetHeight()
        my_bmp = wx.StaticBitmap(self, bitmap=bmp)
        
        img_sizer = wx.BoxSizer()
        img_sizer.Add(my_bmp)
        
        self.SetSizer(img_sizer)
        self.SetupScrolling()
        self.Layout()        

     
class MainFrame(wx.Frame):
    def __init__(self):
        super(MainFrame, self).__init__( None, -1, "four scrolled panels", size=(800, 400))
        
        frame_up = wx.BoxSizer(wx.HORIZONTAL)
        frame_bo = wx.BoxSizer(wx.HORIZONTAL)
        main_frame = wx.BoxSizer(wx.VERTICAL)
        
        self.scrolled_panel_01 = ScrolledImg(self)
        frame_up.Add(self.scrolled_panel_01, 1, wx.EXPAND)
        
        self.scrolled_panel_02 = ScrolledImg2(self)
        frame_up.Add(self.scrolled_panel_02, 1, wx.EXPAND)
        
        self.scrolled_panel_03 = ScrolledImg3(self)
        frame_bo.Add(self.scrolled_panel_03, 1, wx.EXPAND)
        
        self.scrolled_panel_04 = ScrolledImgMatplotlib(self)
        frame_bo.Add(self.scrolled_panel_04, 1, wx.EXPAND)
        
        main_frame.Add(frame_up, 1, wx.EXPAND)
        main_frame.Add(frame_bo, 1, wx.EXPAND)
        
        self.SetSizer(main_frame)
        
        menubar = wx.MenuBar()
        file_menu = wx.Menu()
        edit_menu = wx.Menu()
        help_menu = wx.Menu()
        help_menu.Append(001, '&FAQ\tCtrl+H', 'Frequently Asked Questions')
        file_menu.Append(101, '&Open\tCtrl+O', 'Open a new document')
        file_menu.Append(102, '&Save\tCtrl+S', 'Save the document')
        file_menu.AppendSeparator()
        quit = wx.MenuItem(file_menu, 105, '&Quit\tCtrl+Q', 'Quit the Application')
        quit.SetBitmap(wx.Image('stock_exit.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        file_menu.AppendItem(quit)
        
        edit_menu.Append(201, 'main item1', '', wx.ITEM_CHECK)
        edit_menu.Append(202, 'main item2', kind=wx.ITEM_CHECK)
        submenu = wx.Menu()
        submenu.Append(301, 'subitem1', kind=wx.ITEM_RADIO)
        submenu.Append(302, 'subitem2', kind=wx.ITEM_RADIO)
        submenu.Append(303, 'subitem3', kind=wx.ITEM_RADIO)
        edit_menu.AppendMenu(203, 'submenu', submenu)

        menubar.Append(file_menu, '&File')
        menubar.Append(edit_menu, '&Edit')
        menubar.Append(help_menu, '&Help')
        self.SetMenuBar(menubar)
        self.CreateStatusBar()
        self.Centre()
        self.Bind(wx.EVT_MENU, self.OnQuit, id=105)
        
    def OnQuit(self, event):
        self.Close()
        
if __name__ == "__main__":
    
    app = wx.App()
    frame = MainFrame()
    frame.Show()
    app.MainLoop()
