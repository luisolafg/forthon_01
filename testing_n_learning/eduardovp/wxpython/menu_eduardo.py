#!/usr/bin/python

import wx

class MyMenu(wx.Frame):
    def __init__(self, parent, id, title):
        wx.Frame.__init__(self, parent, id, title, wx.DefaultPosition, wx.Size(350, 250))

        menubar = wx.MenuBar()
        file = wx.Menu()
        edit = wx.Menu()
        help = wx.Menu()
        file.Append(101, '&Open\tCtrl+O', 'Open a new document')
        file.Append(102, '&Save\tCtrl+S', 'Save the document')
        file.AppendSeparator()
        quit = wx.MenuItem(file, 105, '&Quit\tCtrl+Q', 'Quit the Application')
        quit.SetBitmap(wx.Image('stock_exit.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        file.AppendItem(quit)
        
        edit.Append(201, 'main item1', '', wx.ITEM_CHECK)
        edit.Append(202, ' main item2', kind=wx.ITEM_CHECK)
        submenu = wx.Menu()
        submenu.Append(301, 'subitem1', kind=wx.ITEM_RADIO)
        submenu.Append(302, 'subitem2', kind=wx.ITEM_RADIO)
        submenu.Append(303, 'subitem3', kind=wx.ITEM_RADIO)
        edit.AppendMenu(203, 'submenu', submenu)

        menubar.Append(file, '&File')
        menubar.Append(edit, '&Edit')
        menubar.Append(help, '&Help')
        self.SetMenuBar(menubar)
        self.CreateStatusBar()
        self.Centre()
        self.Bind(wx.EVT_MENU, self.OnQuit, id=105)
        
    def OnQuit(self, event):
        self.Close()

class MyApp(wx.App):
    def OnInit(self):
        frame = MyMenu(None, -1, 'menu Eduardo')
        frame.Show(True)
        return True

app = MyApp(0)
app.MainLoop()