import wx
import wx.lib.scrolledpanel as scroll_pan
import math

class ScrolledImg(scroll_pan.ScrolledPanel):

    def __init__(self, parent):
        super(ScrolledImg, self).__init__(parent)
        self.img = wx.Image('cute.jpg', wx.BITMAP_TYPE_JPEG)
        self.bmp = wx.BitmapFromImage(self.img)

        self.set_scroll_content()



    def set_scroll_content(self):

        self.size = self.bmp.GetWidth(), self.bmp.GetHeight()
        self.my_static_bmp = wx.StaticBitmap(self, bitmap=self.bmp)

        img_sizer = wx.BoxSizer(wx.VERTICAL)
        img_sizer.Add(self.my_static_bmp)
        self.SetSizer(img_sizer)
        self._ini_bind()
        self.SetupScrolling()
        self.Layout()

    def _ini_bind(self):

        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.my_static_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.my_static_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.my_static_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.scroll_rot = 0

    def OnLeftButDown(self, event):
        print "OnLeftButDown"

    def OnMouseMotion(self, event):
        print "Hi MOTION"

    def OnLeftButUp(sef, event):
        print "OnLeftButUp"

    def OnMouseWheel(self, event):
        print "Hi scrolled"

        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        self.scroll_rot += sn_mov
        print 'sn_mov =', sn_mov
        print 'event.GetWheelRotation()', event.GetWheelRotation()

    def OnIdle(self, event):

        if self.scroll_rot != 0:
            w, h = self.size
            NewW = w * (self.scroll_rot * 0.05 + 1.0)
            NewH = h * (self.scroll_rot * 0.05 + 1.0)
            image = self.img.Scale(NewW, NewH, wx.IMAGE_QUALITY_HIGH)
            self.bmp = wx.BitmapFromImage(image)
            self.set_scroll_content()


class MainFrame(wx.Frame):
    def __init__(self):
        super(MainFrame, self).__init__( None, -1, "four scrolled panels", size=(800, 400))

        frame_up = wx.BoxSizer(wx.HORIZONTAL)
        frame_bo = wx.BoxSizer(wx.HORIZONTAL)
        main_frame = wx.BoxSizer(wx.VERTICAL)

        self.scrolled_panel_01 = ScrolledImg(self)
        frame_up.Add(self.scrolled_panel_01, 1, wx.EXPAND)

        self.scrolled_panel_02 = ScrolledImg(self)
        frame_up.Add(self.scrolled_panel_02, 1, wx.EXPAND)

        main_frame.Add(frame_up, 1, wx.EXPAND)
        main_frame.Add(frame_bo, 1, wx.EXPAND)

        self.SetSizer(main_frame)


if __name__ == "__main__":

    app = wx.App()
    frame = MainFrame()
    frame.Show()
    app.MainLoop()
