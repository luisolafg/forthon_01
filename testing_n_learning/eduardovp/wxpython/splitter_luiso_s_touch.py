import wx
import wx.lib.scrolledpanel as scroll_pan

class PaintPanel(wx.Panel):
    def __init__(self, outer_panel):
        super(PaintPanel, self).__init__(outer_panel)

        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.Mouse_Left_clk = False
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)

        self.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        #self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.lin_xy_lst = []
        self.Bind(wx.EVT_IDLE, self.OnIdle)

        self.Layout()

    def OnPaint(self, event):
        #print "OnPaint"
        if(self.Mouse_Pos_x > 0 and self.Mouse_Pos_y > 0):
            self.Drawing()

    def Drawing(self):
        #print "Drawing"
        self.DevCont = wx.PaintDC(self)
        self.DevCont.Clear()
        self.DevCont.SetPen(wx.Pen("BLACK", 4))
        self.lin_xy_lst.append([self.Mouse_Pos_x, self.Mouse_Pos_y])
        if( len(self.lin_xy_lst) > 1 ):
            prev_crd = [0,0]
            for loc_crd in self.lin_xy_lst:
                self.DevCont.DrawLine(prev_crd[0], prev_crd[1], loc_crd[0], loc_crd[1])
                prev_crd = [loc_crd[0], loc_crd[1]]
        self.Layout()


    def OnLeftButDown(self, event):
        #print "OnLeftButDown"
        self.Mouse_Left_clk = True


    def OnLeftButUp(self, event):
        #print "OnLeftButUp"
        self.Mouse_Left_clk = False

    def OnIdle(self, event):
        #print "OnIdle"
        if( self.Mouse_Left_clk == True):
            #print "self.Mouse_Pos_x, self.Mouse_Pos_y =", self.Mouse_Pos_x, self.Mouse_Pos_y
            self.Drawing()

    def OnMouseMotion(self, event):
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()


class MyForm(wx.Frame):

    def __init__(self):
        wx.Frame.__init__(self, None, title="Splitter Tutorial")

        splitter = wx.SplitterWindow(self)
        leftP = PaintPanel(splitter)
        rightP = PaintPanel(splitter)

        # split the window
        splitter.SplitVertically(leftP, rightP)
        splitter.SetMinimumPaneSize(20)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(splitter, 1, wx.EXPAND)
        self.SetSizer(sizer)

if __name__ == "__main__":
    app = wx.App(False)
    frame = MyForm()
    frame.Show()
    app.MainLoop()
