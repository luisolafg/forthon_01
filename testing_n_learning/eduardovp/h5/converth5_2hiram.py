#!/usr/bin/env python
import h5py
import numpy as np


first_csv_column = ['Enegoat ']
file_path = 'Ti_soil_Eliana/blc12472_Ti_soil_Eliana.h5'

def h5man():

    with h5py.File(file_path, "r") as h5file:

        first_keys = h5file.keys()
        matchers = ['poi']
        matchers2 = ['2.1']
        poi_files = [s for s in first_keys if (any(xs in s for xs in matchers) and any(xs in s for xs in matchers2))]
        print("poi_files = ", poi_files)
        number_keys_poi_files = len(poi_files)

        orange_file = []
        enegoat_global = h5file['Ti_soil_Eliana_poi3534_3371: 2.1/measurement/enegoat'][...]

        for i in range(number_keys_poi_files):

            file_name = str(poi_files[i])
            enegoat_path = file_name + '/measurement/enegoat'
            enegoat = h5file[enegoat_path][...]
            # print('enegoat' , enegoat)

            iodet_path = file_name + '/measurement/iodet'
            iodet = h5file[iodet_path][...]
            # print('iodet', iodet)

            det1_path = file_name + '/measurement/fx2_det1_TiKa'
            det1 = h5file[det1_path][...]
            # print('det1', det1)
            det1_dead_time_path = file_name + '/measurement/fx2_det1_fractional_dead_time'
            det1_dead_time = h5file[det1_dead_time_path][...]
            # print('det1_dead_time', det1_dead_time )
            absorb_norm1 = (det1 / (iodet * (1 - det1_dead_time)))
            # print(absorb_norm1)

            det0_path = file_name + '/measurement/fx2_det0_TiKa'
            det0 = h5file[det0_path][...]
            # print('det0', det0)
            det0_dead_time_path = file_name + '/measurement/fx2_det0_fractional_dead_time'
            det0_dead_time = h5file[det0_dead_time_path][...]
            # print('det0_dead_time', det0_dead_time)
            absorb_norm0 = (det0 / (iodet * (1 - det0_dead_time)))
            # print('absorb_norm0', absorb_norm0)

            absorb_avg = ((absorb_norm0 + absorb_norm1) / 2)
            # print('absorb_avg', absorb_avg)

            combined = np.vstack((enegoat, absorb_avg)).T
            #print(combined)
            cycle = str(i)
            #print('Cycle', cycle)
            orange_file.append(absorb_avg)
            first_csv_column.append(file_name)

            tittle_file = poi_files[i]
            tittle_file2 = "{0}.txt".format(str(tittle_file[:27]))
            header = 'Header no ' + cycle + '\n'
            np.savetxt(tittle_file2, combined, fmt='%10.5f', header=header)

        #print(type(orange_file))
        #print(orange_file)
        orange_file.insert(0,enegoat_global)
        np.savetxt("Orange.csv", orange_file, fmt='%10.5f', delimiter=',')

        #print('first_csv_column = ', first_csv_column)


def add_col(path_in, lst_col, path_out):

    file_in = open(path_in, "r")
    in_line_lst = file_in.readlines()
    file_in.close()

    file_out = open(path_out, "w")
    for position, str_line in enumerate(in_line_lst):
        wrstring = str(lst_col[position] + ',') + str_line
        file_out.write(wrstring)

    file_out.close()

if __name__ == "__main__":

    h5man()
    add_col("Orange.csv", first_csv_column, "orange_final.csv")
