# Studying code

def getArgNr(searchStr):
   for ii in range(len(sys.argv)):
       if sys.argv[ii]==searchStr:
           return ii
   else:
      usage_message()

def getOptions():
   options={}
   scanList=[]
   if "-h" in sys.argv:
      usage_message()
   if "-s" in sys.argv:
      scanListStr=sys.argv[getArgNr("-s")+1]
      scanList=str2list(scanListStr)
      #print(scanList)
      #for scan in scanList:
      #    scanList.append(dict(type="scan/zap",nr=scan))
   """
   if "-ascan" in sys.argv:
      ascanListStr=sys.argv[getArgNr("-ascan")+1]
      ascanList=str2list(ascanListStr)
      for scan in ascanList:
          scanList.append(dict(type="ascan",nr=scan))
   if "-dscan" in sys.argv:
      dscanListStr=sys.argv[getArgNr("-dscan")+1]
      dscanList=str2list(dscanListStr)
      for scan in dscanList:
          scanList.append(dict(type="dscan",nr=scan))
   if "-timescan" in sys.argv:
      timescanListStr=sys.argv[getArgNr("-timescan")+1]
      timescanList=str2list(timescanListStr)
      for scan in timescanList:
          scanList.append(dict(type="timescan",nr=scan))
   if "-loopscan" in sys.argv:
      loopscanListStr=sys.argv[getArgNr("-loopscan")+1]
      loopscanList=str2list(loopscanListStr)
      for scan in loopscanList:
          scanList.append(dict(type="loopscan",nr=scan))
   if "-zapscan" in sys.argv:
      zapscanListStr=sys.argv[getArgNr("-zapscan")+1]
      zapscanList=str2list(zapscanListStr)
      for scan in zapscanList:
          scanList.append(dict(type="zapscan",nr=scan))
   if "-zapdcm" in sys.argv:
      zapscanListStr=sys.argv[getArgNr("-zapdcm")+1]
      zapscanList=str2list(zapscanListStr)
      for scan in zapscanList:
          scanList.append(dict(type="zapdcm",nr=scan))
   """
   options["scanList"]=scanList
   if "-o" in sys.argv:
      options["prefix"]=sys.argv[getArgNr("-o")+1]
      if options["prefix"][-1]!="_":
           options["prefix"]=sys.argv[getArgNr("-o")+1]+"_"
   else:
      options["prefix"]=""
   if "-ctr" in sys.argv:
      options["counters"]=sys.argv[getArgNr("-ctr")+1].split(",")
   else:
      options["counters"]="all"
   if "-crop" in sys.argv:
      options["crop"]="crop"
   elif "-pad" in sys.argv:
      options["crop"]="pad"
   else:
      options["crop"]="RaiseError"
   return options

def usage_message():
   print("Usage is: %s <options> hdf5-file" % sys.argv[0])
   print("   -h                       shows help")
   print("   -s <scanlist>            extract specified scans, e.g. 1-23,25,30-40")
   #print("   -ascan <scanlist>        extract specified scans, e.g. 1-23,25,30-40")
   #print("   -dscan <scanlist>        extract specified scans, e.g. 1-23,25,30-40")
   #print("   -zapscan <scanlist>      extract specified scans, e.g. 1-23,25,30-40")
   #print("   -zapdcm <scanlist>       extract specified scans, e.g. 1-23,25,30-40")
   #print("   -timescan <scanlist>     extract specified scans, e.g. 1-23,25,30-40")
   #print("   -loopscan <scanlist>     extract specified scans, e.g. 1-23,25,30-40")
   print("   -o <prefix>              prefix for output files")
   print("   -pad                     add 0 to columns with inssufficient length")
   print("   -crop                    reduces all columns to shortest column")
   print("                            default behaviour is Exit on different column length")
   print("   -ctr <counterlist>       default: all, else e.g. bv4.X,bv4.Y,bv4.Int")
   print("   -epoch                   adds epoch column")
   sys.exit(0)

def getEpoch(timeStr):
   thistime=time.mktime(datetime.strptime(timeStr, '%Y-%m-%d %H:%M:%S').timetuple())
   starttime=time.mktime(datetime.strptime('2018-11-14 08:00:00', '%Y-%m-%d %H:%M:%S').timetuple())
   return thistime-starttime

#=======================================================================
def str2list(liststr,remove_duplicates=True,sort=True,abort_on_error=True):
   try:
      commaparts=liststr.split(",");
      slist=[]
      scans=0
      for idx in range(len(commaparts)):
          colonparts=commaparts[idx].split("-");
          if len(colonparts)==1:
             colonparts=commaparts[idx].split(":");
          if len(colonparts)==1:
             scstart=int(commaparts[idx]);
             scend=scstart;
          else:
             scstart=int(colonparts[0]);
             scend=int(colonparts[1]);
          for ii in range(scend-scstart+1):
             scans=scans+1
             slist.append(scstart+ii)
      if remove_duplicates==True:
          slist=list(set(slist)) #removes duplicates                                                              
      if sort==True:
          slist.sort()
      return slist
   except:
      print("Incomprehensive number list. Try a:b,c,d:f")
      if abort_on_error==True:
         usage_message()
      else:
         return []

def listprint(list,sortit=False,title="",endline=""):
   if title!="":
      print("%s" % title)
      print("------------------------")
   if sortit:
      for item in sorted(list):
         print(item)
   else:
      for item in list:
         print(item)
   if endline!="":
      print(endline)

#=======================================================================
def getScanList(h5obj):
   #print("Scans in %s:" % sys.argv[-1])
   listprint(h5obj.keys())
   scanList={}
   scanTypeList=[]
   for item in h5obj.keys():
      scantype=item.split("_")[0]
      scanNr=int(item.split("_")[1])
      #print("%s %4d" % (scantype,scanNr))
      if scantype not in scanTypeList:
         scanTypeList.append(scantype)
         scanList[scantype]=[]
      scanList[scantype].append(scanNr)
   #print("Scantypes:")
   for item in scanTypeList:
      scanList[item]
      #print("   %10s: %s" % (item, scanList[item]))
   return scanList["ascan"]

def getMotNameInScan(h5f,MeasurementString):
   meas_keys=h5f[MeasurementString].keys()
   for item in meas_keys:
      if item.startswith("group_"):
         newpath='%s/%s' % (MeasurementString,item)
         subitems=h5f[newpath]
         for sitem in subitems:
             if ":" in sitem:
                 motname=sitem.split(":")[-1]
                 motpath="%s/%s" % (newpath,sitem)
                 return motname,motpath

#=======================================================================
def writeHeader(outf,scan,motors,positions,mytime):
   outf.write("#S %s\n" % scan)
   outf.write("#T %s\n" % mytime)   
   outf.write("#M ")
   for mm in motors:
      outf.write("%s " % mm)
   outf.write("\n")
   outf.write("#P ")
   for pp in positions:
      try:
         outf.write("%s " % pp[0])
      except:
         outf.write("%s " % pp)
   outf.write("\n")

def write2file(outFile,header,xcolNames,xcols,ycolNames,ycols,colLength):
   print("xcolNames", xcolNames)
   print("ycolNames", ycolNames)
   print(colLength)

   outf=open(outFile,"w")
   writeHeader(outf,header["scan"],header["motors"],header["positions"],header["time"])
   outf.write("#L ")
   for label in  xcolName:
      outf.write("%s " % label)
   for label in  ycolName:
      outf.write("%s " % label)
   outf.write("\n")
   allcols=np.concatenate((xcols,ycols))
   towrite=allcols[:,:colLength]
   np.savetxt(outf,towrite.transpose(),fmt='%.6f')
   outf.close()

#================================================================================
# MAIN
#================================================================================

if "-h" in sys.argv:
   usage_message()
input_file = sys.argv[-1]
if os.path.isfile(input_file)==0 or "h5extract" in input_file:
   usage_message()
options=getOptions()
# Read data
print(input_file)
#h5f = h5py.File(input_file)
h5f=h5py.File(open(input_file, "rb"), mode="r")

ctrList=options["counters"]
scanList=options["scanList"]
print("Desired scans:")
print(scanList)
print("")
ctrList=options["counters"]
print("Desired columns:")
print(ctrList)
print("")

listprint(h5f.keys(),title="Scans in hdf5 file:",endline="\n")

for scan in scanList:
    scankey="%d.1" % scan

    print("%s:" % (scankey))
    print("-----------------")

    measure_path = '%s/measurement/' % (scankey)
    instrum_path = '%s/instrument/' % (scankey)
    base_path = '%s/' % (scankey)

    minlength=1e6
    nrYcols=0
    nrXcols=0
    xcolName=[]
    xcolPath=[]
    xcolLength=[]
    xcols=[]
    ycolName=[]
    ycolPath=[]
    ycols=[]
    tcolName=[]
    tcolPath=[]
    ycolLength=[]
    tcols=[]


    try:
       keystemp=h5f[measure_path].keys() 
    except:
       print("################## SKIP SCAN ###################")
       print("File:  %s " % input_file)
       print("%s %d:" % (scan_type,scan_nr))
       print("Can not execute: print h5f[measure_path].keys()")
       print("################## SKIP SCAN ###################")
       continue

    startTime_path = '%s/start_time/' % (scankey)
    startTime= h5f[startTime_path].value
    scanDate=startTime.split("T")[0]
    scanTime=startTime.split("T")[-1].split(".")[0]
    startTime="%s %s" % (scanDate,scanTime) 
    print(startTime)


    title_path = '%s/title/' % (scankey)
    title= h5f[title_path].value
    print(title)

    if "time" in title.split()[0]:
       print("Timescan, no motors moved")
    else:
       print("At least motor %s did move. this motor is x-axis" % title.split()[1])


    print("")
    listprint(h5f[measure_path].keys(),title="column-labels in scan:",endline="-------------------\n\n")




    print("Getting motor positions at start for header ...")           
    pos_path = instrum_path + "/positioners/"
    motors=h5f[pos_path].keys()
    positions=[]
    for motor in h5f[pos_path].values():
        positions.append(motor.value)
    #print motors
    #print positions
    print("%d motor positions read." % len(motors))
    print("")


    #run through columns of scan (the data)
    for column_key in h5f[measure_path].keys():

       column_path =  measure_path + column_key + "/"
       thiscol=h5f[column_path][()]
       print("%s %d points" % (column_key,len(thiscol)))



       if column_key.startswith("pstn_"):
          instkey=column_key.split("pstn_")[-1]
       else:
          instkey=column_key
       #is it x-axis or y-axis?
       #check in instrument_path and whether it is NXdetector class or NXpositioner class
       myStr="%s" % h5f[instrum_path + instkey].keys()
       myType=myStr.split("'")[1]                     #possiblities are data (detector), value (motor)
       
       if 1:
          if "time" in column_key:
             datatype="time"
          elif myType=="value" or column_key=="trajmot":
             datatype="motor"
          else:
             datatype="detector"
          print("column ",column_key," is of type ",datatype)



       if datatype in ["motor","time"]:
          xcols.append(thiscol)
          xcolName.append(instkey)
          xcolPath.append(column_path)
          xcolLength.append(len(thiscol))
          nrXcols+=1
          minlength=min(minlength,len(thiscol))
       else:
          if instkey in ctrList or ctrList=="all":
             # datatype=="detector" or other
             ycols.append(thiscol)
             ycolName.append(instkey)
             ycolPath.append(column_path)
             ycolLength.append(len(thiscol))
             nrYcols+=1
             minlength=min(minlength,len(thiscol))

    print(xcolName)
    print(xcolLength)
    print(ycolName)
    print(ycolLength)
    maxLength=np.max(np.append(xcolLength,ycolLength))
    minLength=np.min(np.append(xcolLength,ycolLength))

    if maxLength!=minLength:
       print("Different length in output columns!!!") 
       if options["crop"]=="RaiseError":
          print("Default behaviour in this case is exit. Try options -crop or -pad if desired.")
          continue
       elif options["crop"]=="pad":
          for col in xcols:
             pass
          for col in ycols:
             pass
       else:   #options["crop"]=="crop":
          for col in xcols:
             if len(col)>minLength:
                print("not implemented yet")
          for col in ycols:
             if len(col)>minLength:
                print("not implemented yet")

    if "-epoch" in sys.argv or xcols==[]:
       if xcols==[]:
          #e.g. timescan, or loopscan. No motors moved. no motor group.
          print("No motors found. Timescan")
       print("searching for time axis...")
       ycolCtr=-1
       for name in ycolName:
          ycolCtr+=1
          if name=="elapsed_time" or name=="time":
             print("Found elapsed_time column")
             epochcol=ycols[ycolCtr]+getEpoch(startTime)
       xcolCtr=-1
       for name in xcolName:
          xcolCtr+=1
          if name=="elapsed_time" or name=="time":
             print("Found elapsed_time column")
             epochcol=xcols[xcolCtr]+getEpoch(startTime)
       try:
          xcols.append(epochcol)
          xcolName.append("epoch")
       except:
          pass


    outname="%s_%s.txt" % (options["prefix"],scan)

    if len(title.split())>1:
       scanStr=title
    else:
       #early days of hdf5 in Bliss at ESRF did not generate a title for the scan themselves!!!
       scanStr="%s  %d  %s %f %f %d <ct-time?>" % (scan_type, scan_nr, xcolName[0], xcols[0][0], xcols[0][-1], len(xcols[0]))

    print("")
    print("")
    print(scanStr)

    if len(title.split())<3:
       print("%d points" % minlength)
    print("===> %s" % (outname))

    if minlength>1:
       header=dict(scan= scanStr, motors=motors, positions=positions, time=startTime)


       try:
          test1=np.array(xcols)
          test2=np.array(ycols)
       except:
          print("Different length of columns. BAD")
          sys.exit(0)

       write2file(outname,header,xcolName,np.array(xcols),ycolName,np.array(ycols),minlength)


