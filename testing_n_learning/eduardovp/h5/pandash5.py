#!/usr/bin/env python3
import h5py
import numpy as np
import pandas as pd

def h5man():
 
    dataset = h5py.File('TPI_M2_poi4888_4749.h5', 'r')
    dataset.keys()
    measurement = dataset['2.1/measurement']
    measurement.keys()
    #print(measurement.keys())
    
    print("enetraj: {}".format(measurement['enetraj']))
    print("enetraj data attributes: {}".format(list(measurement['enetraj'].attrs)))

    #print("Unit: {}".format(measurement['enetraj'].attrs['units'].decode()))
    longitude_values = np.repeat(list(measurement['enetraj']), 1)
    dataset = pd.DataFrame({"enetraj": longitude_values})
    
    #dataset.columns = [measurement['enetraj'].attrs['units'].decode()]
    dataset.head()
    
    dataset.to_csv("precipitation_jan_2020.csv", index = False)

if __name__ == "__main__":
    h5man()
