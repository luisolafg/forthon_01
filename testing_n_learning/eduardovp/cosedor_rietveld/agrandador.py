import numpy as np
import sys


def tarea():
    
    fichero1 = raw_input("\n Escribe el nombre del primer fichero \n ")
    fichero2 = raw_input("\n Escribe el nombre del segundo fichero \n ")
    fichero_out = open(raw_input("\n Escribe el nombre del fichero de salida \n "), "w+")
    print
    
    rangle1 = np.loadtxt(fichero1, usecols = [0])
    
    rangle2 = np.loadtxt(fichero2, usecols = [0])
    
    mangle1 = np.loadtxt(fichero1, usecols = [1])
    
    mangle2 = np.loadtxt(fichero2, usecols = [1])
    
    theta1 = np.loadtxt(fichero1, usecols = [2])
    
    theta2 = np.loadtxt(fichero2, usecols = [2])
  
    rintensity1 = np.loadtxt(fichero1, usecols = [3])
   
    rintensity2 = np.loadtxt(fichero2, usecols  = [3])
    
    mintensity1 = np.loadtxt(fichero1, usecols = [4])
    
    mintensity2 = np.loadtxt(fichero2, usecols = [4])
    
    srangle = []

    smangle = []
    
    stheta = []
    
    srintensity = []
    
    smintensity = []

    for a in rangle1:
       
        srangle.append(a)
        
    for a in rangle2:
        
        srangle.append(a)   
        
    for a in mangle1:
       
        smangle.append(a)
        
    for a in mangle2:
        
        smangle.append(a) 
        
    for a in theta1:
       
        stheta.append(a)
        
    for a in theta2:
        
        stheta.append(a)
    
    for a in rintensity1:
        
        srintensity.append(a)
        
    for a in rintensity2:
        
        srintensity.append(a)
        
    for a in mintensity1:
       
        smintensity.append(a)
        
    for a in mintensity2:
        
        smintensity.append(a)    
     
    sys.stdout = fichero_out
    for a,b,c,d,e in zip (srangle,smangle,stheta,srintensity,smintensity):
        
        print "%f %f %f %f %f" %(a,b,c,d,e)
    
tarea()