*** TITLE ***
[                   ]
_________________________________



*** CRYSTAL STRUCTURE ***
_________________________________

Space group
[        ]
_________________________________

    a         b         c
[        ][        ][        ]
________________________________

alpha        beta     gamma
[        ][        ][        ]
_________________________________

Atomic positions
#########
#########
#########
_________________________________



*** POLYCRYSTAL STRUCTURE ***
_________________________________

Average crystal size (Angstrom)
[       ]   <-- default: 1000
_________________________________

Average strain (%%)
[       ]   <-- default: 0
_________________________________

Preferred orientation
     H        K          L
[        ][         ][       ]   
_________________________________

IPF Gaussian distribution FWHM
[        ]
_________________________________

Inverse pole figure azimuth
[        ]   <-- default: 0
_________________________________

IPF number of horizontal steps
{        ]
_________________________________




*** EXPERIMENTAL DATA ***
_________________________________

LOAD 2D PATTERN
_________________________________

- Detector type
- Data type
- Find file
_________________________________


PATTERN PARAMETERS
_________________________________

Wavelength (Angstrom)
[        ]
_________________________________

Sample-detector distance (mm)
[        ]
_________________________________

X_Beam centre     Y_Beam centre
[        ]        [        ]
_________________________________

X_max             Y_max
[        ]        [        ]
_________________________________

Detector diameter (mm) 
[        ]   
_________________________________







