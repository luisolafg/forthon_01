class data(object):
    pass

def read_dat_file(my_file_path = "param.dat"):


    myfile = open(my_file_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    lst_dat = []

    for lin_char in all_lines:

        commented = False

        for delim in ',;':
            lin_char = lin_char.replace(delim, ' ')

        for litle_block in lin_char.split():
            if( litle_block == "!" or litle_block == "#" ):
                commented = True
            if( not commented and litle_block != ","):
                lst_dat.append(litle_block)

    xres = lst_dat[1]
    yres = lst_dat[3]
    xbeam = lst_dat[5]
    ybeam = lst_dat[7]
    lambd = lst_dat[9]
    dst_det = lst_dat[11]
    diam_det = lst_dat[13]
    thet_min = lst_dat[15]

    dt = data()


    dt.xres     = xres
    dt.yres     = yres
    dt.xbeam    = xbeam
    dt.ybeam    = ybeam
    dt.lambd    = lambd
    dt.dst_det  = dst_det
    dt.diam_det = diam_det
    dt.thet_min = thet_min

    return dt

if( __name__ == "__main__" ):
    dat_file = read_dat_file(my_file_path = "param.dat")
