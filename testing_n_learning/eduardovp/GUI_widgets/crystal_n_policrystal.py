#!/usr/bin/python
# input by Eduardo

import wx
import os
from readcfl import *
from crystal import write_cfl

def write_cfl(dto):

    myfile = open("cfl_out.cfl", "w")
    wrstring = "SIZE_LG " + dto.new_input8 + "\n"
    myfile.write(wrstring);
    wrstring = "IPF_RES " + dto.new_input16 + "\n"
    myfile.write(wrstring);
    wrstring =  "HKL_PREF " + dto.new_input11 + " " + dto.new_input12 + " " + dto.new_input13 + "\n"
    myfile.write(wrstring);
    wrstring = "HKL_PREF_WH " + dto.new_input14 + "\n"
    myfile.write(wrstring);
    wrstring = "AZM_IPF " + dto.new_input15 + "\n"
    myfile.write(wrstring);

    myfile.close()



class MyForm(wx.Frame):
    def __init__(self):
        super(MyForm, self).__init__( None, -1, 'Input Data')
        self.panel_02 = PoliCrystalPanel(self)
        self.panel_01 = CrystalPanel(self)

        MainSizer = wx.BoxSizer(wx.VERTICAL)
        MainSizer.Add(self.panel_01, 0, wx.ALL|wx.CENTER, 5)
        MainSizer.Add(self.panel_02, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)


class CrystalPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(CrystalPanel, self).__init__(parent)
        print "Hi"


        title = wx.StaticText(self, wx.ID_ANY, 'Title')
        self.inputtitle = wx.TextCtrl(self, wx.ID_ANY, '')

        subt1 = wx.StaticText(self, wx.ID_ANY, '*** CRYSTAL STRUCTURE ***')

        label1 = wx.StaticText(self, wx.ID_ANY, '  Space group  ')
        self.input1 = wx.TextCtrl(self, wx.ID_ANY, '')

        label2 = wx.StaticText(self, wx.ID_ANY, '           a')
        self.input2 = wx.TextCtrl(self, wx.ID_ANY, '')

        label3 = wx.StaticText(self, wx.ID_ANY, '           b')
        self.input3 = wx.TextCtrl(self, wx.ID_ANY, '')

        label4 = wx.StaticText(self, wx.ID_ANY, '           c')
        self.input4 = wx.TextCtrl(self, wx.ID_ANY, '')

        label5 = wx.StaticText(self, wx.ID_ANY, '      alpha')
        self.input5 = wx.TextCtrl(self, wx.ID_ANY, '')

        label6 = wx.StaticText(self, wx.ID_ANY, '       beta')
        self.input6 = wx.TextCtrl(self, wx.ID_ANY, '')

        label7 = wx.StaticText(self, wx.ID_ANY, '      gamma')
        self.input7 = wx.TextCtrl(self, wx.ID_ANY, '')



        ReadBtn = wx.Button(self, wx.ID_ANY, 'Read')
        OpenBtn = wx.Button(self, wx.ID_ANY, 'Open .cfl')
        self.Bind(wx.EVT_BUTTON, self.onRead, ReadBtn)
        self.Bind(wx.EVT_BUTTON, self.onOpen, OpenBtn)

        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        titleSizer       = wx.BoxSizer(wx.HORIZONTAL)
        subt1Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        spcgrpSizer      = wx.BoxSizer(wx.HORIZONTAL)
        abcSizer         = wx.BoxSizer(wx.HORIZONTAL)
        anglesSizer      = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)

        inputa           = wx.BoxSizer(wx.VERTICAL)
        inputb           = wx.BoxSizer(wx.VERTICAL)
        inputc           = wx.BoxSizer(wx.VERTICAL)

        inputalpha       = wx.BoxSizer(wx.VERTICAL)
        inputbeta        = wx.BoxSizer(wx.VERTICAL)
        inputgamma       = wx.BoxSizer(wx.VERTICAL)


        inputa.Add(label2, 0, wx.ALL, 2)
        inputa.Add(self.input2, 2, wx.ALL| wx.EXPAND, 2)
        inputb.Add(label3, 0, wx.ALL, 2)
        inputb.Add(self.input3, 2, wx.ALL| wx.EXPAND, 2)
        inputc.Add(label4, 0, wx.ALL, 2)
        inputc.Add(self.input4, 2, wx.ALL| wx.EXPAND, 2)

        inputalpha.Add(label5, 0, wx.ALL, 2)
        inputalpha.Add(self.input5, 2, wx.ALL| wx.EXPAND, 2)
        inputbeta.Add(label6, 0, wx.ALL, 2)
        inputbeta.Add(self.input6, 2, wx.ALL| wx.EXPAND, 2)
        inputgamma.Add(label7, 0, wx.ALL, 2)
        inputgamma.Add(self.input7, 2, wx.ALL| wx.EXPAND, 2)


        titleSizer.Add(title, 0, wx.ALL, 2)
        titleSizer.Add(self.inputtitle, 2, wx.ALL| wx.EXPAND, 2)

        subt1Sizer.Add(subt1, 0, wx.ALL, 2)

        spcgrpSizer.Add(label1, 0, wx.ALL, 2)
        spcgrpSizer.Add(self.input1, 2, wx.ALL| wx.EXPAND, 2)

        abcSizer.Add(inputa, 0, wx.ALL| wx.EXPAND, 2)
        abcSizer.Add(inputb, 0, wx.ALL| wx.EXPAND, 2)
        abcSizer.Add(inputc, 0, wx.ALL| wx.EXPAND, 2)

        anglesSizer.Add(inputalpha, 0, wx.ALL| wx.EXPAND, 2)
        anglesSizer.Add(inputbeta, 0, wx.ALL| wx.EXPAND, 2)
        anglesSizer.Add(inputgamma, 0, wx.ALL| wx.EXPAND, 2)

        btnSizer.Add(ReadBtn, 0, wx.ALL, 5)
        btnSizer.Add(OpenBtn, 0, wx.ALL, 5)


        MainSizer.Add(titleSizer, 0, wx.EXPAND)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt1Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(spcgrpSizer, 0, wx.LEFT)
        MainSizer.Add(abcSizer, 0, wx.CENTER)
        MainSizer.Add(anglesSizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRead(self, event):

        new_inputtitle = self.inputtitle.GetValue()
        new_input1 = self.input1.GetValue()
        new_input2 = self.input2.GetValue()
        new_input3 = self.input3.GetValue()
        new_input4 = self.input4.GetValue()
        new_input5 = self.input5.GetValue()
        new_input6 = self.input6.GetValue()
        new_input7 = self.input7.GetValue()

        dto = data()

        dto.new_inputtitle =  new_inputtitle
        dto.new_input1     =  new_input1
        dto.new_input2     =  new_input2
        dto.new_input3     =  new_input3
        dto.new_input4     =  new_input4
        dto.new_input5     =  new_input5
        dto.new_input6     =  new_input6
        dto.new_input7     =  new_input7


        print dto.new_inputtitle
        print dto.new_input1
        print dto.new_input2
        print dto.new_input3
        print dto.new_input4
        print dto.new_input5
        print dto.new_input6
        print dto.new_input7

        write_cfl(dto)

        return dto

    def onOpen(self, event):

        dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
        if dlg.ShowModal() == wx.ID_OK:
                 path = dlg.GetPath()
                 mypath = os.path.basename(path)
        dlg.Destroy()

        my_cfl_content = read_cfl_file(my_file_path = mypath)

        self.inputtitle.SetValue(my_cfl_content.title)
        self.input1.SetValue(my_cfl_content.spgr)
        self.input2.SetValue(my_cfl_content.a)
        self.input3.SetValue(my_cfl_content.b)
        self.input4.SetValue(my_cfl_content.c)
        self.input5.SetValue(my_cfl_content.alpha)
        self.input6.SetValue(my_cfl_content.beta)
        self.input7.SetValue(my_cfl_content.gamma)



class PoliCrystalPanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(PoliCrystalPanel, self).__init__(parent)



        subt2 = wx.StaticText(self, wx.ID_ANY, '*** POLYCRYSTAL STRUCTURE ***')

        label8 = wx.StaticText(self, wx.ID_ANY, 'Average crystal size ')
        self.input8 = wx.TextCtrl(self, wx.ID_ANY, '')
        label81 = wx.StaticText(self, wx.ID_ANY, ' (Angstrom)')

        label9 = wx.StaticText(self, wx.ID_ANY, 'Average strain          ')
        self.input9 = wx.TextCtrl(self, wx.ID_ANY, '')
        label91 = wx.StaticText(self, wx.ID_ANY, ' (%)')

        label10 = wx.StaticText(self, wx.ID_ANY, '                   Preferred orientation')
        label11 = wx.StaticText(self, wx.ID_ANY, '           H')
        self.input11 = wx.TextCtrl(self, wx.ID_ANY, '')
        label12 = wx.StaticText(self, wx.ID_ANY, '           K')
        self.input12 = wx.TextCtrl(self, wx.ID_ANY, '')
        label13 = wx.StaticText(self, wx.ID_ANY, '           L')
        self.input13 = wx.TextCtrl(self, wx.ID_ANY, '')

        label14 = wx.StaticText(self, wx.ID_ANY, 'IPF Gaussian distribution FWHM')
        self.input14 = wx.TextCtrl(self, wx.ID_ANY, '')

        label15 = wx.StaticText(self, wx.ID_ANY, 'Inverse pole figure azimuth      ')
        self.input15 = wx.TextCtrl(self, wx.ID_ANY, '')

        label16 = wx.StaticText(self, wx.ID_ANY, 'IPF number of horizontal steps ')
        self.input16 = wx.TextCtrl(self, wx.ID_ANY, '')

        RunBtn = wx.Button(self, wx.ID_ANY, 'Run')
        OpenBtn = wx.Button(self, wx.ID_ANY, 'Open .cfl')
        self.Bind(wx.EVT_BUTTON, self.onRun, RunBtn)
        self.Bind(wx.EVT_BUTTON, self.onOpen, OpenBtn)


        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        subt2Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        avgeszSizer      = wx.BoxSizer(wx.HORIZONTAL)
        avgestrSizer     = wx.BoxSizer(wx.HORIZONTAL)
        hklSizer         = wx.BoxSizer(wx.VERTICAL)
        fwhmSizer        = wx.BoxSizer(wx.HORIZONTAL)
        ipfazSizer       = wx.BoxSizer(wx.HORIZONTAL)
        ipfnsSizer       = wx.BoxSizer(wx.HORIZONTAL)
        btnSizer         = wx.BoxSizer(wx.HORIZONTAL)


        hSizer           = wx.BoxSizer(wx.VERTICAL)
        kSizer           = wx.BoxSizer(wx.VERTICAL)
        lSizer           = wx.BoxSizer(wx.VERTICAL)
        hklhSizer        = wx.BoxSizer(wx.HORIZONTAL)

        hSizer.Add(label11, 0, wx.ALL, 2)
        hSizer.Add(self.input11, 2, wx.ALL| wx.EXPAND, 2)
        kSizer.Add(label12, 0, wx.ALL, 2)
        kSizer.Add(self.input12, 2, wx.ALL| wx.EXPAND, 2)
        lSizer.Add(label13, 0, wx.ALL, 2)
        lSizer.Add(self.input13, 2, wx.ALL| wx.EXPAND, 2)

        hklhSizer.Add(hSizer, 0, wx.ALL| wx.EXPAND, 2)
        hklhSizer.Add(kSizer, 0, wx.ALL| wx.EXPAND, 2)
        hklhSizer.Add(lSizer, 0, wx.ALL| wx.EXPAND, 2)

        subt2Sizer.Add(subt2, 0, wx.ALL, 2)

        avgeszSizer.Add(label8, 0, wx.ALL, 2)
        avgeszSizer.Add(self.input8, 2, wx.ALL| wx.EXPAND, 2)
        avgeszSizer.Add(label81, 0, wx.ALL, 2)

        avgestrSizer.Add(label9, 0, wx.ALL, 2)
        avgestrSizer.Add(self.input9, 2, wx.ALL| wx.EXPAND, 2)
        avgestrSizer.Add(label91, 0, wx.ALL, 2)

        hklSizer.Add(label10, 0, wx.ALL, 2)
        hklSizer.Add(hklhSizer, 0, wx.ALL| wx.EXPAND, 2)

        fwhmSizer.Add(label14, 0, wx.ALL, 2)
        fwhmSizer.Add(self.input14, 2, wx.ALL| wx.EXPAND, 2)

        ipfazSizer.Add(label15, 0, wx.ALL, 2)
        ipfazSizer.Add(self.input15, 2, wx.ALL| wx.EXPAND, 2)

        ipfnsSizer.Add(label16, 0, wx.ALL, 2)
        ipfnsSizer.Add(self.input16, 2, wx.ALL| wx.EXPAND, 2)

        btnSizer.Add(RunBtn, 0, wx.ALL, 5)
        btnSizer.Add(OpenBtn, 0, wx.ALL, 5)


        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt2Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(avgeszSizer, 0, wx.LEFT)
        MainSizer.Add(avgestrSizer, 0, wx.LEFT)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(hklSizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(fwhmSizer, 0, wx.LEFT)
        MainSizer.Add(ipfazSizer, 0, wx.LEFT)
        MainSizer.Add(ipfnsSizer, 0, wx.LEFT)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(btnSizer, 0, wx.ALL|wx.CENTER, 5)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def onRun(self, event):

        new_input8  = self.input8.GetValue()
        new_input9  = self.input9.GetValue()
        new_input11 = self.input11.GetValue()
        new_input12 = self.input12.GetValue()
        new_input13 = self.input13.GetValue()
        new_input14 = self.input14.GetValue()
        new_input15 = self.input15.GetValue()
        new_input16 = self.input16.GetValue()

        dto = data()

        dto.new_input8  =  new_input8
        dto.new_input9  =  new_input9
        dto.new_input11 =  new_input11
        dto.new_input12 =  new_input12
        dto.new_input13 =  new_input13
        dto.new_input14 =  new_input14
        dto.new_input15 =  new_input15
        dto.new_input16 =  new_input16


        print dto.new_input8
        print dto.new_input9
        print dto.new_input11
        print dto.new_input12
        print dto.new_input13
        print dto.new_input14
        print dto.new_input15
        print dto.new_input16

        write_cfl(dto)

        return dto


    def onOpen(self, event):

       dlg = wx.FileDialog(self, "Choose a file", os.getcwd(), "", "*.*", wx.OPEN)
       if dlg.ShowModal() == wx.ID_OK:
                path = dlg.GetPath()
                mypath = os.path.basename(path)
       dlg.Destroy()

       my_cfl_content = read_cfl_file(my_file_path = mypath)

       self.input8.SetValue(my_cfl_content.size_lg)
       self.input9.SetValue('none')
       self.input11.SetValue(my_cfl_content.h)
       self.input12.SetValue(my_cfl_content.k)
       self.input13.SetValue(my_cfl_content.l)
       self.input14.SetValue(my_cfl_content.hklwh)
       self.input15.SetValue(my_cfl_content.azm_ipf)
       self.input16.SetValue(my_cfl_content.ipf_res)


if __name__ == '__main__':
    app = wx.App()
    frame = MyForm().Show()
    app.MainLoop()
