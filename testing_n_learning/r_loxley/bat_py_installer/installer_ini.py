import subprocess
import sys

def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

if __name__ == "__main__":

    #tmp_off = '''
    install("PySide2")
    install("numpy")
    install("fabio")
    install("zipfile38")
    install("requests")
    #'''

    subprocess.call("python installer_gui.py")
