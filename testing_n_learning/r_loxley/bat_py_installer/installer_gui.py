import sys, os, requests, zipfile

from PySide2.QtWidgets import (
    QApplication, QLabel, QPushButton,
    QVBoxLayout, QWidget, QFileDialog
)

class MyWidget(QWidget):
    def __init__(self):
        QWidget.__init__(self)

        self.button = QPushButton("open dir install dialog")
        self.text = QLabel(
            " " * 80 + "."
            '''
            Welcome to Anaelu 2.0 Installer
            \n\n
              part 2 (installing core apps)
            \n
            '''
        )
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.button)
        self.setLayout(self.layout)
        self.button.clicked.connect(self.select_dir)

    def select_dir(self):
        print('os.expanduser =', os.path.expanduser(os.getenv('USERPROFILE')))
        self.dir_install_name = QFileDialog.getExistingDirectory(
            parent = self,
            caption = 'select install dir',
            dir = os.path.expanduser(os.getenv('USERPROFILE')),
            filter='*.*')

        print("dir install name =", self.dir_install_name,"\n")
        self.download_core_data()

    def download_core_data(self):
        url_raw = "https://gitlab.com/Vidal95/anaelu2/-/archive/v1.0/anaelu2-v1.0.zip"

        req = requests.get(url_raw, stream=True)
        r_cont = req.content

        path_4_zip = self.dir_install_name + "\code.zip"

        print("Starting 2 download  ...")
        with open(path_4_zip, 'wb') as fd:
            for chunk in req.iter_content(chunk_size=128):
                fd.write(chunk)

        print("...  done downloading")

        print("Starting 2 unzip  ...")
        with zipfile.ZipFile(path_4_zip, 'r') as zip_ref:
            zip_ref.extractall(self.dir_install_name)
        print("...  done unzipping ")


if __name__ == "__main__":
    app = QApplication(sys.argv)

    widget = MyWidget()
    widget.resize(120, 50)
    widget.show()

    sys.exit(app.exec_())

