import subprocess
import sys, os

def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])

def write_py_gui():
    source_file = [
        "# contained source code for installer's GUI",
        "import sys, os, requests, zipfile",
        "from PySide2.QtWidgets import QApplication, QFileDialog",
        "app = QApplication(sys.argv)",
        "dir_install_name = QFileDialog.getExistingDirectory(",
        "    caption = 'select install dir',filter='*.*')",
        "print('dir install name = ', dir_install_name)",
        "url_raw = 'https://gitlab.com/luisolafg/anaelu2-dist/-/archive/v0.99/anaelu2-dist-v0.99.zip'",
        "req = requests.get(url_raw, stream=True)",
        "r_cont = req.content",
        "path_4_zip = dir_install_name + os.sep + 'code.zip'",
        "print('Starting 2 download  ...')",
        "with open(path_4_zip, 'wb') as fd:",
        "    for chunk in req.iter_content(chunk_size=128):",
        "        fd.write(chunk)",
        "print('...  done downloading')",
        "print('Starting 2 unzip  ...')",
        "with zipfile.ZipFile(path_4_zip, 'r') as zip_ref:",
        "    zip_ref.extractall(dir_install_name)",
        "print('...  done unzipping ')",
        "os.remove(path_4_zip)",
        "bat_file = open(dir_install_name + os.sep + 'anaelu2.bat', 'w')",
        "bat_char = 'python ' + dir_install_name + os.sep + 'anaelu2-dist-v0.99' + os.sep +  'src' + os.sep +  'gui' + os.sep + 'ANAELU_Interface.py'",
        "bat_file.write(bat_char)",
        "bat_file.close()",
        "sys.exit()"
    ]
    try:
        temp_dir = os.getenv("HOMEDRIVE") + os.getenv("HOMEPATH")
        print("temp_dir =", temp_dir)
        tmp_pop_path = temp_dir + os.sep + "installer_gui.py"
        print("tmp_pop_path =", tmp_pop_path)

        f = open(tmp_pop_path, "w")
        for line_str in source_file:
            f.write(line_str + "\n")

        f.close()

        return tmp_pop_path

    except Exception as e:
        print("e:", e)


if __name__ == "__main__":
    legasy_url_raw = 'https://gitlab.com/luisolafg/anaelu2-dist/-/archive/v0.95/anaelu2-dist-v0.95.zip'

    print("\n\n WELCOME TO ANAELU 2.0 INSTALLER \n\n")
    print("installing dependencies with python 3.x where x >= 7")
    print("\n\n")
    print("if after installation Anaelu is capable to run ok")
    print("you may ignore the warning messages coming next")
    print("\n\n")
    print("Installing Deps ...")
    install("PySide2")
    install("numpy")
    install("fabio")
    install("zipfile37")
    install("requests")
    print(" ... Done Installing Deps")
    print("writing core code installer")

    pop_path = write_py_gui()

    a = input("\n\n press Enter to select installation dir for Anaelu main code")
    print("launching Anaelu 2.0 install dir selector ...")
    s = subprocess.Popen(["python", pop_path])
    s.wait()
    os.remove(pop_path)
    print(" ... Done Installing core code")
    a = input("\n\n press Enter to leave \n\n")
