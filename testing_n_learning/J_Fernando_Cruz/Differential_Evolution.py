from random import *
import numpy as np
import matplotlib.pyplot as plt
import time


def check(i,ind,popsize):
    a=randint(0, popsize-1)
    if a!=i and a not in ind:
        ind.append(a)
    if len(ind)==3:
        return ind
    else:
        check(i,ind,popsize-1)


def Differential_Evolution(bounds, population_size, number_evaluations):
    start = time.time()
    D = len(bounds)
    mutation_rate = 0.9
    F = 0.5
    v1 = np.empty((population_size, D))
    v2 = np.empty((population_size, D))

    for i in range(population_size):
        for j in range(D):
            v1[i][j] = uniform(bounds[j][0], bounds[j][1])


    plt.ion()
    fig = plt.figure()
    ax = fig.add_subplot(1,1,1)
    ax.grid(True)


    for eval in range(number_evaluations):
        for i in range(population_size):
            ind=[]
            check(i,ind,population_size)
            vector_rand = randint(0, population_size)

            for j in range(D):
                if (random() <= mutation_rate or j == vector_rand):
                    v2[i][j] = v1[ind[0]][j] + F * (v1[ind[1]][j] - v1[ind[2]][j])
                else:
                    v2[i][j] = v1[i][j]

        #
        for t in range(population_size):
            if function(v2[t][0], v2[t][1]) < function(v1[t][0], v1[t][1]):
                v1[t][0] = v2[t][0]
                v1[t][1]=v2[t][1]

        #

        line1 = ax.plot(v2[:, 0], v2[:, 1], 'r*')
        line2 = ax.plot(v1[:, 0], v1[:, 1], 'b*')
        ax.set_xlim(bounds[0][0],bounds[0][1])
        ax.set_ylim(bounds[1][0], bounds[1][1])
        fig.canvas.draw()
        ax.clear()
        ax.grid(True)
        print("Minimun near: ",v1[-1,0],v1[-1,1])
    end = time.time()
    elapsed = end - start
    print('It took {} seconds for the program to perform {} iterations'.format(elapsed,number_evaluations))



def function(x, y):
    return (x ** 2 +y ** 2) #You can change it for whathever function might like, however, consider it must be two dimensional, although the algorithm is aplicable to any number of dimensions, we can only graph up to 3
    #You cand add noise to the function by using the random function and see that the algorithm still works

Differential_Evolution([(-100, 100)] * 2, 40, 100) #This is an example where the algorith tries to find the global minima for -100<x<100 and -100<y<100 although this interval is considerably large it does not take to long for the algorith to reach a resonable value

  