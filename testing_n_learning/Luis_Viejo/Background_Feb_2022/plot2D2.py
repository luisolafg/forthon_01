
import numpy as np
from mayavi import mlab
z = np.loadtxt('polar_spec.pat')
mp = np.max(z)
z = 1000 * z / mp
x,y = np.mgrid[0:1800:1800j,0:1150:1150j]
mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(x,y,z)
mlab.scalarbar(orientation="vertical")
#mlab.show()

zz = np.loadtxt('smooth_spec.pat')
ms = np.max(zz)
zz = 1000 * zz / ms
xx,yy = np.mgrid[0:1800:1800j,0:1150:1150j]
mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(xx,yy,zz)
mlab.scalarbar(orientation="vertical")
mlab.show()
