
module inner_tools
    implicit none

    real(kind=8),  dimension(:,:), allocatable  :: img_out
    real(kind=8)                                :: r, r_2

    contains

    subroutine rotate_img(img_in)
        real(kind=8),  dimension(:,:), intent (in) :: img_in
        integer                     :: xmax, ymax, x, y

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        do x = 1, xmax
            do y = 1, ymax
                img_out(y, x) = img_in(xmax - x + 1, y)
            end do
        end do
    end subroutine rotate_img

    subroutine smooth_img(img_in, times_in)
        real(kind=8),    dimension(:,:), intent (in) :: img_in
        integer(kind=8),                 intent (in) :: times_in
        real(kind=8),    dimension(:,:), allocatable :: img_tmp, z, zcalc
        real(kind=8),   dimension(:), allocatable:: radio, alfa, zr, smo
        integer :: xmax,ymax,x,y,x_sc,y_sc,ex_flag,xmin,ymin,nalfa,ialfa,nradio,iradio,x0,y0,x1,y1,x2,y2,times
        !real(kind=8)    :: pi, fac,alfamax, dalfa, x0, y0
        real(kind=8)    :: pi, fac,alfamax, dalfa
        pi=4.d0*atan(1.d0); fac = pi/180.d0; x1 = 150
        alfamax = 180.d0; dalfa = 0.1d0; nalfa = int(alfamax/dalfa)
        times = times_in

        xmin = lbound(img_in,1)
        xmax = ubound(img_in,1)
        ymin = lbound(img_in,2)
        ymax = ubound(img_in,2)
        x0 = xmax/2; y0 = ymax/2

        write(*,*) "xmin, xmax =", xmin, xmax
        write(*,*) "ymin, ymax =", ymin, ymax
        write(*,'(A,2f8.4,I8)') "alfamax, dalfa, nalfa =",alfamax,dalfa,nalfa


        write(*,*) "Allocating arrays"
        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(xmax, ymax))
        if( allocated(z) )then
            deallocate(z)
        end if
        allocate(z(xmax, ymax))
        !write(*,*) "Allocating zcalc"
        if( allocated(zcalc) )then
            deallocate(zcalc)
        end if
        allocate(zcalc(xmax, ymax))
        !write(*,*) "Allocating radio"
        if( allocated(radio) )then
            deallocate(radio)
        end if
        allocate(radio(xmax))
        !write(*,*) "Allocating alfa"
        if( allocated(alfa) )then
            deallocate(alfa)
        end if
        allocate(alfa(xmax))
        !write(*,*) "Allocating zr"
        if( allocated(zr) )then
            deallocate(zr)
        end if
        allocate(zr(xmax))
        !write(*,*) "Allocating smo"
        if( allocated(smo) )then
            deallocate(smo)
        end if
        allocate(smo(xmax))
        !write(*,*) "Allocating img_tmp"
        if( allocated(img_tmp) )then
            deallocate(img_tmp)
        end if
        allocate(img_tmp(xmax, ymax))

        img_tmp = img_in
        img_out = img_in

        write(*,*) "# of iterations/polynomial degree =", times_in
!       Smoothing in cartesian coordinates LATER

        do x = xmin, xmax
            do y = ymin, ymax
                z(x, y) = 0.d0
            end do
        end do
        write (*,*) "Start transformation to polar coordinates"
        do ialfa = 1, nalfa + 1
            alfa(ialfa) = (ialfa-1) * dalfa
            do iradio = x1, x0
                radio(iradio) = iradio - 1
                x = x0 + int(radio(iradio)*cos(fac*alfa(ialfa)))
                y = y0 - int(radio(iradio)*sin(fac*alfa(ialfa)))
                z(iradio, ialfa) = img_in(x,y)
            end do
        end do
        zcalc = z
        write (*,*) "Final alfa, radius interval =", alfa(nalfa+1), radio(x1), radio(x0)
        write (*,*) "Transformation to polar coordinates accomplished"
        write(*,*)"Writing 'polar_spec.pat' with polar coordinates cutted spectrum"
        open(unit=3, status='replace',file = 'polar_spec.pat', action='write')
        do ialfa = 1, nalfa
            write(3, *), (z(iradio,ialfa), iradio = 1,y0)
        end do
        close(3)
        do ialfa = 1, nalfa
            do iradio = 1, y0
                zr(iradio) = z(iradio, ialfa)
            end do
            call fit_pol(y0, times_in, radio, zr, smo)
            do iradio = 1, y0
                zcalc(iradio, ialfa) = smo(iradio)
            end do
        end do
        write(*,*)"Writing 'smooth_spec.pat' with polar coordinates smoothed spectrum"
        open(unit=3, status='replace',file = 'smooth_spec.pat', action='write')
        do ialfa = 1, nalfa
            write(3, *), (zcalc(iradio,ialfa), iradio = 1,y0)
        end do
        close(3)

    end subroutine smooth_img

    SUBROUTINE resid(arr_1, arr_2)
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:) :: arr_1, arr_2
        REAL(KIND=8)        :: r_pics, sum_tot
        INTEGER(KIND=8)     :: i, j, xmax, ymax

        xmax = ubound(arr_1, 2)
        ymax = ubound(arr_1, 1)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        r = 0.0
        r_2 = 0.0
        sum_tot = 0.0

        Do i = 1, xmax
            Do j = 1, ymax
                if(arr_1(i, j) >= 0.0 .and. arr_2(i, j) >= 0.0 )then
                    sum_tot = sum_tot + arr_2(i, j)
                end if
            End Do
        End Do

        Do i = 1, xmax
            Do j = 1, ymax
                if(arr_1(i, j) >= 0.0 .and. arr_2(i, j) >= 0.0 )then
                    r_pics = abs(arr_1(i, j) - arr_2(i, j))
                    r = r + r_pics
                    r_2 = r_2 + r_pics * r_pics
                    img_out(i, j) = arr_1(i,j) - arr_2(i, j)
                end if
            End Do
        End Do

        r = 100.0 * (r / sum_tot)
        r_2 = 100.0 * (r_2 / sum_tot)

        WRITE(*,*) 'r =', r
        WRITE(*,*) 'r_2 =', r_2

    END SUBROUTINE resid

    SUBROUTINE multi(img_in, nb)
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:)    :: img_in
        REAL(KIND=8), INTENT(IN)                    :: nb
        INTEGER(KIND=8)                             :: i, j, xmax, ymax

        xmax = ubound(img_in, 2)
        ymax = ubound(img_in, 1)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        write(*,*) "nb =", nb
        img_out(:,:) = 0

        Do i = 1, xmax
            Do j = 1, ymax
                if(img_in(i, j) >= 0.0)then
                    img_out(i,j) = img_in(i,j) * nb
                else
                    img_out(i,j) = img_in(i,j)
                end if
            End Do
        End Do

        WRITE (*,*) 'done multiplying'

    END SUBROUTINE multi

    SUBROUTINE suma_img(arr_1, arr_2)
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:)    :: arr_1, arr_2
        INTEGER(KIND=8)                             :: i, j, xmax, ymax

        xmax = ubound(arr_1, 2)
        ymax = ubound(arr_1, 1)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        img_out(:,:) = 0

        Do i = 1, xmax
            Do j = 1, ymax
                if(arr_1(i, j) >= 0.0 .and. arr_2(i, j) >= 0.0 )then
                    img_out(i,j) = arr_1(i,j) + arr_2(i,j)
                else
                    if(arr_1(i,j) < arr_2(i,j))then
                        img_out(i,j) = arr_1(i,j)
                    else
                        img_out(i,j) = arr_2(i,j)
                    end if

                end if
            End Do
        End Do

        write(*,*) "two images added"

    END SUBROUTINE suma_img

    subroutine mark_mask(img_in, x1, x2, y1, y2, figure)
        real(kind=8),    dimension(:,:), intent (in)    :: img_in
        integer(kind=8)                , intent (in)    :: x1, x2, y1, y2
        character(len = *)             , intent (in)    :: figure

        integer(kind=8)                    :: xmax, ymax, x, y, x_mid, y_mid, tmp_xy
        real(kind=8)                       :: r_max, r_local, dx, dy, mx, my

        xmax = ubound(img_in, 1)
        ymax = ubound(img_in, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(xmax, ymax))

        write(*,*) " "
        write(*,*) "figure =", trim(figure)
        write(*,*) " "

        if(trim(figure) == 'rectangle' )then
            write(*,*) "marking/excuding a rectangle"
            write(*,*) "xmax, ymax =", xmax, ymax
            write(*,*) "x1, x2, y1, y2 =", x1, x2, y1, y2
            Do x = 1, xmax
                Do y = 1, ymax
                    if(x > x1 .and. x < x2 .and. y > y1 .and. y < y2)then
                        img_out(x, y) = -1.0
                    else
                        img_out(x, y) = img_in(x, y)
                    end if
                End Do
            End Do
        elseif( trim(figure) == 'circumference' )then
            write(*,*) "marking/excuding a circumference"
            dx = abs(x2 - x1)
            dy = abs(y2 - y1)
            r_max = sqrt(dx * dx + dy * dy)

            Do x = 1, xmax
                Do y = 1, ymax
                    dx = abs(x - x1)
                    dy = abs(y - y1)
                    r_local = sqrt(dx * dx + dy * dy)
                    if( r_local < r_max )then
                        img_out(x, y) = -1.0
                    else
                        img_out(x, y) = img_in(x, y)
                    end if
                End Do
            End Do

        elseif( trim(figure) == 'ellipse' )then
            write(*,*) "marking/excuding an ellipse"
            mx = abs(x2 - x1) / 2.0
            my = abs(y2 - y1) / 2.0

            r_max = sqrt(mx * mx + my * my) * 2.0
            mx = mx / r_max
            my = my / r_max

            x_mid = (x1 + x2) / 2.0
            y_mid = (y1 + y2) / 2.0

            Do x = 1, xmax
                Do y = 1, ymax
                    dx = abs(x - x_mid) / mx
                    dy = abs(y - y_mid) / my
                    r_local = sqrt(dx * dx + dy * dy)
                    if( r_local < r_max )then
                        img_out(x, y) = -1.0
                    else
                        img_out(x, y) = img_in(x, y)
                    end if
                End Do
            End Do
        end if

    end subroutine mark_mask

end module inner_tools
