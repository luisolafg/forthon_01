
import numpy as np
from mayavi import mlab
z = np.loadtxt('smooth_spec.pat')
m = np.max(z)
z = 1000 * z / m
x,y = np.mgrid[0:1800:1800j,0:1150:1150j]
mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(x,y,z)
mlab.scalarbar(orientation="vertical")
mlab.show()


