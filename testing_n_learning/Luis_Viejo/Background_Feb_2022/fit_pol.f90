SUBROUTINE fit_pol(n,m,x,y,z)
! A simple program to fit a polynomial in one variable.
! Data must be store in the form of pairs (x,y).
! Polynomial to be fitted:

! Y = a(0) + a(1).X + a(2).X^2 + ... + a(m).X^m

USE lsq
IMPLICIT NONE

CHARACTER (LEN=50)  :: fname
REAL (dp)           :: x(10000), y(10000), xrow(0:20), wt = 1.0_dp, beta(0:20), &
                       var, covmat(231), sterr(0:20), totalSS, z(10000)
INTEGER             :: i, ier, iostatus, j, m, n
LOGICAL             :: fit_const = .TRUE., lindep(0:20)

! Least-squares calculations

CALL startup(m, fit_const)
DO i = 1, n
  xrow(0) = 1.0_dp
  DO j = 1, m
    xrow(j) = x(i) * xrow(j-1)
  END DO
  CALL includ(wt, xrow, y(i))
END DO

CALL sing(lindep, ier)
IF (ier /= 0) THEN
  DO i = 0, m
    IF (lindep(i)) WRITE(*, '(a, i3)') ' Singularity detected for power: ', i
  END DO
END IF

! Calculate progressive residual sums of squares
CALL ss()
var = rss(m+1) / (n - m - 1)

! Calculate least-squares regn. coeffs.
CALL regcf(beta, m+1, ier)

! Calculate covariance matrix, and hence std. errors of coeffs.
CALL cov(m+1, var, covmat, 231, sterr, ier)

do i = 1, n
    z(i) = 0.d0
    do j = 0, m
        z(i) = z(i) + beta(j) * x(i)**(j)
    end do
end do
END SUBROUTINE fit_pol
