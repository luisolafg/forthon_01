
module inner_tools
    implicit none

    real(kind=8),  dimension(:,:), allocatable  :: img_out
    real(kind=8)                                :: r, r_2

    contains

    subroutine smooth_img(img_in, times_in)
        real(kind=8),    dimension(:,:), intent (in) :: img_in
        integer(kind=8),                 intent (in) :: times_in
        real(kind=8),    dimension(:,:), allocatable :: img_tmp

        integer                     :: xmax, ymax, x, y, times, x_sc, y_sc, ex_flag

        times = times_in

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)
        write(*,*) "Allocating img_out"
        write(*,*) "xmax =", xmax, "ymax =", ymax
        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(xmax, ymax))
        img_out=img_in
        img_out(1:400,1:800) = -5.0
        write(*,*) "Here"
!       write(*,*) "Allocating img_tmp"
!       if( allocated(img_tmp) )then
!           deallocate(img_tmp)
!       end if
!       allocate(img_tmp(xmax, ymax))
!       img_tmp = img_in
!       write(*,*) "# of iterations =", times_in
!       do times = 1, times_in
!
!           do x=2, xmax - 1
!                do y=2, ymax - 1
!                    ex_flag = 1
!                    do x_sc = x - 1, x + 1
!                        do y_sc = y - 1, y + 1
!                            if(img_tmp(x_sc, y_sc) < 0)then
!                                ex_flag = -1
!                            end if
!                       end do
!                   end do
!                   if(ex_flag == 1)then
!                       img_out(x, y) = (sum( &
!                                       img_tmp(x - 1: x + 1, y - 1: y + 1)) - &
!                                       img_tmp(x, y) ) / 8.0
!                   else
!                       img_out(x, y) = img_tmp(x, y)
!                   end if
!               end do
!           end do
!
!           img_tmp = img_out
!
!           write(*,*) "iteration:", times, "done"
!       end do

    end subroutine smooth_img

!   subroutine smooth_img(img_in, times_in)
!       real(kind=8),    dimension(:,:), intent (in) :: img_in
!       integer(kind=8),                 intent (in) :: times_in
!       real(kind=8),    dimension(:,:), allocatable :: img_tmp
!
!       integer                     :: xmax, ymax, x, y, times, x_sc, y_sc, ex_flag
!
!       times = times_in
!
!       xmax = ubound(img_in,1)
!       ymax = ubound(img_in,2)
!       write(*,*) "Allocating img_out"
!       if( allocated(img_out) )then
!           deallocate(img_out)
!       end if
!       allocate(img_out(xmax, ymax))
!
!       write(*,*) "Allocating img_tmp"
!       if( allocated(img_tmp) )then
!           deallocate(img_tmp)
!       end if
!       allocate(img_tmp(xmax, ymax))
!       img_tmp = img_in
!       write(*,*) "# of iterations =", times_in
!       do times = 1, times_in
!
!           do x=2, xmax - 1
!                do y=2, ymax - 1
!                    ex_flag = 1
!                    do x_sc = x - 1, x + 1
!                        do y_sc = y - 1, y + 1
!                            if(img_tmp(x_sc, y_sc) < 0)then
!                                ex_flag = -1
!                            end if
!                       end do
!                   end do
!                   if(ex_flag == 1)then
!                       img_out(x, y) = (sum( &
!                                       img_tmp(x - 1: x + 1, y - 1: y + 1)) - &
!                                       img_tmp(x, y) ) / 8.0
!                   else
!                       img_out(x, y) = img_tmp(x, y)
!                   end if
!               end do
!           end do
!
!           img_tmp = img_out
!
!           write(*,*) "iteration:", times, "done"
!       end do
!
!   end subroutine smooth_img
!

end module inner_tools

