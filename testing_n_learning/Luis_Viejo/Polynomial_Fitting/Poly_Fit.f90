Program poli2
real (kind = 8)   :: X(0:5000), Y(0:5000),A(0:10,0:10),GG(0:5000)
real (kind = 8)   :: YY
integer(kind = 4) :: I, IN, NORD, N, M, K, J, JJ, JEX
call system ('cls')
write (*,*)  ' '
write (*,*) ' Polynomial fitting '
write (*,*)  ' '

OPEN(unit=1, status='old',file = 'curva.dat', action='read')
IN = 1875                !IN = number of points
Do J=1,IN
    read(1,*) X(J), Y(J)
!   write(*,*) X(J), Y(J)
End Do
CLOSE(1)

write (*,'(A$)')'  Enter the polynomial degree: '
READ (*,*) NORD
M= NORD + 1
DO K = 1, M               !Matrix initialization
        DO J=1,M+1
                A(K,J)=0.d0
        END DO
END DO

DO K=1, M                 !Matrix for polynomial fitting
        DO I=1, IN
                DO J=1,M
                        JJ=K- 1+J- 1
                        YY=1.d0
                        IF (JJ /= 0) YY=X(I)**JJ
                        A(K,J)=A(K,J) + YY
                END DO
                JEX=K- 1
                YY=1.d0
                IF(JEX /= 0) YY=X(I)**JEX
                A(K,M+1)=A(K,M+1)+Y(I)*YY
        END DO
END DO

write(*,*)' '
write (*,*)' System matrix and vector: '
Write(*,*)' '
DO I=1,M
        write (*,'(12D12.3)') (A(I,J),J=1,M+1)
END DO

N=M

CALL GAUSS(M,A)

write (*,*) ' '
write (*,*) ' Expansion coefficients:'
write (*,*) ' '
write (*,*) ' Power     Coefficient'
write (*,*) ' -----------------------------'
DO I=1,M
        WRITE (*,'(I4,4X,F12.6)') I-1,A(I,M+1)
END DO

write (*,*) ' -----------------------------'
write (*,*) ' '
DO I=1,IN
        GG(I)=0.d0
        DO K=1,M
                GG(I)=GG(I)+A(K,M+1)*X(I)**(K-1)
        END DO
END DO

OPEN(unit=1, status='replace', file='curva_calc.dat', action='write')

do J = 1, IN
        write (1,'(3F10.2)') x(J), gg(J)
end do

write(*,*)' Curve extremes, obs and calc:'
write (*,*) x(1), y(1), GG(1)
write (*,*) x(IN), y(IN), GG(IN)
close(1)

STOP
END

SUBROUTINE GAUSS (N, A)
Real (kind = 8):: A(0:10,0:10), TM, VA, P
integer (kind = 4) :: I, IPV, J, N, JC, JR, NV
DO I = 1, N-1
        IPV=I
        DO J=I+1,N
                IF (DABS(A(IPV,I)) < DABS(A(J,I))) IPV=J
        END DO
        IF (IPV /= I) THEN
                DO JC=1,N+1
                        TM=A (I, JC)
                        A(I,JC)=A(IPV,JC)
                        A (IPV, JC) = TM
                END DO
        END IF
        DO JR=I+1,N
                IF (A(JR,I) /= 0.d0) THEN
                        IF (A(I,I) == 0.d0) GOTO 840
                        R=A(JR,I)/A(I, I)
                        DO KC=I+1,N+1
                                A(JR,KC) = A(JR,KC) - R*A(I,KC)
                        END DO
                END IF
        END DO
END DO

! - - SUSTITUCION HACIA ATRAS

IF (A(N,N) == 0.d0) GOTO 840
A(N,N+1) = A(N,N+1) / A(N,N)
DO NV=N-1,1,-1
        VA=A(NV, N+1)
        DO K=NV+1,N
                VA=VA-A(NV,K) * A(K,N+1)
        END DO
        A(NV,N+1) = VA/A(NV, NV)
END DO
RETURN
840 write (*,*) ' SINGULAR MATRIX'
RETURN
END
