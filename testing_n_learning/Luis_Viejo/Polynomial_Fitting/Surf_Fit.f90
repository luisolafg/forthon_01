Program Surf_Fit
real (kind = 8):: X(0:100), Y(0:100), A(0:10,0:10), GG(0:100), YY
real (kind = 8):: X1(0:100), Y1(0:100),zexp(0:100, 0:100), zcalc(0:100, 0:100)
integer(kind=4):: I, IN, i1, inx, j1, jny, NORD, N, M, K, J, JJ, JEX
call system ('cls')
write (*,*)  ' '
write (*,*) ' Surface fitting '
write (*,*) ' Experimental data:'

INX = 10; JNY = 10; IN = INX       !INX, INY = numbers of points in X and in Y
Do i = 0,INX
    X1(i) = dble(i); Y1(i) = dble(i)
    x(i)= x1(i)
!   write(*,*) X1(i), Y1(i)
End Do

OPEN(unit=1, status='old',file = 'gauss.dat', action='read')
do i1 = 0, inx
        read(1,*)(zexp(i1,j1), j1 = 0, jny)
        write (*, '(11f6.2)')(zexp(i1,j1), j1 = 0, jny)
end do
CLOSE(1)

write (*,'(A$)')'  Enter the polynomials degrees (<= 8): '
READ (*,*) NORD
M= NORD + 1

do i1 = 0, inx
  i = 0
  do j1 = 0, jny
    y(i)= zexp(i1, j1)
    i = i +1
  end do

  DO K = 1, M               !matrix initialization
        DO J=1,M+1
                A(K,J)=0.d0
        END DO
  END DO

  DO K=1, M               !Matrix for polynomial fitting
        DO I=0, IN
                DO J=1,M
                        JJ=K- 1+J- 1
                        YY=1.d0
                        IF (JJ /= 0) YY=X(I)**JJ
                        A(K,J)=A(K,J) + YY
                END DO
                JEX=K- 1
                YY=1.d0
                IF(JEX /= 0) YY=X(I)**JEX
                A(K,M+1)=A(K,M+1)+Y(I)*YY
        END DO
  END DO
  N=M

  CALL GAUSS(M,A)

  DO I=0,IN
        GG(I)=0.d0
        DO K=1,M
                GG(I)=GG(I)+A(K,M+1)*X(I)**(K-1)
        END DO
  END DO
  do j = 0, jny
        zcalc(i1,j)= gg(j)
  end do
end do

open(unit=1, status='replace', file='surf_calc.dat', action='write')
write(*,*)' Calculated surface:'
do i = 0, inx
       write(*, '(11f6.2)')(zcalc(i, j), j = 0, jny)
       write(1, '(11f6.2)')(zcalc(i, j), j = 0, jny)
end do
close (1)

STOP
END

SUBROUTINE GAUSS (N, A)
Real (kind = 8):: A(0:10,0:10), TM, VA, P
integer (kind = 4) :: I, IPV, J, N, JC, JR, NV
DO I = 1, N-1
        IPV=I
        DO J=I+1,N
                IF (DABS(A(IPV,I)) < DABS(A(J,I))) IPV=J
        END DO
        IF (IPV /= I) THEN
                DO JC=1,N+1
                        TM=A (I, JC)
                        A(I,JC)=A(IPV,JC)
                        A (IPV, JC) = TM
                END DO
        END IF
        DO JR=I+1,N
                IF (A(JR,I) /= 0.d0) THEN
                        IF (A(I,I) == 0.d0) GOTO 300
                        R=A(JR,I)/A(I, I)
                        DO KC=I+1,N+1
                                A(JR,KC) = A(JR,KC) - R*A(I,KC)
                        END DO
                END IF
        END DO
END DO
! - - backward substituion:
IF (A(N,N) == 0.d0) GOTO 300
A(N,N+1) = A(N,N+1) / A(N,N)
DO NV=N-1,1,-1
        VA=A(NV, N+1)
        DO K=NV+1,N
                VA=VA-A(NV,K) * A(K,N+1)
        END DO
        A(NV,N+1) = VA/A(NV, NV)
END DO
RETURN
300 write (*,*) ' SINGULAR MATRIX'
RETURN
END
