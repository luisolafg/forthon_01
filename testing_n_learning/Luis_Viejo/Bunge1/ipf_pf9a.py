print("Modeling pole figure (PF) from given inverse pole figure (IPF)")
import numpy as np
import scipy.integrate as sq
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D

# Defining parameters:
rtg = 180.0 / pi
thetadeg = 0.0; phideg = 0.00; widthdeg = 30.0
if thetadeg == 0.0: thetadeg = 0.01
theta = thetadeg/rtg; phi = phideg/rtg; distwidth = widthdeg/rtg

taudeg = [0.01, 45.0, 90.0]
gammadeg = [0.0, 30.0, 60.0]
PFtau=np.zeros(3); PFgamma=np.zeros(3)
for Npeak in range (0, 3):
    PFtau[Npeak] = taudeg[Npeak] * pi / 180.0
    PFgamma[Npeak] = gammadeg[Npeak] * pi / 180.0
    nombre = "PoleFig" + str(Npeak) + ".dat"
    print(nombre)

print (" Max of IPF at theta, phi (degrees) = ", thetadeg, phideg)
print (" Max of IPF at theta, phi (radians) = ", theta, phi)
print (" IPF width (degrees and radians) = ", widthdeg, distwidth)
#print (" Pole figure tau, gamma (degrees) = ", taudeg, gammadeg)
#print (" Pole figures tau, gamma (radians) = ", tau, gamma)

# Defining functions:
def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    if d >= 1.0: d = 1.0
    elif -1.0 < d < 1.0 : d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    elif d <= -1.0 : d = -1.0
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnorma(eta, chi):
    return (4.0*pi/R0)*R1(eta, chi)
def Rvar(psi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if B >= 1.0: B = 1.0
    elif -1.0 < B < 1.0 : B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    elif B <= -1.0 : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))

def pfg(fi):
    pfgr = sq.quad(Rvar, 0, 2.0*pi)
    return pfgr[0]

# Calculating R surface integral:
R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization:
Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)

# Maximum of normalized IPF, R:
print (" R_normalized(max) =", Rnorma(theta, phi)/sin(theta))
print ()

# Calculating the inverse pole figure surface:
eta, chi = np.meshgrid(np.linspace (0, pi, 36) , np.linspace(0, 2.0*pi,72))
#calculating R
p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
p1 = arccos(p)
R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))
#plotting inverse pole figure
fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface(eta*rtg, chi*rtg, R, cmap='viridis', linewidths=0.2)
ax.set_xlim(0, 360); ax.set_ylim(0, 360); ax.set_zlim(0, 11)
plt.show()

# Calculating and plotting the direct pole figure curve:

for Npeak in range(0, 3):
    file = open("polefig" + str(Npeak) + ".dat", "w")
    tau = PFtau[Npeak]; gamma = PFgamma[Npeak]
    fidg = np.zeros(180); pfg = np.zeros(180)
    fideg = 0.0
    for fideg in range(0, 181, 5):
        n = int(fideg/5)
        fidg[n] = fideg
        fi = fideg/rtg
        pf = sq.quad(Rvar, 0, 2.0*pi)
        pfg[n] = pf[0]/(2.0*pi)
        print (" n, fidg[n], Pole Figure[n] = ", n, fidg[n], pfg[n])
        file.write(str(fidg[n])+" "+ str(pfg[n])+"\n")
    plt.plot(fidg, pfg)
    plt.show()
    file.close()
