# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 13:20:09 2020

@author: alesk
"""
import numpy
import pandas
from numpy import sin,cos,pi,abs, sqrt, max # makes the code more readable
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy as sci
import scipy.special as sp
from matplotlib import pyplot as plt
import scipy.integrate as sq
import time

def pleg(polar):
   K1 = sqrt((2*l+1)/(4*pi) * sp.factorial(l-m)/sp.factorial(l+m))
   delkron = 0
   if m == 0: delkron = 1
   K = sqrt(2-delkron) * K1
   y =sp.lpmv(m,l,cos(polar))
   return K * y

print("\n Legendre polinomial expansion of a given curve")

# Reading curve
#data=pandas.read_csv('polefigure110.txt',header=1,delim_whitespace=True)
data=pandas.read_csv('polefig1.dat',header=1,delim_whitespace=True)
df=pandas.DataFrame(data)
x=data.iloc[:,0]
x1 = x * pi / 180.0
y1 = data.iloc[:,1]

y2 = sin(x1)

plt.plot(x1,y1,"b-")
plt.xlabel("polar angle (radians)")
plt.ylabel("observed pole figure")
plt.show()

lmax = int(input("\n Enter lmax: "))

# Generating matrix
x3=[]
for l in range(0, lmax+1):
    x3.append(l)
    j=numpy.linspace(0, l, l + 1)
    j=numpy.ndarray.tolist(j)
    x3[l]=j

# Calculating coefficients and curve expansion
YR = 0.0
for l in range(0, lmax + 1):
   for m in range(0, 1):
       p = sqrt(2.0*pi) * pleg(x1)
       npl = y1 * y2 * p
       r=sq.cumtrapz(npl,x1)
       x3[l][m] = r[-1]
       YR=x3[l][m] * p + YR
       print(" l, coef = ", l, "{:.6f}".format(x3[l][m]))
       plt.plot(x1,YR,"r-")
       plt.plot(x1,y1,"b-")
       plt.xlabel("polar angle (radians)")
       plt.ylabel("observed and calculated pole figures")
       plt.show()




