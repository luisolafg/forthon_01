# Legendre polinomial expansion of a given pole figure
import numpy
from numpy import sin,cos,pi,abs, sqrt, max # makes the code more readable
import matplotlib.pyplot as plt
import scipy.special as sp
import scipy.integrate as sq

def pleg(polar):
   K = sqrt((2*l+1)/2.0)
   y = sp.lpmv(0, l, cos(polar))
   return K * y

print("\n Legendre polinomial expansion of a given pole figure")

datafile = input("\n Enter data file: ")
x = numpy.loadtxt(datafile, usecols = [0])
x1 = x * pi / 180.0
y1 = numpy.loadtxt(datafile, usecols = [1])
y2 = sin(x1)

plt.plot(x1,y1,"b-")
plt.xlabel("polar angle (radians)")
plt.ylabel("observed pole figure")
plt.show()

lmax = int(input("\n Enter lmax: "))

# Calculating coefficients and curve expansion

m = 0; yr = 0.0; coef = numpy.zeros(50)
for l in range(0, lmax + 1):
    p = pleg(x1)
    npl = y1 * y2 * p
    r=sq.cumtrapz(npl,x1)
    coef[l] = r[-1]
    yr = coef[l] * p + yr
    print(" l, coef = ", l, "{:.6f}".format(coef[l]))
    plt.plot(x1,yr,"r-")
    plt.plot(x1,y1,"b-")
    plt.xlabel("polar angle (radians)")
    plt.ylabel("observed and calculated pole figures")
    plt.show()




