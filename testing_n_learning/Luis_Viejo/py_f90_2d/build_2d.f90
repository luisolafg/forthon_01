program build_2d
    implicit none

    integer                     :: i, j, log_uni
    real(kind=8), allocatable   :: data_out(:,:)
    allocate(data_out(6,4))

    do i = 1, 6, 1
        do j = 1, 4, 1
            data_out(i, j) = real(i + j * 2)
        end do
    end do

    write(*,*) data_out

    log_uni = 10
    open(unit = log_uni, file = "data_out.raw", status = "replace", access = "stream", form = "unformatted")
        write(unit = log_uni) data_out
    close(unit=log_uni)

end program build_2d
