import numpy as np
import matplotlib.pyplot as plt

import fabio
import os

if __name__ == "__main__":
    print("Fourier representation of a 2D periodic function")

    back_dir = ".." + os.sep
    img1_path = back_dir * 2 + "img_data" + os.sep \
    + "APT73_ddd_no_direct_beam.edf"

    print("Determining the initial function")

    z = fabio.open(img1_path).data.astype("float64")

    print("Plotting the initial function")
    #plt.imshow(z, interpolation = "nearest" )
    plt.imshow(z)
    plt.show()

    print("Calculating the Fast Fourier Transform")
    b = np.fft.rfft2(z, norm ='forward')
    #Fourier inverse transformation = Fourier synthesis
    zfr = np.fft.irfft2(b, norm='forward')
    #zfr = np.fft.irfft2(b[0:50,0:50],s =(2300, 2300), norm='forward')

    print("Plotting the recalculated function")

    #plt.imshow(zfr, interpolation = "nearest" )
    plt.imshow(zfr)
    plt.show()


