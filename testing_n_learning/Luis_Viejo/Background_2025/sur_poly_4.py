import numpy as np
import matplotlib.pyplot as plt
from numpy.polynomial.polynomial import Polynomial

# Crear datos de ejemplo
np.random.seed(42)
x = np.linspace(-5, 5, 50)
y = np.linspace(-5, 5, 50)
x, y = np.meshgrid(x, y)

# Generar una superficie polinomial de cuarto grado con algo de ruido
z = (1 + 2*x + 3*y + 0.5*x**2 + 0.3*x*y + 0.8*y**2 +
     0.1*x**3 + 0.05*x**2*y + 0.02*x*y**2 + 0.07*y**3 +
     0.01*x**4 + 0.005*x**3*y + 0.002*x**2*y**2 + 0.001*x*y**3 + 0.004*y**4)

z +=  np.random.normal(0, 0.5, z.shape)

# Aplanar los datos para el ajuste
x_flat = x.flatten()
y_flat = y.flatten()
z_flat = z.flatten()

# Crear la matriz de diseño para el polinomio de cuarto grado
A = np.column_stack([
    np.ones_like(x_flat),
    x_flat, y_flat,
    x_flat**2, x_flat*y_flat, y_flat**2,
    x_flat**3, x_flat**2*y_flat, x_flat*y_flat**2, y_flat**3,
    x_flat**4, x_flat**3*y_flat, x_flat**2*y_flat**2, x_flat*y_flat**3, y_flat**4
])

# Resolver el sistema de ecuaciones normales
coeff, _, _, _ = np.linalg.lstsq(A, z_flat, rcond=None)

# Reconstruir la superficie ajustada
z_fit = np.dot(A, coeff).reshape(x.shape)

# Visualización de los datos originales y la superficie ajustada
fig = plt.figure(figsize=(12, 6))
ax = fig.add_subplot(121, projection='3d')
ax.plot_surface(x, y, z, cmap='viridis', alpha=0.8)
ax.set_title('Datos Originales')

ax2 = fig.add_subplot(122, projection='3d')
ax2.plot_surface(x, y, z_fit, cmap='plasma', alpha=0.8)
ax2.set_title('Superficie Ajustada (Polinomio 4to Grado)')

plt.tight_layout()
plt.show()
