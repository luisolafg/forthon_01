program fopy2
    real(8) :: radio, polo, azi, zm, fw, d2, fw2
    real(8) :: x(0:100), y(0:200), z(0:100,0:200)
    open(unit=1, status='old',file = 'datos_in.dat', action='read')
    open(unit=2,status='replace',file = 'datos_out.dat', action='write')
    open(unit=3, status='replace', file='Gauss.dat', action='write')
    read (1,*) radio, polo, azi, zm, fw
    close(1)
    write(*,'(A,5F6.2)') " Data inside Fortran = ", radio, polo, azi, zm, fw
    write(2,'(5F6.2)')radio, polo, azi, zm, fw
    close(2)
    write (*,*) 'Modelling the surface with Fortran:'
    DO i = 0, 100               !Defining z
        x(i) = dble(i)
        DO j = 0, 200
                y(j) = dble(j)
                d2 = (x(i) - polo)**2 + (y(j) - azi)**2
                fw2 = fw**2
                z(i,j) = zm * dexp(-2.77258 * d2 / fw2)
        END DO
    END DO
    do i = 0, 100
        write (3,'(201F6.2)') (z(i,j), j = 0, 200)
        !write (*,'(11F6.2)') (z(i,j), j = 0, 200)
    end do
    close(3)
end program fopy2
