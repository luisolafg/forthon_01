import subprocess
import numpy as np
from mayavi import mlab
if __name__== "__main__":
    print("\n Generating a Gaussian surface by (some kind of) mixing python and fortran")
    polar = 45; azimuth = 60; zmax = 50; fwhm = 30
    print(" Python: polar, azimuth, zmax, fwhm = ", polar, azimuth, zmax, fwhm)
#    polar = float(input(" Enter the polar angle (degrees): "))
#    azimuth = float(input(" Enter the azimuth (degrees): "))
#    zmax = float(input(" Enter the Gaussian surface maximum: "))
#    fwhm = float(input(" Enter the Gaussian surface FWHM: "))
    datos = [polar, azimuth, zmax, fwhm]
    np.savetxt('datos_in.dat', datos)
    subprocess.call("fopy2.exe")
    print(" Ploting with python")
    z = np.loadtxt('Gauss.dat')
    x,y = np.mgrid[0:100:101j,0:200:201j]
    mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
    mlab.mesh(x,y,z)
    mlab.scalarbar(orientation="vertical")
    mlab.show()

