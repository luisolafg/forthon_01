import subprocess
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
if __name__== "__main__":
    print("\n Generating a Gaussian surface by (some kind of) mixing python and fortran")
    polar = 45; azimuth = 60; zmax = 50; fwhm = 30
    print(" Python: polar, azimuth, zmax, fwhm = ", polar, azimuth, zmax, fwhm)
#    polar = float(input(" Enter the polar angle (degrees): "))
#    azimuth = float(input(" Enter the azimuth (degrees): "))
#    zmax = float(input(" Enter the Gaussian surface maximum: "))
#    fwhm = float(input(" Enter the Gaussian surface FWHM: "))
    datos = [polar, azimuth, zmax, fwhm]
    np.savetxt('datos_in.dat', datos)
    subprocess.call("fopy3.exe")
    print(" Ploting with python")
    z = np.loadtxt('Gauss.dat')
    x,y = np.mgrid[0:100:101j,0:200:201j]
    fig= plt.figure()
    ax = Axes3D(fig)
    ax.plot_surface(x, y, z, cmap='cool')
#    ax.plot_surface(x, y, z)
#    ax.plot_surface(x, y, z, cmap='cool', linewidths=0.2)
    ax.set_xlim(0, 100); ax.set_ylim(0, 200); ax.set_zlim(0, zmax)
    plt.show()

