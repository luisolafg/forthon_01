program fopy5
    real:: z(0:100,0:200)
    open(unit=1, status='old',file = 'datos_in.dat', action='read')
    open(unit=2, status='replace', file='Gauss.dat', action='write')
    read (1,*) polo, azi, fw
    close(1)
    pi = 4.d0 * atan(1.d0); delta = pi / 100.d0; grad_rad = pi / 180.d0
    write(*,'(A,5F6.2)') " Data inside Fortran = ", polo, azi, fw
    write (*,*) 'Modelling the surface with Fortran'
    polo = polo * grad_rad; azi = azi * grad_rad; fw = fw * grad_rad
    do i = 0, 100
        x = real(i) * delta
        do j = 0, 200
            y = real(j) * delta
            p = cos(x)*cos(polo)+sin(x)*sin(polo)*cos(y-azi)
            p1 = acos(p)
            z(i, j) = exp(-2.7725887*(p1/fw)**2)
        end do
    end do
    write(*,*) "Fortran writing file 'Gauss.dat' "
    do i = 0, 100
        write (2,'(201F6.2)') (z(i,j), j = 0, 200)
    end do
    close(2)
end program fopy5
