import subprocess
import numpy as np
from numpy import sin, cos, pi
import mayavi
from mayavi import mlab
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
if __name__== "__main__":
    print("\n Generating a Gaussian surface by (some kind of) mixing of python and fortran")
    theta0deg = float(input(" theta0deg = "))
    phi0deg = float(input(" phi0deg = "))
    FWHMdeg = float(input(" FWHMdeg = "))
    #theta0deg = 45.0; phi0deg = 0.0; FWHMdeg = 30.0
    print(" Input data with Python: theta0deg, phi0deg, FWHMdeg = ", theta0deg, phi0deg, FWHMdeg)
    datos = [theta0deg, phi0deg, FWHMdeg]
    print(" Writing file 'datos_in.dat' with data to be used by a Fortran program")
    np.savetxt('datos_in.dat', datos)
    print (" Calling Fortran program")
    subprocess.call("fopy5.exe")
    print(" Python reading the output file from the Fortran program")
    r = np.loadtxt('Gauss.dat')
    print(" Preparing output graphics with Python")
    theta, phi = np.mgrid[0:pi:101j, 0:2.0*pi:201j]
    r = 3.0 + r
    x = r * sin(theta) * cos(phi)
    y = r * sin(theta) * sin(phi)
    z = r * cos(theta)

    # Surface plot with mayavi
    mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
    mlab.mesh(x, y, z, scalars = r)
    mlab.scalarbar(orientation="vertical")
    mlab.show()

    # Surface plot with matplotlib
    fig= plt.figure()
    ax = Axes3D(fig)
    ax.plot_surface(x, y, z, cmap='cool')
    plt.show()

