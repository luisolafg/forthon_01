lst = ["pedro", "juan", "ed"]

array = [(s + " ") * 3 for s in lst]
print(array)

array = []
for s in lst:
    array.append((s + " ") * 3)

print(array)
