# Gaussian surface
# Importing libraries:
import numpy as np
from numpy import sin, cos, pi, arccos, exp
import mayavi
from mayavi import mlab
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Defining Gaussian distribution parameters:
grad_rad = pi / 180.0
theta0deg = 45.0; phi0deg = 0.0; FWHMdeg = 30.0     # IPF parameters
if theta0deg == 0.0: theta0deg = 0.001
theta0 = theta0deg * grad_rad; phi0 = phi0deg * grad_rad
FWHM = FWHMdeg * grad_rad
print (" Max of IPF at theta_0, phi_0 (degrees) = ", theta0deg, phi0deg)
print (" Max of IPF at theta_0, phi_0 (radians) = ", theta0, phi0)
print (" IPF FWHM (degrees and radians) = ", FWHMdeg, "{:.4f}".format(FWHM))
# FWHM printed with selected format

Q = 3.0     # Radius of the reference sphere

theta, phi = np.mgrid[0:pi:101j, 0:2*pi:201j]

# Generating and Plotting the considered Gaussian:
p = cos(theta)*cos(theta0)+sin(theta)*sin(theta0)*cos(phi-phi0)
p1 = arccos(p)
R = Q + exp(-2.7725887*(p1/FWHM)**2)
x = R * sin(theta) * cos(phi)
y = R * sin(theta) * sin(phi)
z = R * cos(theta)

mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(x, y, z, scalars = R)
mlab.scalarbar(orientation="vertical")
mlab.show()

fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface(x, y, z, cmap='cool')
plt.show()
