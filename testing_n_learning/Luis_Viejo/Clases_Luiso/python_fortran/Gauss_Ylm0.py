# Gaussian via Spherical harmonics

# Importing libraries:
import numpy as np; import math
import matplotlib.pyplot as plt
import scipy as sci
import scipy.special as sp
import scipy.integrate as sq
from numpy import sin, cos, pi, abs, sqrt, arccos, exp, tan, max
from mayavi import mlab
from mpl_toolkits.mplot3d import Axes3D

# Defining Gaussian distribution parameters:
rtg = 180.0/math.pi
theta0deg = 90.0; phi0deg = 90.0; widthdeg = 30.0     # IPF parameters
if theta0deg == 0.0: theta0deg = 0.001
theta0 = theta0deg/rtg; phi0 = phi0deg/rtg; FWHM = widthdeg/rtg
         # https://es.qwe.wiki/wiki/Gaussian_function in terms of FWHM
p = 0.2  # Representation sphere ratio = R(max)/sphere radius
print (" Max of IPF at theta_0, phi_0 (degrees) = ", theta0deg, phi0deg)
print (" Max of IPF at theta_0, phi_0 (radians) = ", theta0, phi0)
print (" IPF FWHM (degrees and radians) = ", widthdeg, FWHM)

# Defining harmonics expansion parameters:
# l = degree, m = order
# lmax = int(input("lmax = "))
lmax = 10

# Defining functions
# Gaussian distribution:
def Gauss1(theta, phi):
    d = cos(theta)*cos(theta0)+sin(theta)*sin(theta0)*cos(phi-phi0)
    if d >= 1.0: d = 1.0
    elif -1.0 < d < 1.0 : d = cos(theta)*cos(theta0)+sin(theta)*sin(theta0)*cos(phi-phi0)
    elif d <= -1.0 : d = -1.0
    rho = arccos(d)
    return exp(-2.7725887*(rho/FWHM)**2)
def Gauss2(theta, phi):
    return Gauss1(theta, phi)*sin(theta)
def Gauss(theta, phi):
    return 4.0 * pi * Gauss1(theta, phi) / nor
def Gaussn(theta, phi):
    return Gauss(theta, phi) * sin(theta)

# Sperical harmonics:
def K(l, m):
    K1 = sqrt((2*l+1)/(4*pi) * sp.factorial(l-m)/sp.factorial(l+m))
    delkron = 0
    if m == 0: delkron = 1
    return sqrt(2-delkron) * K1
def yf(theta, phi):             # Y_l^m(theta, phi)
    y = sp.lpmv(m,l,cos(theta))*cos(m*phi)
    return K(l, m)*y
def finteg(theta, phi):         # Coefficients' integrand
    return yf(theta, phi)*(Q + Gauss(theta, phi)) * sin(theta)

# Calculating Gauss1 surface integral:
G1suma = sq.dblquad(Gauss2, 0, 2.0*pi, 0, pi)
nor = G1suma[0]
print (" Integral of initial IPF = ", nor)
# Calculating normalized Gauss surface integral:
G1nsuma = sq.dblquad(Gaussn, 0, 2.0*pi, 0, pi)
norn = G1nsuma[0]
Gmax = Gauss(theta0, phi0)
Q = Gmax/p
print (" Integral of normalized ditribution, Q = ", norn, Q)

# Defining spherical grid:
theta, phi = np.mgrid[0:pi:100j, 0:2*pi:200j]

#Generating and Plotting the considered Gaussian:
p = cos(theta)*cos(theta0)+sin(theta)*sin(theta0)*cos(phi-phi0)
p1 = arccos(p)
R = Q + (4.0*pi/nor)*exp(-2.7725887*(p1/FWHM)**2)
XR = R * sin(theta) * cos(phi)
YR = R * sin(theta) * sin(phi)
ZR = R * cos(theta)
mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(XR, YR, ZR, scalars=abs(R))
mlab.scalarbar(orientation="vertical")
mlab.show()

# Calculation of expansion coefficients:
print()
print(" Calculating coefficients and expanding function:")
print()
fc = np.zeros((20,20))
for l in range (0, lmax):
    for m in range (0, l+1):
        fs = sq.dblquad(finteg, 0, 2.0*pi, 0, pi)
        fc[l, m] = fs[0]
        #print(" l, m, coefficient = ", l, m, "{:.4f}".format(fc[l,m]))

#Generating and Plotting the Function Expansion:
s = 0
for l in range (0, lmax+1):
    for m in range (0, l+1):
        print(" l, m, coefficient = ", l, m, "{:.4f}".format(fc[l,m]))
        s = s + fc[l, m] * yf(theta, phi)
    print()
    X = s * sin(theta) * cos(phi)
    Y = s * sin(theta) * sin(phi)
    Z = s * cos(theta)
    mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
    mlab.mesh(X, Y, Z, scalars=abs(s))
    mlab.scalarbar(orientation="vertical")
    mlab.show()


