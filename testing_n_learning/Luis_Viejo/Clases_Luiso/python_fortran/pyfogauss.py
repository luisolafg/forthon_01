import subprocess
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
if __name__== "__main__":
    print("\n Generating a Gaussian surface by mixing python and fortran")
    x0 = input(" enter x0 (0 < x0 < 100): ")
    y0 = input(" enter y0 (0 < y0 < 100): ")
    zmax = input(" zmax = ")
    width = input(" width = ")
    print(" Input data inside Python: x0, y0, zmax, width = ", x0, y0, zmax, width)
    print(" Writing file 'datos_in.dat' with data to be used by a Fortran program")
    fil_out = open("datos_in.dat", "w")
    dat = x0 + " " + y0 + " " + zmax + " " + width
    fil_out.write(dat)
    fil_out.close()
    print (" Calling Fortran program")
    subprocess.call("fopygauss.exe")
    print(" Python reading the output file from the Fortran program")
    z = np.loadtxt('Gauss.dat')
    x, y = np.mgrid[0:100:101j, 0:100:101j]
    # Surface plot with matplotlib
    print(" Preparing output graphics with Python")
    fig = plt.figure()
    ax = Axes3D(fig)
    ax.plot_surface(x, y, z, cmap='jet')
    plt.show()
