# -*- coding: utf-8 -*-
class kulungele(object):
    #defining class kulungele
    def __init__(self, first_value):
        #first function to be called
        self.my_num = 0
        print "Hi"
        print "self.my_num(before) = ", self.my_num
        self.my_num = first_value
        print "self.my_num(after) = ", self.my_num

    def cuad(self):
        return self.my_num ** 2


if( __name__ == "__main__" ):
    # object a which is an instance of class "kulungele"
    a = kulungele(2)   # ( Se define el objeto “a” al instanciar la clase kulungele, pasándole el valor “2”).
    # object b which is an instance of kulungele
    b = kulungele(3)
    # Ahora corre la clase "cuad"
    print "cuad =", a.cuad()
    print "cuad =", b.cuad()
