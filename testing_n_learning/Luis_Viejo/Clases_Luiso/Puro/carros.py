class coche:
    '''Abstraccion de los objetos coche'''
    def __init__(self, gasolina):
        self.gasolina = gasolina
        print("Tenemos", gasolina, "litros")
    def arrancar(self):
        if self.gasolina > 0:
            print("Arranca")
        else:
            print("No arranca")
    def conducir(self):
        if self.gasolina > 0:
            self.gasolina -= 1
            print("Quedan", self.gasolina, "litros")
        else:
            print ("No se mueve")
mi_carro = coche(3)
print(mi_carro.gasolina)
mi_carro.arrancar()
mi_carro.conducir()
mi_carro.conducir()
mi_carro.conducir()



