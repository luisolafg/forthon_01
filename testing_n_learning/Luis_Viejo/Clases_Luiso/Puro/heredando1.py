class clase1():
    def __init__(self, x):
        self.n = float(x)
    def potencia(self):
        return(self.n)**2

class hijo(clase1):
    def potencia(self):
        return(self.n)**3


if __name__ == "__main__":
    print("\nPrograma de entrenamiento con clases y herencia # 1")
    a = input("\nEntre a: ")
    print("\nEn el prog ppal: a = ", a, "(una cadena)")
    obje1 = clase1(a)
    y = obje1.potencia()
    obje2 = hijo(a)
    z = obje2.potencia()
    print("\nResultado: ", y, z)
