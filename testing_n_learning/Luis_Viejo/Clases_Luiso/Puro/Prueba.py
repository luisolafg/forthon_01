from os import system
import numpy as np
class clase1(object):
    def prod(self):
        c = self.a * self.b
        return c
    def suma(self, a, b):
        c = a + b
        return c
    def resta(self, a, b):
        c = a - b
        return c
    def seno(self):
        c = np.sin(np.pi/180.0*self.b)
        return c

if __name__ == "__main__":
    system('cls')
    print("\n Programa de entrenamiento con clases y bobjetos")
    obje1 = clase1()
    u, v = 3, 4
    obje1.a = 5
    obje1.b = 30.0
    d = obje1.prod()
    e = obje1.suma(u, v)
    f = obje1.resta(u, v)
    g = obje1.seno()
    print("\n Los resultados son: ", d, e, f,"{:.4f}".format(g))
