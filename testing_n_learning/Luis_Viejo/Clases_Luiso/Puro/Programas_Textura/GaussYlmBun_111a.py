# Spherical harmonics expansion of a Gaussian distribution
print()
print(" Spherical harmonics expansion of a Gaussian distribution")
print()
print(" (during the run, please close figures to advance)")
print()

# Importing libraries:
import numpy as np; import math
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
from matplotlib import cm, colors
from mpl_toolkits.mplot3d import Axes3D
import scipy as sci
import scipy.special as sp
import scipy.integrate as sq
from numpy import sin, cos, pi, abs, sqrt, arccos, exp, tan, max
from mayavi import mlab

rtg = 180.0/math.pi; gtr = math.pi/180.0
# Defining single-Gaussian distribution parameters:
theta0deg =54.7; phi0deg = 45.0; widthdeg = 30.0
if theta0deg == 0.0: theta0deg = 0.01
theta0 = theta0deg *gtr; phi0 = phi0deg * gtr; FWHM = widthdeg * gtr
print (" Gaussian maximum at polar, azimuth (degrees) = ",  "{:.4f}".format(theta0deg), "{:.4f}".format(phi0deg))
#print (" Gaussian maximum at polar, azimuth (radians) = ",  "{:.4f}".format(theta0), "{:.4f}".format(phi0))
print (" Gaussian FWHM (degrees and radians) = ", "{:.4f}".format(widthdeg), "{:.4f}".format(FWHM))

#Defining IPF multiple maxima locations:
t0_ldeg = [54.7, 54.7, 54.7, 54.7,125.3, 125.3, 125.3, 125.3]
p0_ldeg = [45.0, 135.0,225.0, 315.0, 45.0, 135.0,225.0, 315.0]
lp = len(t0_ldeg)
print(" Number of Gaussian peaks in the IPF = ", lp)
t0_lst = [0.9546951, 0.9546951, 0.9546951, 0.9546951, 2.18689755, 2.18689755, 2.18689755, 2.18689755]
p0_lst = [0.78539816, 2.35619449, 3.92699082, 5.49778714, 0.78539816, 2.35619449, 3.92699082, 5.49778714]

# Considered direct pole figure:
taudeg = 54.7; gammadeg = 45.0
tau = taudeg * gtr; gamma = gammadeg * gtr

print (" pole figure at polar, azimuth (degrees) = ",  "{:.4f}".format(taudeg), "{:.4f}".format(gammadeg))
#print (" pole figure at polar, azimuth (radians) = ",  "{:.4f}".format(tau), "{:.4f}".format(gamma))

# Defining harmonics expansion parameters and representative sphere radius:
# l = degree, m = order
# lmax = int(input("lmax = "))

lmax = 12; Q = 10.0

# Defining functions
# Gaussian distribution:
# 2.7725887 = 4*log2 relates the FWHM to the Gaussian sigma.
# https://cxc.harvard.edu/sherpa4.6/ahelp/gauss2d.html

#Calculating superposition of Gaussian peaks:
def Gauss1(theta, phi):
    Gs1 = 0.0
    for np in range(0, lp):
        theta0 = t0_lst[np]
        phi0 = p0_lst[np]
        d = cos(theta)*cos(theta0)+sin(theta)*sin(theta0)*cos(phi-phi0)
        rho = arccos(d)
        Gs1 = Gs1 + exp(-2.7725887*(rho/FWHM)**2)
    return Gs1

def Gauss2(theta, phi):
    return Gauss1(theta, phi)*sin(theta)
def Gauss(theta, phi):
    return 4.0 * pi * Gauss1(theta, phi) / nor
def Gaussn(theta, phi):
    return Gauss(theta, phi) * sin(theta)
# Sperical harmonics:
def K(l, m):
    K1 = sqrt((2*l+1)/(4.0*pi) * sp.factorial(l-m)/sp.factorial(l+m))
    delkron = 0
    if m == 0: delkron = 1
    return sqrt(2-delkron) * K1
def yfc(theta, phi):             # Y_l^m(theta, phi)*cos
    y = sp.lpmv(m,l,cos(theta))*cos(m*phi)
    return K(l, m)*y
def yfs(theta, phi):             # Y_l^m(theta, phi)*sin
    y = sp.lpmv(m,l,cos(theta))*sin(m*phi)
    return K(l, m)*y
def fintegc(theta, phi):         # Coefficients' integrand
    return yfc(theta, phi)* Gauss(theta, phi) * sin(theta)
def fintegs(theta, phi):         # Coefficients' integrand
    return yfs(theta, phi)* Gauss(theta, phi) * sin(theta)

def yfcn(theta, phi):
    return yfc(theta, phi)*yfc(theta, phi) * sin(theta)
def yfsn(theta, phi):
    return yfs(theta, phi)*yfs(theta, phi) * sin(theta)

print()
#print(" Gaussian distribution")
print(" Radius of reference sphere, lmax = ", Q, lmax)
print(" Remember: close figures to advance")

# Modelling the IPF:
# Defining spherical grid:

zeta, fi = np.mgrid[0:pi:100j, 0:2*pi:200j]

# Calculating Gauss1 surface integral:
G1suma = sq.dblquad(Gauss2, 0, 2.0*pi, 0, pi)
nor = G1suma[0]
print (" Integral of initial IPF = ", nor)

# Calculating normalized Gauss surface integral:
G1nsuma = sq.dblquad(Gaussn, 0, 2.0*pi, 0, pi)
norn = G1nsuma[0]
Gmax = Gauss(theta0, phi0)
print (" Integral of the normalized ditribution = ", norn)
print (" Maximum of the normalized ditribution = ", Gmax)

R = Q + Gauss(zeta, fi)
x = R * sin(zeta) * cos(fi)
y = R * sin(zeta) * sin(fi)
z = R * cos(zeta)

#print(" R: ", R)
colorfunction= R
norm = colors.Normalize()
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x, y, z)
ax.plot_surface(x, y, z,  rstride=1, cstride=1 , facecolors=cm.jet(norm(colorfunction)))
val = 1.2198085261644226024779405283314
ax.set_xlim(-val*np.max(R), val*np.max(R)); ax.set_ylim(-val*np.max(R),val*np.max(R))
plt.show()

# Figura altenativa con Mayavi:
mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(x, y, z, scalars=abs(R))
mlab.scalarbar(orientation="vertical")
mlab.show()

# Calculation of coefficients and function expansion:
print(" Calculating coefficients and expanding function:")
print(" (it takes some time)")
file = open("coef.dat", "w")
fcc = np.zeros((25,25)); fcs = np.zeros((25,25))
s = 0.0; fl = np.zeros(25)
for l in range (0, lmax+1, 2):
    print("\n l = ", l)
    file.write("l = " + str(l) + "\n")
    for m in range (0, l+1, 4):
        fsc = sq.dblquad(fintegc, 0, 2.0*pi, 0, pi)
        fcc[l, m] = fsc[0]
        fss = sq.dblquad(fintegs, 0, 2.0*pi, 0, pi)
        fcs[l, m] = fss[0]
        fl[l] = fl[l] + fcc[l, m] * yfc(tau, gamma) + fcs[l, m] * yfs(tau, gamma)
        print(" m, coefficients = ", m, "{:.4f}".format(fcc[l,m]), "{:.4f}".format(fcs[l,m]))
        file.write("m, coef = " + str(m)+"  "+str(fcc[l,m]) + "\n")
        s = s + fcc[l, m] * yfc(zeta, fi) +  fcs[l, m] * yfs(zeta, fi)
    fl[l] = fl[l]* sqrt(2.0 /(2.0 * l + 1.0))
    print(" F_l(Bunge) = ",fl[l])
    file.write("F_l(Bunge) = " + str(fl[l]) + "\n" + "\n")
    X = (s + Q) * sin(zeta) * cos(fi)
    Y = (s + Q) * sin(zeta) * sin(fi)
    Z = (s + Q) * cos(zeta)
    mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
    mlab.mesh(X, Y, Z, scalars=abs(s+Q))
    mlab.scalarbar(orientation="vertical")
    mlab.show()
file.close()

