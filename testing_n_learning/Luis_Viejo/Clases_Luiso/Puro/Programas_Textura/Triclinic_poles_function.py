'''
Program TRICLIN for crystal geometry
Based on Young and Lytton: Journal of Applied Physics 43, 1408 (1972)
'''
import numpy as np
from numpy import sin,cos,pi,abs, arccos, exp, tan
import math
from os import(system)

def calculations(a, b, c, alpha, beta, gamma):
    np.set_printoptions(suppress=True,precision = 3)
    rtg = 180.0/math.pi
    alpha = alpha/rtg; beta = beta/rtg; gamma = gamma/rtg
    print("Program TRICLIN for reciprocal space calculations")
    print("\nCrystallographic basis:")
    print("a, b, c:", a, b, c)
    print("alpha, beta, gamma:", alpha* rtg, beta*rtg,gamma*rtg)
    #Matrix L characterizes the crystal basis as seen from a cartesian basis
    l33a = 1.0 + 2.0*cos(alpha)*cos(beta)*cos(gamma)
    l33b = cos(alpha)**2 + cos(beta)**2 + cos(gamma)**2
    l33c = l33a - l33b
    L = np.array([[a,b*cos(gamma),c*cos(beta)],
    [0,b*sin(gamma),c*(cos(alpha)-(cos(beta)*cos(gamma)))/sin(gamma)],
    [0,0, (c*np.sqrt(l33c))/sin(gamma)]])
    print("\nMatrix L:\n", np.array(L))

    Linv = np.linalg.inv(L)
    print("\nInverse Matrix Linv:\n", Linv)

    #Crystallographic direct basis vectors
    #("t" goes for general "triclinic" possible case)
    at = np.array([1,0,0])
    print("\nVector a in crystallographic: at = ",at)
    ac = L.dot(at)
    print("Vector a in cartesian: ac = ",ac)
    bt = np.array([0,1,0])
    print("Vector b in crystallographic: bt = ",bt)
    bc = L.dot(bt)
    print("Vector b in cartesian: bc = ",bc)
    ct = np.array([0,0,1])
    print("Vector c in crystallographic: ct =",ct)
    cc = L.dot(ct)
    print("Vector c in cartesian: cc = ",cc)

    #Volume of the direct unit cell
    a2a3 = np.cross(bc,cc)
    volu = np.dot(ac,a2a3)
    print("Direct cel volume = ","{:.4f}".format(volu))

    print("\nReciprocal basis vectors, referred to the cartesian basis")
    ar = a2a3 / volu
    print("ar = ", ar)
    br = np.cross(cc,ac) / volu
    print("br = ", br)
    cr = np.cross(ac,bc) / volu
    print("cr = ", cr)

    print("\nTransformation matrix for reciprocal vectors Q:")
    Q = np.array([ar, br, cr]).T
    print(Q)

    # HKL list of interest:
    list_hkl = [
        [1.0, 1.0, 1.0],
        [0.0, 0.0, 1.0],
        [1.0, 0.0, 0.0],
        [1.0, 1.0, 0.0],
        [2.0, 1.0, 0.0],
        [3.0, 2.0, 1.0]
    ]
    print("\nHKL list, shape:", list_hkl, np.shape(list_hkl))

    Vc_list=[]
    for i in list_hkl:
        Vc_list.append(Q.dot(i))

    Vc_list = np.array(Vc_list)
    print(
        "\nReciprocal vectors list (referred to cartesian coordinates), shape:\n",
        Vc_list,np.shape(Vc_list)
    )

    modVc_list=[]
    for i in Vc_list:
        modVc_list.append(np.sqrt(i[0]**2 + i[1]**2 + i[2]**2))

    modVc_list = np.array(modVc_list)
    lenVc = len(modVc_list)
    print("\nReciprocal vectors moduli, len:\n",modVc_list,lenVc)

    #print("\nVc_list[0]/modVc_list[0] =",Vc_list[0]/modVc_list[0])
    Vc_polar = np.zeros(lenVc)
    Vc_azim = np.zeros(lenVc)
    for k in range(0, lenVc):
        Vc_polar[k] = math.acos(Vc_list[k,2]/modVc_list[k])
        if Vc_polar[k] == 0:
            Vc_azim[k] = 0

        else:
            Vc_azim[k] = math.acos(
                (Vc_list[k,0]/modVc_list[k])/sin(Vc_polar[k])
            )

    return rtg, lenVc, list_hkl, Vc_polar, Vc_azim


def main():
    system('cls')
    #Definition of crystallographic basis
    #Study case: WO3
    a = 7.688 ; alpha = 90.0
    b = 7.539 ; beta  = 136.06
    c = 10.515 ; gamma = 90.0
    rtg, lenVc, list_hkl, Vc_polar, Vc_azim = calculations(
        a, b, c, alpha, beta, gamma
    )
    print("\n     Poles    angular coordinates")
    print("   [h, k, l]    polar azimuth")
    for k in range(0, lenVc):
        print(
            list_hkl[k],
            "{:.3f}".format(rtg * Vc_polar[k]),
            "{:.3f}".format(rtg * Vc_azim[k])
        )


if __name__ == "__main__":
    main()

