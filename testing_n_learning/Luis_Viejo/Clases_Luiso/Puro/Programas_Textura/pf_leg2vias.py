# Legendre polinomial expansion of a given pole figure
# Legendre polinomials calculated from two different library functions
import numpy
from numpy import sin,cos,pi,abs, sqrt, max # makes the code more readable
import matplotlib.pyplot as plt
import scipy.special as sp
from scipy.special import legendre
import scipy.integrate as sq

def pleg(polar):                  # Legendre polinomial from spherical harm Ylm
   K = sqrt((2*l+1)/2.0)
   y = sp.lpmv(0, l, cos(polar))
   return K * y

print("\n Legendre polinomial expansion of a given pole figure")

#datafile = input("\n Enter data file: ")
datafile = "polefig2.dat"
x = numpy.loadtxt(datafile, usecols = [0])
x1 = x * pi / 180.0
y1 = numpy.loadtxt(datafile, usecols = [1])
y2 = sin(x1)
"""
plt.plot(x1,y1,"b-")
plt.xlabel("polar angle (radians)")
plt.ylabel("observed pole figure")
plt.show()
"""
#lmax = int(input("\n Enter lmax: "))
lmax = 12
# Calculating and writing coefficients and curve expansion

file = open(datafile + ".coe", "w")
m = 0; yr = 0.0; coef = numpy.zeros(50)
for l in range(0, lmax + 1, 2):
    pn = legendre(l)                        # Legendre polinomial fron library
    p = sqrt((2.0*l+1.0)/2.0)*pn(cos(x1))   # Normailization
    pl = pleg(x1)                           # Legendre polinomial from Ylm
    #npl = y1 * y2 * pl
    npl = y1 * y2 * p
    r=sq.cumtrapz(npl,x1)
    coef[l] = r[-1]
    file.write(str(l) + " " + str(coef[l]) + "\n" )
    """
    veriying normalization
    u = p * p *y2
    uu=sq.cumtrapz(u, x1)
    un = uu[-1]
    print (" integral of pn = ", un)
    v = pl * pl *y2
    vv=sq.cumtrapz(v, x1)
    vn = vv[-1]
    print (" integral of pl = ", vn)
    """
    yr = coef[l] * pl + yr
    print(" l, coef = ", l, "{:.6f}".format(coef[l]))
    plt.plot(x1,yr,"r-")
    plt.plot(x1,y1,"b-")
    plt.plot(x1,p,"g-")
    plt.plot(x1,pl,"y-")
    plt.xlabel("polar angle (radians)")
    plt.ylabel("observed and calculated pole figures")
    plt.show()
file.close()



