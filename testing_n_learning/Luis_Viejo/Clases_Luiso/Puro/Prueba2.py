from os import system
import numpy as np
class clase1(object):
    def prod(self):
        c = self.a * self.b
        return c
    def suma(self, a, b):
        c = a + b
        return c
    def llama_suma(self, x = 2, y = 3):
        z = self.suma(x, y)
        return z

if __name__ == "__main__":
    system('cls')
    print("\n Programa de entrenamiento con clases y bobjetos")
    obje1 = clase1()
    u, v = 3, 4
    x, y = 5, 6
    obje1.a = 5
    obje1.b = 30.0
    d = obje1.prod()
    e = obje1.suma(u, v)
    z1 = obje1.llama_suma(10, 5)
    z2 = obje1.llama_suma(x, y)
    z3 = obje1.llama_suma()
    print("\n Los resultados son: ", d, e, z1, z2,z3)
