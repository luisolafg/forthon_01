from os import system
import numpy as np
class clase1(object):
    def __init__(self,ab):
        self.a = float(ab.split(",")[0])
        self.b = float(ab.split(",")[1])
    def prod(self):
        c = self.a * self.b
        return c

if __name__ == "__main__":
    system('cls')
    print("\nPrograma de entrenamiento con clases y objetos # 6")
    ab = input("\nEntre a, b: ")
    print("\nEn el prog ppal: a, b = ", ab, "(una cadena)")
    obje1 = clase1(ab)
    d = obje1.prod()
    print("\nResultado: ", d)
