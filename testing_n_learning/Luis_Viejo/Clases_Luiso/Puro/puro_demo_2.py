class my_obj_func(object):
    def __init__(self, a, b):
        '''
        The __init__ function (or method) is called
        when the class is instantiated as a new object
        '''
        self.c = a + b
        '''
        self is a needed reference to the class / object,
        it is available and persistent across the entire class / object

        now we save the value of << c >> and it is available
        anywhere within the class
        '''
    def __call__(self):
        '''
        The __call__ function (or method) is called every time an object of type "my_obj_func" is called, for example in this file
        when in the main code is used as obj_trabajo()
        '''
        self.c = 2 * self.c
        return self.c
    def imprime(self):
        print("imprimiendo")

if __name__ == "__main__":
    u, v = 4, 5
    # instantiating the class my_obj_func as a new object called obj_trabajo
    obj_trabajo = my_obj_func(u, v)
    # calling the << __call__ >> method
    w = obj_trabajo()
    obj_trabajo.imprime()
    print("2 * ...c = ", w)
    print("obj_trabajo.c =", obj_trabajo.c)
