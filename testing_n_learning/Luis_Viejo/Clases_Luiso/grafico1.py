import numpy as np
import pylab as pl
x = np.loadtxt('fe3o4.dat', usecols = [0])
y = np.loadtxt('fe3o4.dat', usecols = [1])
pl.figure()
pl.plot(x, y)
pl.xlabel('dispersion angle')
pl.ylabel('intensity')
pl.title('XRD pattern')
pl.show()
