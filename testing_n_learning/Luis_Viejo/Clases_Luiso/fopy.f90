program fopy
    implicit none
    character(len = 80) :: texto
    real(8) :: x, y
    OPEN(unit=1, status='old',file = 'datos_in.dat', action='read')
    open(unit=2,status='replace',file = 'datos_out.dat', action='write')
    read (1,'(A)')texto
    read (1,*) x
    y = 2.0 * x
    write(*,'(A,2F6.2)') " x, 2*x desde Fortran = ", x, y
    write(2, '(2F6.2)')  x, y
    close(1)
    close(2)
end program fopy
