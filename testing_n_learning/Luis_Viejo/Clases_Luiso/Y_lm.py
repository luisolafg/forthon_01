#Spherical harmonics
import numpy as np
from numpy import sin,cos,abs,max,real,pi # makes the code more readable
from mayavi import mlab
import scipy.special as sp

l = int(input(" l = "))
m = int(input(" m = "))

phi, theta = np.mgrid[0:2*pi:200j, 0:pi:100j]    # phi = azimuth, theta = polar angle

R = real(sp.sph_harm(m, l, phi, theta))
# Example of alternatives: R = sp.sph_harm(m, l, phi, theta).imag

Rm = max(R)
print(" Ylm(max) =", Rm)
Q = float(input(" Radius of the reference sphere = "))
R = Q + R

X = abs(R) * sin(theta) * cos(phi)
Y = abs(R) * sin(theta) * sin(phi)
Z = abs(R) * cos(theta)

mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(X, Y, Z, scalars = R - Q)
mlab.scalarbar(orientation="vertical")
mlab.show()
