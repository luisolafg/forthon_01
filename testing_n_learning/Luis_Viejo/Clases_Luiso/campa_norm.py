# Normalized Gaussian surface
import numpy as np
from numpy import sin,cos,pi,abs, exp   # makes the code more readable
from mayavi import mlab
import scipy.integrate as sq

# Defining functions:
def R1(x, y):   # x = polar angle, y = azimuth
    d2 = (x-pi/2)**2 + (y-pi)**2
    return exp(-d2)
def R1sin(x, y):
    return R1(x, y)*sin(x)

# Calculating the surfaceintegral:
R1suma = sq.dblquad(R1sin, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print ("\n Integral of initial IPF, R0 = ", R0)

# Generating a normalized Gaussian surface:
x, y = np.mgrid[0:pi:19j,0:2*pi:37j]
z = 4.0 * pi * R1(x, y) / R0
mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(x,y,z)
mlab.scalarbar(orientation="vertical")
mlab.show()

