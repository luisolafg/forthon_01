if __name__ == "__main__":
    fil_in = open("cell.1.pcbm-tb-d03-pos-1.xyz", "r")
    file_str_lines = fil_in.readlines()
    fil_in.close()
    max_i = 0
    for str_lin in file_str_lines:
         if "i = " in str_lin:
            max_i += 1

    num_i = 0
    for n_lin, str_lin in enumerate(file_str_lines):
        if "i = " in str_lin:
            num_i += 1
            if num_i == max_i:
                new_file_lst=file_str_lines[n_lin - 1:]

    fil_out = open("opt_cells.xyz","w")
    for str_lin in new_file_lst:
        fil_out.write(str_lin)
    fil_out.close()
