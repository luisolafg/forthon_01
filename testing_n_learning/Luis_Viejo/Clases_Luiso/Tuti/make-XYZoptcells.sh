#!/bin/bash

NATOMS=464
INI=1
FIN=100
PASO=1
i=$INI

#ciclo para las cells
while [ $i -le $FIN ]; do

#chequeo de convergencia en opt
#  if [[ "$i" == '2' ]]; then
#    break
#  fi

#ciclo dentro de una cell, incluir el nombre del fichero xyz salida de CP2K
grep "i =" cell.$i.pcbm-tb-d03-pos-1.xyz > a
#wc -l < a > b
nopt=`awk 'END{print NR}' a`

grep -A $NATOMS -e " $nopt," cell.$i.pcbm-tb-d03-pos-1.xyz > a$i
grep -v -e "i =" a$i > coord$i
awk '{if(NR==1) print $0}' a$i > b
paste <(echo "CELL: $i,") b > c

cat <(echo "$NATOMS") c coord$i > cell-$i-d03-opt.xyz
rm a b c a$i coord$i

#grep "COMPLETED" relax-vdw-bfgsXY-dftb.cell.$i.out > check
#paste <(echo "$i") check >> check-list
#min=`awk 'BEGIN{a=1000}{if ($1<0+a) a=$1} END{print a}' mydata.dat`
#max=`awk 'BEGIN{a=   0}{if ($1>0+a) a=$1} END{print a}' mydata.dat`

#agrupar de estructuras optimizadas 
cat cell-$i-d03-opt.xyz >> cells-pcbm-tb-d03-opt.xyz

   i=$[i+${PASO}]

grep "i =" cells-pcbm-tb-d03-opt.xyz > energies
#awk '{printf "%10.7f %2s %10.7f\n", $8, "", $8*27.2113}' e > energies
awk '{printf "%10.7f\n", $8*27.2113}' energies > energies-ev

done

exit

