# Gaussian surface
import numpy as np
from numpy import sin,cos,pi,abs, exp   # makes the code more readable
from mayavi import mlab
a = 2.0
x, y = np.mgrid[0:pi:19j,0:2*pi:37j]
d2 = (x-pi/2)**2 + (y-pi)**2
z = a * exp(-d2)
mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(x,y,z)
mlab.scalarbar(orientation="vertical")
mlab.show()
