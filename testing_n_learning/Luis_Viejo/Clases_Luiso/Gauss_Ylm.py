# Spherical harmonics expansion of a Gaussian distribution
print()
print(" Spherical harmonics expansion of a Gaussian distribution")
print()
print(" (during the run, please close figures to advance)")
print()

# Importing libraries:
import numpy as np; import math
import matplotlib.pyplot as plt
import scipy as sci
import scipy.special as sp
import scipy.integrate as sq
from numpy import sin, cos, pi, abs, sqrt, arccos, exp, tan, max
from mayavi import mlab
from mpl_toolkits.mplot3d import Axes3D

# Defining Gaussian distribution parameters:
theta0deg = 90.0; phi0deg = 90.0; widthdeg = 30.0
if theta0deg == 0.0: theta0deg = 0.001
rtg = 180.0/math.pi
theta0 = theta0deg/rtg; phi0 = phi0deg/rtg; FWHM = widthdeg/rtg
rat = 0.2  # Representation sphere ratio = R(max)/sphere radius

print (" Gaussian maximum at polar, azimuth (degrees) = ",  "{:.4f}".format(theta0deg), "{:.4f}".format(phi0deg))
print (" Gaussian maximum at polar, azimuth (radians) = ",  "{:.4f}".format(theta0), "{:.4f}".format(phi0))
print (" Gaussian FWHM (degrees and radians) = ", "{:.4f}".format(widthdeg), "{:.4f}".format(FWHM))

# Defining harmonics expansion parameters:
# l = degree, m = order
# lmax = int(input("lmax = "))
lmax = 10

# Defining functions
# Gaussian distribution:
def Gauss1(theta, phi):
    d = cos(theta)*cos(theta0)+sin(theta)*sin(theta0)*cos(phi-phi0)
    if d >= 1.0: d = 1.0
    elif -1.0 < d < 1.0 : d = cos(theta)*cos(theta0)+sin(theta)*sin(theta0)*cos(phi-phi0)
    elif d <= -1.0 : d = -1.0
    rho = arccos(d)
    return exp(-2.7725887*(rho/FWHM)**2)
def Gauss2(theta, phi):
    return Gauss1(theta, phi)*sin(theta)
def Gauss(theta, phi):
    return 4.0 * pi * Gauss1(theta, phi) / nor
def Gaussn(theta, phi):
    return Gauss(theta, phi) * sin(theta)
# Sperical harmonics:
def K(l, m):
    K1 = sqrt((2*l+1)/(4*pi) * sp.factorial(l-m)/sp.factorial(l+m))
    delkron = 0
    if m == 0: delkron = 1
    return sqrt(2-delkron) * K1
def yfc(theta, phi):             # Y_l^m(theta, phi)
    y = sp.lpmv(m,l,cos(theta))*cos(m*phi)
    return K(l, m)*y
def yfs(theta, phi):             # Y_l^m(theta, phi)
    y = sp.lpmv(m,l,cos(theta))*sin(m*phi)
    return K(l, m)*y
def fintegc(theta, phi):         # Coefficients' integrand
    return yfc(theta, phi)*(Q + Gauss(theta, phi)) * sin(theta)
def fintegs(theta, phi):         # Coefficients' integrand
    return yfs(theta, phi)*(Q + Gauss(theta, phi)) * sin(theta)

print()
print(" Gaussian distribution")
print(" (remember: close figures to advance)")
# Calculating Gauss1 surface integral:
G1suma = sq.dblquad(Gauss2, 0, 2.0*pi, 0, pi)
nor = G1suma[0]
#print (" Integral of initial IPF = ", nor)
# Calculating normalized Gauss surface integral:
G1nsuma = sq.dblquad(Gaussn, 0, 2.0*pi, 0, pi)
norn = G1nsuma[0]
Gmax = Gauss(theta0, phi0)
Q = Gmax/rat
#print (" Integral of normalized ditribution, Q = ", norn, Q)

# Defining spherical grid:
theta, phi = np.mgrid[0:pi:100j, 0:2*pi:200j]

# Generating and Plotting the considered Gaussian:
# https://es.qwe.wiki/wiki/Gaussian_function in terms of FWHM
p = cos(theta)*cos(theta0)+sin(theta)*sin(theta0)*cos(phi-phi0)
p1 = arccos(p)
R = Q + (4.0*pi/nor)*exp(-2.7725887*(p1/FWHM)**2)
XR = R * sin(theta) * cos(phi)
YR = R * sin(theta) * sin(phi)
ZR = R * cos(theta)
mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
mlab.mesh(XR, YR, ZR, scalars=abs(R))
mlab.scalarbar(orientation="vertical")
mlab.show()

# Calculation of expansion coefficients:
print()
print(" Calculating coefficients and expanding function:")
print(" (it takes some time)")
print()
fcc = np.zeros((20,20)); fcs = np.zeros((20,20))
for l in range (0, lmax):
    for m in range (0, l+1):
        fsc = sq.dblquad(fintegc, 0, 2.0*pi, 0, pi)
        fcc[l, m] = fsc[0]
        fss = sq.dblquad(fintegs, 0, 2.0*pi, 0, pi)
        fcs[l, m] = fss[0]


#Generating and Plotting the Function Expansion:
s = 0
for l in range (0, lmax+1):
    for m in range (0, l+1):
        print(" l, |m|, coefficients = ", l, m, "{:.4f}".format(fcc[l,m]), "{:.4f}".format(fcs[l,m]) )
        s = s + fcc[l, m] * yfc(theta, phi) +  fcs[l, m] * yfs(theta, phi)
    print()
    X = s * sin(theta) * cos(phi)
    Y = s * sin(theta) * sin(phi)
    Z = s * cos(theta)
    mlab.figure(bgcolor=(1, 1, 1), fgcolor=(0, 0, 0))
    mlab.mesh(X, Y, Z, scalars=abs(s))
    mlab.scalarbar(orientation="vertical")
    mlab.show()


