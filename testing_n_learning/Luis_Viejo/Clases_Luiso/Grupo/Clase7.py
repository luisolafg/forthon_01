import numpy as np
def disc(a,b,c):
    d = b**2 - 4*a*c
    return d

# Ejemplo 1:
a = 1.0
b = 4.0
c = 7.0/4.0
# Ejemplo 2:
#a = 1.0
#b = 4.0
#c = 4.0
# Ejemplo 3:
#a = 1.0
#b = 4.0
#c = 5.0

d = disc(a,b,c)
print("disc =", d)

if d > 0:
    y1 = (-b + np.sqrt(d)) / (2.0*a)
    y2 = (-b - np.sqrt(d)) / (2.0*a)
    print("y1 =", y1)
    print("y2 =", y2)
elif d == 0:
    y = (-b) / (2.0*a)
    print("y = ", y)
else:
    print("no real solution")
