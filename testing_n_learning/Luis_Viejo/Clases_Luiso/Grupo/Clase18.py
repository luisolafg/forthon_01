
if __name__== "__main__":
    source_file = ["a = 5", "b = 6", "esto es una cadena de caracteres"]
    f = open("ejemplo.txt", "w")
    for line_str in source_file:
        f.write(line_str + "\n")
    f.close()

    fil = open('ejemplo.txt', "r")
    a = None
    while True:
        a = fil.readline()
        if a == '':
            break
        print(a[0:-1])  # -1 es el LF
    fil.close()


