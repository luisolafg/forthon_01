class a:
    '''
        clase ejemplo
    '''
    #x = 8  # will be overwritten

    def __init__(self, x_in):
        self._z = x_in
        print("beginning")

    def imprime(self):
        print("z =", self._z)


if __name__== "__main__":
    b = a(5)
    print(b._z)  #bad practice. it's supposed to be local
    b.imprime()
