if __name__ == "__main__":
    a = {"juan": 1, 6.28: 5}
    print("dic(a) =", a)
    print("numero de juan =",a["juan"])
    print("numero de 6.28 =",a[6.28])
    a["pablo"]=7
    print("dic(a) =",a)
    a["lista"] = ["a", "b", "c"]
    print("dic(a) =",a)
    print("a['lista'][1] = ", a["lista"][1])
