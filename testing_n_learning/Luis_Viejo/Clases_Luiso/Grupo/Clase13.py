class client():
    def __init__(self, first_deposit, client_number):
        self.saldo = first_deposit
        self.number = client_number

    def deposit(self, amount):
        self.saldo += amount

if __name__== "__main__":
    tuti = client(50, 5)
    puro = client(500, 2)
    luija = client(1000, 3)
    print("tuti tiene ", tuti.saldo, " pesos")
    print("puro tiene ", puro.saldo, " pesos")
    print("luija tiene ", luija.saldo, " pesos")
        # la variable tuti es un objeto de tipo client,
        # que es una instancia o un objeto de la clase client que estamos
        # instanciando
    print("tuti es el cliente # ", tuti.number)
    print("tuti es el cliente # ", puro.number)
    print("tuti es el cliente # ", luija.number)

    tuti.deposit(100)
    print("\n tuti tiene ", tuti.saldo, " pesos")
    print(" tuti tiene ", tuti.saldo, " pesos")

    tuti.deposit(10)
    print("\n tuti tiene ", tuti.saldo, " pesos")
    print(" tuti tiene ", tuti.saldo, " pesos")

