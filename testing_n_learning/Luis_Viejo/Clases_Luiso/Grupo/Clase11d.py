class a:
    '''
        clase ejemplo
    '''
    #x = 5  # will be overwritten

    def __init__(self, x_in):
        self._z = x_in
        print("beginning")

    def __call__(self):
        print("each time call")
        return self._z

    def imprime(self):
        print("z =", self._z)


if __name__== "__main__":
    b = a(5)
    print(b._z)  #bad practice. it's supposed to be local
    b.imprime()
    z_out = b()
    print("z_out = ", z_out)
    b()
    b()

    #print(dir(b))

