class client():
    def __init__(self, first_deposit):
        self.saldo = first_deposit

if __name__== "__main__":
    tuti = client(50)
    puro = client(500)
    luija = client(1000)
    print("tuti tiene ", tuti.saldo, " pesos")
    print("puro tiene ", puro.saldo, " pesos")
    print("luija tiene ", luija.saldo, " pesos")
        # la variable tuti es un objeto de tipo client,
        # que es una instancia o un objeto de la clase client que estamos
        # instanciando

