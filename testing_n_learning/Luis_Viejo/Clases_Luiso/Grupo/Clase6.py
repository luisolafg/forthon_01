#a = [1,2,3,4]
#print(a)
#for e in [1,2,3,4]:
    #print (e ** 2)

#a = range(0, 8, 2)
#print(a)
#for e in a:
#    print (e * 2)

l = ["pedro", "juan", "rafael"]
for i, e in enumerate(l):
    print(i, e)
print()
for e in l:
    print(e)

array = [(s + " ")*3 for s in l]
print(array)
############ Viene lo mismo:
array = []
for s in l:
    array.append((s + " ")*3)
print(array)


