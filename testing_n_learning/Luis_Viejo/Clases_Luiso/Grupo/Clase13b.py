class client():
    def __init__(self, first_deposit, client_number):
        self.saldo = first_deposit
        self.number = client_number

    def deposit(self, amount):
        self.saldo += amount

    def withdraw(self, amount):
        if amount > self.saldo:
            print("no puedes retirar ", amount)
        else:
            self.saldo -= amount

class employee(client):
    def __init__(self, first_deposit, client_number, employee_number):
        self.saldo = first_deposit
        self.number = client_number
        self.employee = employee_number

# Las palabras que son de python son class, self, init, main, def, ...
if __name__== "__main__":
    tuti = client(50, 5)
    puro = client(500, 2)
    print("tuti tiene ", tuti.saldo, " pesos")
    print("puro tiene ", puro.saldo, " pesos")
        # la variable tuti es un objeto de tipo client,
        # que es una instancia o un objeto de la clase client que estamos
        # instanciando
    print("tuti es el cliente # ", tuti.number)
    print("puro es el cliente # ", puro.number)
    tuti.deposit(100)
    print("\n tuti tiene ", tuti.saldo, " pesos")
    print(" tuti tiene ", tuti.saldo, " pesos")
    puro.withdraw(20)
    print("\n puro tiene ", puro.saldo, " pesos")

    juan = employee(500, 3, 1)
    print("\n juan tiene ", juan.saldo, " pesos")
    juan.withdraw(50)
    print("\n juan tiene ahora", juan.saldo, " pesos")
