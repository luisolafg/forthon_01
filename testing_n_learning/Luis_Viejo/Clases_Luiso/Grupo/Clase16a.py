
if __name__== "__main__":
    source_file = ["a = 5", "esto es una cadena de caracteres"]
    f = open("ejemplo.txt", "w")
    for line_str in source_file:
        f.write(line_str + "\n")
    f.close()
# Variante 1:
#    with open('ejemplo.txt') as file_in:
#       for lin in file_in:
#           print("lin =", lin)
# Variante 2:
    fil = open('ejemplo.txt', "r")
    for lin in fil:
           print(lin)


