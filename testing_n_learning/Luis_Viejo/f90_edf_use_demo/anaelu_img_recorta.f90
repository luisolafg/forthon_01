
program img_scale

    use input_arg_tools,                only: handling_kargs, arg_def_list
    use edf_utils,                      only: get_efd_info, write_edf
    use inner_tools,                    only: recorta, img_out

    implicit none

    character(len=9999)                             :: str_data
    real(kind = 8), dimension(:,:), allocatable     :: img_in
    real(kind = 8)                                  :: scale_no

    Type(arg_def_list)              :: my_def

    my_def%num_of_vars = 3
    allocate(my_def%field(1:my_def%num_of_vars))
    my_def%field(1)%var_name = "img_in"
    my_def%field(1)%value = "my_img_in.edf"

    my_def%field(2)%var_name = "img_out"
    my_def%field(2)%value = "scaled_img.edf"

    my_def%field(3)%var_name = "scale"
    my_def%field(3)%value = "0.5"

    call handling_kargs(my_def)

    write(*,*) "img_in: ", my_def%field(1)%value
    write(*,*) "img_out: ", my_def%field(2)%value
    write(*,*) "scale: ", my_def%field(3)%value

    read(my_def%field(3)%value,*) scale_no
    write(*,*) "scale_no: " , scale_no

    call get_efd_info(my_def%field(1)%value, str_data, img_in)
    write(*,*) "header: ", trim(str_data)


    call recorta(img_in, scale_no)
    call write_edf(my_def%field(2)%value, img_out)


end program img_scale
