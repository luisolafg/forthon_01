echo removing previous binrs, objs and imgs
del /f anaelu_img_recorta scaled_img.edf
del /f anaelu_img_recorta.exe

echo compiling
gfortran -c -O2 -Wall input_args.f90
gfortran -c -O2 -Wall rw_n_use_edf.f90
gfortran -c -O2 -Wall tools.f90

gfortran -c -O2 -Wall anaelu_img_recorta.f90

echo linking
gfortran -o anaelu_img_recorta.exe *.o -static

echo running binary exe
anaelu_img_recorta.exe img_64bit_xrd.edf scaled_img.edf 0.55

del /f *.o *.mod
