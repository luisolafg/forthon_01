module edf_utils
    use iso_fortran_env
    implicit none
    contains

    subroutine write_edf(file_out, img_out)
        character(len = *) ,    intent(in)                      :: file_out
        real(kind = 8), dimension(:,:), allocatable, intent(in) :: img_out
        integer                                                 :: log_uni, dim1, dim2

        dim1 = ubound(img_out, 1)
        dim2 = ubound(img_out, 2)

        log_uni = 15
        OPEN(unit = log_uni, file = trim(adjustl(file_out)), status = "replace", form = "formatted")

        write(unit = log_uni, fmt = '(A)') "{"
        write(unit = log_uni, fmt = '(A)') "HeaderID = EH:000001:000000:000000;"
        write(unit = log_uni, fmt = '(A)') "Image = 1 ;"
        write(unit = log_uni, fmt = '(A)') "ByteOrder = LowByteFirst ;"
        write(unit = log_uni, fmt = '(A)') "DataType = DoubleValue ;"
        write(unit = log_uni, fmt = 1010 ) dim1
        write(unit = log_uni, fmt = 1011 ) dim2
        write(unit = log_uni, fmt = 1012 ) 8 * dim1 * dim2
        write(unit = log_uni, fmt = '(A)') "Title = EDF file by ANAELU;"
        write(unit = log_uni, fmt = '(A)') "}"

1010        format('Dim_1 = ',I8,' ;')
1011        format('Dim_2 = ',I8,' ;')
1012        format('Size = ',I8,' ;')

        close(unit = log_uni)
        OPEN(unit = log_uni, file = trim(file_out), position = "append", access = "stream", &
         form = "unformatted", convert="LITTLE_ENDIAN")

        write(unit = log_uni) img_out

        close(unit = log_uni)
        write(unit=*,advance = 'yes',fmt = '(A)') char(13)
        write(unit=*,fmt = "(a)") " Result image in File: "//file_out

    end subroutine write_edf


    subroutine get_efd_info(file_in, header_data, img_in)
        character(len = *) ,    intent(in)                          :: file_in
        character(len = 9999), intent(out)                          :: header_data
        real(kind = 8), dimension(:,:), allocatable, intent(out)    :: img_in
        integer(kind = 1)                                           :: byte_read
        character(len = 1)                                          :: local_char
        character(len = 555)                    :: single_line, l_side
        character(len = 555)                    :: r_side_tr, r_side
        integer                                 :: log_uni = 12, h_end = -1
        integer                                 :: lin_ini = -1, eq_pos = -1
        integer                                 :: i, j, longi, dim1, dim2

        write(*,*) "reading file:", trim(file_in)

        open(unit=log_uni, file=trim(file_in), status="old",action="read", position="rewind", access="stream", form="unformatted")

        header_data=''
        single_line = ''
        do i=1,9999
            read( unit=log_uni, end=999 ) byte_read
            local_char = char(byte_read)
            header_data(i:i) = local_char
            if( local_char == '}' )then
                read( unit=log_uni, end=999 ) byte_read
                h_end = i
                exit
            elseif( local_char == '{')then
                lin_ini = i + 2
            elseif( local_char == ';' )then
                single_line = header_data(lin_ini:i)
                longi = len(trim(single_line))
                do j = 1, longi
                    if( single_line(j:j) == "=" )then
                        eq_pos = j
                    end if
                end do
                l_side = single_line(1:eq_pos - 1)
                r_side = single_line(eq_pos + 1:longi)
                lin_ini = i + 2

                if( trim(l_side) == 'Dim_1' )then
                    read(unit = r_side, fmt = *) dim1
                    write(*,*) "dim1 =", dim1
                end if

                if( trim(l_side) == 'Dim_2' )then
                    read(unit = r_side, fmt = *) dim2
                    write(*,*) "dim2 =", dim2
                end if

                if( trim(l_side) == 'DataType' )then
                    r_side_tr = r_side(1:len(trim(r_side)) - 1)

                    write(*,*) "adjustl(trim(r_side_tr)) =", adjustl(trim(r_side_tr)), "="
                    write(*,*) "trim(adjustl(r_side_tr)) =", trim(adjustl(r_side_tr)), "="

                    if( trim(adjustl(r_side_tr)) == 'DoubleValue' )then
                        write(*,*) "real(kind=8)"
                    else
                        write(*,*) "r_side(...) =", r_side_tr, "="
                        write(*,*) "not supported format"
                        eq_pos = -1
                        h_end = -1
                        exit
                    end if
                end if

            end if
        end do

        if( h_end == -1 .or. lin_ini == -1 .or. eq_pos == -1 )then
            write(*,*) "error"
            stop
        end if

        write(*,*) "allocating 2d array"
        allocate(img_in(dim1, dim2))
        write(*,*) "reading 2d array"
        read( unit=log_uni, end=999 ) img_in
        write(*,*) "Done"

999     write(unit=*,fmt=*) 'end of file'
        if( h_end == -1 .or. lin_ini == -1 .or. eq_pos == -1 )then
            write(*,*) "error"
            stop
        end if
        close(unit=log_uni)

    end subroutine get_efd_info

end module edf_utils

subroutine com_arg(file_in, num1, num2, num3, num4, file_out)
    character(len = 254),       intent(out)     :: file_in
    integer,                    intent(out)     :: num1, num2, num3, num4
    character(len = 254),       intent(out)     :: file_out

    character(len = 254),  dimension(1:254)      :: arg
    integer                                     :: narg

    narg = COMMAND_ARGUMENT_COUNT()

    if(narg /= 3)then
        write(*,*) "error #1"
        write(*,*) "wrong arguments "
        write(*,*) "example: "
        write(*,*) "img_marker img_in.edf 2,4,6,7 img_out.edf"

        stop
    else
        do i = 1, narg, 1
            call GET_COMMAND_ARGUMENT(i, arg(i))
        end do
    end if
    file_in = trim(arg(1))

    read(unit = arg(2), fmt = *) num1, num2, num3, num4
    write(*,*) "num1, num2, num3, num4 =", num1, num2, num3, num4

    file_out = trim(arg(3))

end subroutine com_arg
