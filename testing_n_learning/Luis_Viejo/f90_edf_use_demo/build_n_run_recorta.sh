echo "removing previous binrs, objs and imgs"
rm anaelu_img_recorta scaled_img.edf
rm anaelu_img_recorta.exe

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall tools.f90

gfortran -c -Wall anaelu_img_recorta.f90

echo "linking"
gfortran -o anaelu_img_recorta.exe *.o -static

echo "running binary exe"
./anaelu_img_recorta.exe ../../img_data/APT73_ddd_no_direct_beam.edf scaled_img.edf 0.55

rm *.o *.mod
