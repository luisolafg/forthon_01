
module inner_tools
    implicit none

    real(kind=8),  dimension(:,:), allocatable  :: img_out
    real(kind=8)                                :: r, r_2

    contains


    SUBROUTINE recorta(img_in, nb)
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:)    :: img_in
        REAL(KIND=8), INTENT(IN)                    :: nb
        INTEGER(KIND=8)                             :: i, j, xmax, ymax

        xmax = ubound(img_in, 1)
        ymax = ubound(img_in, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        write(*,*) "nb =", nb

        img_out(:,:) = img_in(:,:)

        Do i = 1, xmax / 2
            Do j = 1, ymax / 2
                img_out(i,j) = 0
            End Do
        End Do

        !img_out(:,:) = img_in(:,:)*nb

        WRITE (*,*) 'done multiplying'

    END SUBROUTINE recorta



end module inner_tools
