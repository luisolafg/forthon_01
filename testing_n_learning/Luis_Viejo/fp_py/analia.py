import os
os.system('cls')
import numpy as np
from numpy import  max, min # makes the code more readable
from math import ceil
from statistics import mean
import matplotlib.pyplot as plt
import subprocess, shutil

print("\n   Program for XRD analysis of sediments")
print("\n   First part: Estimating the Degree of Crystallinity (D.o.C.)")
# Initial parameters and arrays
nmuestra = 23
muestra = np.zeros(nmuestra+1)
montmo = np.zeros(nmuestra+1)
crist = np.zeros(nmuestra+1)
pctm = np.zeros(nmuestra+1)
angbac = [4.75, 9.55, 12.0, 14.4, 15.5, 16.0, 18.3, 31.8, 33.8, 44.3, 52.8, 59.0]
numbac = len(angbac)                          # Number of slected background points
i = np.zeros(numbac, dtype = np.int32)        # Index of slected background points in ybac array
ybac0 = np.zeros(numbac)                      # Background value in selected points
slope = np.zeros(numbac)                      # Background solpe in i[ik] interval

# Principal loop, scaning all samples
for caso in range(1,nmuestra+1):
    muestra[caso] = caso
    data_file = "M" + str(caso)+ ".dat"
    x = np.loadtxt(data_file, usecols = [0])
    yobs = np.loadtxt(data_file, usecols = [1])
    numdat = len(x)                           # Number of points in XRD pattern
    # Interpoling background, pivoting on points averaged around selected ones
    ybac = np.zeros(numdat)
    for ik in range(numbac):
        i[ik] = round(min(np.where(x >= angbac[ik])))
        ybac0[ik] = mean(yobs[i[ik]-5:i[ik]+5])
    slope[0] =  (ybac0[2] - ybac0[0]) / (i[2] - i[0])
    for jk in range(i[0], i[2]):
        ybac[jk] = ybac0[0] + slope[0] * (jk - i[0])
    for ik in range(2, numbac-1):
        slope[ik] = (ybac0[ik + 1] - ybac0[ik]) / (i[ik+1] - i[ik])
        for jk in range(i[ik], i[ik+1]):
            ybac[jk] = ybac0[ik] + slope[ik] * (jk - i[ik])
    # Saving bakground pivoting points for Fullprof
    bgrfile = open(data_file[0:-4] + ".bgr", "w")
    bgrfile.write(str(numbac) + "\n" )
    for n in range(numbac):
        bgrfile.write(str('%8.4f' % angbac[n]) + "  " + str('%8.4f' % ybac0[n])+ "\n" )
    bgrfile.close()
    # Calculating %s of considered phases
    print ("\n   Sample:", data_file[0:-4])
    montmo[caso] = sum(yobs[i[0]:i[1]]) - sum(ybac[i[0]:i[1]])
    crist[caso] =  sum(yobs[i[5]:i[9]]) - sum(ybac[i[5]:i[9]])
    #print (" crist:  ", "{:8.3e}".format(crist[caso]))
    pctm[caso] = 100.0 * montmo[caso] / (montmo[caso] + crist[caso])
    print(" % montmo = ", round(pctm[caso],2))
    '''
    # Plotting XRD pattern and interpolated background
    plt.title('Sample '+data_file[0:-4]+' interpolated background', fontweight ="bold")
    plt.plot(x,yobs, 'b')
    plt.plot(x,ybac, 'r')
    plt.xlabel('2 theta (degrees)')
    plt.ylabel('Intensity (counts)')
    plt.plot([angbac[0],angbac[0]],[0.0,yobs[i[0]]],color ='white',linewidth = 2)
    plt.plot([angbac[0],angbac[0]],[0.0,yobs[i[0]]],color ='red',linestyle ='--')
    plt.plot([angbac[1],angbac[1]],[0.0,yobs[i[1]]],color ='red',linestyle ='--')
    plt.plot([angbac[5],angbac[5]],[0.0,yobs[i[5]]],color ='blue',linestyle ='--')
    plt.plot([angbac[9],angbac[9]],[0.0,yobs[i[9]]],color ='blue',linestyle ='--')
    plt.plot([0,x[numdat-1]],[0.0,0.0],color ='black',linewidth = 0.5)
    plt.savefig(data_file[0:-4] + ".jpg")
    plt.show()
    '''
# Final plotting and writing
plt.title('Degree of crystallinity - estimate',fontweight ="bold")
plt.plot(muestra[1:],pctm[1:],'r')
plt.ylim(0,ceil(max(pctm)))
plt.xlabel("sample")
plt.ylabel("% montmorillonite")
plt.grid(True)
plt.savefig("montmo_areas.jpg")
plt.show()
outfile = open("montmo_areas.out", "w")
for caso in range(1,nmuestra + 1):
    outfile.write(
        str('%8.3f' % pctm[caso]) + "\n" )
outfile.close()

print("\n Second part: >Running Fullprof \n")

lst_2_run = ["fp2k", "multest.pcr"]
lst_2_run[0] = str(shutil.which(lst_2_run[0]))

print("\n Running >> ", lst_2_run)

try:
    my_proc = subprocess.call(
        lst_2_run,
        shell = False,
    )

except FileNotFoundError:
    print(
        "   *** ERROR ***"
        " "
        " FullProf  NOT properly installed"
        " "
    )
    exit(1)


print("\n Third part: Summarizing and output \n")
nmuestras = 23
muestra = np.zeros(nmuestras+1); montmo = np.zeros(nmuestras+1)
montmo1 = np.zeros(nmuestras+1); montmo2 = np.zeros(nmuestras+1)
montmo3 = np.zeros(nmuestras+1); dst = np.zeros(nmuestras+1)
qua = np.zeros(nmuestras+1); calci = np.zeros(nmuestras+1)
magne = np.zeros(nmuestras+1); anor1 = np.zeros(nmuestras+1)
anor2 = np.zeros(nmuestras+1); anor = np.zeros(nmuestras+1)
sani = np.zeros(nmuestras+1); kao = np.zeros(nmuestras+1)
dstA = np.zeros(nmuestras+1)
#num = input("\n Enter files' number: ")
#fiche = "multi" + num + ".sum"
fiche = "multest.sum"
file = open(fiche, "r")
print("\n",fiche)
info = file.readlines()
nlin = len(info)

y = np.zeros(nmuestras+1)
for k in range(nmuestras+1):
    y[k] = pctm[k]
texto = "BRAGG R-Factors and weight fractions for Pattern #  1"
for lin in range(nlin):
    if(texto in info[lin]):
        im = lin + 4
#print(" Las concentraciones comienzan a partir de la  línea ",im)
for i in range(1, nmuestras + 1):
    muestra[i] = i
    print(" \n Pattern: ",i)
    quar = info[im]
    print(" quarzo =    ", quar[66:-1])
    qua[i] = float(quar[66:-8])#Agregada
    calc = info[im + 4]
    print(" calcita =   ", calc[66:-1])
    calci[i] = float(calc[66:-8])
    magnet = info[im + 8]
    print(" magnetita = ", magnet[66:-1])
    magne[i] = float(magnet[66:-8])
    mon1 = info[im + 12]
    print(" montmo1 =   ", mon1[66:-1])
    montmo1[i] = float(mon1[66:-8])
    dst1 = float(mon1[74:-2])
    mon2 = info[im+16]
    print(" montmo2 =   ", mon2[66:-1])
    montmo2[i] = float(mon2[66:-8])
    dst2 = float(mon2[74:-2])
    mon3 = info[im + 20]
    print(" montmo3 =   ", mon3[66:-1])
    montmo3[i] = float(mon3[66:-8])
    dst3 = float(mon3[74:-2])
    montmo[i] = montmo1[i] + montmo2[i] + montmo3[i]
    dst[i]=np.sqrt(dst1*dst1+dst2*dst2+dst3*dst3)
    print(" Montmo =      ", round(montmo[i],2), "(",round(dst[i],2),")")
    anortita1 = info[im + 24]
    print(" anor1 =     ", anortita1[66:-1])
    anor1[i] = float(anortita1[66:-8])
    dstA4 = float(anortita1[74:-2])
    anortita2 = info[im + 28]
    print(" anor2 =     ", anortita2[66:-1])
    anor2[i] = float(anortita2[66:-8])
    dstA5 = float(anortita2[74:-2])
    anor[i] = anor1[i] +  anor2[i]
    dstA[i]=np.sqrt(dstA4*dstA4+dstA5*dstA5)
    print(" Anortite =    ", round(anor[i],2), "(",round(dstA[i],2),")")
    caolinita = info[im + 32]
    print(" kao =       ", caolinita[66:-1])
    kao[i] = float(caolinita[66:-8])
    sanidina = info[im + 36]
    print(" sani =      ", sanidina[66:-1])
    sani[i] = float(sanidina[66:-8])
    im = im + 36 + 8  # VAMOS AL INICIO DE LA SIGUIENTE MUESTRA
#Plotting and writting:
plt.plot(muestra[1:],montmo[1:],'r')
plt.plot(muestra[1:],y[1:],'b')
plt.title('Clays presence in investigated samples',fontweight ="bold")
plt.ylim(0,np.ceil(max(montmo)))
plt.grid(True)
plt.xlabel("pattern")
plt.ylabel("% montmorillonite")
plt.text(17.0,12.0,'Rietveld',fontsize= 14, color='red')
plt.text(17.0,7.0,'D.o.C',fontsize= 14, color='blue')
plt.savefig("multest" + ".jpg")
plt.show()

quamin = np.min(qua[1:]); quamax = np.max(qua[1:]); lqua = qua.tolist()
minqua = lqua.index(quamin); maxqua = lqua.index(quamax)

monmin = np.min(montmo[1:]); monmax = np.max(montmo[1:]); lmon = montmo.tolist()
minmon = lmon.index(monmin); maxmon = lmon.index(monmax)

kamin = np.min(kao[1:]); kamax = np.max(kao[1:]); lka = kao.tolist()
minka = lka.index(kamin); maxka = lka.index(kamax)

anomin = np.min(anor[1:]); anomax = np.max(anor[1:]); lanor = anor.tolist()
minano = lanor.index(anomin); maxano = lanor.index(anomax)

sanmin = np.min(sani[1:]); sanmax = np.max(sani[1:]); lsani = sani.tolist()
minsan = lsani.index(sanmin); maxsan = lsani.index(sanmax)

print("      qz mo ka an sa")
print(" min", '%3.0f'*5% (minqua, minmon, minka, minano, minsan))
print(" max", '%3.0f'*5% (maxqua, maxmon, maxka, maxano, maxsan))

outfile = open("multest" + ".out", "w")
outfile.write(" Pattern   Quartz  Calcite  Magnetite   Montmo1  Montmo2  Montmo3  MONTMO  Anorthite1  Anorthit2  Anorthit  Kaolinite  Sanidine\n" )
for k in range(1, nmuestras+1):
    outfile.write('  '+ '%3.0f'%k + '    '  +  '%8.3f' % qua[k] +" "+'%8.3f' % calci[k] + " " +'%8.3f' % magne[k] + "   "+'%8.3f' % montmo1[k] + " " +  '%8.3f' % montmo2[k] +   " " + '%8.3f' % montmo3[k] + ' ' + '%8.3f' % montmo[k] +"  "+ '%8.3f' % anor1[k]+"   "+'%8.3f' % anor2[k]+"   "+'%8.3f' % anor[k] + "  "+'%8.3f' % kao[k] + "   "+'%8.3f' % sani[k] +"\n" )
outfile.write(" Min at     " + '%3.0f'% minqua +"                                                     " '%3.0f'% minmon + "                              " + '%3.0f'% minano + "       " + '%3.0f'% minka + "       "+ '%3.0f'%  minsan+"\n")
outfile.write(" Max at     " + '%3.0f'% maxqua +"                                                     " '%3.0f'% maxmon + "                              " + '%3.0f'% maxano + "       " + '%3.0f'% maxka + "       "+ '%3.0f'%  maxsan+"\n")
outfile.close()
