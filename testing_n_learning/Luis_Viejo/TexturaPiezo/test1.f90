PROGRAM test1
IMPLICIT NONE
REAL (KIND=8), DIMENSION (3, 6) :: dmat
REAL (KIND=8), DIMENSION (3,3,3) :: dtens
REAL (KIND=8), DIMENSION (3,3) :: Q
REAL (KIND=8), DIMENSION (3,3,3) :: drotten
REAL (KIND=8), DIMENSION (3,6) :: drotmat
REAL (KIND=8), DIMENSION (3,6) :: dtot
REAL (KIND=8) :: phi1, phi, phi2, pi
INTEGER :: i, j, k, l, m, n

pi = 4.d0*Datan(1.d0)
call system('cls')
write(*,'(/A)')' Piezoelectricidad para cristales rotados'
write(*,'(A)')' Matriz monocristalina en <dato.dat>:'
OPEN(unit=1, status='old',file = 'dato.dat', action='read')
Do m=1,3
    read(1,*) (dmat(m,i), i=1,6)
    write (*,'(6F8.2)')(dmat(m,i), i=1,6)
End Do
CLOSE(1)

! Elementos de las diagonales en el tensor
Do i=1,3
    Do j=1,3
        dtens(i,j,j)=dmat(i,j)
    End Do
End Do

! Elementos fuera de las diagonales
Do i=1,3
    dtens(i,2,3)=0.5*dmat(i,4)
    dtens(i,3,2)=dtens(i,2,3)
    dtens(i,1,3)=0.5*dmat(i,5)
    dtens(i,3,1)=dtens(i,1,3)
    dtens(i,1,2)=0.5*dmat(i,6)
    dtens(i,2,1)=dtens(i,1,2)
End Do

do i =1,3
    write (*,'(A, I1)') ' i = ', i
    do j = 1, 3
        write (*,'(3F8.2)') (dtens(i,j,k), k = 1, 3)
    end do
end do

! Angulos de Euler
write (*,'(A$)')' Entre phi1, phi, phi2: '
read(*,*) phi1,phi,phi2
!write (*,*) phi1, phi, phi2
phi1=phi1*pi/180.00
phi=phi*pi/180.00
phi2=phi2*pi/180.00

Q(1,1) = dcos(phi2)*dcos(phi1)-dcos(phi)*dsin(phi1)*dsin(phi2)
Q(1,2) = dcos(phi2)*dsin(phi1)+dcos(phi)*dcos(phi1)*dsin(phi2)
Q(1,3) = dsin(phi2)*dsin(phi)
Q(2,1) = -dsin(phi2)*dcos(phi1)-dcos(phi)*dsin(phi1)*dcos(phi2)
Q(2,2) = -dsin(phi2)*dsin(phi1)+dcos(phi)*dcos(phi1)*dcos(phi2)
Q(2,3) = dcos(phi2)*dsin(phi)
Q(3,1) = dsin(phi)*dsin(phi1)
Q(3,2) = -dsin(phi)*dcos(phi1)
Q(3,3) = dcos(phi)

! Cálculo del tensor trasformado
Do i = 1, 3
    Do j = 1, 3
        Do k = 1,3
            drotten(i,j,k) = 0.0
        End do
    End do
End do

Do i = 1, 3
    Do j = 1, 3
        Do k = 1,3
            Do l=1,3
                Do m = 1, 3
                    Do n =1, 3
                        drotten(i,j,k) = drotten(i,j,k) + Q(i,l)*Q(j,m)*Q(k,n)*dtens(l,m,n)
                    End do
                End Do
            End Do
        End do
    End do
End do

Do i = 1, 3
    Do j = 1, 3
        drotmat(i,j)= drotten(i,j,j)
    End do
    drotmat(i,4)=2.000*drotten(i,2,3)
    drotmat(i,5)=2.000*drotten(i,1,3)
    drotmat(i,6)=2.000*drotten(i,1,2)
End do
write(*,'(A)') ' Tensor piezoelectrico del cristal rotado:'
Do i = 1,3
    write (*,'(6F8.2)') (drotmat(i,j), j =1,6)
End do

END PROGRAM test1
