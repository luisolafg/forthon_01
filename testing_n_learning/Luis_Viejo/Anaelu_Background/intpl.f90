module data
    implicit none
    character(len=80) :: texto
    integer :: i, j, k, l, m, n, mn
    integer(4), dimension(50) :: cont, mi, mf
    integer(4), dimension(1150,2300) :: intens, z
    real(8) :: pi, fac, p, d, dp, dthi, dthf, wtime, wtime1, wtime2
    real(8), dimension(50)::dmi, dmf, zmip, zmfp
    real(8), dimension(180):: dth, alfa, fra1, fra2
    save
end module data

program main
    call intpol
end program main

subroutine intpol
    use data
    call system('cls')
    write(*,*)' '
    write(*,*)' Start program intpl '
    write(*,*) " Reading 'ZnO_2D.asc' and 'param.dat'"
    call cpu_time(wtime1)
    !OPEN(unit=1, status='old',file = 'Platinum_2D_XRD.asc', action='read')
    OPEN(unit=1, status='old',file = 'ZnO_2D.asc', action='read')
    OPEN(unit=2, status='old',file = 'param.dat', action='read')
    read (2,*) p, d, dthi, dthf, n
    pi=4.d0*atan(1.d0)
    dp = d / p; fac = pi/180.d0
    do i = 1, n
        read(2,*) dmi(i), dmf(i)
        mi(i)= 2*dmi(i)
        mf(i)= 2*dmf(i)
    end do
    do i =1, 6
        read(1,'(A)')texto
    end do
    do m= 1, 1150
        do k = 1, 230
            read(1, *)(cont(l), l = 1,10)
        end do
    end do
    do i = 1, 1150
        j = 0
        do k = 1, 230
            read(1, *)(cont(l), l = 1,10)
            do l = 1, 10
               j = j + 1
               intens(i, j) = cont(l)
            end do
        end do
    end do
    ! At this point we have the array intens(1150, 2300) filled with the 2D-XRD pattern
    ! (Upper semi-circle)
    ! Now we transform to polar coordinates: 2theta = "dth" up to 45�, "alfa" up to 180�
    ! (dth starts at 0.5�, alfa at 1�)
    write(*,*)' Processing'
    do k = 1, 90
        dth(k) = k/2.d0
        do l = 1, 180
            alfa(l) = l
            i= dp * tan(fac*dth(k)) * sin(fac*alfa(l))
            j= 1150 + dp * tan(fac*dth(k)) * cos(fac*alfa(l))
            z(k,l) = intens(i,j)
        end do
    end do
    ! Interpolating the intensities inside the mask defined in "param.dat"
    do j = 1, 180
        do m = 1,n
            zmip(m)= (z(mi(m),j)+z(mi(m)-1,j)+z(mi(m)-2,j))/3.d0
            zmfp(m)= (z(mf(m),j)+z(mf(m)+1,j)+z(mf(m)+2,j))/3.d0
            do i = mi(m), mf(m)
                fra1(i)= dble(i-mi(m))
                fra2(m)= dble(mf(m)-mi(m))
                !z(i,j) = z(mi(m),j)+(z(i,j)-z(mi(m),j))*(fra1(i)/fra2(m))
                z(i,j) = z(mi(m),j)+(z(mf(m),j)-z(mi(m),j)) * (fra1(i)/fra2(m))
                !write(*,*) m, mi(m), mf(m), i, fra1(i),fra2(m),z(mi(m),j),z(mf(m),j), z(i, j)
            end do
        end do
    end do
    ! Write 180 x 90 file "&&&_2D_dth.pat" to be ploted by "PlotD2_2.py"
    !OPEN(unit=3, status='replace',file = 'Pt_corte_dth.pat', action='write')
    write(*,*)" Writing 'ZnO_corte.pat'"
    OPEN(unit=3, status='replace',file = 'ZnO_corte.pat', action='write')
    do j = 1, 180
        write(3, '(90I7)'), (z(i,j), i =1,90)
    end do
    close(1)
    close(2)
    close(3)
    call cpu_time(wtime2)
    wtime = wtime2 - wtime1
    write (*,'(A, f8.4)') '  End. Run time (secs) = ', wtime
end subroutine intpol
