
import numpy as np
from mayavi import mlab
z = np.loadtxt('ZnO_suav.pat')
zz = np.loadtxt('ZnO_corte.pat')
z = z / 1000
zz = zz / 1000
xx,yy = np.mgrid[1:180:180j,1:90:90j]
x,y = np.mgrid[1:180:180j,1:90:90j]
mlab.figure()
mlab.mesh(x,y,z)
mlab.mesh(xx,yy,zz, opacity=0.5, colormap="black-white")
mlab.scalarbar(orientation="vertical")
mlab.show()


