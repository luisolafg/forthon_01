module data
    implicit none
    character(len=80) :: texto
    integer :: inx, jny
    integer(4), dimension(50) :: cont, mi, mf
    integer(4), dimension(1150,2300) :: intens, z
    real(8) :: pi, fac, p, d, dp, dthi, dthf, wtime, wtime1, wtime2
    real(8), dimension(50)::dmi, dmf, zmip, zmfp
    real(8), dimension(180):: dth, alfa, fra1, fra2
    save
end module data

program main
    call intpol
    call bgsm
end program main

subroutine intpol
    use data
    integer::i, j, k, l, m, n
    call system('cls')
    write(*,*)' '
    write(*,*)' Program intsmo '
    write(*,*)' Start subroutine intpl '
    write(*,*) " Reading 'ZnO_2D.asc' and 'param.dat'"
    call cpu_time(wtime1)
    !OPEN(unit=1, status='old',file = 'Platinum_2D_XRD.asc', action='read')
    OPEN(unit=1, status='old',file = 'ZnO_2D.asc', action='read')
    OPEN(unit=2, status='old',file = 'param.dat', action='read')
    read (2,*) p, d, dthi, dthf, n
    pi=4.d0*atan(1.d0); inx = 180; jny = 90
    dp = d / p; fac = pi/180.d0
    do i = 1, n
        read(2,*) dmi(i), dmf(i)
        mi(i)= 2*dmi(i)
        mf(i)= 2*dmf(i)
        !write(*,*) i, mi(i), mf(i)
    end do
    do i =1, 6
        read(1,'(A)')texto
    end do
    do m= 1, 1150
        do k = 1, 230
            read(1, *)(cont(l), l = 1,10)
        end do
    end do
    do i = 1, 1150
        j = 0
        do k = 1, 230
            read(1, *)(cont(l), l = 1,10)
            do l = 1, 10
               j = j + 1
               intens(i, j) = cont(l)
            end do
        end do
    end do
    ! At this point we have the array intens(1150, 2300) filled with the 2D-XRD pattern
    ! (Upper semi-circle)
    ! Now we transform to polar coordinates: 2theta = "dth" up to 45�, "alfa" up to 180�
    ! (dth starts at 0.5�, alfa at 1�)
    write(*,*)' Processing'
    do k = 1, jny
        dth(k) = k/2.d0
        do l = 1, inx
            alfa(l) = l
            i= dp * tan(fac*dth(k)) * sin(fac*alfa(l))
            j= 1150 + dp * tan(fac*dth(k)) * cos(fac*alfa(l))
            z(k,l) = intens(i,j)
        end do
    end do
    ! Interpolating the intensities inside the mask defined in "param.dat"
    do j = 1, inx
        do m = 1,n
            zmip(m)= (z(mi(m),j)+z(mi(m)-1,j)+z(mi(m)-2,j))/3.d0
            zmfp(m)= (z(mf(m),j)+z(mf(m)+1,j)+z(mf(m)+2,j))/3.d0
            do i = mi(m), mf(m)
                fra1(i)= dble(i-mi(m))
                fra2(m)= dble(mf(m)-mi(m))
                z(i,j) = z(mi(m),j)+(z(mf(m),j)-z(mi(m),j)) * (fra1(i)/fra2(m))
                end do
        end do
    end do
    write(*,*)" Writing 'ZnO_corte.pat'"
    OPEN(unit=3, status='replace',file = 'ZnO_corte.pat', action='write')
    !OPEN(unit=3, status='replace',file = 'Pt_corte_dth.pat', action='write'
    do j = 1, inx
        write(3, '(90I7)'), (z(i,j), i =1,jny)
    end do
    close(1)
    close(2)
    close(3)
end subroutine intpol

SUBROUTINE BGSM
    use data
    real (kind = 8):: X(2300), Y(2300), A(0:10,0:10), GG(0:2300)
    real (kind = 8):: zexp(2300, 2300), zcalc(2300, 2300), YY
    integer:: i1, j1, NORD, N, M, K, J, JJ, JEX
    write (*,*) ' Start subroutine bgsm'
    write (*,*) ' Receiving data from intpol  '
    Do i = 1,INX
        X(i) = dble(i)
        !Y(i) = dble(i)
        !x(i)= x1(i)
        !write(*,*) X1(i), Y1(i)
    End Do

    do i1 = 1, inx
        do j1 = 1, jny
            zexp(i1,j1) = z(j1, i1)
        end do
    end do
    write (*,'(A$)')"  Enter the polynomials' degrees (<= 8): "
    READ (*,*) NORD
    M= NORD + 1
    write(*,*)' Processing'
    do i1 = 1, inx
        !i = 0
        i = 1
        do j1 = 1, jny
            y(i)= zexp(i1, j1)
            i = i +1
        end do
        DO K = 1, M               !matrix initialization
            DO J=1,M+1
                A(K,J)=0.d0
            END DO
        END DO
        DO K=1, M               !Matrix for polynomial fitting
            !DO I=0, INX
            DO I=1, INX
                DO J=1,M
                    JJ=K- 1+J- 1
                    YY=1.d0
                    IF (JJ /= 0) YY=X(I)**JJ
                    A(K,J)=A(K,J) + YY
                END DO
                JEX=K- 1
                YY=1.d0
                IF(JEX /= 0) YY=X(I)**JEX
                A(K,M+1)=A(K,M+1)+Y(I)*YY
            END DO
        END DO
        N=M

        CALL GAUSS(M,A)

        !DO I=0,INX
        DO I=1,INX
        GG(I)=0.d0
            DO K=1,M
                GG(I)=GG(I)+A(K,M+1)*X(I)**(K-1)
            END DO
        END DO
        do j = 1, jny
            zcalc(i1,j)= gg(j)
        end do
    end do
    write (*,*) " Writing 'ZnO_suav.pat'"
    open(unit=3, status='replace', file='ZnO_suav.pat', action='write')
    do i = 1, inx
       !write(*, '(90f8.1)')(zcalc(i, j), j = 1, jny)
       write(3, '(90f8.1)')(zcalc(i, j), j = 1, jny)
    end do
    close (1)
    call cpu_time(wtime2)
    wtime = wtime2-wtime1
    write (*,'(A, f8.4)') '  End. Run time (secs) = ', wtime
    STOP
END subroutine bgsm

SUBROUTINE GAUSS (N, A)
    Real (kind = 8):: A(0:10,0:10), TM, VA, P
    integer (kind = 4) :: I, IPV, J, N, JC, JR, NV
    DO I = 1, N-1
        IPV=I
        DO J=I+1,N
                IF (DABS(A(IPV,I)) < DABS(A(J,I))) IPV=J
        END DO
        IF (IPV /= I) THEN
                DO JC=1,N+1
                        TM=A (I, JC)
                        A(I,JC)=A(IPV,JC)
                        A (IPV, JC) = TM
                END DO
        END IF
        DO JR=I+1,N
                IF (A(JR,I) /= 0.d0) THEN
                        IF (A(I,I) == 0.d0) GOTO 300
                        R=A(JR,I)/A(I, I)
                        DO KC=I+1,N+1
                                A(JR,KC) = A(JR,KC) - R*A(I,KC)
                        END DO
                END IF
        END DO
    END DO
    ! - - backward substituion:
    IF (A(N,N) == 0.d0) GOTO 300
    A(N,N+1) = A(N,N+1) / A(N,N)
    DO NV=N-1,1,-1
        VA=A(NV, N+1)
        DO K=NV+1,N
                VA=VA-A(NV,K) * A(K,N+1)
        END DO
        A(NV,N+1) = VA/A(NV, NV)
    END DO
    RETURN
    300 write (*,*) ' SINGULAR MATRIX'
    RETURN
end subroutine gauss
