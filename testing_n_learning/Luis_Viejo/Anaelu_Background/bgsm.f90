Program bgsm
real (kind = 8):: X(2300), Y(2300), A(0:10,0:10), GG(0:2300), YY, wtime, wtime1, wtime2
real (kind = 8):: X1(2300), Y1(2300),zexp(2300, 2300), zcalc(2300, 2300)
integer(kind=4):: I, IN, i1, inx, j1, jny, NORD, N, M, K, J, JJ, JEX
call system ('cls')
write (*,*)  ' '
write (*,*) ' Start program bgsm'
write (*,*) ' Reading '
call cpu_time(wtime1)
INX = 180; JNY = 90; IN = INX       !INX, INY = numbers of points in X and in Y
Do i = 1,INX
    X1(i) = dble(i); Y1(i) = dble(i)
    x(i)= x1(i)
!   write(*,*) X1(i), Y1(i)
End Do

!OPEN(unit=1, status='old',file = 'cuted_img.asc', action='read')
OPEN(unit=1, status='old',file = 'ZnO_corte.pat', action='read')
do i1 = 1, inx
        read(1,*)(zexp(i1,j1), j1 = 1, jny)
        !write (*, '(10f8.1)')(zexp(i1,j1), j1 = 0, jny)
end do
CLOSE(1)

write (*,'(A$)')"  Enter the polynomials' degrees (<= 8): "
READ (*,*) NORD
100 M= NORD + 1
write(*,*)' Processing'
do i1 = 1, inx
!  i = 0
  i = 1
  do j1 = 1, jny
    y(i)= zexp(i1, j1)
    i = i +1
  end do

  DO K = 1, M               !matrix initialization
        DO J=1,M+1
                A(K,J)=0.d0
        END DO
  END DO

  DO K=1, M               !Matrix for polynomial fitting
        !DO I=0, IN
        DO I=1, INX
                DO J=1,M
                        JJ=K- 1+J- 1
                        YY=1.d0
                        IF (JJ /= 0) YY=X(I)**JJ
                        A(K,J)=A(K,J) + YY
                END DO
                JEX=K- 1
                YY=1.d0
                IF(JEX /= 0) YY=X(I)**JEX
                A(K,M+1)=A(K,M+1)+Y(I)*YY
        END DO
  END DO
  N=M

  CALL GAUSS(M,A)

  !DO I=0,IN
  DO I=1,IN
        GG(I)=0.d0
        DO K=1,M
                GG(I)=GG(I)+A(K,M+1)*X(I)**(K-1)
        END DO
  END DO
  do j = 1, jny
        zcalc(i1,j)= gg(j)
  end do
end do
write (*,*) ' Writing '
open(unit=3, status='replace', file='ZnO_suav.pat', action='write')
do i = 1, inx
       !write(*, '(90f8.1)')(zcalc(i, j), j = 1, jny)
       write(3, '(90f8.1)')(zcalc(i, j), j = 1, jny)
end do
close (1)
call cpu_time(wtime2)
wtime = wtime2-wtime1
write (*,'(A, f8.4)') '  End. Run time (secs) = ', wtime
!write(*,'(A$)')'  Try a new expansion? Enter polynomials degrees ("0" for end): '
!read(*,'(i1)') nord
!if (nord /= 0) go to 100
STOP
END

SUBROUTINE GAUSS (N, A)
Real (kind = 8):: A(0:10,0:10), TM, VA, P
integer (kind = 4) :: I, IPV, J, N, JC, JR, NV
DO I = 1, N-1
        IPV=I
        DO J=I+1,N
                IF (DABS(A(IPV,I)) < DABS(A(J,I))) IPV=J
        END DO
        IF (IPV /= I) THEN
                DO JC=1,N+1
                        TM=A (I, JC)
                        A(I,JC)=A(IPV,JC)
                        A (IPV, JC) = TM
                END DO
        END IF
        DO JR=I+1,N
                IF (A(JR,I) /= 0.d0) THEN
                        IF (A(I,I) == 0.d0) GOTO 300
                        R=A(JR,I)/A(I, I)
                        DO KC=I+1,N+1
                                A(JR,KC) = A(JR,KC) - R*A(I,KC)
                        END DO
                END IF
        END DO
END DO
! - - backward substituion:
IF (A(N,N) == 0.d0) GOTO 300
A(N,N+1) = A(N,N+1) / A(N,N)
DO NV=N-1,1,-1
        VA=A(NV, N+1)
        DO K=NV+1,N
                VA=VA-A(NV,K) * A(K,N+1)
        END DO
        A(NV,N+1) = VA/A(NV, NV)
END DO
RETURN
300 write (*,*) ' SINGULAR MATRIX'
RETURN
END
