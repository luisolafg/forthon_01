
import numpy as np
from mayavi import mlab
z = np.loadtxt('ZnO_corte.pat')
#z = np.loadtxt('ZnO_2th.pat')
z = z / 1000
x,y = np.mgrid[1:180:180j,1:90:90j]
mlab.figure()
mlab.mesh(x,y,z)
mlab.scalarbar(orientation="vertical")
mlab.show()


