PROGRAM test1
IMPLICIT NONE

REAL (KIND=8), DIMENSION (3, 6) :: dmat
REAL (KIND=8), DIMENSION (3,3,3) :: dtens
REAL (KIND=8), DIMENSION (3,3) :: Q
REAL (KIND=8), DIMENSION (3,3,3) :: drotten
REAL (KIND=8), DIMENSION (3,6) :: drotmat
INTEGER :: i, j, k, l, m, n

OPEN(unit= 1, status='old',file = 'dato.dat', action='read')
!OPEN(unit= 2, status='old',file = 'sale.dat', action='write')

Do m=1, 3
        read(1,*) (Q(m,i), i = 1,3)
        write (*,'(I2, 3F8.3)') m, Q(m,1), Q(m,2), Q(m,3)
End Do
Do m=1, 3
        read(1,*) (dmat(m,i), i=1,6)
        write (*,'(6F6.2)')(dmat(m,i), i=1,6)
End Do
CLOSE(1)

! Elementos de las diagonales en el tensor
Do i=1,3
    Do j=1,3
        dtens(i,j,j)=dmat(i,j)
!        write (*,'(2I2,F8.2)') i,j, dtens(i,j,j)
    End Do
End Do

! Elementos fuera de las diagonales
Do i=1,3
    dtens(i,2,3)=0.5*dmat(i,4)
    dtens(i,3,2)=dtens(i,2,3)
!    write (*,'(F8.2)') dtens(i,2,3)
    dtens(i,1,3)=0.5*dmat(i,5)
    dtens(i,3,1)=dtens(i,1,3)
!    write (*,'(F8.2)') dtens(i,1,3)
    dtens(i,1,2)=0.5*dmat(i,6)
    dtens(i,2,1)=dtens(i,1,2)
!    write (*,'(F8.2)') dtens(i,1,2)
End Do

! Cálculo del tensor trasformado
Do i = 1, 3
    Do j = 1, 3
        Do k = 1,3
            drotten(i,j,k) = 0.0
        End do
    End do
End do

Do i = 1, 3
    Do j = 1, 3
        Do k = 1,3
            Do l=1,3
                Do m = 1, 3
                    Do n =1, 3
                        drotten(i,j,k) = drotten(i,j,k) + Q(i,l)*Q(j,m)*Q(k,n)*dtens(l,m,n)
                    End do
                End Do
            End Do
        End do
    End do
End do

Do i = 1, 3
    Do j = 1, 3
        drotmat(i,j)= drotten(i,j,j)
    End do
    drotmat(i,4)=2.000*drotten(i,2,3)
    drotmat(i,5)=2.000*drotten(i,1,3)
    drotmat(i,6)=2.000*drotten(i,1,2)
End do

do i =1,3
    write (*,*) (drotmat(i,j), j =1,6)
end do




!Write(2,'(A)') ' Salida del programa test1'
!close (2)

END PROGRAM test1
