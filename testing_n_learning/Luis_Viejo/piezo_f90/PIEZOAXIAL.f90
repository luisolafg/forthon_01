PROGRAM PIEZOAXIAL
IMPLICIT NONE
REAL (KIND=8), DIMENSION (3, 6) :: dmat
REAL (KIND=8), DIMENSION (3,3,3) :: dtens
REAL (KIND=8), DIMENSION (3,3) :: Q
REAL (KIND=8), DIMENSION (3,3,3) :: drotten
REAL (KIND=8), DIMENSION (3,6) :: drotmat
REAL (KIND=8), DIMENSION (3,6) :: dtot
REAL (KIND=8) :: phi1, phi, phi2, pi, beta, R
REAL (KIND=8) :: a, b, omega, norm, total
INTEGER :: i, j, k,ii,jj,kk, l, m, n, nn, npas, np, nphi, nb, nbeta, nt

!R(phi,beta)= 4.d0 * pi * exp(-(180.d0*phi/(pi*omega))**2) * sin(phi) / norm
R(phi,beta)= exp(-(180.d0*phi/(pi*omega))**2) * sin(phi) / norm

!nphi = 1024; nbeta = 1024; nt = nphi * nbeta
nphi = 256; nbeta = 256; nt = nphi * nbeta
pi = 3.141592653589793D+00
omega = 30.d0; norm = 0.8229
!a = 0.d0; b = pi/2.d0
a = 0.d0; b = 2.d0*pi
call system('cls')
write(*,'(/A)')' Piezoelectricity for axial textures'
write(*,'(/A)')' Single crystal matrix in <dato.dat>:'
OPEN(unit=1, status='old',file = 'BaTiO3.dat', action='read')
!OPEN(unit=1, status='old',file = 'dato.dat', action='read')
Do m=1,3
    read(1,*) (dmat(m,i), i=1,6)
    write (*,'(6F8.2)')(dmat(m,i), i=1,6)
End Do
CLOSE(1)

! Tensor diagonal elements
Do i=1,3
    Do j=1,3
        dtens(i,j,j)=dmat(i,j)
    End Do
End Do

! Off-diagonal tensor elements
Do i=1,3
    dtens(i,2,3)=0.5*dmat(i,4)
    dtens(i,3,2)=dtens(i,2,3)
    dtens(i,1,3)=0.5*dmat(i,5)
    dtens(i,3,1)=dtens(i,1,3)
    dtens(i,1,2)=0.5*dmat(i,6)
    dtens(i,2,1)=dtens(i,1,2)
End Do

phi1=0.d0   !Fibre texture

! START THE GREAT LOOP FOR TENSOR COMPONENTS
Do i = 1, 3
   Do j = 1, 3
      Do k = 1,3
        total =0.d0     ! calculating the integral of R(phi, beta) * drotten(i,j,k)
        do np =1, nphi  ! loop on polar angle
                phi = ( ( 2 * nphi - 2 * np + 1 ) * a + ( 2 * np - 1 ) * b ) / ( 2 * nphi )
                do nb = 1, nbeta   ! loop on azimuth
                        ! Setting to "0" the considered polycrystal piezotensor component:
                        drotten(i,j,k) = 0.d0
                        beta = ( ( 2 * nbeta - 2 * nb + 1 ) * a + ( 2 * nb - 1 ) * b ) / ( 2 * nbeta )
                        phi2= (pi/2.d0) - beta    ! Bunge convention
                        Q(1,1) =  cos(phi2)* cos(phi1) - cos(phi)*sin(phi1)*sin(phi2)
                        Q(1,2) =  cos(phi2)* sin(phi1) + cos(phi)*cos(phi1)*sin(phi2)
                        Q(1,3) =  sin(phi2)* sin(phi)
                        Q(2,1) = -sin(phi2)* cos(phi1) - cos(phi)*sin(phi1)*cos(phi2)
                        Q(2,2) = -sin(phi2)* sin(phi1) + cos(phi)*cos(phi1)*cos(phi2)
                        Q(2,3) =  cos(phi2)* sin(phi)
                        Q(3,1) =  sin(phi) * sin(phi1)
                        Q(3,2) = -sin(phi) * cos(phi1)
                        Q(3,3) =  cos(phi)
                        Do l=1,3
                           Do m = 1, 3
                              Do n =1, 3
                                 drotten(i,j,k) = drotten(i,j,k) + Q(i,l)*Q(j,m)*Q(k,n)*dtens(l,m,n)
                              End do
                           End Do
                        End Do
                        total = total + R(phi, beta)*drotten(i,j,k)
                end do
        end do
        total = ( b - a ) * ( b - a ) * total / dble ( nphi ) / dble ( nbeta )
        drotten(i,j,k) = total
        End do
    End do
End do

Do i = 1, 3
        Do j = 1, 3
            drotmat(i,j)= drotten(i,j,j)
        End do
        drotmat(i,4)=2.000*drotten(i,2,3)
        drotmat(i,5)=2.000*drotten(i,1,3)
        drotmat(i,6)=2.000*drotten(i,1,2)
End do
write(*,'(/A)') 'Textured polycrystal matrix:'
Do i = 1,3
         write (*,'(6F8.2)') (drotmat(i,j), j =1,6)
End do

END PROGRAM PIEZOAXIAL
