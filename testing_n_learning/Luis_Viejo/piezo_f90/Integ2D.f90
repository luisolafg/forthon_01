program Integ2D
  implicit none
  real (kind = 8):: a, b, c, d, pi, omega
  real (kind = 8):: x, y, hx, hy, f, suma
  integer (kind = 4) :: i, j, nx, ny

  f(x,y)= exp(-(180.d0*x/(pi*omega))**2) * sin(x)

  pi = 3.141592653589793D+00
  nx = 1000; ny = 2000; omega = 30.d0
  a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi

  hx = (b-a)/dble(nx); hy = (d-c)/dble(ny)
  suma = 0.0D+00
  do i = 1, nx
     x = a + hx/2.d0 + dble(i-1)*hx
     do j = 1, ny
     y = c + hy/2.d0 + dble(j-1)*hy
     suma = suma + hx * hy * f (x, y)
     end do
  end do

  write ( *, '(a)' ) ' '
  write ( *, '(a, F8.4)' ) '  Integral approx value = ', suma
end
