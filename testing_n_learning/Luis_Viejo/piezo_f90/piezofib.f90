PROGRAM PIEZOFIB
IMPLICIT NONE
REAL (KIND=8), DIMENSION (3, 6) :: dmat
REAL (KIND=8), DIMENSION (3,3,3) :: dtens
REAL (KIND=8), DIMENSION (3,3) :: Q
REAL (KIND=8), DIMENSION (3,3,3) :: drotten, dtexten
REAL (KIND=8), DIMENSION (3,6) :: dtexmat
REAL (KIND=8) :: phi1, phi, phi2, pi, beta, R
REAL (KIND=8) :: a, b, c, d, dphi, dbeta, omeg, su, total
INTEGER :: i, j, k, l, m, n, nn, npas, np, nphi, nb, nbeta, nt
character(len=50) :: entra

!dV/V = (1/4*pi)*R(phi,beta)*sin(phi)*dphi*dbeta [Bunge, eq(5.13)]
R(phi,beta)= (1.d0/su) * exp(-(180.d0*phi/(pi*omeg))**2) * sin(phi)

nphi = 128; nbeta = 256

pi = 3.141592653589793D+00

a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi
dphi= (b-a)/dble(nphi); dbeta=(d-c)/dble(nbeta)

call system('cls')
write(*,'(/A)')' Piezoelectricity for axial textures'
write(*,'(/A$)')' Enter data file: '
read (*, '(A)') entra
write(*,'(/A)')' Single crystal matrix in data file:'

!OPEN(unit=1, status='old',file = 'BaTiO3.dat', action='read')
OPEN(unit=1, status='old',file = entra, action='read')

Do m=1,3
    read(1,*) (dmat(m,i), i=1,6)
    write (*,'(6F10.2)')(dmat(m,i), i=1,6)
End Do
CLOSE(1)

write(*,'(/A$)')' Enter Omega (degrees): '
read(*,*) omeg

call inte2D(omeg, su)

! Tensor diagonal elements
Do i=1,3
    Do j=1,3
        dtens(i,j,j)=dmat(i,j)
    End Do
End Do

! Off-diagonal tensor elements
Do i=1,3
    dtens(i,2,3)=0.5*dmat(i,4)
    dtens(i,3,2)=dtens(i,2,3)
    dtens(i,1,3)=0.5*dmat(i,5)
    dtens(i,3,1)=dtens(i,1,3)
    dtens(i,1,2)=0.5*dmat(i,6)
    dtens(i,2,1)=dtens(i,1,2)
End Do

phi1=0.d0   !Fibre texture, Bunge convention

! START THE GREAT LOOP FOR POLYCRYSTAL TENSOR COMPONENTS
Do i = 1, 3
   Do j = 1, 3
      Do k = 1,3
        dtexten(i,j,k) =0.d0  ! Setting to "0" the textured polycrystal piezotensor component:
           do np =1, nphi  ! loop on polar angle
                phi = a + dphi/2.d0 + dble(np - 1)*dphi
                do nb = 1, nbeta   ! loop on azimuth
                        beta = c + dbeta/2.d0 + dble(nb - 1)*dbeta
                        phi2= (pi/2.d0) - beta    ! Bunge convention
                        Q(1,1) =  cos(phi2)* cos(phi1) - cos(phi)*sin(phi1)*sin(phi2)
                        Q(1,2) =  cos(phi2)* sin(phi1) + cos(phi)*cos(phi1)*sin(phi2)
                        Q(1,3) =  sin(phi2)* sin(phi)
                        Q(2,1) = -sin(phi2)* cos(phi1) - cos(phi)*sin(phi1)*cos(phi2)
                        Q(2,2) = -sin(phi2)* sin(phi1) + cos(phi)*cos(phi1)*cos(phi2)
                        Q(2,3) =  cos(phi2)* sin(phi)
                        Q(3,1) =  sin(phi) * sin(phi1)
                        Q(3,2) = -sin(phi) * cos(phi1)
                        Q(3,3) =  cos(phi)
                        ! Calculating a rotated crystal piezotensor component:
                        Do l=1,3
                           Do m = 1, 3
                              Do n =1, 3
                                 drotten(i,j,k) =0.d0
                                 End do
                           End Do
                        End Do
                        Do l=1,3
                           Do m = 1, 3
                              Do n =1, 3
                                 drotten(i,j,k) = drotten(i,j,k) + Q(i,l)*Q(j,m)*Q(k,n)*dtens(l,m,n)
                              End do
                           End Do
                        End Do
                 ! Integrating textured polycrystal piezoelectric tensor components
                 dtexten(i,j,k) = dtexten(i,j,k)+R(phi, beta)*drotten(i,j,k)*dphi*dbeta
                 end do
           end do
      End do
   End do
End do

Do i = 1, 3
        Do j = 1, 3
            dtexmat(i,j)= dtexten(i,j,j)
        End do
        dtexmat(i,4)=2.000*dtexten(i,2,3)
        dtexmat(i,5)=2.000*dtexten(i,1,3)
        dtexmat(i,6)=2.000*dtexten(i,1,2)
End do
write(*,'(/A)') ' Textured polycrystal matrix:'
Do i = 1,3
         write (*,'(6F10.2)') (dtexmat(i,j), j =1,6)
End do

END

subroutine inte2D(omega, suma)
  ! Calculating the surface integral of a Gaussian distribution
  implicit none
  real (kind = 8):: a, b, c, d, pi, omega
  real (kind = 8):: x, y, hx, hy, f, suma
  integer (kind = 4) :: ii, jj, nx, ny

  f(x,y)= exp(-(180.d0*x/(pi*omega))**2) * sin(x)

  pi = 3.141592653589793D+00
  nx = 1024; ny = 2048
  a = 0.d0; b = pi; c = 0.d0; d = 2.d0*pi

  hx = (b-a)/dble(nx); hy = (d-c)/dble(ny)
  suma = 0.0D+00
  do ii = 1, nx
     x = a + hx/2.d0 + dble(ii-1)*hx
     do jj = 1, ny
     y = c + hy/2.d0 + dble(jj-1)*hy
     suma = suma + hx * hy * f (x, y)
     end do
  end do
return
end
