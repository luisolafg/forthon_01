#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"
#include <QGraphicsSceneMouseEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->gView->setScene(& this->my_scene);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_EditEntry_textEdited(const QString &arg1)
{
    this->texto = arg1.toStdString();
    std::cout << "typed:" << this->texto << std::endl;
}
void MainWindow::on_RunButton_clicked()
{
    std::cout << "Time to run:" << this->texto << std::endl;
    this->my_scene.addRect(QRect(0, 0, 800, 600));
    std::cout << " drawing Qrect " << std::endl;
}
void a_scene::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    QPointF NewPos = event->scenePos();
    double x, y;
    x = NewPos.x();
    y = NewPos.y();
    std::cout << " *** a_scene.mouseMoveEvent at: (" << x << ", "<< y << ") ***" << std::endl;
    this->addLine(0, 0, x, y);
}
