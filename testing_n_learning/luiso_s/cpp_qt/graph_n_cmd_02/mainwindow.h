#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE
class a_scene : public QGraphicsScene
{
    Q_OBJECT
public slots:
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event) override;
};
class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_EditEntry_textEdited(const QString &arg1);
    void on_RunButton_clicked();

private:
    Ui::MainWindow *ui;
    std::string texto;
    a_scene my_scene;
};
#endif // MAINWINDOW_H
