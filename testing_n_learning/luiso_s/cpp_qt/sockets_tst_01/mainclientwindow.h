#ifndef MAINCLIENTWINDOW_H
#define MAINCLIENTWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainClientWindow; }
QT_END_NAMESPACE

class MainClientWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainClientWindow(QWidget *parent = nullptr);
    ~MainClientWindow();

private slots:
    void on_GoButton_clicked();

private:
    Ui::MainClientWindow *ui;
};
#endif // MAINCLIENTWINDOW_H
