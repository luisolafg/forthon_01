#include "mainclientwindow.h"
#include "ui_mainclientwindow.h"
#include "iostream"

MainClientWindow::MainClientWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainClientWindow)
{
    ui->setupUi(this);
}

MainClientWindow::~MainClientWindow()
{
    delete ui;
}

void MainClientWindow::on_GoButton_clicked()
{
    std::cout << "Should run:" << ui->IntroCmdLineEdit->text().toStdString()  << std::endl;
}
