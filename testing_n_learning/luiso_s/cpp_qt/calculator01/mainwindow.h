#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    private slots:
        void on_EditSecondNumber_textChanged(const QString &arg1);
        void on_EditFirstNumber_textChanged(const QString &arg1);
        void on_ButtonAdd_clicked();
        void on_ButtonSubstract_clicked();

private:
        Ui::MainWindow *ui;
        double NumResl;
        double NumOne;
        double NumTwo;
};

#endif // MAINWINDOW_H
