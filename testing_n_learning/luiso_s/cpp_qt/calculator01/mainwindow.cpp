#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow() {
    delete ui;
}
void MainWindow::on_EditFirstNumber_textChanged(const QString &arg1) {
    this->NumOne = arg1.toDouble();
}
void MainWindow::on_EditSecondNumber_textChanged(const QString &arg1) {
    this->NumTwo = arg1.toDouble();
}
void MainWindow::on_ButtonAdd_clicked() {
    this->NumResl = this->NumOne + this->NumTwo;
    ui->EditOutPutResult->setText(QString::number(this->NumResl, 'f', 3));
}
void MainWindow::on_ButtonSubstract_clicked() {
    this->NumResl = this->NumOne - this->NumTwo;
    ui->EditOutPutResult->setText(QString::number(this->NumResl, 'f', 3));
}
