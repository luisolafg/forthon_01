#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    std::cout << "clicked" << std::endl;
    std::cout << "time to put the value " << this->cad.toStdString() << " in the second line edit"<< std::endl;

    ui->lineEdit02->setText(this->cad);
}

void MainWindow::on_lineEdit01_textChanged(const QString &arg1)
{
    this->cad = arg1;
    this->to_print = "text canged to: " + this->cad;
    std::cout << "text changed to " << this->cad.toStdString() << " from first line edit"<< std::endl;

    ui->textBrowser->append(this->to_print);

}
