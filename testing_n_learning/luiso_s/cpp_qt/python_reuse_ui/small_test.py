import sys
#from PySide import QtUiTools
from PySide2.QtCore import *
from PySide2.QtWidgets import *

from PySide2 import QtUiTools

class Form(QObject):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("mainwindow.ui")

        self.window.lineEdit01.textChanged.connect(self.txt_chg)
        self.window.pushButton.clicked.connect(self.btn_clk)

        self.cad = ""

        self.window.show()

    def txt_chg(self, new_txt):
        print("self.txt_chg")
        print("new txt =", new_txt)
        self.cad = new_txt

    def btn_clk(self):
        print("self.btn_clk")
        self.window.lineEdit02.setText(self.cad)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())

