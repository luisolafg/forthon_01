#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    std::cout << "clicked" << std::endl;
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    std::cout << "text changed" << "len(New txt) =" << arg1.count() << std::endl;
    std::cout << "text changed ... New txt =" << arg1.toStdString() << std::endl;
}
