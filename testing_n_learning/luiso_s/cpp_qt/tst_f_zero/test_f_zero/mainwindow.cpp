#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    std::cout << " *** testing << QGraphicsScene scene >> *** #1 " << std::endl;
    ui->gView->setScene(& this->my_scene);

    std::cout << " *** testing << QGraphicsScene scene >> *** #2 " << std::endl;
    //QGraphicsRectItem *rect = this->.addRect(QRectF(0, 0, 100, 100));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::on_EditEntry_textEdited(const QString &arg1)
{
    this->texto = arg1.toStdString();
    std::cout << "typed:" << this->texto << std::endl;
}
void MainWindow::on_RunButton_clicked()
{
    std::cout << "Time to run:" << this->texto << std::endl;
    this->my_scene.addRect(QRect(0, 0, 100, 200));
}
