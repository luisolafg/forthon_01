#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "iostream"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    std::cout << "clicked" << std::endl;
    std::cout << "time to put the value " << this->cad.toStdString() << " in the second line edit"<< std::endl;

    ui->lineEdit02->setText(this->cad);
}

void MainWindow::on_lineEdit01_textChanged(const QString &arg1)
{
    this->cad = arg1;

    std::cout << "text changed" << "len(New txt) =" << arg1.count() << std::endl;
    std::cout << "text changed ... New txt =" << this->cad.toStdString() << std::endl;
}
