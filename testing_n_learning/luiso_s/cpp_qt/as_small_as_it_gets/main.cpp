#include <QtWidgets>
#include "mainwindow.h"
#include "iostream"

ArrowPad::ArrowPad(QWidget *parent)
    : QWidget(parent)
{
    txt_in = new QLineEdit(tr("Type here"));
    repeat_button = new QPushButton(tr("Repeat"));
    txt_line = new QLineEdit(tr(" - None yet - "));

    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->addWidget(txt_in);
    mainLayout->addWidget(repeat_button);
    mainLayout->addWidget(txt_line);
    setLayout(mainLayout);

    connect(repeat_button, &QPushButton::clicked, this, &ArrowPad::clikeo);
    connect(txt_in, &QLineEdit::textChanged, this, &ArrowPad::cambio);
}

void ArrowPad::clikeo()
{
    std::cout << "clikeo" << std::endl;
    this->txt_line->setText(this->cad);
}
void ArrowPad::cambio(const QString &arg1)
{
    this->cad = arg1;
    std::cout << "New txt =" << this->cad.toStdString() << std::endl;
}

MainWindow::MainWindow()
{
    arrowPad = new ArrowPad;
    setCentralWidget(arrowPad);

    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(exitAct);
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    MainWindow mainWindow;
    mainWindow.show();
    return app.exec();
}
