#! [0]
HEADERS      = \
               mainwindow.h
SOURCES      = \
               main.cpp

target.path = $$[QT_INSTALL_EXAMPLES]/linguist/arrowpad
INSTALLS += target

QT += widgets

simulator: warning(This example might not fully work on Simulator platform)
