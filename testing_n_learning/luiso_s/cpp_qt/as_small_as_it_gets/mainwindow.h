#include <QMainWindow>
#include <QWidget>

QT_BEGIN_NAMESPACE
class QPushButton;
class QLineEdit;
QT_END_NAMESPACE

class ArrowPad : public QWidget
{
    Q_OBJECT

public:
    ArrowPad(QWidget *parent = 0);

private:
    QLineEdit *txt_in;
    QPushButton *repeat_button;
    QLineEdit *txt_line;
    QString cad;

    void clikeo();
    void cambio(const QString &arg1);
};

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
QT_END_NAMESPACE
class ArrowPad;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();

private:
    ArrowPad *arrowPad;
    QMenu *fileMenu;
    QAction *exitAct;
};
