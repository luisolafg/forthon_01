import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("interface.ui")

        self.exp_scene = QGraphicsScene()
        self.window.graphicsView_Exp.setScene(self.exp_scene)
        self.window.graphicsView_Exp.setDragMode(QGraphicsView.ScrollHandDrag)

        self.window.pushButton_load_1.clicked.connect(self.set_img_1)
        self.window.pushButton_load_gray.clicked.connect(self.set_img_gray)
        self.window.pushButton_load_inv.clicked.connect(self.set_img_inv)
        self.window.pushButton_load_gray_inv.clicked.connect(self.set_img_gray_inv)

        self.window.show()

    def set_img_1(self):
        self.put_img("../../../miscellaneous/lena.jpeg")

    def set_img_gray(self):
        self.put_img("../../../miscellaneous/lena_gray.jpeg")

    def set_img_inv(self):
        self.put_img("../../../miscellaneous/lena_inverted.jpeg")

    def set_img_gray_inv(self):
        self.put_img("../../../miscellaneous/lena_gray_inverted.jpeg")

    def put_img(self, img_path):
        image1 = QImage(img_path)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.exp_scene.addPixmap(self.pixmap_1)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
