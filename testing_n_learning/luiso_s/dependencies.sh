apt-get install iceweasel
apt-get install chromium
apt-get install kdesvn
apt-get install subversion
apt-get install gfortran
apt-get install python-numpy
apt-get install python-wxgtk2.8
apt-get install python-matplotlib
apt-get install gedit
apt-get install kate
apt-get install virtuoso-minimal
apt-get install less

echo "installing general use packages "

apt-get install pulseaudio
apt-get install okular kolourpaint4
apt-get install iceweasel
apt-get install chromium
apt-get install ksnapshot
apt-get install virtuoso-minimal

echo "installing developer packages"

apt-get install kdesvn
apt-get install subversion
apt-get install gfortran
apt-get install python-numpy
apt-get install python-wxgtk2.8
apt-get install python-matplotlib
apt-get install gedit
apt-get install kate
apt-get install ssh
#apt-get install ia32-libs ia32-libs-i386
apt-get install libc6-dev-i386


echo " g95 related                \n
dpkg -i g95-x86.deb               \n
apt-get install binutils          \n
apt-get -f install                \n
cd /home/robert/Downloads/        \n
dpkg -i g95-x86_64.deb            \n
"
