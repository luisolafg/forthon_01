echo "installing general use packages "

apt install pulseaudio
apt install okular kolourpaint4
apt install firefox
apt install chromium
apt install ksnapshot

echo "installing developer packages"

apt install kdesvn
apt install subversion
apt install gfortran
apt install python3-numpy
apt install python3-matplotlib
apt install gedit
apt install kate
apt install ssh
#apt install libc6-dev-i386

