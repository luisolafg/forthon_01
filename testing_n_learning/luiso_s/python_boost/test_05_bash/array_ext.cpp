#include<boost/python.hpp>
//#include <iostream>
namespace py = boost::python;
char const* greet()
{
    return "hello, world";
}
int give_int()
{
    return 5;
}

py::list square(py::list num_lst)
{
    py::list points;
    int one = 1;
    int two = 2;
    int three = 3;

    points.append(one);
    points.append(two);
    points.append(three);
    points.append(num_lst);
    return points;
}


py::list lst_arr(py::list lst_in)
{
    py::list points, tmp_lst;
    int one = 1;
    //int two = 2;
    //int three = 3;

    tmp_lst = py::extract<py::list>(lst_in[0]);
    tmp_lst.append(one);

    tmp_lst = py::extract<py::list>(lst_in[2]);
    tmp_lst.append(5);

    points = lst_in;

    return points;
}



BOOST_PYTHON_MODULE(array_ext)
{
    using namespace boost::python;
    def("greet", greet);
    def("give_int", give_int);

    def("lst_arr", lst_arr);
    def("square", square);
}
