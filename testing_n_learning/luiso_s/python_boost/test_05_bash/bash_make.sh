#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo "before assignations"
PYTHON_VERSION="2.7"
PYTHON_INCLUDE="/usr/include/python$PYTHON_VERSION"
BOOST_INC="/usr/include"
BOOST_LIB="/usr/lib"
TARGET="array_ext"

echo "$TARGET.o ==> $TARGET.cpp"
g++ -I$PYTHON_INCLUDE -I$BOOST_INC -fPIC -c $TARGET.cpp

#next commented line is imported from Makefile example
#g++ -shared -Wl,--export-dynamic $TARGET.o -L$BOOST_LIB -lboost_python -L/usr/lib/python$PYTHON_VERSION/config -lpython$PYTHON_VERSION -o $TARGET.so

echo "$TARGET.so ==> $TARGET.o"
g++ -shared $TARGET.o -L$BOOST_LIB -lboost_python -L/usr/lib/python$PYTHON_VERSION/config -lpython$PYTHON_VERSION -o $TARGET.so
