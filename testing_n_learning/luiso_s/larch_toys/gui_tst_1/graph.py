from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from PyQt5 import QtWidgets, uic
import sys

import numpy as np

def read_dat_file(f_path):
    myfile = open(f_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    head_lines = all_lines[:5]

    tmp_off = '''
    for hd_lin in head_lines:
        print(hd_lin)

    print("\n ----------- \n")
    '''

    spec_lines = all_lines[5:]

    lenght = len(spec_lines)
    print("len(spec_lines) =", lenght)
    spec_en = np.zeros(lenght)
    spec_re = np.zeros(lenght)
    spec_im = np.zeros(lenght)

    for num, sp_lin in enumerate(spec_lines):
        lst_dat = sp_lin[:-1].split(" ")
        #print("lst_dat =", lst_dat)
        lst_num = []
        for uni_dat in lst_dat:
            try:
                lst_num.append(float(uni_dat))

            except ValueError:
                pass

        if len(lst_num) != 3:
            print("error reading spectra")

        else:
            [spec_en[num], spec_re[num], spec_im[num]] = lst_num

    return spec_en, spec_re, spec_im


class Ui(QtWidgets.QMainWindow):
    def __init__(self, path2ui):
        super(Ui, self).__init__()
        uic.loadUi(path2ui, self)
        self.show()

class Form(QObject):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)
        self.window = Ui('mainwindow.ui')
        self.window.setWindowTitle("Spectrum Data n#")

        self.window.pushButton_ini.clicked.connect(self.clicked_ini)
        self.window.pushButton_save.clicked.connect(self.clicked_save)

        self.my_scene_1 = QGraphicsScene()
        self.window.MyGraphicsView.setScene(self.my_scene_1)
        self.window.MyGraphicsView.setDragMode(QGraphicsView.ScrollHandDrag)

        self.window.MyGraphicsView.scale(20, 500)

        self.pen_blue = QPen(
            Qt.blue, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        self.pen_red = QPen(
            Qt.red, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        self.pen_green = QPen(
            Qt.green, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        self.pen_black = QPen(
            Qt.black, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        self.pen_blue.setCosmetic(True)
        self.pen_red.setCosmetic(True)
        self.pen_green.setCosmetic(True)
        self.pen_black.setCosmetic(True)
        self.window.show()

    def clicked_ini(self):
        print("clicked")
        path4spectL = "../../quanty_toys/xas_n02/XASSpecL.dat"
        path4spectR = "../../quanty_toys/xas_n02/XASSpecR.dat"
        path4spectZ = "../../quanty_toys/xas_n02/XASSpecZ.dat"

        path4spectA = "../../quanty_toys/xas_n02/XASSpecAVER.dat"

        spec_en_L, spec_re_L, spec_im_L = read_dat_file(path4spectL)
        spec_en_R, spec_re_R, spec_im_R = read_dat_file(path4spectR)
        spec_en_Z, spec_re_Z, spec_im_Z = read_dat_file(path4spectZ)

        spec_en_avg, spec_re_avg, spec_im_avg = read_dat_file(path4spectA)

        xmax = 0
        xmin = 0
        ymax = 0
        ymin = 0

        y1a = spec_im_avg[0]
        y2a = spec_im_avg[1]

        m_old = (y2a - y1a) / (spec_en_L[1] - spec_en_L[0])

        for num in range(len(spec_en_L) - 1):
            x1 = spec_en_L[num]
            x2 = spec_en_L[num + 1]
            y1r = spec_im_L[num]
            y2r = spec_im_L[num + 1]
            y1i = spec_im_R[num]
            y2i = spec_im_R[num + 1]

            y1z = spec_im_Z[num]
            y2z = spec_im_Z[num + 1]

            y1a = spec_im_avg[num]
            y2a = spec_im_avg[num + 1]

            #self.my_scene_1.addLine(x1, y1r, x2, y2r, self.pen_blue)
            #self.my_scene_1.addLine(x1, y1z, x2, y2z, self.pen_red)
            #self.my_scene_1.addLine(x1, y1i, x2, y2i, self.pen_green)
            self.my_scene_1.addLine(x1, y1a, x2, y2a, self.pen_black)

            if x1 > xmax:
                xmax = x1

            if x1 < xmin:
                xmin = x1

            if y1r > ymax:
                ymax = y1r

            if y1r < ymin:
                ymin = y1r

            if y1i > ymax:
                ymax = y1i

            if y1i < ymin:
                ymin = y1i

            if y1z > ymax:
                ymax = y1z

            if y1z < ymin:
                ymin = y1z

            if y1a > ymax:
                ymax = y1a

            if y1a < ymin:
                ymin = y1a

            m_new = (y2a - y1a) / (x2 - x1)

            sig_old = abs(m_old) / m_old
            sig_new = abs(m_new) / m_new
            if sig_old != sig_new:
                self.my_scene_1.addLine(x1, y1a, x1, 0, self.pen_green)

                print("max or min at:", x1)

            m_old = m_new

        self.my_scene_1.addLine(xmin, 0, xmax, 0, self.pen_black)
        self.my_scene_1.addLine(0, ymin, 0, ymax, self.pen_black)

    def clicked_save(self):
        self.my_scene_1.clear()
        print("save")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())


