from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *

from PyQt5 import QtWidgets, uic
import sys

import numpy as np

def read_dat_file(f_path):
    myfile = open(f_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    head_lines = all_lines[:5]

    tmp_off = '''
    for hd_lin in head_lines:
        print(hd_lin)

    print("\n ----------- \n")
    '''

    spec_lines = all_lines[5:]

    lenght = len(spec_lines)
    print("len(spec_lines) =", lenght)
    spec_en = np.zeros(lenght)
    spec_re = np.zeros(lenght)
    spec_im = np.zeros(lenght)

    for num, sp_lin in enumerate(spec_lines):
        lst_dat = sp_lin[:-1].split(" ")
        #print("lst_dat =", lst_dat)
        lst_num = []
        for uni_dat in lst_dat:
            try:
                lst_num.append(float(uni_dat))

            except ValueError:
                pass

        if len(lst_num) != 3:
            print("error reading spectra")

        else:
            [spec_en[num], spec_re[num], spec_im[num]] = lst_num

    return spec_en, spec_re, spec_im


class Ui(QtWidgets.QMainWindow):
    def __init__(self, path2ui):
        super(Ui, self).__init__()
        uic.loadUi(path2ui, self)
        self.show()

class Form(QObject):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)
        self.window = Ui('mainwindow.ui')
        self.window.setWindowTitle("Spectrum Data n#")

        self.window.pushButton_ini.clicked.connect(self.clicked_ini)
        self.window.pushButton_save.clicked.connect(self.clicked_save)

        self.my_scene_1 = QGraphicsScene()
        self.window.MyGraphicsView.setScene(self.my_scene_1)
        self.window.MyGraphicsView.setDragMode(QGraphicsView.ScrollHandDrag)

        self.window.MyGraphicsView.scale(1, 5000)

        self.pen_blue = QPen(
            Qt.blue, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        self.pen_red = QPen(
            Qt.red, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        self.pen_green = QPen(
            Qt.green, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        self.pen_black = QPen(
            Qt.black, 2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        self.pen_blue.setCosmetic(True)
        self.pen_red.setCosmetic(True)
        self.pen_green.setCosmetic(True)
        self.pen_black.setCosmetic(True)
        self.window.show()

    def clicked_ini(self):
        print("clicked")

        path4spect = "/home/lui/tst_tmp/Cu2+_Oh_XAS_1s_iso.spec"

        #Energy               Re[0]                  Im[0]
        spec_en, spec_Re, spec_Im = read_dat_file(path4spect)

        xmax = spec_en[1]
        xmin = spec_en[1]
        ymax = spec_Re[1]
        ymin = spec_Re[1]

        #m_old = (y2a - y1a) / (spec_en_L[1] - spec_en_L[0])

        trick_scale = 5.0
        for num in range(len(spec_en) - 1):
            x1 = spec_en[num]
            x2 = spec_en[num + 1]
            y1r = spec_Re[num]
            y2r = spec_Re[num + 1]
            y1i = spec_Im[num]
            y2i = spec_Im[num + 1]

            self.my_scene_1.addLine(
                x1 * trick_scale, y1r * trick_scale,
                x2 * trick_scale, y2r * trick_scale, self.pen_blue
            )
            self.my_scene_1.addLine(
                x1 * trick_scale, y1i * trick_scale,
                x2 * trick_scale, y2i * trick_scale, self.pen_green
            )

            if x1 > xmax:
                xmax = x1

            if x1 < xmin:
                xmin = x1

            if y1r > ymax:
                ymax = y1r

            if y1r < ymin:
                ymin = y1r

            if y1i > ymax:
                ymax = y1i

            if y1i < ymin:
                ymin = y1i


        self.my_scene_1.addLine(
            xmin * trick_scale, 0, xmax * trick_scale, 0, self.pen_black
        )
        self.my_scene_1.addLine(
            0, ymin * trick_scale, 0, ymax * trick_scale, self.pen_black
        )

        print("xmin, ymin, xmax, ymax =", xmin, ymin, xmax, ymax)

        self.my_scene_1.setSceneRect(
            xmin * trick_scale, ymin * trick_scale,
            xmax * trick_scale, ymax * trick_scale
        )

    def clicked_save(self):
        self.my_scene_1.clear()
        print("save")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())


