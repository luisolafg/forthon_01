#from larch import Group
import numpy as np
from matplotlib import pyplot as plt

'''
mygroup = Group(a = 1, message='hello', x=np.linspace(-10, 10, 201))
print(len(mygroup.x))

plt.figure()
plt.eventplot(mygroup.x)
plt.show()
'''

x = np.linspace(0, 10, 101)
y1 = np.sin(x)
y2 = -2 +0.2*x + (0.2*x)**2
plt.plot(x, y1)
plt.show()

