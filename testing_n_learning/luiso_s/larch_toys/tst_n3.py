from larch import Group
import numpy as np
from matplotlib import pyplot as plt

mygroup = Group(a = 1, message='hello', x=np.linspace(-10, 10, 201))
print(len(mygroup.x))

plt.figure()
#plt.hlines(1,1,20)  # Draw a horizontal line
#plt.eventplot(mygroup.x, orientation='horizontal', colors='b')
plt.eventplot(mygroup.x)

plt.show()
