from matplotlib import pyplot as plt
import numpy as np
if __name__== "__main__":
    x = np.loadtxt('ct60_table_only.dat', usecols = 0)
    y = np.loadtxt('ct60_table_only.dat', usecols = 1)
    plt.plot(x, y)
    plt.show()
