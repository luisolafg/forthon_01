from matplotlib import pyplot as plt
import numpy as np
import fabio

if( __name__ == "__main__" ):

    path_to_img ="../../../../../Downloads/Naica1.asc"
    print "img_in =", path_to_img, "___\n"

    myfile = open(path_to_img, "r")
    all_lines = myfile.readlines()
    myfile.close()

    for l_num, lin_char in enumerate(all_lines[0:5]):
        #print l_num, "  ",lin_char
        lin_str_lst = lin_char.split(" ")
        #print "lin_str_lst =", lin_str_lst
        if(l_num == 1):
            nx = int(lin_str_lst[-1])
            print "n of pixels in X =", nx

        if(l_num == 2):
            ny = int(lin_str_lst[-1])
            print "n of pixels in Y =", ny


    data_out = np.zeros((nx, ny), dtype=np.float64)

    xpos = 0
    ypos = 0

    for l_num, lin_char in enumerate(all_lines[6:]):
        lin_str_lst = lin_char.split(' ')
        for single_str in lin_str_lst:
            if(single_str != ''):
                data_out[xpos, ypos] = float(int(single_str))
                xpos += 1
                if(xpos == nx):
                    xpos = 0
                    ypos += 1


    res_img = fabio.edfimage.edfimage()
    res_img.data = data_out

    img_file_out =  "Naica_out_2.edf"
    print "img_file_out =", img_file_out

    res_img.write(str(img_file_out))



