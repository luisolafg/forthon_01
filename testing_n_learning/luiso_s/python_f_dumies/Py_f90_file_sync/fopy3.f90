program fopy3
    real:: x(0:100), y(0:200), z(0:100,0:200)
    open(unit=1, status='old',file = 'datos_in.dat', action='read')
    open(unit=2, status='replace', file='Gauss.dat', action='write')
    read (1,*) polo, azi, zm, fw
    close(1)
    write(*,'(A,5F6.2)') " Data inside Fortran = ", polo, azi, zm, fw
    write (*,*) 'Modelling the surface with Fortran'
    do i = 0, 100
        x(i) = real(i)
        do j = 0, 200
            y(j) = real(j)
            d2 = (x(i) - polo)**2 + (y(j) - azi)**2
            fw2 = fw**2
            z(i,j) = zm * exp(-2.77258 * d2 / fw2)
        end do
    end do
    do i = 0, 100
        write (2,'(201F6.2)') (z(i,j), j = 0, 200)
    end do
    close(2)
end program fopy3
