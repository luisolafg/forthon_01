import fabio
import numpy as np
from matplotlib import pyplot as plt

path_to_img ="/home/luisolafg/anaelu_git/mccd_test/bnbt6-5_Hi/bnbt6-5_Hi_0001.mccd"
arr_img = fabio.open(path_to_img).data.astype(np.float64)

plt.imshow( arr_img , interpolation = "nearest" )
plt.show()
