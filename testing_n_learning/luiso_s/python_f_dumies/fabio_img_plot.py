# Matrix do some stuff
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import numpy as np
from matplotlib import pyplot as plt
import fabio

def plott_img(arr):
    print ("Plotting arr")


    cmap_lst = ["Spectral", "summer", "coolwarm", "Wistia_r", "pink_r", "Set1", "Set2", "Set3", "brg_r", "Dark2", "prism", "PuOr_r", "afmhot_r", "terrain_r", "PuBuGn_r", "RdPu", "gist_ncar_r", "gist_yarg_r", "Dark2_r", "YlGnBu", "RdYlBu", "hot_r", "gist_rainbow_r", "gist_stern", "PuBu_r", "cool_r", "cool", "gray", "copper_r", "Greens_r", "GnBu", "gist_ncar", "spring_r", "gist_rainbow", "gist_heat_r", "Wistia", "OrRd_r", "CMRmap", "bone", "gist_stern_r", "RdYlGn", "Pastel2_r", "spring", "terrain", "YlOrRd_r", "Set2_r", "winter_r", "PuBu", "RdGy_r", "spectral", "rainbow", "flag_r", "jet_r", "RdPu_r", "gist_yarg", "BuGn", "Paired_r", "hsv_r", "bwr", "cubehelix", "Greens", "PRGn", "gist_heat", "spectral_r", "Paired", "hsv", "Oranges_r", "prism_r", "Pastel2", "Pastel1_r", "Pastel1", "gray_r", "jet", "Spectral_r", "gnuplot2_r", "gist_earth", "YlGnBu_r", "copper", "gist_earth_r", "Set3_r", "OrRd", "gnuplot_r", "ocean_r", "brg", "gnuplot2", "PuRd_r", "bone_r", "BuPu", "Oranges", "RdYlGn_r", "PiYG", "CMRmap_r", "YlGn", "binary_r", "gist_gray_r", "Accent", "BuPu_r", "gist_gray", "flag", "bwr_r", "RdBu_r", "BrBG", "Reds", "Set1_r", "summer_r", "GnBu_r", "BrBG_r", "Reds_r", "RdGy", "PuRd", "Accent_r", "Blues", "autumn_r", "autumn", "cubehelix_r", "nipy_spectral_r", "ocean", "PRGn_r", "Greys_r", "pink", "binary", "winter", "gnuplot", "RdYlBu_r", "hot", "YlOrBr", "coolwarm_r", "rainbow_r", "Purples_r", "PiYG_r", "YlGn_r", "Blues_r", "YlOrBr_r", "seismic", "Purples", "seismic_r", "RdBu", "Greys", "BuGn_r", "YlOrRd", "PuOr", "PuBuGn", "nipy_spectral", "afmhot"]

    for local_cmap in cmap_lst:
        print "ploting ", local_cmap, "\n"
        plt.imshow(  arr , interpolation = "nearest" , cmap=local_cmap)
        plt.show()


if(__name__ == "__main__"):

    file_arr_iod = "../../../../test_data/APT73_d122_from_m02to02__01_41.mar2300"
    print "opening ", file_arr_iod
    img_arr = fabio.open(file_arr_iod).data.astype(np.float64)
    plott_img(img_arr)

    with open("img.raw", "w") as my_file:
        my_file.write(img_arr)

    if my_file.closed == False:
        my_file.close()

