#from matplotlib import pyplot as plt
import numpy as np
import fabio
#SWEBIII_phi_0.5_to_0.5_90s_01_01_06111730.bin

if( __name__ == "__main__" ):


    path_to_img ="Naica1.bin"
    #path_to_img ="SWEBIII_phi_0.5_to_0.5_90s_01_01_06111730.bin"
    print "img_in =", path_to_img

    data_in = np.fromfile(path_to_img, dtype=np.uint16)
    read_data = data_in.reshape((2300, 2300), order="FORTRAN")

    data_out = np.zeros((2300, 2300), dtype=np.float64)
    data_out[:,:] = read_data[:,:]


    res_img = fabio.edfimage.edfimage()
    res_img.data = data_out

    img_file_out =  "Naica_out.edf"
    #img_file_out =  "Naica_out_fromfi2d.edf"
    print "img_file_out =", img_file_out

    res_img.write(str(img_file_out))


