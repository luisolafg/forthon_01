program number_cr
    character(len = 255)                    :: file_in
    character(len = 255)                    :: str_data
    integer(kind = 16)                      :: log_uni
    log_uni = 12
    file_in = "datos_in.dat"
    write(*,*) "reading file:", trim(file_in)

    open(unit=log_uni, file=trim(file_in), action="read", position="rewind", form="formatted")

    do i = 1, 99
        read(unit = log_uni,fmt="(a)", end=999) str_data
        write(*,*) "str_data = ", trim(str_data)
    end do
    close(unit = log_uni)
999 write(unit=*,fmt=*) 'end of file'

end program number_cr
