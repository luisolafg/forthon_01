import sys
#from PySide import QtUiTools
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

from PySide2 import QtUiTools

class Form(QObject):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("launcher.ui")
        self.window.Run1Button.clicked.connect(self.btn_clk)

        self.window.show()

    def btn_clk(self):
        print("self.btn_clk")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())

