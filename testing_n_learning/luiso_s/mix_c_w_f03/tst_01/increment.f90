
subroutine increment(n)

    integer n

    write(*,*) "from Fortran ( before summation ) n =", n
    n = n + 5
    write(*,*) "from Fortran ( after summation ) n =", n

    return

end subroutine
