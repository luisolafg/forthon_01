public class class_n_func_02{

    public static class tst_cls{
        public int x = 0;

        public tst_cls( int w){
            System.out.println("Yes");
            x = w;
        }
    }

    public static void fnc_tst(String arg){
        System.out.println("Hi from fnc_tst");
    }

    public static void main(String[] args){
        System.out.println("Hi");
        tst_cls a = new tst_cls(1);
        System.out.println("after a = new tst_cls(1) :" + a.x);
        a.x = 5;
        System.out.println("after a.x = 5  :" + a.x);

        tst_cls b = new tst_cls(2);
        System.out.println("after b = new tst_cls(2) :" + b.x);
        b.x = 8;
        System.out.println("after b.x = 8  :" + b.x);



        System.out.print("\n\n\n\n a \n");
        fnc_tst("aaaaaaaa");
    }
}
