INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"
LIBSTATIC="-lcrysfml"
OPT1="-c -O2"

echo "removing leftovers"
rm *.o *.mod *.run
echo " Program Compilation"
gfortran $OPT1 $INC                              Calc_2d_pat.f90

echo " Linking"
gfortran -o calc_2d.run *.o  $LIB $LIBSTATIC

echo "Running"
./calc_2d.run Pt_COD

rm *.o *.mod *.run
