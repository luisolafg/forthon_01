INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"

LIBSTATIC="-lcrysfml"

OPT1="-c -O3"

echo " Program Compilation"
 gfortran $OPT1 $INC                              calc.f90
# gfortran $OPT1 $INC                              pmain.f90

echo " Linking"

 gfortran -o first.run *.o  $LIB $LIBSTATIC
rm *.o *.mod


