program cuts
    implicit none

    real(kind=8), allocatable   :: mask(:,:), img_in(:,:), img_out(:,:)
    integer                     :: IOstatus, log_uni, x, y, xmax = 2300, ymax = 2300
    real(kind=8)                :: xc, yc, dx, dy, tot_in, avg_in, cont_px
    integer                     :: lx, ly, xb1, yb1, xb2, yb2, y_end, x_end
    real(kind=8)                :: xstp, ystp, d1, d2, dt
    character(len = 255)        :: file_out

    allocate(mask(xmax,ymax))
    allocate(img_in(xmax,ymax))
    allocate(img_out(xmax,ymax))

    log_uni = 10
    open(unit = log_uni, status='old',file = '2d_mask.raw',form='unformatted', access='stream', action='read')
        read(log_uni, IOSTAT=IOstatus) mask
        write (*,*) "IOstatus=", IOstatus
    close(unit = log_uni)

    log_uni = 11
    open(unit= log_uni, status='old',file = 'expr.raw',form='unformatted', access='stream', action='read')
        read(log_uni, IOSTAT=IOstatus) img_in
        write (*,*) "IOstatus=", IOstatus
    close(unit = log_uni)

    !cutting hard coded cut of the beam center
    !img_in(1000:1200, 1100:1200) = 0.0

    tot_in = 0

    img_out = img_in
    do x = 1, xmax
        do y = 1, ymax
            if( mask(x, y) /= 1.0 )then
                tot_in = tot_in + img_in(x, y)
                cont_px = cont_px + 1
            end if
        end do
    end do
    write(*, *) "tot_in =", tot_in
    write(*, *) "cont_px =", cont_px
    avg_in = tot_in / cont_px
    write(*,*) "avg_in =", avg_in

    !TODO make xc, yc adjustable
    yc = real(ymax) / 2.0
    xc = real(xmax) / 2.0

    img_out = img_in
    do x = 1, xmax, 1
        do y = 1, ymax, 1
            if( mask(x, y) == 1.0 )then

                xb1 = -1
                yb1 = -1
                xb2 = -1
                yb2 = -1

                dx = x - xc
                dy = y - yc
                if( abs(dx) < abs(dy) )then
                ! case of y advancing pixel per pixel and x slower

                    dx = dx / abs(dy)
                    ystp = (yc - y) / abs(yc - y)
                    if(ystp /= 0)then

                        ! looking from x,y to the center
                        xstp = -1.0
                        do ly = y, int(yc), int(ystp)
                            xstp = xstp + 1.0
                            lx = x - int(dx * xstp)
                            if( mask(lx, ly) == -1 )then
                                xb1 = lx
                                yb1 = ly
                                exit
                            end if
                        end do

                        ! looking from x,y to the borders
                        ystp = -ystp
                        if( ystp > 0 )then
                            y_end = ymax
                        else
                            y_end = 1
                        end if
                        xstp = -1.0
                        do ly = y, y_end, int(ystp)
                            xstp = xstp + 1.0
                            lx = x + int(dx * xstp)
                            if( lx > 0 .and. ly > 0 .and. lx < xmax .and. ly < ymax )then
                                if( mask(lx, ly) == -1 )then
                                    xb2 = lx
                                    yb2 = ly
                                    exit
                                end if
                            else
                                exit
                            end if
                        end do
                    end if

                else
                ! case of x advancing pixel per pixel and y slower

                    dy = dy / abs(dx)
                    xstp = (xc - x) / abs(xc - x)
                    if(xstp /= 0)then

                        ! looking from x,y to the center
                        ystp = -1.0
                        do lx = x, int(xc), int(xstp)
                            ystp = ystp + 1.0
                            ly = y - int(dy * ystp)
                            if( mask(lx, ly) == -1 )then
                                xb1 = lx
                                yb1 = ly
                                exit
                            end if
                        end do

                        ! looking from x,y to the borders
                        xstp = -xstp
                        if( xstp > 0 )then
                            x_end = xmax
                        else
                            x_end = 1
                        end if
                        ystp = -1.0
                        do lx = x, x_end, int(xstp)
                            ystp = ystp + 1.0
                            ly = y + int(dy * ystp)
                            if( lx > 0 .and. ly > 0 .and. lx < xmax .and. ly < ymax )then
                                if( mask(lx, ly) == -1 )then
                                    xb2 = lx
                                    yb2 = ly
                                    exit
                                end if
                            else
                                exit
                            end if
                        end do
                    end if
                end if

                if( xb1 == -1 .or. yb1 == -1 )then
                    img_out(x,y) = img_in(xb2, yb2)
                elseif( xb2 == -1 .or. yb2 == -1 )then
                    img_out(x,y) = img_in(xb1, yb1)
                else
                    d1 = sqrt(float(x - xb1) ** 2.0 + float(y - yb1) ** 2.0)
                    d2 = sqrt(float(x - xb2) ** 2.0 + float(y - yb2) ** 2.0)
                    dt = d1 + d2
                    img_out(x,y) = (img_in(xb1,yb1) * (d2/dt) + img_in(xb2, yb2) * (d1/dt))
                end if

            end if
        end do
    end do




    file_out = "img_w_cuts.raw"
    log_uni = 12
    open(unit = log_uni, file = trim(file_out), status = "replace", access = "stream", form = "unformatted")
        write(unit = log_uni) img_out
    close(unit=log_uni)


end program cuts
