program cuts

    REAL (KIND=8), DIMENSION (2300, 2300) :: mask, img_in
    INTEGER :: IOstatus, log_uni
    character(len = 255) :: file_out

    log_uni = 10
    OPEN(unit = log_uni, status='old',file = '2d_mask.raw',form='unformatted', access='stream', action='read')
    Do m=1, 2300
        Do n=1, 2300
            read(log_uni, IOSTAT=IOstatus) mask(m,n)
        End Do
    End Do
    WRITE (*,*) "IOstatus=", IOstatus
    close(unit = log_uni)

    log_uni = 11
    OPEN(unit= log_uni, status='old',file = 'expr.raw',form='unformatted', access='stream', action='read')
    Do m=1, 2300
        Do n=1, 2300
            read(log_uni, IOSTAT=IOstatus) img_in(m,n)
        End Do
    End Do
    WRITE (*,*) "IOstatus=", IOstatus
    close(unit = log_uni)

    file_out = "img_w_cuts.raw"
    log_uni = 12
    open(unit = log_uni, file = trim(file_out), status = "replace", access = "stream", form = "unformatted")

    Do m=1, 2300
        Do n=1, 2300
            write(unit = log_uni) img_in(m, n)
        end do
    end do

    close(unit=log_uni)


end program cuts
