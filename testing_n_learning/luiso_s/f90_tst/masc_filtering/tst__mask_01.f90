program mask_test
    real,    dimension(6, 6)    :: img_in
    real,    dimension(6, 6)    :: img_out
    logical, dimension(6, 6)    :: mask
    integer                     :: i, j, k, l
    real                        :: eps_symm = 5.0, tot, div

    img_out= - 1.0
    img_in = 0.0
    mask(:,:) = .True.
    mask(2:4,3:5) = .False.

    Do i = 1, 6
        write(*,*) mask(i,:)
    end Do

    ! building ascending matrix
    !forall (i=1:6, j=1:6) img_in(i,j) = (i*i*j*j*j) * 0.2
    forall (i=1:6, j=1:6) img_in(i,j) = (i + j)


    !where (abs(var_02) < eps_symm)
    !  img_out=1.0 / var_02
    !end where
    write(*,*) "img_in ="
    write(*,*) transpose(img_in)


    where (mask .eqv. .True.)
        img_out = - 1
    end where

    do i = 1, 6
        do j = 1,6
            if( mask(i, j) .eqv. .False. )then
                tot = 0.0
                div = 0.0
                do k = i - 1, 1, -1
                    if(mask(k, j) .eqv. .True.)then
                        tot = tot + img_in(k, j)
                        div = div + 1
                        exit
                    end if
                end do

                do k = i + 1, 6, 1
                    if(mask(k, j) .eqv. .True.)then
                        tot = tot + img_in(k, j)
                        div = div + 1
                        exit
                    end if
                end do


                do k = j - 1, 1, -1
                    if(mask(i, k) .eqv. .True.)then
                        tot = tot + img_in(i, k)
                        div = div + 1
                        exit
                    end if
                end do

                do k = j + 1, 6, 1
                    if(mask(i, k) .eqv. .True.)then
                        tot = tot + img_in(i, k)
                        div = div + 1
                        exit
                    end if
                end do

                img_out(i,j) = tot / div
            end if
        end do
    end do

    write(*,*) "img_out ="
    write(*,*) transpose(img_out)

    !forall (i=2:4, j=2:4) img_in(i,j) = (sum(img_in(i - 1: i + 1, j - 1: j + 1)) &
    !                                      - img_in(i, j) ) / 8.0

end program mask_test
