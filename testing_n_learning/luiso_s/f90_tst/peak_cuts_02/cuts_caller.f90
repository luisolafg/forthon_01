
program cuts_main
    use cuts_low_lev
    implicit none
    real(kind=8), allocatable   :: mask(:,:), img_in(:,:), img_out(:,:)
    integer                     :: IOstatus, log_uni, xmax = 2300, ymax = 2300
    character(len = 255)        :: file_out
    real(kind=8)                :: xc, yc

    !TODO make xc, yc adjustable
    yc = real(ymax) / 2.0
    xc = real(xmax) / 2.0

    allocate(mask(xmax,ymax))
    allocate(img_in(xmax,ymax))
    allocate(img_out(xmax,ymax))

    log_uni = 10
    open(unit = log_uni, status='old',file = '2d_mask.raw',form='unformatted', access='stream', action='read')
        read(log_uni, IOSTAT=IOstatus) mask
        write (*,*) "IOstatus=", IOstatus
    close(unit = log_uni)

    log_uni = 11
    open(unit= log_uni, status='old',file = 'expr.raw',form='unformatted', access='stream', action='read')
        read(log_uni, IOSTAT=IOstatus) img_in
        write (*,*) "IOstatus=", IOstatus
    close(unit = log_uni)

    write(*,*) "calling cutter"
    call cut_peaks_w_mask(xc, yc, mask, img_in, img_out)

    file_out = "img_w_cuts.raw"
    log_uni = 12
    open(unit = log_uni, file = trim(file_out), status = "replace", access = "stream", form = "unformatted")
        write(unit = log_uni) img_out
    close(unit=log_uni)

end program cuts_main
