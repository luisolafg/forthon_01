program growing_data
    implicit none
    integer , allocatable, dimension(:)  :: arr_1d
    integer                              :: num, i

    num = 10
    allocate(arr_1d(1:num))
    do i = 1, num
        !write(*,*) i

        arr_1d(i) = i
    end do

    do i = 1, num
        write(*,*) arr_1d(i)
    end do


end program growing_data
