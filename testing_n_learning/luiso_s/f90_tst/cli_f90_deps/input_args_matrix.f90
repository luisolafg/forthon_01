
module input_arg_tools

    implicit none

    contains

    subroutine handling_kargs(matrix)
        real(kind = 8), intent(out), dimension(3)   :: matrix
        integer                                     :: i, narg, num_val
        character(len = 255)            :: value
        matrix = 0
        narg = COMMAND_ARGUMENT_COUNT()
        do i = 1,narg,1
            call GET_COMMAND_ARGUMENT(i, value)
            write(*,*) "arg # ", i, " = ", value
        end do
    end subroutine handling_kargs


end module input_arg_tools

program reading_cifs


    use input_arg_tools,                only : handling_kargs

    implicit none

    real(kind = 8), dimension(3)   :: matrix
    call handling_kargs(matrix)
    write(*,*) "matrix =", matrix

end program reading_cifs
