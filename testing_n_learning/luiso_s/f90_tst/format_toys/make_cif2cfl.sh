#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
INC="-I$CRYSFML/GFortran64/LibC"
LIB="-L$CRYSFML/GFortran64/LibC"
LIBSTATIC=" -static -lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly
#OPT1="-c -O2 -Wall "
OPT1="-c -O2"
echo "removing traces of previous runs"
rm cif2cfl.r
rm *cfl

echo "Program Compilation -static"
gfortran $OPT1 $INC                        cif2cfl.f90

echo "Linking"
gfortran -o cif2cfl.r *.o  $LIB $LIBSTATIC
rm *.o
echo "running"
echo " "

#rm *.mod
echo "tst 1"

./cif2cfl.r cif_in=1000022.cif cfl_out=1000022.cfl
./cif2cfl.r cif_in=1000082.cif cfl_out=1000082.cfl
./cif2cfl.r cif_in=1000107.cif cfl_out=1000107.cfl
./cif2cfl.r cif_in=1000500.cif cfl_out=1000500.cfl
./cif2cfl.r cif_in=1001055.cif cfl_out=1001055.cfl
./cif2cfl.r cif_in=1001069.cif cfl_out=1001069.cfl
./cif2cfl.r cif_in=1001124.cif cfl_out=1001124.cfl
./cif2cfl.r cif_in=1000071.cif cfl_out=1000071.cfl
./cif2cfl.r cif_in=1000086.cif cfl_out=1000086.cfl
./cif2cfl.r cif_in=1000194.cif cfl_out=1000194.cfl
./cif2cfl.r cif_in=1000501.cif cfl_out=1000501.cfl
./cif2cfl.r cif_in=1001056.cif cfl_out=1001056.cfl
./cif2cfl.r cif_in=1001070.cif cfl_out=1001070.cfl
./cif2cfl.r cif_in=1001157.cif cfl_out=1001157.cfl
./cif2cfl.r cif_in=1000079.cif cfl_out=1000079.cfl
./cif2cfl.r cif_in=1000098.cif cfl_out=1000098.cfl
./cif2cfl.r cif_in=1000233.cif cfl_out=1000233.cfl
./cif2cfl.r cif_in=1001054.cif cfl_out=1001054.cfl
./cif2cfl.r cif_in=1001068.cif cfl_out=1001068.cfl
./cif2cfl.r cif_in=1001072.cif cfl_out=1001072.cfl
./cif2cfl.r cif_in=1001218.cif cfl_out=1001218.cfl


