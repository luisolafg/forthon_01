
module input_arg_tools

    implicit none

    Type   :: arg_def
        character(len = 255)   :: data_type
        character(len = 255)   :: var_name
        character(len = 255)   :: value
    end Type arg_def

    Type   :: arg_def_list
        integer                                     :: num_of_vars = 10
        Type (arg_def), dimension(:), allocatable   :: field   ! 1D data of the peak
    end Type arg_def_list

    contains

    subroutine handling_kargs_03(my_def)
        type(arg_def_list)  , intent(in out)        :: my_def

        character(len = 255),  dimension(1:10)      :: arg
        integer                                     :: narg, lg_fil, i, j
        logical                                     :: explicit_mode, tmp_explicit, found_var
        narg = COMMAND_ARGUMENT_COUNT()

        if(narg == 0 .or. narg >= 10)then
            write(*,*) "error #1"
            stop
        else
            do i = 1,narg,1
                call GET_COMMAND_ARGUMENT(i, arg(i))
            end do
            if(SCAN(trim(arg(1)), "=") == 0)then
                tmp_explicit = .False.
            else
                tmp_explicit = .True.
            end if
            do i = 1,narg,1
                if(SCAN(trim(arg(i)), "=") == 0)then
                    explicit_mode = .False.
                else
                    explicit_mode = .True.
                end if

                if(explicit_mode .neqv. tmp_explicit)then
                    write(*,*) "error #2"
                    stop
                end if
            end do

            write(*,*) "explicit_mode =", explicit_mode

            if(explicit_mode .eqv. .True.)then
                do i = 1, narg, 1
                    found_var = .False.
                    do j = 1, my_def%num_of_vars
                        if(arg(i)(1:len(trim(my_def%field(j)%var_name))) == trim(my_def%field(j)%var_name))then
                            found_var = .True.
                            my_def%field(j)%value = trim(arg(i)(  len(trim(my_def%field(j)%var_name) ) + 2 :len(arg(2))))
                        end if
                    end do
                    if(found_var .eqv. .False.)then
                        write(*,*) "error #3"
                        stop
                    end if
                end do
            else
                write(*,*) "implicit mode"

                do i = 1,narg,1
                    my_def%field(i)%value = trim(arg(i))
                end do
            end if

        end if
    end subroutine handling_kargs_03


end module input_arg_tools

program reading_cifs
    use input_arg_tools,                only : handling_kargs_03, arg_def_list

    implicit none

    integer                         :: i
    Type(arg_def_list)              :: my_def

    my_def%num_of_vars = 2
    allocate(my_def%field(1:my_def%num_of_vars))
    my_def%field(1)%var_name = "cif_in"
    my_def%field(1)%data_type = "character"
    my_def%field(1)%value = "my_CIF_file.cfl"

    my_def%field(2)%var_name = "cfl_out"
    my_def%field(2)%data_type = "character"
    my_def%field(2)%value = "my_CFL_file.cfl"

    write(*,*) "my_def:"
    do i = 1, my_def%num_of_vars
        write(*,*) "my_def%field(", (i), ")%var_name = ", trim(my_def%field(i)%var_name)
        write(*,*) "my_def%field(", (i), ")%data_type = ", trim(my_def%field(i)%data_type)
        write(*,*) "my_def%field(", (i), ")%value = ", trim(my_def%field(i)%value)
        write(*,*)
    end do

    call handling_kargs_03(my_def)

    write(*,*) "my_def:"
    do i = 1, my_def%num_of_vars
        write(*,*) "my_def%field(", (i), ")%var_name = ", trim(my_def%field(i)%var_name)
        write(*,*) "my_def%field(", (i), ")%data_type = ", trim(my_def%field(i)%data_type)
        write(*,*) "my_def%field(", (i), ")%value = ", trim(my_def%field(i)%value)
        write(*,*)
    end do


end program reading_cifs
