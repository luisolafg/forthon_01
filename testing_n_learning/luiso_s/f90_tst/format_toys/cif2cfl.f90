
module input_arg_tools

    implicit none

    Type   :: arg_def
        character(len = 255)   :: var_name
        character(len = 255)   :: value
    end Type arg_def

    Type   :: arg_def_list
        integer                                     :: num_of_vars = 10
        Type (arg_def), dimension(:), allocatable   :: field   ! 1D data of the peak
    end Type arg_def_list

    contains

    subroutine handling_kargs(my_def)
        type(arg_def_list)  , intent(in out)        :: my_def

        character(len = 255),  dimension(1:10)      :: arg
        integer                                     :: narg, lg_fil, i, j
        logical                                     :: explicit_mode, tmp_explicit, found_var
        narg = COMMAND_ARGUMENT_COUNT()

        if(narg == 0 .or. narg >= 10)then
            write(*,*) "error #1"
            stop
        else
            do i = 1,narg,1
                call GET_COMMAND_ARGUMENT(i, arg(i))
            end do
            if(SCAN(trim(arg(1)), "=") == 0)then
                tmp_explicit = .False.
            else
                tmp_explicit = .True.
            end if
            do i = 1,narg,1
                if(SCAN(trim(arg(i)), "=") == 0)then
                    explicit_mode = .False.
                else
                    explicit_mode = .True.
                end if

                if(explicit_mode .neqv. tmp_explicit)then
                    write(*,*) "error #2"
                    stop
                end if
            end do

            write(*,*) "explicit_mode =", explicit_mode

            if(explicit_mode .eqv. .True.)then
                do i = 1, narg, 1
                    found_var = .False.
                    do j = 1, my_def%num_of_vars
                        if(arg(i)(1:len(trim(my_def%field(j)%var_name))) == trim(my_def%field(j)%var_name))then
                            found_var = .True.
                            my_def%field(j)%value = trim(arg(i)(  len(trim(my_def%field(j)%var_name) ) + 2 :len(arg(2))))
                        end if
                    end do
                    if(found_var .eqv. .False.)then
                        write(*,*) "error #3"
                        stop
                    end if
                end do
            else
                write(*,*) "implicit mode"

                do i = 1,narg,1
                    my_def%field(i)%value = trim(arg(i))
                end do
            end if

        end if
    end subroutine handling_kargs


end module input_arg_tools

program reading_cifs

    use CFML_string_utilities,          only: Get_LogUnit
    use CFML_Crystallographic_Symmetry
    use CFML_Atom_TypeDef
    use CFML_IO_Formats,       only: Readn_set_Xtal_Structure, Write_CFL
    use CFML_Crystal_Metrics,  only: Crystal_Cell_Type

    use input_arg_tools,                only : handling_kargs, arg_def_list

    implicit none

    type (space_group_type)         :: SpG
    type (Atom_list_Type)           :: A
    type (Crystal_Cell_Type)        :: Cell


    integer                         :: i_cfl
    Type(arg_def_list)              :: my_def

    my_def%num_of_vars = 2
    allocate(my_def%field(1:my_def%num_of_vars))
    my_def%field(1)%var_name = "cif_in"
    my_def%field(1)%value = "my_CIF_file.cfl"

    my_def%field(2)%var_name = "cfl_out"
    my_def%field(2)%value = "my_CFL_file.cfl"


    call handling_kargs(my_def)

    write(*,*) "opening cif"
    !call Readn_set_Xtal_Structure(file_in, Cell, SpG, A, Mode="CIF")
    call Readn_set_Xtal_Structure( trim(my_def%field(1)%value) , Cell, SpG, A, Mode="CIF")

    write(*,*) "writing cfl"
    call Get_LogUnit(i_cfl)
    !open(unit=i_cfl, file="out.cfl", status="replace", action="write")
    open(unit=i_cfl, file=trim(my_def%field(2)%value), status="replace", action="write")
    call Write_CFL(i_cfl, Cell, SpG, A, "temporary cfl file, do NOT EDIT this file ")
    close(unit=i_cfl)

end program reading_cifs
