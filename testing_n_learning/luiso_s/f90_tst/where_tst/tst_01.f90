program where_test
       real, dimension(3,3)     :: var_02
       real, dimension(3,3)     :: var_01
       integer                  :: i,j,k
       real                     :: eps_symm = 5.0

       var_01=3.0
       var_02=4.0
       write(*,*) "var_01 =", transpose(var_01)
       where (abs(var_02) < eps_symm)
          var_01=1.0/var_02
       end where
       write(*,*) "var_01 =", transpose(var_01)

end program where_test
