program bin_text_write
    implicit none
    integer(kind=1)     :: byte_write
    character(len=250)  :: txt_wr1 , txt_wr2

    integer             :: i, j, k
    integer             :: log_uni

    log_uni=10
    txt_wr1='hola'
    txt_wr2='size = 20'


    j=len_trim(txt_wr1)
    write(unit=*,fmt='(a)') txt_wr1
    do i=1,j,1
        write(unit=*,fmt=*) ichar(txt_wr1(i:i))
    end do


    j=len_trim(txt_wr2)
    write(unit=*,fmt='(a)') txt_wr2
    do i=1,j,1
        write(unit=*,fmt=*) ichar(txt_wr2(i:i))
    end do


    open(unit=log_uni,file='tst_mix.lui', status="replace", position="rewind", access="stream", form="unformatted",action="write")

    j=len_trim(txt_wr1)
    do i=1,j,1
        byte_write = ichar(txt_wr1(i:i))
        write(unit=log_uni) byte_write
    end do

    byte_write=10
    write(unit=log_uni) byte_write

    j=len_trim(txt_wr2)
    do i=1,j,1
        byte_write = ichar(txt_wr2(i:i))
        write(unit=log_uni) byte_write
    end do

    byte_write=10
    write(unit=log_uni) byte_write

    do i=1,20
        byte_write=i*2
        write(unit=log_uni) byte_write

    end do

    close(unit=log_uni)
end program bin_text_write
