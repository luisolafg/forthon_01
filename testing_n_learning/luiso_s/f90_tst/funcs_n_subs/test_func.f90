module func_deps
    contains
    Function get_cartesian(ang, dist) Result(xy_coord)
       !--- Argument ----!
       real,  intent(in)         :: ang, dist
       real,  dimension(2)       :: xy_coord

       xy_coord(1) = dist * sin(ang)
       xy_coord(2) = dist * cos(ang)

       return
    End Function get_cartesian
end module func_deps

program main_caller
    use func_deps

    implicit none

    real, dimension(2)    :: xy_coord
    real                  :: a, d
    a = 45
    d = 10

    xy_coord = get_cartesian(3.14159235358/4.0, d)
    write(*,*) "coord =", xy_coord

end program main_caller
