module calc_tools
    implicit none
    contains

    function get_y(x) result(y)
        real,  intent(in)       :: x
        real                    :: y
        if(x >= 0.0 .and. x < 1.0 )then
            y = 2.0
        else if( x >= 1.0 .and. x < 2.0 )then
            y = 1
        end if
        write(*,*) "y = ", y

    end function get_y

    function trap_sum(ini, fin, pas) result(add)

        real, intent(in)        :: ini, fin, pas
        real                    :: y, x, add
        integer                 :: i, n
        add = 0.0
        n = int((fin - ini) / pas)
        write(*,*) "n =", n
        do i = 0, n - 1, 1
            write(*,*) "i = ", i
            x = ini + real(i) * pas
            write(*,*) "x = ", x
            y = get_y(x)
            add = add + pas * y
        end do

    end function trap_sum

end module calc_tools

program evaluate
    use calc_tools,                only : trap_sum

    implicit none

    real                          :: area
    area =  trap_sum(0.0, 2.0, 0.3)
    write(*,*) "sum = ", area

end program evaluate
