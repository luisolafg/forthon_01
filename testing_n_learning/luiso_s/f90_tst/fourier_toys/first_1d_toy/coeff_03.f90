module calc_tools
    implicit none
        real    ::  pi = 3.14159235358
    contains

    function get_y(x) result(y)
        real,  intent(in)       :: x
        real                    :: y
        if(x >= 0.0 .and. x < 1.0 )then
            y = 2.0
        else if( x >= 1.0 .and. x < 2.0 )then
            y = 1
        end if
    end function get_y

    subroutine get_coeff(ini, fin, pas, n_coefs, lst_coeff_a, lst_coeff_b)
        real,       intent(in)      :: ini, fin, pas
        integer,    intent(in)      :: n_coefs
        real, dimension(:), allocatable, intent(in out) :: lst_coeff_a, lst_coeff_b
        real                    :: y, x, elem_a, elem_b, coeff_a, coeff_b
        integer                 :: i, m, n
        lst_coeff_a = 0.0
        lst_coeff_b = 0.0
        m = int((fin - ini) / pas)
        do n = 0, n_coefs, 1
            coeff_a = 0.0
            coeff_b = 0.0
            do i = 0, m - 1, 1
                x = ini + real(i) * pas
                y = get_y(x)

                elem_a = y * cos(real(n) * pi * x)
                elem_b = y * sin(real(n) * pi * x)
                coeff_a = coeff_a + pas * elem_a
                coeff_b = coeff_b + pas * elem_b
            end do
            lst_coeff_a(n) = coeff_a
            lst_coeff_b(n) = coeff_b
        end do

    end subroutine get_coeff

    function get_f(x, n_coefs, lst_a, lst_b) result(f)
        real,       intent(in)                      :: x
        integer,    intent(in)                      :: n_coefs
        real, dimension(:), allocatable, intent(in) :: lst_a, lst_b
        real                                :: f, sumat, t = 2.0
        real                                :: monom_1, monom_2
        integer                             :: n

        sumat = 0.0
        do n = 1, n_coefs, 1
            monom_1 = lst_a(n) * cos(2.0 * real(n) * pi * x / t)
            monom_2 = lst_b(n) * sin(2.0 * real(n) * pi * x / t)
            sumat = sumat + monom_1 + monom_2
        end do
        f = (lst_a(0) / 2.0) + sumat
    end function get_f

end module calc_tools


program evaluate
    use calc_tools,                only : get_coeff, get_f
    implicit none
    real,   dimension(:),   allocatable :: lst_a, lst_b
    integer                             :: n, n_coefs = 3
    real                                :: s_f, x

    allocate(lst_a(0:n_coefs))
    allocate(lst_b(0:n_coefs))

    call get_coeff(0.0, 2.0, 0.01, n_coefs, lst_a, lst_b)
    write(*,*) "lst_a = ", lst_a
    write(*,*) "lst_b = ", lst_b

    write(*,*) "lst_a(0) = ", lst_a(0)

    open(unit=2, status='replace', file='table.dat', action='write')
    do n = 0, 1000, 1
        x = real(n) / 150.0
        s_f = get_f(x, n_coefs, lst_a, lst_b)
        write(2,*) x, s_f
    end do
    close(2)

end program evaluate
