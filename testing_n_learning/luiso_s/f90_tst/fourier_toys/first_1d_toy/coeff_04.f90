module calc_tools
    implicit none
        real    ::  pi = 3.14159235358
    contains

    function get_y(x) result(y)
        real,  intent(in)       :: x
        real                    :: y
        if(x >= 0.0 .and. x < 1.0 )then
            y = 2.0
        else if( x >= 1.0 .and. x < 2.0 )then
            y = 1.0
        end if
    end function get_y

    subroutine get_coeff(ini, fin, pas, n_coefs, lst_comp)
        real,       intent(in)      :: ini, fin, pas
        integer,    intent(in)      :: n_coefs
        complex, dimension(:), allocatable, intent(in out)  :: lst_comp
        real                    :: y, x, elem_a, elem_b, coeff_a, coeff_b
        integer                 :: i, m, n
        lst_comp = complex(0.0, 0.0)
        m = int((fin - ini) / pas)
        do n = -n_coefs, n_coefs, 1
            coeff_a = 0.0
            coeff_b = 0.0
            do i = 0, m - 1, 1
                x = ini + real(i) * pas
                y = get_y(x)

                elem_a = y * cos(real(n) * pi * x)
                elem_b = y * sin(real(n) * pi * x)
                coeff_a = coeff_a + pas * elem_a
                coeff_b = coeff_b + pas * elem_b
            end do
            lst_comp(n) = complex(coeff_a / 2.0, coeff_b / 2.0)

        end do

    end subroutine get_coeff

    function get_f(x, n_coefs, lst_coeff) result(suma)
        real,       intent(in)                          :: x
        integer,    intent(in)                          :: n_coefs
        complex, dimension(:), allocatable, intent(in)  :: lst_coeff
        complex                             :: suma
        real                                :: t = 2.0
        integer                             :: n

        suma = complex(0.0, 0.0)
        do n = -n_coefs, n_coefs, 1
            suma = suma + lst_coeff(n) * exp(2.0 * pi * complex(0.0, 1.0) * real(n) * x / t)
        end do
        write(*,*) "suma =", suma
    end function get_f

end module calc_tools


program evaluate
    use calc_tools,                only : get_coeff, get_f
    implicit none
    complex,   dimension(:),   allocatable  :: lst_coeff
    integer                                 :: n, n_coefs = 35
    real                                    :: x
    complex                                 :: s_f
    allocate(lst_coeff(-n_coefs:n_coefs))

    call get_coeff(0.0, 2.0, 0.01, n_coefs, lst_coeff)

    write(*,*) "lst_coeff = ", lst_coeff
    write(*,*) "lst_coeff(0) = ", lst_coeff(0)

    open(unit=2, status='replace', file='table4.dat', action='write')
    do n = -500, 500, 1
        x = real(n) / 150.0
        s_f = get_f(x, n_coefs, lst_coeff)

        write(*,*) "s_f =", s_f


        write(2,*) x, real(s_f), aimag(s_f)
    end do
    close(2)

end program evaluate
