module calc_tools
    implicit none
        real    ::  pi = 3.14159235358
    contains

    function get_y(x) result(y)
        real,  intent(in)       :: x
        real                    :: y
        if(x >= 0.0 .and. x < 1.0 )then
            y = 2.0
        else if( x >= 1.0 .and. x < 2.0 )then
            y = 1
        end if
    end function get_y

    subroutine get_coeff(ini, fin, pas, add_i, coeff_a, coeff_b)
        real, intent(in)        :: ini, fin, pas
        real, intent(out)       :: add_i, coeff_a, coeff_b
        real                    :: y, x, elem_a, elem_b
        integer                 :: i, n
        add_i = 0.0
        coeff_a = 0.0
        coeff_b = 0.0
        n = int((fin - ini) / pas)
        do i = 0, n - 1, 1
            x = ini + real(i) * pas
            y = get_y(x)
            add_i = add_i + pas * y
            elem_a = y * sin(3.0 * pi * x)
            elem_b = y * cos(3.0 * pi * x)
            coeff_a = coeff_a + pas * elem_a
            coeff_b = coeff_b + pas * elem_b
        end do
    end subroutine get_coeff
end module calc_tools

program evaluate
    use calc_tools,                only : get_coeff
    implicit none
    real                          :: area, sum_i, lst_a, lst_b
    call get_coeff(0.0, 2.0, 0.00001, sum_i, lst_a, lst_b)
    write(*,*) "sum_i = ", sum_i
    write(*,*) "lst_a, lst_b = ", lst_a, lst_b
end program evaluate
