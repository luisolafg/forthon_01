import numpy as np
import matplotlib.pyplot as plt
import os
os.system('cls')
if __name__ == "__main__":
    print("\nFourier representation of a 3D (periodic) function")
    tx = 1.0; ty = 1.0; tz = 1.0
    tam = 256; paso = 30

    rho = np.zeros((tam, tam, tam), dtype = float)
    f = np.zeros((tam, tam), dtype = float)
    rho_fft = np.zeros((tam, tam, tam), dtype = complex)
    rho_calc = np.zeros((tam, tam, tam), dtype = float)
    f_calc = np.zeros((tam, tam), dtype = float)

    rho[int(0.2*tam):int(0.4*tam),int(0.2*tam):int(0.4*tam) \
        ,int(0.2*tam):int(0.4*tam)] = 1.0

    rho_fft = np.fft.rfftn(rho, norm = 'forward')

    rho_calc = np.fft.irfftn(rho_fft, norm = 'forward')
    #rho_calc = np.fft.irfftn(rho_fft*np.conjugate(rho_fft), norm = 'forward')

    print("rho shape =", rho.shape)
    print("rho_fft shape =", rho_fft.shape)
    print("rho_calc shape =", rho_calc.shape)

    print("Initial distribution:")
    for l in range(0, tam, paso):
        print("level =",l)
        for h in range(tam):
            for k in range(tam):
                f[h, k] = rho[h, k, l]
        plt.imshow(f,cmap = "Greys")
        plt.show()

    print("Now calculated:")
    for l in range(0, tam, paso):
        print("level =",l)
        for h in range(tam):
            for k in range(tam):
                f_calc[h, k] = rho_calc[h, k, l]
        plt.imshow(f_calc,cmap = "Greys")
        plt.show()


