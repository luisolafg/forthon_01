import numpy as np
import matplotlib.pyplot as plt#
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D

def expon(h, k, x, y):
    w = exp(2.0*pi*complex(0.0,1.0)*(h*x/tx+k*y/ty))
    return w

def double_Integral(xmin, xmax, ymin, ymax, nx, ny, A):
    dS = ((xmax-xmin)/(nx-1)) * ((ymax-ymin)/(ny-1))
    A_Internal = A[1:-1, 1:-1]
    # sides: up, down, left, right
    (A_u, A_d, A_l, A_r) = (A[0, 1:-1], A[-1, 1:-1], A[1:-1, 0], A[1:-1, -1])
    # corners
    (A_ul, A_ur, A_dl, A_dr) = (A[0, 0], A[0, -1], A[-1, 0], A[-1, -1])
    return dS * (np.sum(A_Internal)\
                + 0.5 * (np.sum(A_u) + np.sum(A_d) + np.sum(A_l) + np.sum(A_r))\
                + 0.25 * (A_ul + A_ur + A_dl + A_dr))

if __name__ == "__main__":
    print("Fourier representation of a 2D periodic function")
    tx = 1.0; ty = 1.0
    tam = 64
    hkmax = 30
    hmin = -hkmax; hmax = hkmax
    kmin = -hkmax; kmax = hkmax
    x, y = np.meshgrid(np.linspace (0, tx, tam) , np.linspace(0, ty, tam))
    z = np.zeros((tam, tam), dtype = float)
    f = np.zeros((tam, tam), dtype = complex)
    c = np.zeros((tam, tam), dtype = complex)
    zfr = np.zeros((tam, tam), dtype = float)

    z[int(0.2*tam):int(0.4*tam),int(0.2*tam):int(0.4*tam)] = 1.0

    zmin = np.min(z); zmax = np.max(z)

    plt.imshow(z,cmap = "Greys_r")
    plt.show()

    for h in range(hmin, hmax+1):
        for k in range(kmin, kmax+1):
            f = z * expon(h, k, x, y)
            c[h, k] = double_Integral(0, tx, 0, ty, tam, tam, f)
            #print("h, k, c[h, k] = ", h, k, c[h, k])
    print()
    zf = np.zeros((tam, tam), dtype = complex)
    for h in range(hmin, hmax+1):
        for k in range(kmin, kmax+1):
            zf = zf + c[h, k] * expon(-h, -k, x, y)
    zfs = np.real(zf)

    print("\nCalculating the Fast Fourier Transform:")
    b = np.fft.rfft2(z, norm ='forward')
    print("h k     FFT[h, k]        c[h,k]")
    for h in range(0, hmax+1):
        for k in range(0, kmax+1):
            print(h, k, '%0.4f  %0.4fi'%(b[h, k].real,b[h, k].imag), \
                ' %0.4f  %0.4fi'%(c[h, k].real,c[h, k].imag))

    #Fourier inverse transformation = Fourier synthesis
    #zfr = np.fft.irfft2(b,s =(tam, tam), norm='forward')
    zfr = np.fft.irfft2(b*np.conjugate(b),s =(tam, tam), norm='forward')

    #minzfr = np.min(zfr); maxzfr = np.max(zfr)
    print("\nzfr =", zfr)
    print("len & shape(x) =", len(x), x.shape)
    print("len & shape(z) =", len(z), z.shape)
    print("len & shape(b) =", len(b), b.shape)
    print("len & shape(zfr) =", len(zfr), zfr.shape)

    plt.imshow(zfr,cmap = "Greys_r")

    #ax.set_xlim(0, tx); ax.set_ylim(0, ty); ax.set_zlim(minzfr, maxzfr)
    plt.show()


