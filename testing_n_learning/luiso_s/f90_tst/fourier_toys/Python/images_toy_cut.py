import numpy as np
import matplotlib.pyplot as plt

import fabio
import os

if __name__ == "__main__":

    b = fabio.open("brenda.jpg").data.astype("float64")
    print("len & shape(b) =", len(b), b.shape)
    plt.imshow(b)
    plt.show()

    l = fabio.open("luiso.jpg").data.astype("float64")
    print("len & shape(l) =", len(l), l.shape)
    plt.imshow(l)
    plt.show()

    lc = fabio.open("luiso.jpg").data.astype("float64")[0:925, 0:773]
    print("len & shape(lc) =", len(lc), lc.shape)
    plt.imshow(lc)
    plt.show()
