
import sympy as sp
from sympy import E, I, pi
print('Double integral computed by SymPy definite integrate')
print('Coefs C11 de Fourier de funcion escalon')

x, y = sp.symbols('x y')
f_cos = sp.cos(2.0 * pi * (x + y))
f_sin = sp.sin(2.0 * pi * (x + y))
#f_cos = sp.cos(2.0 * pi * (x))
#f_sin = sp.sin(2.0 * pi * (x))

ya = 0.
yb = 0.5
xa = 0.
xb = 0.5

integral_cos = sp.integrate(f_cos, (x, xa, xb), (y, ya, yb))
integral_cos = integral_cos.evalf()
print('integral coseno = ', integral_cos)

integral_sin = sp.integrate(f_sin, (x, xa, xb), (y, ya, yb))
integral_sin = integral_sin.evalf()
print('integral seno = ', integral_sin)
