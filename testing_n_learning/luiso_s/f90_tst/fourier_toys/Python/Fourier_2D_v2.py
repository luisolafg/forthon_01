import numpy as np
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D

def expon(h, k, x, y):
    w = exp(2.0*pi*complex(0.0,1.0)*(h*x/tx+k*y/ty))
    return w

def double_Integral(xmin, xmax, ymin, ymax, nx, ny, A):
    dS = ((xmax-xmin)/(nx-1)) * ((ymax-ymin)/(ny-1))
    A_Internal = A[1:-1, 1:-1]
    # sides: up, down, left, right
    (A_u, A_d, A_l, A_r) = (A[0, 1:-1], A[-1, 1:-1], A[1:-1, 0], A[1:-1, -1])
    # corners
    (A_ul, A_ur, A_dl, A_dr) = (A[0, 0], A[0, -1], A[-1, 0], A[-1, -1])
    return dS * (np.sum(A_Internal)\
                + 0.5 * (np.sum(A_u) + np.sum(A_d) + np.sum(A_l) + np.sum(A_r))\
                + 0.25 * (A_ul + A_ur + A_dl + A_dr))

if __name__ == "__main__":
    import os
    os.system('cls')
    print("Fourier representation of a 2D periodic function")
    tx = 1.0; ty = 1.0
    hkmax = 1
    hmin = -hkmax; hmax = hkmax
    kmin = -hkmax; kmax = hkmax

    #x, y = np.mgrid[0.0:1.0:101j, 0.0:1.0:101j]
    x, y = np.meshgrid(np.linspace (0, tx, 101) , np.linspace(0, ty, 101))
    '''
    Alternativas exploadas para los arreglos
    x, y = (np.linspace (0, tx, 101) , np.linspace(0, ty, 101))
    x = np.linspace (0, tx, 101)
    y = np.linspace (0, ty, 101)
    x = np.zeros((101), dtype = float)
    y = np.zeros((101), dtype = float)
    for i in range(101):
        x[i] = i * 0.01
        y[i] = i * 0.01
    '''
    print("x =",x)
    #(x,y)=np.meshgrid(x,y)
    print("\nx =", x)
    z = np.zeros((101, 101), dtype = float)
    f = np.zeros((101, 101), dtype = complex)
    c = np.zeros((101, 101), dtype = complex)
    zfr = np.zeros((101, 101), dtype = float)
    for i in range(100):
        for j in range(51):
            z[i, j] = 1.0
            #print("i,j,x[i],y[j],z[i,j]:",i,j,x[i],y[j],z[i,j])
        for j in range(50,101):
            z[i, j] = 0.0
            #print("i,j,x[i],y[j],z[i,j]:",i,j,x[i],y[j],z[i,j])

    for i in range(50, 101):
        for j in range(101):
            z[i, j] = 0.0
    zmin = np.min(z); zmax = np.max(z)

    fig= plt.figure()
    ax = Axes3D(fig)
    ax.plot_surface(x, y, z, cmap='viridis', linewidths=0.2)
    ax.set_xlim(0, tx); ax.set_ylim(0, ty); ax.set_zlim(zmin, zmax)
    plt.show()

    for h in range(hmin, hmax+1):
        for k in range(kmin, kmax+1):
            f = z * expon(h, k, x, y)
            c[h, k] = double_Integral(0, tx, 0, ty, 101, 101, f)
            print("h, k, c[h, k] = ", h, k, c[h, k])

    zf = np.zeros((101, 101), dtype = complex)

    for h in range(hmin, hmax+1):
        for k in range(kmin, kmax+1):
            zf = zf + c[h, k] * expon(-h, -k, x, y)

    zfr = np.real(zf)
    #zfr = (np.abs(zfr))**2
    minzfr = np.min(zfr); maxzfr = np.max(zfr)
    #print(zf)
    #(x,y)=np.meshgrid(x,y)
    fig= plt.figure()
    ax = Axes3D(fig)
    ax.plot_surface(x, y, zfr, cmap='viridis', linewidths=0.2)
    ax.set_xlim(0, tx); ax.set_ylim(0, ty); ax.set_zlim(minzfr, maxzfr)
    plt.show()
