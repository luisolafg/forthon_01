import numpy as np
import matplotlib.pyplot as plt
import fabio
import os
back_dir = ".." + os.sep
img1_path = back_dir * 5 + "testing_n_learning" + os.sep + "img_data" + os.sep \
+ "APT73_ddd_no_direct_beam.edf"
img_arr = fabio.open(img1_path).data.astype("float64")

plt.imshow(img_arr, interpolation = "nearest" )
plt.show()
