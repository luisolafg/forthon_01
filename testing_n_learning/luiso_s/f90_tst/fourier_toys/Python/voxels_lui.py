"""
==========================
3D voxel / volumetric plot
==========================

Demonstrates plotting 3D volumetric objects with `.Axes3D.voxels`.
"""

import matplotlib.pyplot as plt
import numpy as np


voxelarray = np.zeros((7, 7, 7), dtype = float)

voxelarray[0:3, 0:3, 0:3] = 1.0
voxelarray[2:4, 2:4, 2:4] = 2.0
voxelarray[3:6, 3:6, 3:6] = 300.0


print("voxelarray =\n", voxelarray)

# and plot everything
ax = plt.figure().add_subplot(projection='3d')
ax.voxels(voxelarray, edgecolor='k')
plt.show()
