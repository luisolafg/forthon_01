
import sympy as sp
from sympy import E, I, pi
#print(E, pi, sp.exp(I * pi))
#print(2.0 * pi, sp.exp(1.0))

print('Triple integral computed by SymPy definite integrate')
#print('Example 2-03 definite')
print('Coef C11 de Fourier de funcion escalon')

x, y, z = sp.symbols('x y z')
f_cos = sp.cos(2.0 * pi * (x + y + z))
f_sin = sp.sin(2.0 * pi * (x + y + z))

ya = -0.25
yb = 0.25
xa = -0.25
xb = 0.25
za = -0.25
zb = 0.25
integral_cos = sp.integrate(f_cos, (x, xa, xb), (y, ya, yb), (z, za, zb))
integral_cos = integral_cos.evalf()
print('integral coseno = ', integral_cos)

integral_sin = sp.integrate(f_sin, (x, xa, xb), (y, ya, yb), (z, za, zb))
integral_sin = integral_sin.evalf()
print('integral seno = ', integral_sin)
