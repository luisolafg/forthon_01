import numpy as np
import matplotlib.pyplot as plt
import fabio

def get_abs_n_phase(fft_in):
    fft_abs = np.absolute(fft_in)
    p = np.arctan2(np.imag(fft_in), np.real(fft_in))
    fft_phase = np.exp(complex(0.0,1.0) * p)
    return fft_abs, fft_phase

def get_recalc(fft_abs, fft_phase):
    bb = fft_abs * fft_phase
    bb_recal = np.fft.irfft2(bb, norm='forward')
    return bb_recal

if __name__ == "__main__":
    print("Fourier representation of a 2D (periodic) function")
    print("Determining the initial function for Brenda")
    zb = fabio.open("brenda.jpg").data.astype("float64")
    print("zb.shape =",zb.shape, "\n")
    print("Plotting the initial function")
    plt.imshow(zb,cmap = "Greys_r")
    plt.show()

    print("Calculating the Fast Fourier Transform (Brenda)")
    zb_fft = np.fft.rfft2(zb, norm ='forward')
    print("zb_fft.shape =",zb_fft.shape, "\n")

    print("Calculating the Fourier Inverse Transformation")
    zb_recal = np.fft.irfft2(zb_fft, norm='forward')
    print("Plotting the recalculated function")
    plt.imshow(zb_recal,cmap = "Greys_r")
    plt.show()
    print("zb_recal.shape =",zb_recal.shape)

    zb_fft_abs ,zb_fft_phase = get_abs_n_phase(zb_fft)
    print("zb_fft_abs.shape =", zb_fft_abs.shape)
    print("zb_ff_phase.shape =",zb_fft_phase.shape)

    zbb_recal = get_recalc(zb_fft_abs, zb_fft_phase)

    plt.imshow(zbb_recal,cmap = "Greys_r")
    plt.show()

    print("Determining the initial function for Luiso")
    zl = fabio.open("luiso.jpg").data.astype("float64")[0:925, 0:773]
    print("len & shape(zl) =", len(zl), zl.shape)
    plt.imshow(zl, cmap = "Greys_r")
    plt.show()

    print("Calculating the Fast Fourier Transform (Luiso)")
    zl_fft = np.fft.rfft2(zl, norm ='forward')

    zl_fft_abs ,zl_fft_phase = get_abs_n_phase(zl_fft)

    #mix1 = get_recalc(zb_fft_abs, zl_fft_phase)
    #mix2 = get_recalc(zl_fft_abs, zb_fft_phase)
    mix1 = np.fft.irfft2(zb_fft*np.conjugate(zb_fft), norm='forward')
    mix2 = np.fft.irfft2(zl_fft*np.conjugate(zl_fft), norm='forward')

    plt.imshow(mix1, cmap = "Greys_r")
    plt.show()

    plt.imshow(mix2, cmap = "Greys_r")
    plt.show()



