from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

import sys
import numpy as np

def crunch_min_max(data2d, i_min_max):
    data2d_ini = np.copy(data2d)
    if(i_min_max == [None, None]):
        i_min_max = [data2d_ini.min(), data2d_ini.max()]
        print("no max and min provided, assuming:" + str(i_min_max))

    elif(i_min_max[0] > data2d_ini.min() or i_min_max[1] < data2d_ini.max()):
        np.clip(data2d_ini, i_min_max[0], i_min_max[1], out = data2d_ini)

    width = np.size( data2d_ini[0:1, :] )
    height = np.size( data2d_ini[:, 0:1] )

    #data2d_pos = data2d_ini[:,:] - i_min_max[0] + 1.0
    data2d_pos = data2d_ini[:,:] - i_min_max[0]
    data2d_pos_max = data2d_pos.max()

    #calc_pos_max = i_min_max[1] - i_min_max[0] + 1.0
    calc_pos_max = i_min_max[1] - i_min_max[0]
    if(calc_pos_max > data2d_pos_max):
        data2d_pos_max = calc_pos_max

    return data2d_pos, data2d_pos_max, width, height


class np2bmp_monocrome(object):
    def __init__(self):
        self.all_chan_byte = np.empty( (255), 'int')
        for i in range(255):
            self.all_chan_byte[i] = i

    def img_2d_rgb(
        self, data2d = None, invert = False, i_min_max = [None, None]
    ):
        data2d_pos, data2d_pos_max, self.width, self.height = crunch_min_max(
            data2d, i_min_max
        )

        div_scale = 254.0 / data2d_pos_max
        data2d_scale = np.multiply(data2d_pos, div_scale)
        if(invert == True):
            for x in np.nditer(
                data2d_scale[:,:], op_flags=['readwrite'],
                flags=['external_loop']
            ):
                x[...] = 254.0 - x[...]

        img_array = np.zeros([self.height, self.width, 4], dtype=np.uint8)
        img_all_chanl = np.empty( (self.height, self.width), 'int')
        scaled_i = np.empty( (self.height, self.width), 'int')
        scaled_i[:,:] = data2d_scale[:,:]

        img_all_chanl[:,:] = scaled_i[:,:]
        for x in np.nditer(
            img_all_chanl[:,:], op_flags=['readwrite'],
            flags=['external_loop']
        ):
            x[...] = self.all_chan_byte[x]

        img_array[:, :, 3] = 255
        img_array[:, :, 2] = img_all_chanl[:,:] #Blue
        img_array[:, :, 1] = img_all_chanl[:,:] #Green
        img_array[:, :, 0] = img_all_chanl[:,:] #Red
        return img_array

def get_ini_img(cub_res_in):


    arr_3d_out = np.zeros(
        (cub_res_in, cub_res_in, cub_res_in), dtype = np.float64
    )

    arr_3d_out[
        int(0.2*cub_res_in):int(0.4*cub_res_in),
        int(0.2*cub_res_in):int(0.4*cub_res_in),
        int(0.2*cub_res_in):int(0.4*cub_res_in)
    ] = 1.0

    arr_3d_out[
        int(0.5*cub_res_in):int(0.6*cub_res_in),
        int(0.5*cub_res_in):int(0.6*cub_res_in),
        int(0.5*cub_res_in):int(0.6*cub_res_in)
    ] = 1.0

    print("len & shape(arr_3d_out) =", len(arr_3d_out), arr_3d_out.shape)
    return arr_3d_out

def get_irfft_res(rho):
    print("rho[..] =", rho[80,80,80])

    b = np.fft.rfftn(rho)
    print("len & shape(b) =", len(b), b.shape)
    #arr_3d_out= np.fft.irfftn(b)
    arr_3d_out= np.fft.irfftn(b*np.conjugate(b))
    print("len & shape(arr_3d) =", len(arr_3d_out), arr_3d_out.shape)
    return arr_3d_out
    return rho_calc

class TmpTstWidget(QWidget):
    def __init__(self, parent = None):
        super(TmpTstWidget, self).__init__()

        self.bmp_m_cro = np2bmp_monocrome()
        self.cub_res = 256
        self.siz_scl = 1

        self.np_full_img = np.zeros((self.cub_res, self.cub_res), dtype = np.float64)

        self.draw_widget = QLabel("drawing here")
        self.but4z_in = QPushButton("Zoom In")
        self.but4z_out = QPushButton("Zoom Out")
        self.but_fft = QPushButton("Do the FFT thing")
        self.slide1 = QSlider()
        self.slide1.setMaximum(0)
        self.slide1.setMaximum(self.cub_res - 1)

        self.but4z_in.clicked.connect(self.butt_zoom_in_clicked)
        self.but4z_out.clicked.connect(self.butt_zoom_out_clicked)
        self.but_fft.clicked.connect(self.butt_fft_clicked)
        self.slide1.valueChanged.connect(self.reload_img)

        my_main_box = QHBoxLayout()

        my_h_box = QHBoxLayout()
        my_h_box.addWidget(self.but4z_in)
        my_h_box.addWidget(self.but4z_out)
        my_h_box.addWidget(self.but_fft)

        my_v_box = QVBoxLayout()
        my_v_box.addLayout(my_h_box)
        my_v_box.addWidget(self.draw_widget)

        my_main_box.addLayout(my_v_box)
        my_main_box.addWidget(self.slide1)


        self.arr_3d = get_ini_img(self.cub_res)
        self.i_min_max = [self.arr_3d.min(), self.arr_3d.max()]

        self.setLayout(my_main_box)
        self.show()

    def butt_zoom_in_clicked(self):
        self.siz_scl *= 1.4
        print("self.siz_scl =", self.siz_scl)
        self.set_pixmap()

    def butt_zoom_out_clicked(self):
        self.siz_scl *= 0.8
        print("self.siz_scl =", self.siz_scl)
        self.set_pixmap()

    def butt_fft_clicked(self):
        print("butt_fft_clicked")
        self.arr_3d = get_irfft_res(self.arr_3d)
        self.i_min_max = [self.arr_3d.min(), self.arr_3d.max()]

    def reload_img(self, new_value):
        self.np_full_img[:,:] = self.arr_3d[:,:,int(new_value)]
        self.set_pixmap()

    def set_pixmap(self):
        rgb_np = self.bmp_m_cro.img_2d_rgb(
            data2d = self.np_full_img, invert = False,
            i_min_max = self.i_min_max
        )
        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )
        new_pixmap = QPixmap.fromImage(q_img).scaled(
            self.cub_res * self.siz_scl, self.cub_res * self.siz_scl
        )
        self.draw_widget.setPixmap(new_pixmap)


if( __name__ == "__main__" ):
    app =  QApplication(sys.argv)
    ex = TmpTstWidget()
    sys.exit(app.exec_())
