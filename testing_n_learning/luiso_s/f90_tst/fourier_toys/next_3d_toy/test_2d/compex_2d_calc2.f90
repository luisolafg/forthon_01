module calc_tools
    implicit none
        real            ::  pi = 3.14159235358
        real            :: ini_x, ini_y, fin_x, fin_y, dx, dy
        integer         :: hk_min, hk_max, nx, ny
        real            :: tx, ty
    contains

    function get_z(x, y) result(z)
        real,  intent(in)       :: x, y
        real                    :: mid_p_x, mid_p_y, z
        mid_p_x = (ini_x + fin_x) / 2.0
        mid_p_y = (ini_y + fin_y) / 2.0
        if(x >= ini_x .and. y >= ini_y .and. x <= mid_p_x  .and. y <= mid_p_y )then
            z = 1.0
        else
            z = 0.0
        end if
    end function get_z

    subroutine get_coeff(lst_comp)
        complex, dimension(:,:), allocatable, intent(in out)  :: lst_comp
        complex                 :: c, cy
        real                    :: x, y, z
        integer                 :: ix, iy, h, k

        write(*,*) "nx, ny =", nx, ny
        write(*,*) "dx, dy =", dx, dy

        lst_comp = complex(0.0, 0.0)

        do h = hk_min, hk_max, 1
            do k = hk_min, hk_max, 1
                c = complex(0.0,0.0)

                do ix = 0, nx, 1
                    x = ini_x + real(ix) * dx
                    cy = complex(0.0,0.0)
                    do iy = 0, ny, 1
                        y = ini_y + real(iy) * dy
                        z = get_z(x, y)
                        cy = cy + z * exp(2.0 * pi * complex(0.0, 1.0) * &
                        (real(h) * ( x / tx) + real(k) * ( y / ty ) ) ) * dy
                    end do
                    write(*,*) "h, k, x, cy =", h, k, x, cy
                    c  = c + cy * dx
                end do
                write(*,*) "h, k, c =", h, k, c
                lst_comp(h, k) = c
            end do
        end do

    end subroutine get_coeff

    function get_f(x, y, lst_coeff) result(suma)
        real,       intent(in)                          :: x, y
        complex, dimension(:,:), allocatable, intent(in)  :: lst_coeff
        complex                             :: suma
        integer                             :: h, k

        suma = complex(0.0, 0.0)
        do h = hk_min, hk_max, 1
            do k = hk_min, hk_max, 1
                suma = suma + lst_coeff(h, k) * &
                exp( -2.0 * pi * complex(0.0, 1.0) * &
                (real(h) * ( x / tx) + real(k) * ( y / ty )))
            end do
        end do
        suma = suma / (tx * ty)
    end function get_f

end module calc_tools


program evaluate
    use calc_tools
    implicit none
    complex,  dimension(:,:),  allocatable  :: lst_coeff
    complex                                 :: z_c
    real                                    :: x, y!, yy(100), zz(100)
    integer                                 :: i, j

    hk_min = -2
    hk_max = 2
    allocate(lst_coeff(hk_min:hk_max,hk_min:hk_max))
    nx = 2
    ny = 2
    ini_x = 0.0
    fin_x = 1.0
    ini_y = 0.0
    fin_y = 1.0
    dx = (fin_x - ini_x) / nx
    dy = (fin_y - ini_y) / ny
    tx = fin_x - ini_x
    ty = fin_y - ini_y
    write(*,*) "dx, dy =", dx, dy
    call get_coeff(lst_coeff)
    write(*,*) "dx, dy =", dx, dy
    !n_pl = nx * ny
    open(unit=2, status='replace', file='table.dat', action='write')
    do i = -nx * 20, nx * 20, 1
        !write(*,*)'x:', x
        !write(*,*)'y:'
        do j = -ny * 20, ny * 20, 1
            x = ini_x + real(i) * dx * 0.1
            y = ini_y + real(j) * dy * 0.1
            !yy(j) = y
            z_c = get_f(x, y, lst_coeff)
            !zz(j) = real(z_c)
            !write(*,*) x, y, real(z_c)
            !write(*,*) 'x, y:', x, y
            write(2,*) x, y, real(z_c)
        end do
        !write(*,'(10f8.3)') (yy(j), j = 1, ny)
        !write(*,*)'z_c:'
        !write(*,'(10f8.3)') (real(zz(j)), j = 1, ny)
    end do
    close(2)

end program evaluate
