from matplotlib import pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
if __name__== "__main__":
    x_list = np.loadtxt('table.dat', usecols = 0)
    y_list = np.loadtxt('table.dat', usecols = 1)
    z_list = np.loadtxt('table.dat', usecols = 2)

    N = int(np.sqrt(len(z_list)))
    z = z_list.reshape(N, N)
    '''
    plt.imshow(
        z, extent=(
            np.amin(x_list), np.amax(x_list), np.amax(y_list), np.amin(y_list)
        ), norm=LogNorm(), aspect = 'auto'
    )
    '''

    plt.imshow(
        z, extent=(
            np.amin(x_list), np.amax(x_list), np.amax(y_list), np.amin(y_list)
        )
    )


    plt.colorbar()
    plt.show()
