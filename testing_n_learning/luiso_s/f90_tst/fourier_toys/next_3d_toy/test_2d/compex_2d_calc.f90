module calc_tools
    implicit none
        real    ::  pi = 3.14159235358
    contains

    function get_z(x, y, ini_x, ini_y, fin_x, fin_y) result(z)
        real,  intent(in)       :: x, y, ini_x, ini_y, fin_x, fin_y
        real                    :: mid_p_x, mid_p_y, z
        mid_p_x = (ini_x + fin_x) / 2.0
        mid_p_y = (ini_y + fin_y) / 2.0
        if(x >= ini_x .and. y >= ini_y .and. x <= mid_p_x  .and. y <= mid_p_y )then
            z = 1.0
        else
            z = 0.0
        end if
    end function get_z

    !subroutine get_coeff(ini_x, ini_y, fin_x, fin_y, dif, n_coefs, lst_comp)
    subroutine get_coeff(ini_x, ini_y, fin_x, fin_y, dx, dy, hk_min, hk_max, lst_comp)
        real,       intent(in)      :: ini_x, ini_y, fin_x, fin_y, dx, dy
        integer,    intent(in)      :: hk_min, hk_max
        complex, dimension(:,:), allocatable, intent(in out)  :: lst_comp
        complex                 :: c, cy
        real                    :: x, y, z
        real                    :: tx, ty
        integer                 :: ix, iy, h, k, nx, ny

        nx = int((fin_x - ini_x) / dx)
        ny = int((fin_y - ini_y) / dy)

        tx = fin_x - ini_x
        ty = fin_y - ini_y

        lst_comp = complex(0.0, 0.0)

        do h = hk_min, hk_max, 1
            do k = hk_min, hk_max, 1
                c = complex(0.0,0.0)
                do ix = 1, nx, 1
                    cy = complex(0.0,0.0)
                    do iy = 1, ny, 1
                        x = ini_x + real(ix) * dx
                        y = ini_y + real(iy) * dy
                        z = get_z(x, y, ini_x, ini_y, fin_x, fin_y)
                        cy = cy + z * exp(2.0 * pi * complex(0.0, 1.0) * &
                        (real(h) * ( x / tx) + real(k) * ( y / ty )) * dy)

                    end do
                    c  = c + cy * dx
                end do
                write(*,*) "h, k, c =", h, k, c
                lst_comp(h, k) = c
            end do
        end do


    end subroutine get_coeff

    function get_f(x, y, hk_min, hk_max, lst_coeff, ini_x, fin_x, ini_y, fin_y &
                   ) result(suma)
        real,       intent(in)                          :: x, y
        integer,    intent(in)                          :: hk_min, hk_max
        real,       intent(in)                          :: ini_x, fin_x, ini_y, fin_y
        complex, dimension(:,:), allocatable, intent(in)  :: lst_coeff
        complex                             :: suma
        real                                :: tx, ty
        integer                             :: h, k
        tx = fin_x - ini_x
        ty = fin_y - ini_y
        suma = complex(0.0, 0.0)
        do h = hk_min, hk_max, 1
            do k = hk_min, hk_max, 1
            suma = suma + lst_coeff(h, k) * exp( -2.0 * pi * complex(0.0, 1.0) * &
                        (real(h) * ( x / tx) + real(k) * ( y / ty )))
            end do
        end do
    end function get_f

end module calc_tools


program evaluate
    use calc_tools,                only : get_coeff, get_f
    implicit none
    complex,  dimension(:,:),  allocatable  :: lst_coeff
    complex                                 :: z
    integer                                 :: hk_min = -3, hk_max = 3
    real                                    :: ini_x = 0.0, fin_x = 3.0, dx = 0.1
    real                                    :: ini_y = 0.0, fin_y = 3.0, dy = 0.1
    real                                    :: x, y
    integer                                 :: i, j, k, n_pl
    integer                                 :: nx = 9, ny = 9

    allocate(lst_coeff(hk_min:hk_max,hk_min:hk_max))

    call get_coeff(ini_x, ini_y, fin_x, fin_y, dx, dy, hk_min, hk_max, lst_coeff)

    n_pl = nx * ny
    dx = (fin_x - ini_x) / nx
    dy = (fin_y - ini_y) / ny
    open(unit=2, status='replace', file='table.dat', action='write')
    do i = 1, nx, 1
        do j = 1, ny, 1
            k = k + 1
            x = ini_x + real(i) * dx
            y = ini_y + real(j) * dy
            z = get_f(x, y, hk_min, hk_max, lst_coeff, ini_x, fin_x, ini_y, fin_y)
            write(*,*) x, y, real(z)
            write(2,*) x, y, real(z)
        end do
    end do
    close(2)

end program evaluate
