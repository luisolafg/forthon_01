from matplotlib import pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm
from mpl_toolkits.mplot3d import Axes3D

if __name__== "__main__":
    x_list = np.loadtxt('table.dat', usecols = 0)
    y_list = np.loadtxt('table.dat', usecols = 1)
    z_list = np.loadtxt('table.dat', usecols = 2)

    N = int(np.sqrt(len(z_list)))
    x = x_list.reshape(N, N)
    y = y_list.reshape(N, N)
    z = z_list.reshape(N, N)

    fig= plt.figure()
    ax = Axes3D(fig)

    ax.plot_surface(x, y, z, cmap='viridis', linewidths=0.2)

    plt.show()
