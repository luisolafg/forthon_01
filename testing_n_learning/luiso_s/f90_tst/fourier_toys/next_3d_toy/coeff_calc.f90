module calc_tools
    implicit none
        real    ::  pi = 3.14159235358
    contains

    function get_y(x, ini, fin) result(y)
        real,  intent(in)       :: x, ini, fin
        real                    :: mid_p, y
        mid_p = (ini + fin) / 2.0
        if(x >= ini .and. x < mid_p )then
            y = 1.0
        else if( x >= mid_p .and. x < fin )then
            y = 0.0
        end if
    end function get_y

    subroutine get_coeff(ini, fin, pas, n_coefs, lst_comp)
        real,       intent(in)      :: ini, fin, pas
        integer,    intent(in)      :: n_coefs
        complex, dimension(:), allocatable, intent(in out)  :: lst_comp
        real                    :: y, x, elem_a, elem_b
        real                    :: m, coeff_a, coeff_b, t
        integer                 :: i, n
        lst_comp = complex(0.0, 0.0)
        t = fin -ini
        m = t / pas
        do n = -n_coefs, n_coefs, 1
            coeff_a = 0.0
            coeff_b = 0.0

            write(*,*)
            write(*,*) "n= ", n
            write(*,*)

            do i = int(ini), int(m - 1), 1
                x = ini + real(i) * pas

                y = get_y(x, ini, fin)

                write(*,*) "(x,y)= ", x, y

                elem_a = y * cos(2.0 * pi * real(n) * x / t)
                elem_b = y * sin(2.0 * pi * real(n) * x / t)
                coeff_a = coeff_a + pas * elem_a
                coeff_b = coeff_b + pas * elem_b
            end do
            lst_comp(n) = complex(coeff_a / t, coeff_b / t)

        end do

    end subroutine get_coeff

    function get_f(x, n_coefs, lst_coeff, ini, fin) result(suma)
        real,       intent(in)                          :: x
        integer,    intent(in)                          :: n_coefs
        real,       intent(in)                          :: ini, fin
        complex, dimension(:), allocatable, intent(in)  :: lst_coeff
        complex                             :: suma
        real                                :: t
        integer                             :: n
        t = fin - ini
        suma = complex(0.0, 0.0)
        do n = -n_coefs, n_coefs, 1
            suma = suma + lst_coeff(n) * exp( -2.0 * pi * complex(0.0, 1.0) * real(n) * x / t)
        end do
        !write(*,*) "suma =", suma
    end function get_f

end module calc_tools


program evaluate
    use calc_tools,                only : get_coeff, get_f
    implicit none
    complex,   dimension(:),   allocatable  :: lst_coeff
    integer                                 :: n, n_coefs = 15
    real                                    :: x, p_ini = 0.0, p_fin = 3.0
    complex                                 :: s_f
    allocate(lst_coeff(-n_coefs:n_coefs))

    call get_coeff(p_ini, p_fin, 0.1, n_coefs, lst_coeff)

    open(unit=2, status='replace', file='table.dat', action='write')
    do n = -1000, 1000, 1
        x = real(n) / 150.0
        s_f = get_f(x, n_coefs, lst_coeff, p_ini, p_fin)
        !write(*,*) "s_f =", s_f
        write(2,*) x, real(s_f), aimag(s_f)
    end do
    close(2)

end program evaluate
