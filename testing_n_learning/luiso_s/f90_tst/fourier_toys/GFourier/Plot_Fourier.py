# Plotting a Fourier map obtained with Fullprof
import os
os.system('cls')
import numpy as np
import matplotlib.pyplot as plt

data_file = "biyig_x=0.asc"
x1 = np.loadtxt(data_file, usecols = [0])
y1 = np.loadtxt(data_file, usecols = [1])
z1 = np.loadtxt(data_file, usecols = [2])
n = int(np.sqrt(len(x1)))
x = np.linspace(0,1,n)
y = np.linspace(0,1,n)
z = np.zeros((n, n), dtype = float)
for i in range(n):
    for j in range(n):
        #z[i, j]= z1[i * n + j]
        z[j, i]= z1[i * n + j]  # ajuste al formato de GFourier
#plt.contour(x, y, z, colors = 'black')  # plotea solamente curvas de nivel
plt.contourf(x, y, z, 20, cmap='viridis_r') # rellena el espacio entre curvas de nivel
plt.colorbar()
plt.show()
