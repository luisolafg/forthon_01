program read_line

    implicit none

    character(len=128)                      :: texto = ""

    texto = "a=5 b=6 x=w a=x aaaaa=w"
    write(*,*) "texto:", trim(texto)
    WRITE(*,*) ' SCAN(trim(texto), "x") =', SCAN(trim(texto), "x")
    WRITE(*,*) ' SCAN(trim(texto), "=") =', SCAN(trim(texto), "=")
    WRITE(*,*) ' SCAN(trim(texto), "w") =', SCAN(trim(texto), "w")
    WRITE(*,*) ' SCAN(trim(texto), "a") =', SCAN(trim(texto), "a")

    WRITE(*,*) ' SCAN(trim(texto), "y") =', SCAN(trim(texto), "y")
    WRITE(*,*) ' SCAN(trim(texto), ",") =', SCAN(trim(texto), ",")
    WRITE(*,*) ' SCAN(trim(texto), "c") =', SCAN(trim(texto), "y")
    WRITE(*,*) ' SCAN(trim(texto), ":") =', SCAN(trim(texto), ":")
    WRITE(*,*) ' SCAN(trim(texto), "/") =', SCAN(trim(texto), "/")
    WRITE(*,*) ' SCAN(trim(texto), "f") =', SCAN(trim(texto), "f")

end program read_line
