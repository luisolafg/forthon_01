program where_test

    real, dimension(5,5)     :: img_out
    real, dimension(5,5)     :: img_in
    integer                  :: i, j

    img_in = 5.0
    img_in(2:3,2:3) = 8.0
    img_in(3:4,3:4) = img_in(3: 4, 3: 4) + 2.0

    write(*,*) "img_in ="
    write(*,*) transpose(img_in)

    img_out = img_in

    forall (i=2:4, j=2:4) img_out(i,j) = (sum(img_in(i - 1: i + 1, j - 1: j + 1)) &
                                          - img_in(i, j) ) / 8.0

    write(*,*) "img_out ="
    write(*,*) transpose(img_out)

end program where_test
