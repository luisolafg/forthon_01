
program scale_image
    use edf_utils
    implicit none
    character(len=9999)                             :: str_data
    character(len=254)                              :: file_in
    character(len=254)                              :: file_out
    real(kind = 8), dimension(:,:), allocatable     :: my_img
    integer                                         :: i, j, dim1, dim2
    integer                                         :: x1, y1, x2, y2

    write(*,*) "processing command line arguments"
    call com_arg(file_in, x1, y1, x2, y2, file_out)
    write(*,*) "reading image"
    call get_efd_info(file_in, str_data, my_img)
    !write(*,*) "header:", trim(str_data)

    dim1 = ubound(my_img, 1)
    dim2 = ubound(my_img, 2)
    forall (i = x1:x2, j = y1:y2) my_img(i,j) = real(i + j)

    write(*,*) "writing new edf image"
    call write_edf(file_out, my_img)
    write(*,*) "done from f90 code"

end program scale_image

