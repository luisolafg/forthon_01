echo "removing previous binrs and objs"
rm img_marker *.edf

echo "running python img creator"
python write_edf.py

echo "compiling"
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall mark_img.f90

echo "linking"
gfortran -o img_marker *.o

echo "running f90 binary exe"
./img_marker py_write.edf 3,4,9,11 f90_img.edf

echo "running python img reader"
python read_edf.py

rm *.o *.mod
