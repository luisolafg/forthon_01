import numpy as np
import fabio

import numpy as np
from matplotlib import pyplot as plt

if( __name__ == "__main__" ):

    lst_files = ["py_write.edf", "f90_test_img.edf", "f90_img_sum.edf"]
    for file_name in lst_files:
        print "Loading edf file"
        new_img = fabio.open(file_name).data.astype(np.float64)
        print "plotting img"
        plt.imshow( new_img , interpolation = "nearest" )
        print "plt.show"
        plt.show()
        plt.close()
        print "done"

