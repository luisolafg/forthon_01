program edf_write
    use edf_utils
    implicit none
    real(kind = 8), dimension(:,:), allocatable :: my_img
    integer                                     :: dim1, dim2, i, j

    dim1 = 50
    dim2 = 50

    allocate(my_img(dim1, dim2))
    forall (i=5:15, j=15:25) my_img(i,j) = real(i + j)

    call write_edf("f90_test_img.edf", my_img)

end program edf_write

