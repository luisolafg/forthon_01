program add2imgs

    use edf_utils
    implicit none
    real(kind = 8), dimension(:,:), allocatable :: img_f90, img_py, img_out
    integer                                     :: dim1, dim2, i, j
    character(len = 9999)                       :: str_dumy

    call get_efd_info("f90_test_img.edf", str_dumy, img_f90)
    call get_efd_info("py_write.edf", str_dumy, img_py)
    dim1 = ubound(img_f90, 1)
    dim2 = ubound(img_f90, 2)

    allocate(img_out(dim1, dim2))
    forall (i=1:dim1, j=1:dim2) img_out(i, j) = img_f90(i, j) + img_py(i, j)

    call write_edf("f90_img_sum.edf", img_out)



end program add2imgs
