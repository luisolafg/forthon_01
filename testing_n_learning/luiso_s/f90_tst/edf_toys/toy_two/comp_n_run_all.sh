echo "creating image with python"
python write_edf.py

echo "removing previous leftover"
rm img_create img_add f90_test_img.edf *.o *.mod
echo "compiling 1"

gfortran -c -Wall edf_n_cmd_deps.f90
gfortran -c -Wall write_edf.f90
echo "linking 2"
gfortran -o img_create *.o
echo "running"
./img_create

rm img_create *.o *.mod

echo "compiling 2"
gfortran -c -Wall edf_n_cmd_deps.f90
gfortran -c -Wall add_2_images.f90
echo "linking 2"
gfortran -o img_add *.o
echo "running"
./img_add

echo "removing bin exe"
python read_edf.py
