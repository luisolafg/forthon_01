import numpy as np
import fabio

if( __name__ == "__main__" ):

    xres, yres = 60, 50
    arr_out = np.zeros((xres, yres),'float64')

    arr_out[20:40,10:30] = 7.0

    new_img = fabio.edfimage.edfimage()
    new_img.data = arr_out

    new_img.write("py_write.edf")


