import fabio
from matplotlib import pyplot as plt

img_arr = fabio.open("cuted2.edf").data.astype("float64")
plt.imshow(img_arr, interpolation = "nearest" )
plt.show()
