
program img_cut

    use input_arg_tools,                only: handling_kargs, arg_def_list
    use edf_utils,                      only: get_efd_info, write_edf
    use cutting,                        only: cut_img2, img_out

    implicit none

    character(len=9999)                             :: str_data1
    real(kind = 8), dimension(:,:), allocatable     :: img_in

    Type(arg_def_list)              :: my_def

    my_def%num_of_vars = 2
    allocate(my_def%field(1:my_def%num_of_vars))

    my_def%field(1)%var_name = "img_in"
    my_def%field(1)%value = "my_img_in.edf"


    my_def%field(2)%var_name = "img_out"
    my_def%field(2)%value = "cut_img.edf"

    call handling_kargs(my_def)

    write(*,*) "img_in: ", my_def%field(1)%value
    write(*,*) "img_out: ", my_def%field(2)%value

    call get_efd_info(my_def%field(1)%value, str_data1, img_in)
    write(*,*) "header 1:", trim(str_data1)
    write(*,*) " _____________________________________________________________________"


    call cut_img2(img_in)

    call write_edf(my_def%field(2)%value, img_out)


end program img_cut
