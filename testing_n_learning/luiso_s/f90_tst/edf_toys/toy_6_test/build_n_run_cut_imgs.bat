echo removing previous binrs, objs and imgs
del /f cuted.edf
del /f anaelu_img_cut.exe

echo compiling
gfortran -c -O2  -ffree-line-length-none -funroll-loops -Wall input_args.f90
gfortran -c -O2  -ffree-line-length-none -funroll-loops -Wall rw_n_use_edf.f90
gfortran -c -O2  -ffree-line-length-none -funroll-loops -Wall cut.f90

gfortran -c -O2  -ffree-line-length-none -funroll-loops -Wall anaelu_img_cut.f90

echo linking
gfortran -o anaelu_img_cut.exe *.o -static

echo running binary exe
anaelu_img_cut.exe APT73_ddd_no_direct_beam.edf cuted.edf

python img_fabio_test.py

del /f *.o *.mod
