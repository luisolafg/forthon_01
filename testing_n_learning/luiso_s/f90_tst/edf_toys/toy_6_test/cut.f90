!  This file is part of the Anaelu Project
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this
! file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
module cutting
    implicit none

    real(kind=8),  dimension(:,:), allocatable :: img_out
    contains

    subroutine cut_img(img_in)
        real(kind=8),  dimension(:,:), intent (in) :: img_in

        integer                     :: xmax, ymax

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)

        write(*,*) "xmax =", xmax
        write(*,*) "ymax =", ymax

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(xmax, ymax))
        img_out = img_in

        img_out(1:1000,1:1000) = 0
        img_out(1:100,1:100) = 99999

    end subroutine cut_img

    subroutine cut_img2(img_in)
        real(kind=8),  dimension(:,:), intent (in) :: img_in

        integer                     :: xmax, ymax

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)

        write(*,*) "xmax =", xmax
        write(*,*) "ymax =", ymax

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(xmax, ymax))
        img_out = img_in

        img_out(1000:2000,1000:2000) = 0

    end subroutine cut_img2


end module cutting
