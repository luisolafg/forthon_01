echo removing previous binrs, objs and imgs
del /f cuted2.edf
del /f anaelu_img_cut2.exe

echo compiling
gfortran -c -O2  -ffree-line-length-none -funroll-loops -Wall input_args.f90
gfortran -c -O2  -ffree-line-length-none -funroll-loops -Wall rw_n_use_edf.f90
gfortran -c -O2  -ffree-line-length-none -funroll-loops -Wall cut.f90

gfortran -c -O2  -ffree-line-length-none -funroll-loops -Wall anaelu_img_cut2.f90

echo linking
gfortran -o anaelu_img_cut2.exe *.o -static

echo running binary exe
anaelu_img_cut2.exe cuted.edf cuted2.edf

python img_fabio_test2.py

del /f *.o *.mod
