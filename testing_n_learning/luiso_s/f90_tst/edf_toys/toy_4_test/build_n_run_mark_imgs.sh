echo "removing previous binrs and objs"
rm img_cutter *.edf

echo "compiling"
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall big_img_cut.f90

echo "linking"
gfortran -o img_cutter *.o

echo "running f90 binary exe"
./img_cutter py_write.edf 3,4,9,11 f90_img.edf

rm *.o *.mod
