
program scale_image
    use edf_utils
    implicit none
    character(len=9999)                             :: str_data
    character(len=254)                              :: file_in
    character(len=254)                              :: file_out
    real(kind = 8), dimension(:,:), allocatable     :: my_img
    integer                                         :: i, j, dim1, dim2
    real(kind = 8)                                  :: num_scl

    write(*,*) "Hi #2"
    call get_efd_info("../../../../img_data/bnbt6-5_Hi_0001.edf", str_data, my_img)
    write(*,*) "header:", trim(str_data)
    num_scl = 5.0
    write(*,*) "num_scl =", num_scl
    dim1 = ubound(my_img, 1)
    dim2 = ubound(my_img, 2)
    forall (i=1:dim1/2, j=1:dim2/2) my_img(i,j) = my_img(i,j) * num_scl

    write(*,*) "Hi there n > 3"
    call write_edf("bnbt6-5_Hi_0001_cutted.edf", my_img)

end program scale_image

