echo "removing previous binrs, objs and imgs"
rm anaelu_dif_res info_dif.dat img_diff.edf

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall tools.f90

gfortran -c -Wall anaelu_dif_res.f90

echo "linking"
gfortran -o anaelu_dif_res *.o

echo "running binary exe"
./anaelu_dif_res cuted.edf img_smooth.edf img_diff.edf info_dif.dat

rm *.o *.mod
