
program reading_cifs

    use input_arg_tools,                only: handling_kargs, arg_def_list
    use edf_utils,                      only: get_efd_info, write_edf
    use inner_tools,                    only: rotate_img, img_out

    implicit none

    character(len=9999)                             :: str_data
    !character(len=254)                              :: file_in
    !character(len=254)                              :: file_out
    real(kind = 8), dimension(:,:), allocatable     :: my_img
    !integer                                         :: i, j, dim1, dim2
    real(kind = 8)                                  :: num_scl

    Type(arg_def_list)              :: my_def

    my_def%num_of_vars = 2
    allocate(my_def%field(1:my_def%num_of_vars))
    my_def%field(1)%var_name = "img_in"
    my_def%field(1)%value = "my_img_in.edf"

    my_def%field(2)%var_name = "img_out"
    my_def%field(2)%value = "rotated_img.edf"


    call handling_kargs(my_def)

    write(*,*) "img_in =", my_def%field(1)%value
    write(*,*) "img_out =", my_def%field(2)%value

    call get_efd_info(my_def%field(1)%value, str_data, my_img)
    write(*,*) "header:", trim(str_data)

    call rotate_img(my_img)

    call write_edf(my_def%field(2)%value, img_out)


end program reading_cifs
