echo "removing previous binrs, objs and imgs"
rm rot_tool img_rot.edf

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall tools.f90

gfortran -c -Wall rot_img.f90

echo "linking"
gfortran -o rot_tool *.o

echo "running binary exe"
./rot_tool img_64bit_xrd.edf img_rot.edf

rm *.o *.mod
