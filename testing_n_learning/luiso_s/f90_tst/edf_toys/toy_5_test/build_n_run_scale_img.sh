echo "removing previous binrs, objs and imgs"
rm anaelu_img_scale scaled_img.edf 

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall tools.f90

gfortran -c -Wall anaelu_img_scale.f90

echo "linking"
gfortran -o anaelu_img_scale *.o

echo "running binary exe"
./anaelu_img_scale img_64bit_xrd.edf scaled_img.edf 0.55

rm *.o *.mod
