echo "removing previous binrs, objs and imgs"
rm anaelu_img_cut cuted.edf
rm anaelu_img_cut.exe 

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall cut.f90

gfortran -c -Wall anaelu_img_cut.f90

echo "linking"
gfortran -o anaelu_img_cut.exe *.o -static

echo "running binary exe"
#./anaelu_img_cut img_64bit_xrd.edf dummy.edf cuted.edf
./anaelu_img_cut.exe img_64bit_xrd.edf dummy.edf cuted.edf

rm *.o *.mod
