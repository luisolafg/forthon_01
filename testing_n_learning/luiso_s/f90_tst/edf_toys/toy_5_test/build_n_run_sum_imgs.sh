echo "removing previous binrs, objs and imgs"
rm anaelu_img_add img_sum.edf

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall tools.f90

gfortran -c -Wall anaelu_img_add.f90

echo "linking"
gfortran -o anaelu_img_add *.o

echo "running binary exe"
./anaelu_img_add img_64bit_xrd.edf img_smooth.edf img_sum.edf

rm *.o *.mod
