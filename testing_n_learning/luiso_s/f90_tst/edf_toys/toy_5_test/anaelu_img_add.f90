
program imgs_add

    use input_arg_tools,                only: handling_kargs, arg_def_list
    use edf_utils,                      only: get_efd_info, write_edf
    use inner_tools,                    only: suma_img, img_out

    implicit none

    character(len=9999)                             :: str_data1, str_data2
    real(kind = 8), dimension(:,:), allocatable     :: img_in_1, img_in_2

    Type(arg_def_list)              :: my_def

    my_def%num_of_vars = 3
    allocate(my_def%field(1:my_def%num_of_vars))
    my_def%field(1)%var_name = "img_in_1"
    my_def%field(1)%value = "my_img_in_1.edf"

    my_def%field(2)%var_name = "img_in_2"
    my_def%field(2)%value = "my_img_in_2.edf"

    my_def%field(3)%var_name = "img_out"
    my_def%field(3)%value = "img_sum.edf"


    call handling_kargs(my_def)

    write(*,*) "img_in_1", my_def%field(1)%value
    write(*,*) "img_in_2", my_def%field(2)%value
    write(*,*) "img_out", my_def%field(3)%value


    call get_efd_info(my_def%field(1)%value, str_data1, img_in_1)
    write(*,*) "header 1:", trim(str_data1)

    call get_efd_info(my_def%field(2)%value, str_data2, img_in_2)
    write(*,*) "header 2:", trim(str_data2)

    call suma_img(img_in_1, img_in_2)

    call write_edf(my_def%field(3)%value, img_out)


end program imgs_add
