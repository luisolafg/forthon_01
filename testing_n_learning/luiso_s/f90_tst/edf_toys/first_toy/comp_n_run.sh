echo "removing previous leftover"
rm img_create f90_test_img.edf *.o *.mod
echo "compiling"

gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall write_edf_1.f90

echo "linking"
gfortran -o img_create *.o


echo "running"
./img_create
echo "removing bin exe"
python read_edf.py
