
program scale_image
    use edf_utils
    implicit none
    character(len=9999)                             :: str_data
    character(len=254)                              :: file_in
    character(len=254)                              :: file_out
    real(kind = 8), dimension(:,:), allocatable     :: my_img
    integer                                         :: i, j, dim1, dim2
    real(kind = 8)                                  :: num_scl

    write(*,*) "here 1"
    call com_arg(file_in, num_scl, file_out)
    write(*,*) "Hi #2"
    call get_efd_info(file_in, str_data, my_img)
    write(*,*) "header:", trim(str_data)

    write(*,*) "num_scl =", num_scl
    dim1 = ubound(my_img, 1)
    dim2 = ubound(my_img, 2)
    forall (i=1:dim1, j=1:dim2) my_img(i,j) = my_img(i,j) * num_scl

    write(*,*) "Hi there n > 3"
    call write_edf(file_out, my_img)

end program scale_image

