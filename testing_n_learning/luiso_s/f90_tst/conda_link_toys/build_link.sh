echo "This script should be caled with"
echo "the source command, otherwise the"
echo "PATH variable will not stay persistent"
echo ""
echo "removing old dir with link on it"

conda install gfortran_linux-64 -y

rm -rf bin_links/

echo "creating new dir and cd to it"
mkdir bin_links
cd bin_links/

echo "creating link and adding one more path"
ln -s $GFORTRAN gfortran
export PATH=$(pwd):$PATH

echo "cd back one level"
cd ..

echo "Done"
