echo "compiling"
OPT1="-c -Wall -O2 "
gfortran  $OPT1             iter_scale.f90
gfortran  $OPT1             sclale_pr.f90

echo "linking"
gfortran -o iter.r *.o

echo "running"
./iter.r

echo "deleting"
rm iter.r *.o *.mod
