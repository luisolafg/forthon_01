#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
INC="-I$CRYSFML/GFortran64/LibC"
LIB="-L$CRYSFML/GFortran64/LibC"
#INC="-I$CRYSFML/GFortran/LibC"
#LIB="-L$CRYSFML/GFortran/LibC"
LIBSTATIC=" -static -lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly
#OPT1="-c -O2 -Wall "
#OPT1="-c -O1"
OPT1="-c -O -ffree-line-length-none -funroll-loops"


echo " Program Compilation -static"

 gfortran $OPT1                  ../cli_f90_deps/gfortran_pbar.f90
 gfortran $OPT1                     ../cli_f90_deps/input_args.f90

 gfortran $OPT1 $INC                       global_types_n_deps.f90
 gfortran $OPT1 $INC                                 mic_tools.f90
 gfortran $OPT1 $INC                                   calc_2d.f90
 gfortran $OPT1 $INC                           sample_CLI_data.f90

 gfortran $OPT1 $INC                              anaelu_C_L_I.f90

echo " Linking"

 gfortran -o anaelu_calc_xrd *.o  $LIB $LIBSTATIC
rm *.o *.mod


