
#!/bin/bash

TMP_ROOT=$(pwd)

wget https://repo.anaconda.com/miniconda/Miniconda2-4.3.31-Linux-x86_64.sh
bash Miniconda2-4.3.31-Linux-x86_64.sh -p "$TMP_ROOT/conda_bundle" -b
export PATH="$TMP_ROOT/conda_bundle/bin:$PATH"
conda install conda=4.3.34 -y
conda install numpy=1.13.0 -y
conda install wxpython=3.0.0.0 -y
conda install -c fable fabio=0.3.0 -y

git clone https://gitlab.com/luisolafg/anaelu.git anaelu_main
cd $TMP_ROOT/anaelu_main

bash build2dev.sh
cd $TMP_ROOT/anaelu_main/examples
wget https://gitlab.com/luisolafg/forthon_01/raw/master/testing_n_learning/img_data/APT73_d122_from_m02to02__01_41.mar2300

cd $TMP_ROOT

echo " "
echo "Anaelu bundle was succesfully installed"
echo " "
echo " "
echo "To get Anaelu ready to run, just add the following line:"
echo " "
echo "source $TMP_ROOT/anaelu_main/setpath.sh"
echo " "
echo "to either ~/.bashrc   or  ~/.profile start scripts, depending "
echo "on your distribution. This will override your Python "
echo "installation. If you prefer to get Anaelu ready to run"
echo "on a temporal state just type:"
echo " "
echo "source $TMP_ROOT/anaelu_main/setpath.sh"
echo " "
echo "and the configuration will last only the current session"
echo " "
echo " "

