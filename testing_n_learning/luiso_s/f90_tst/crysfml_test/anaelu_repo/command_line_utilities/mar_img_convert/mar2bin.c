#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pck.h"

//#include <string.h> // testing

PCKWORD data[MAXPIXELNUM*MAXPIXELNUM];
int mar2bin(char *srcfile, char *tgtfile, int doRotate);


int mar2bin(char *srcfile, char *tgtfile, int doRotate)
{
int numofpixels, array_size, nx, ny, n32;
long cnt;
FILE *fp;
pck_header srcheader;
PCKWORD tmpw;

  // open header
  if ((numofpixels = headerinfo(srcfile, srcheader))==0) {
  	printf("File header could not be read!\n");
  	return 0;
  }

  if (srcheader[0] != 1234 ) {
  	printf("File header could not be read!\n");
  	return 0;
  }

  // get some info about the image (x, y pixel numbers, number in overspill table)
  nx      = srcheader[1];
  ny      = srcheader[5]/srcheader[1];
  n32     = srcheader[2];
  array_size = nx*ny;

  // read data into global array
  if (!(openfile(srcfile, &data[0], doRotate))) {
  	printf("File could not be read!\n");
  	return 0;
  }
  printf("file %s read: size (%u, %u) px, total: %u words.\n", srcfile, nx, ny, numofpixels);

  // try to open the output file
  if ((fp = fopen(tgtfile, "wb")) == NULL) {
  	printf("Output file could not be created!\n");
  	return 0;
  }

  // dump the data
  fwrite (&data[0], sizeof(PCKWORD), array_size, fp);
  fclose (fp);

  return 1;
}


void main(int argc, char* argv[])

// string() fl_out; // testing (fail for now)

{
    if(argc < 2){
        printf("\nSpesify file to convert and output file\n");
        printf("\nExample:\n");
        printf("\n./mar2bin.r My_mar_file.mar2300 My_bin_file.bin\n\n\n");

    } else if(argc == 3) {
        printf("file in = %s\n",argv[1]);
        printf("file out = %s\n",argv[2]);
        //mar2bin("LaB6_feb18_08_11_32_44.mar2300", "LaB6_feb18_08_11_32_44.bin", 0);
        mar2bin(argv[1], argv[2], 0);
    } else if(argc == 2) {
        printf("file in = %s\n",argv[1]);
        printf("\nmissing name of file out\n\n");



    } else {
        printf("to many parameters\n");
    }
}
