import tools
import numpy as np
import fabio
import sys
from arg_interface import get_par


if( __name__ == "__main__" ):

    par_def =(("img_in", "my_img_in.edf"),
              ("img_out", "img_rot.edf"))

    #print "\n sys.argv =", sys.argv, "\n"
    in_par = get_par(par_def, sys.argv[1:])

    path_to_img = in_par[0][1]
    file_out =    in_par[1][1]

    ini_img = fabio.open(path_to_img).data.astype(np.float64)

    tools.inner_tools.rotate_img(ini_img)
    arr_out = tools.inner_tools.img_out

    new_img = fabio.edfimage.edfimage()

    new_img.data = arr_out

    new_img.write(file_out)


