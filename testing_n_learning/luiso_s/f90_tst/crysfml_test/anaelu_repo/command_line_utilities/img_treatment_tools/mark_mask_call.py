import numpy as np
import fabio
import sys
from arg_interface import get_par

import tools


"img_in=my_img_in.edf img_out=marcked_img.edf rect_area=x1,y1,x2,y2"


if( __name__ == "__main__" ):
    par_def =(("img_in", "my_img_in.edf"),
              ("img_out", "marcked_img.edf"),
              ("rect_area", "1101,1002,1203,1204"))

    #print "\n sys.argv =", sys.argv, "\n"
    in_par = get_par(par_def, sys.argv[1:])
    #print "in_par =", in_par

    path_to_img_in =  in_par[0][1]
    path_to_img_out = in_par[1][1]
    str_rect =        in_par[2][1]

    #print "str_rect =", str_rect
    lst_rest = str_rect.split(",")
    #print "lst_rest =", lst_rest

    x1 = int(lst_rest[0])
    y1 = int(lst_rest[1])
    x2 = int(lst_rest[2])
    y2 = int(lst_rest[3])

    print "x1 =", x1
    print "y1 =", y1
    print "x2 =", x2
    print "y2 =", y2

    img_in = fabio.open(path_to_img_in).data.astype(np.float64)

    try:

        tools.inner_tools.mark_mask(img_in, x1, x2, y1, y2)
        arr_out = tools.inner_tools.img_out

        res_img = fabio.edfimage.edfimage()
        res_img.data = arr_out
        res_img.write(str(path_to_img_out))

    except:
        print "something went wrong"

    debugging_only = '''
    from matplotlib import pyplot as plt
    plt.imshow( arr_out , interpolation = "nearest" )
    plt.show()
    '''
