#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo " Cleaning"
rm anaelu_calc_xrd                                #removing previous run
rm 2d_pat_1.asc img_file.edf img_file.raw         #removing data from previous run
rm 1d_pf.dat                                      #removing same as before
echo " Building"                                  #break point print
./make_2d_mask_builder.sh                         #calling compile script
echo "Running"                                    #break point print
./anaelu_calc_mask dat_in=param_01.dat cfl_in=cfl_test.cfl edf_out=mask.edf raw_out=mask.raw
echo "Plotting"                                   #break point print
#python matplot_2d_bin.py                          #show graph
python matplot_2d_mask_edf.py                      #show graph 2
