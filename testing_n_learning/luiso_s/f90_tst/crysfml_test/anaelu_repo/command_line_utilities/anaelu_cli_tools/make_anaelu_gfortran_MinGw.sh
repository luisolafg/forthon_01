#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
INC="-I$CRYSFML/GFortran/LibC"
LIB="-L$CRYSFML/GFortran/LibC"
LAUESUI="$SXTALSOFT/Laue_Suite_Project"
LIBSTATIC="-lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort only
OPT1="-c -O2 "

echo " Program Compilation"

 gfortran $OPT1       $LAUESUI/Laue_Modules/Src/gfortran_specific.f90
 gfortran $OPT1 $INC                                   calc_2d.f90
 gfortran $OPT1 $INC                           sample_CLI_data.f90
 #gfortran $OPT1 $INC                         ed_cell_ane_C_L_I.f90
 gfortran $OPT1 $INC                              anaelu_C_L_I.f90

echo " Linking"
# this output is an ".exe" for compatibility with MinGW
 gfortran -o anaelu_calc_xrd *.o  $LIB $LIBSTATIC
rm *.o *.mod


