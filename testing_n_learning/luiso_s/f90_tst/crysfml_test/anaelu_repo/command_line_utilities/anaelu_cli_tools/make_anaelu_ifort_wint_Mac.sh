#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
#INC="-I$CRYSFML/ifort/LibC"
#LIB="-L$CRYSFML/ifort/LibC"
 INC="-I$CRYSFML/ifort/LibC -I$WINTER/lib.ifi"
 LIB="-L$CRYSFML/ifort/LibW -L$WINTER/lib.ifi -L/usr/OpenMotif/lib -L/usr/X11/lib"
 LIBT="-L$CRYSFML/ifort/LibW"
 LAUESUI="$SXTALSOFT/Laue_Suite_Project"
 LIBSTATIC="-lcrysfml"
 LIB_LINK="-lwcrysfml -lwintGL -lwint -lGL -lGLU -lXm -lXt -lX11 -lXp -lXext -lXmu"
 LIB_WINT="-I$WINTER/lib.if8"
#OPT1="-c -g -debug full -CB -vec-report0"
 OPT1="-c -O2 -vec-report0"

echo " Program Compilation"

 ifort $OPT1       $LAUESUI/Laue_Modules/Src/ifort_specific.f90
 ifort $OPT1 $INC                                   calc_2d.f90
 ifort $OPT1 $LIB_WINT                         Anaelu_resid.f90
 ifort $OPT1 $INC -I$WINTER/lib.if8             ed_cell_ane.f90

echo " Resource Compilation"

 rc -c if8 -i$WINTER/include anaelu_resource.rc

 ifort $OPT1 $INC -I$WINTER/lib.if8             anaelu_wint.f90

echo " Linking"

   ifort -o Anaelu_Wint.r *.o $LIB $LIB_LINK $LIBT
rm *.o *.mod

echo " Runing"
./Anaelu_Wint.r
rm Anaelu_Wint.r
