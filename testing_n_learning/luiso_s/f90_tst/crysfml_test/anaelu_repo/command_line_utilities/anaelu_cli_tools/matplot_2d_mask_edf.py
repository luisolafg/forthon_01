#  This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
import numpy
from matplotlib import pyplot as plt
#from scipy.io.numpyio import fread

def plott_img(arr):
    print ("Plotting arr")
    #plt.imshow(  numpy.transpose(arr) , interpolation = "nearest" )
    plt.imshow(  arr , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):
    import fabio
    print "opening 2d_pat_1.edf"
    img_arr = fabio.open("mask.edf").data.astype("float32")
    plott_img(img_arr)
