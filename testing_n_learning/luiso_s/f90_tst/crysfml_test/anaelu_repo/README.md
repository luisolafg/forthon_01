The Anaelu Project is a software tool for modeling and refining
XRD images from textured crystalline samples.


The package includes stand alone command line tools (CLI) and a graphical
user interface (GUI) that helps the user to manage these tools and provides
some aid with visualizations.


Most of the command line tools, specifically those with strong number
crunching algorithms are written in FORTRAN, with some image
transformation tools written in C language. The GUI is written
mostly in Python with wxPython and NumPy libraries.


The CLI tools written in FORTRAN include a program that models the 2D XRD
images. This program depends on the CHRYSFML library. A clone of this
library is bundled inside the project's repository.


Some real time visualizations in the GUI and some image treatment tools
require fast processing of arrays, this is handled in FORTRAN and connected
to the wxPython GUI with f2py wrappers.


The source code of most of the project is released under Mozilla Public
License Version 2.0 (MPL), there are some files that are GPL because they
depend on the Fabio library. The remaining non MPL files are the clone of
CHRYSFML which is LGPL.


Some "howtos" coming soon
