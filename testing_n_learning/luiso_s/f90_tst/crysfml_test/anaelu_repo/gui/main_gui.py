#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx
from time import time
from subprocess import call as shell_func
from scrollable_multipanel import MultiImagePanel
from sample_n_instrument_in_n_out import *
import sys

class MainPanel(wx.SplitterWindow):
    def __init__(self, parent):
        super(MainPanel, self).__init__(parent)

        print "sys.argv() =", sys.argv
        try:
            self.num_of_panels = int(sys.argv[1])
            if(self.num_of_panels > 8 or self.num_of_panels < 2):
                self.num_of_panels = 4

            else:
                self.num_of_panels = int(self.num_of_panels / 2) * 2

        except:
            self.num_of_panels = 4


        self.data_panel = DataIntroPanel(self)
        self.imgs_panel = MultiImagePanel(self)

        self.SplitVertically(self.data_panel, self.imgs_panel)
        self.SetMinimumPaneSize(400)

        for num, btn in enumerate(self.data_panel.panel_btn_lst):
            self.data_panel.Bind(wx.EVT_BUTTON,
                                self.imgs_panel.scrolled_panel_lst[num].onLoadImgBtn,
                                btn)

        for num, chk in enumerate(self.data_panel.chk_invert_lst):
            self.data_panel.Bind(wx.EVT_CHECKBOX,
                                self.imgs_panel.scrolled_panel_lst[num].onCheck_invert,
                                chk)

        for num, chk in enumerate(self.data_panel.chk_sqrt_lst):
            self.data_panel.Bind(wx.EVT_CHECKBOX,
                                self.imgs_panel.scrolled_panel_lst[num].onCheck_sqrt,
                                chk)

    def OnRun(self, event, repaint = True):
        old_x_pos = self.imgs_panel.scroll_pos_x
        old_y_pos = self.imgs_panel.scroll_pos_y
        print "Cleaning old data"
        shell_func("rm param.dat cfl_out.cfl raw_img_file.raw", shell=True)

        self.data_panel.onRunCfl(event)
        self.data_panel.onRunDat(event)

        print "Running"
        shell_func("anaelu_calc_xrd dat_in=param.dat cfl_in=cfl_out.cfl edf_out=img_xrd.edf raw_out=raw_img_file.raw", shell=True)

        print "Done running Anaelu\n\nUpdating Img"
        self.imgs_panel.scrolled_panel_lst[2].intro_img_path(
                        path_to_img = "img_xrd.edf", use_fabio = True)

        if(repaint == True):
            self.imgs_panel.scrolled_panel_lst[2].intro_img_path_2()

        self.imgs_panel.scroll_pos_x = old_x_pos
        self.imgs_panel.scroll_pos_y = old_y_pos
        wx.CallAfter(self.imgs_panel.scrolled_panel_lst[2].Scrolling_to_2)

    def OnMask(self, event, repaint = True):
        old_x_pos = self.imgs_panel.scroll_pos_x
        old_y_pos = self.imgs_panel.scroll_pos_y
        print "Cleaning old data"
        shell_func("rm param.dat cfl_out.cfl raw_img_file.raw", shell=True)

        self.data_panel.onRunCfl(event)
        self.data_panel.onRunDat(event)

        print "Running"
        shell_func("anaelu_calc_mask dat_in=param.dat cfl_in=cfl_out.cfl edf_out=img_mask.edf raw_out=raw_img_file.raw", shell=True)

        print "Done running Anaelu\n\nUpdating Img"
        self.imgs_panel.scrolled_panel_lst[1].intro_img_path(
                        path_to_img = "img_mask.edf", use_fabio = True)

        if(repaint == True):
            self.imgs_panel.scrolled_panel_lst[1].intro_img_path_2()

        self.imgs_panel.scroll_pos_x = old_x_pos
        self.imgs_panel.scroll_pos_y = old_y_pos
        wx.CallAfter(self.imgs_panel.scrolled_panel_lst[1].Scrolling_to_2)

    def OnCutpeaks(self, event, repaint = True):
        print "click on Cut Peaks"
        old_x_pos = self.imgs_panel.scroll_pos_x
        old_y_pos = self.imgs_panel.scroll_pos_y

        try:
            i_exp = self.imgs_panel.scrolled_panel_lst[0].img_path
            print "i_exp1 =", i_exp
            cmd2run = "anaelu_img_cut "
            cmd2run += "img_in=" + str(i_exp) + " "
            cmd2run += "mask_in=img_mask.edf "
            cmd2run += "img_out=cut_img.edf"
            shell_func(cmd2run, shell=True)
            self.imgs_panel.scrolled_panel_lst[3].intro_img_path(
                      path_to_img = "cut_img.edf", use_fabio = True)
            self.imgs_panel.scrolled_panel_lst[3].img_path = "cut_img.edf"

            if(repaint == True):
                self.imgs_panel.scrolled_panel_lst[3].intro_img_path_2()

            self.imgs_panel.scroll_pos_x = old_x_pos
            self.imgs_panel.scroll_pos_y = old_y_pos
            wx.CallAfter(self.imgs_panel.scrolled_panel_lst[3].Scrolling_to_2)

        except:
            print "something went wrong with the cut thing"

    def OnSmoothbckgr(self, event, repaint = True):
        old_x_pos = self.imgs_panel.scroll_pos_x
        old_y_pos = self.imgs_panel.scroll_pos_y

        print "click on Smooth Bckgr"
        times_value = self.data_panel.input_times_data.GetValue()

        print "path =", self.imgs_panel.scrolled_panel_lst[3].img_path
        try:
            i_cut = self.imgs_panel.scrolled_panel_lst[3].img_path
            print "i_exp1 =", i_cut
            cmd2run = "anaelu_img_smooth "
            cmd2run += "img_in=" + str(i_cut) + " "
            cmd2run += "img_out=img_smooth.edf "
            cmd2run += "n_times=" + str(times_value) + " "
            shell_func(cmd2run, shell=True)
            self.imgs_panel.scrolled_panel_lst[3].intro_img_path(
                 path_to_img = "img_smooth.edf", use_fabio = True)

            if(repaint == True):
                self.imgs_panel.scrolled_panel_lst[3].intro_img_path_2()

            self.imgs_panel.scroll_pos_x = old_x_pos
            self.imgs_panel.scroll_pos_y = old_y_pos
            wx.CallAfter(self.imgs_panel.scrolled_panel_lst[3].Scrolling_to_2)

        except:
            print "something went wrong with the cut thing"

    def OnSumbckgr(self, event, repaint = True):
        old_x_pos = self.imgs_panel.scroll_pos_x
        old_y_pos = self.imgs_panel.scroll_pos_y

        self.OnScale(None, repaint = repaint)

        print "click on Sum Bckgr"

        shell_func("anaelu_img_add img_in_1=scaled_img.edf img_in_2=img_smooth.edf img_out=img_sum.edf raw_out=raw_img_file.raw", shell=True)

        print "Done running Anaelu_img_add\n\nUpdating Img"
        self.imgs_panel.scrolled_panel_lst[2].intro_img_path(
             path_to_img = "img_sum.edf", use_fabio = True)

        if(repaint == True):
            self.imgs_panel.scrolled_panel_lst[2].intro_img_path_2()

            self.imgs_panel.scroll_pos_x = old_x_pos
            self.imgs_panel.scroll_pos_y = old_y_pos
            wx.CallAfter(self.imgs_panel.scrolled_panel_lst[2].Scrolling_to_2)

    def OnScale(self, event, repaint = True):
        old_x_pos = self.imgs_panel.scroll_pos_x
        old_y_pos = self.imgs_panel.scroll_pos_y
        print "click on Scale"
        scale_value = self.data_panel.input_scale_data.GetValue()

        if scale_value =="" :
            shell_func("anaelu_img_scale img_in=img_xrd.edf img_out=scaled_img.edf scale=1.0", shell=True)
        else:
            scale_value = str(scale_value)
            cmd2run = "anaelu_img_scale img_in=img_xrd.edf img_out=scaled_img.edf scale="
            cmd2run += scale_value
            shell_func(cmd2run, shell=True)

        print "Done running Anaelu_img_scale\n\nUpdating Img"
        self.imgs_panel.scrolled_panel_lst[2].intro_img_path(
                        path_to_img = "scaled_img.edf", use_fabio = True)

        if(repaint == True):
            self.imgs_panel.scrolled_panel_lst[2].intro_img_path_2()

        self.imgs_panel.scroll_pos_x = old_x_pos
        self.imgs_panel.scroll_pos_y = old_y_pos
        wx.CallAfter(self.imgs_panel.scrolled_panel_lst[2].Scrolling_to_2)

    def OnR(self, event):
        old_x_pos = self.imgs_panel.scroll_pos_x
        old_y_pos = self.imgs_panel.scroll_pos_y

        print "click on Calculating R"

        i_exp = self.imgs_panel.scrolled_panel_lst[0].img_path

        cmd2run = "anaelu_dif_res img_calc_in=img_sum.edf img_exp_in="
        cmd2run += i_exp
        cmd2run += " dif_img_out=diff_img.edf res_data_out=img_diff_info.dat"
        shell_func(cmd2run, shell=True)

        my_file_path = "img_diff_info.dat"
        print "my_file_path =", my_file_path

        myfile = open(my_file_path, "r")

        all_lines = myfile.readlines()
        myfile.close()

        r_found = None
        r_2_found = None

        for lin_char in all_lines:
            commented = False
            for delim in ',;=':
                lin_char = lin_char.replace(delim, ' ')

            lst_litle_blocks = lin_char.split()
            for pos, litle_block in enumerate(lst_litle_blocks):

                if( litle_block == "!" or litle_block == "#" ):
                    commented = True

                elif( litle_block.upper() == "R" ):
                    print "found r"
                    r_found = float(lst_litle_blocks[pos+1])

                elif( litle_block.upper() == "R_2" ):
                    print "found r_2"
                    r_2_found = float(lst_litle_blocks[pos+1])

        print "r_found =", r_found
        print "r_2_found =", r_2_found

        print "\nDone running Anaelu_dif_res\nAdding Residue Image"
        print "\n"

        self.data_panel.output_r.SetValue(str(r_found))

        ##################################################################3
        #self.data_panel.output_r_2.SetValue(str(r_2_found))
        ##################################################################3

        self.imgs_panel.scrolled_panel_lst[1].intro_img_path(
                        path_to_img = "diff_img.edf", use_fabio = True)

        self.imgs_panel.scrolled_panel_lst[1].intro_img_path_2()
        self.imgs_panel.scrolled_panel_lst[2].intro_img_path_2()

        self.imgs_panel.scroll_pos_x = old_x_pos
        self.imgs_panel.scroll_pos_y = old_y_pos
        for n in range(4):
            wx.CallAfter(self.imgs_panel.scrolled_panel_lst[n].Scrolling_to_2)

    def OnMark_ex(self, cmd_lst):
        old_x_pos = self.imgs_panel.scroll_pos_x
        old_y_pos = self.imgs_panel.scroll_pos_y

        scr_panel0 = self.imgs_panel.scrolled_panel_lst[0]
        i_exp = self.imgs_panel.scrolled_panel_lst[0].img_path

        lst_cmd2run = []
        for in_lst in cmd_lst:
            cmd2run = "anaelu_img_mark_rect"
            img_nam = " img_in=" + i_exp
            cmd2run += img_nam

            rec_par = " rect_area="
            for num_str in in_lst:
                rec_par += str(num_str)
                rec_par += ","

            cmd2run += rec_par[0:-1]
            cmd2run += " img_out=marcked_img.edf"

            lst_cmd2run.append(cmd2run)
            i_exp = "marcked_img.edf"

        for cmd2run in lst_cmd2run:
            shell_func(cmd2run, shell=True)

        try:
            self.imgs_panel.scrolled_panel_lst[0].intro_img_path(
                      path_to_img = i_exp, use_fabio = True)
            self.imgs_panel.scrolled_panel_lst[0].img_path = i_exp
            self.imgs_panel.scrolled_panel_lst[0].intro_img_path_2()
            self.imgs_panel.scroll_pos_x = old_x_pos
            self.imgs_panel.scroll_pos_y = old_y_pos
            wx.CallAfter(self.imgs_panel.scrolled_panel_lst[0].Scrolling_to_2)

        except:
            print "something went wrong with the marking thing"


class MainFrame(wx.Frame):
    def __init__(self):
        super(MainFrame, self).__init__( None, -1, "ANAELU 2.0", size = (1200,600))

        self.mPanel = MainPanel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(self.mPanel, 1, wx.EXPAND)
        self.SetSizer(sizer)

        menubar = wx.MenuBar()
        file_menu = wx.Menu()
        edit_menu = wx.Menu()
        help_menu = wx.Menu()
        run_menu  = wx.Menu()

        file_menu.Append(100, '&Open .cfl\tCtrl+O', 'Open the .cfl file')
        file_menu.Append(101, '&Open .dat\tCtrl+P', 'Open the .dat file')
        file_menu.Append(102, '&Save .cfl\tCtrl+S', 'Save the .cfl file')
        file_menu.Append(103, '&Save .dat\tCtrl+W', 'Save the .dat file')
        help_menu.Append(104, '&FAQ\tCtrl+F', 'Frequently Asked Questions')
        help_menu.Append(104, '&About\tCtrl+A', 'Credits and Licenses')
        file_menu.AppendSeparator()
        quit = wx.MenuItem(file_menu, 105, '&Quit\tCtrl+Q', 'Quit the Application')
        #quit.SetBitmap(wx.Image('exit.png', wx.BITMAP_TYPE_PNG).ConvertToBitmap())
        file_menu.AppendItem(quit)
        run_menu.Append(106, '&Run Model\tCtrl+R', 'Save and Run The Calculating XRD Image')
        run_menu.Append(107, "&Mask's Model\tCtrl+M", 'Save and Run The Calculating Mask Image')
        run_menu.Append(108, "&Cut Mask's Peaks\tCtrl+T", 'Save and Cut The Peaks From Calculating Mask Image')
        run_menu.Append(109, '&Smooth the Backgound\tCtrl+D', 'Smoothing The Background Image')
        run_menu.Append(110, '&Sum Backgound\tCtrl+J', 'Summing The Backgound to The Calculating XRD Image')
        run_menu.Append(111, '&Scale and Difference\tCtrl+B', 'Scale The Model')
        run_menu.Append(112, '&Re Model\tCtrl+K', 'Calculating The R of Both Images')

        edit_menu.Append(201, 'main item1', kind = wx.ITEM_CHECK)
        edit_menu.Append(202, 'main item2', kind = wx.ITEM_CHECK)
        submenu = wx.Menu()
        submenu.Append(301, 'subitem1', kind=wx.ITEM_RADIO)
        submenu.Append(302, 'subitem2', kind=wx.ITEM_RADIO)
        submenu.Append(303, 'subitem3', kind=wx.ITEM_RADIO)
        edit_menu.AppendMenu(203, 'submenu', submenu)

        menubar.Append(file_menu, '&File')
        menubar.Append(edit_menu, '&Edit')
        menubar.Append(help_menu, '&Help')
        menubar.Append(run_menu, '&Refinement Tools')
        self.SetMenuBar(menubar)
        self.CreateStatusBar()
        self.Centre()

        self.Bind(wx.EVT_MENU, self.mPanel.data_panel.onOpenCfl, id=100)
        self.Bind(wx.EVT_MENU, self.mPanel.data_panel.onOpenDat, id=101)
        self.Bind(wx.EVT_MENU, self.mPanel.data_panel.onRunCfl, id=102)
        self.Bind(wx.EVT_MENU, self.mPanel.data_panel.onRunDat, id=103)
        self.Bind(wx.EVT_MENU, self.OnAboutBox, id=104)
        self.Bind(wx.EVT_MENU, self.OnQuit, id=105)
        self.Bind(wx.EVT_MENU, self.mPanel.OnRun, id=106)
        self.Bind(wx.EVT_MENU, self.mPanel.OnMask, id=107)
        self.Bind(wx.EVT_MENU, self.mPanel.OnCutpeaks, id=108)
        self.Bind(wx.EVT_MENU, self.mPanel.OnSmoothbckgr, id=109)
        self.Bind(wx.EVT_MENU, self.mPanel.OnSumbckgr, id=110)
        self.Bind(wx.EVT_MENU, self.mPanel.OnScale, id=112)
        self.Bind(wx.EVT_MENU, self.mPanel.OnR, id=112)

    def OnQuit(self, event):
        self.Close()

    def OnAboutBox(self, e):
        description = """ANAELU is a computer programa for the treatment of 2D-XRD patterns of textured samples
"""

        licence = """

---------------------------------------------------
ANAELU ()
---------------------------------------------------
 The Anaelu project is subject to the terms of the

          Mozilla Public License, v. 2.0.

 If a copy of the MPL was not distributed with this program,
 You can obtain one at http://mozilla.org/MPL/2.0/.


 Copyright (C) 2013-2016  CIMAV / DLS


 Authors: Luis Fuentes Montero (DLS)
          Eduardo Villalobos Portillo (CIMAV)
          Diana C. Burciaga (CIMAV)
          Luis E. Fuentes Cobas (CIMAV)

 Contributors: Juan Rodrgiguez Carvajal (ILL)
               Jose Alfredo (S. Queretaro)
               Javier (ULL)

"""
        info = wx.AboutDialogInfo()

        #info.SetIcon(wx.Icon('Anaelu.png', wx.BITMAP_TYPE_PNG))
        info.SetName('ANAELU')
        info.SetVersion('2.0')
        info.SetDescription(description)
        info.SetCopyright('(C) 2010 - 2017 Luis Fuentes Montero Et. Al.')
        info.SetWebSite('https://gitlab.com/luisolafg/anaelu')
        info.SetLicence(licence)
        info.AddDeveloper('The Anaelu group \n Luis Fuentes-Montero \n Eduardo Villalobos-Portillo \n Diana C. Burciaga')
        info.AddDocWriter('Luis Fuentes Montero and Eduardo Villalobos')
        info.AddArtist('Eduardo Villalobos')

        wx.AboutBox(info)


if __name__ == "__main__":

    wxapp = wx.App(redirect = False)
    frame = MainFrame()
    frame.Show()
    wxapp.MainLoop()
