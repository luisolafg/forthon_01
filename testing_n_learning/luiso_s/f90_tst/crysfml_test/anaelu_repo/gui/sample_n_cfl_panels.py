#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx
import os
from readcfl import *

class CrystalPanel(wx.Panel):
    '''creating the monocrystal panel to display it in the gui'''
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(CrystalPanel, self).__init__(parent)

        title = wx.StaticText(self, wx.ID_ANY, ' Title ')
        self.inputtitle = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        subt1 = wx.StaticText(self, wx.ID_ANY, '*** CRYSTAL STRUCTURE ***')
        label_spgr = wx.StaticText(self, wx.ID_ANY, ' Space group ')
        self.input_spgr = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_a = wx.StaticText(self, wx.ID_ANY, '              a')
        self.input_a = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_b = wx.StaticText(self, wx.ID_ANY, '              b')
        self.input_b = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_c = wx.StaticText(self, wx.ID_ANY, '              c')
        self.input_c = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_alpha = wx.StaticText(self, wx.ID_ANY, '          alpha')
        self.input_alpha = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_beta = wx.StaticText(self, wx.ID_ANY, '           beta')
        self.input_beta = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_gamma = wx.StaticText(self, wx.ID_ANY, '         gamma')
        self.input_gamma = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)

        MainSizer        = wx.BoxSizer(wx.VERTICAL)
        titleSizer       = wx.BoxSizer(wx.HORIZONTAL)
        subt1Sizer       = wx.BoxSizer(wx.HORIZONTAL)
        spcgrpSizer      = wx.BoxSizer(wx.HORIZONTAL)
        abcSizer         = wx.BoxSizer(wx.HORIZONTAL)
        anglesSizer      = wx.BoxSizer(wx.HORIZONTAL)

        inputa           = wx.BoxSizer(wx.VERTICAL)
        inputb           = wx.BoxSizer(wx.VERTICAL)
        inputc           = wx.BoxSizer(wx.VERTICAL)

        inputalpha       = wx.BoxSizer(wx.VERTICAL)
        inputbeta        = wx.BoxSizer(wx.VERTICAL)
        inputgamma       = wx.BoxSizer(wx.VERTICAL)

        inputa.Add(label_a)
        inputa.Add(self.input_a)
        inputb.Add(label_b)
        inputb.Add(self.input_b)
        inputc.Add(label_c)
        inputc.Add(self.input_c)

        inputalpha.Add(label_alpha)
        inputalpha.Add(self.input_alpha)
        inputbeta.Add(label_beta)
        inputbeta.Add(self.input_beta)
        inputgamma.Add(label_gamma)
        inputgamma.Add(self.input_gamma)


        titleSizer.Add(title)
        titleSizer.Add(self.inputtitle)

        subt1Sizer.Add(subt1, 0, wx.ALL, 2)

        spcgrpSizer.Add(label_spgr)
        spcgrpSizer.Add(self.input_spgr)

        abcSizer.Add(inputa)
        abcSizer.Add(inputb)
        abcSizer.Add(inputc)

        anglesSizer.Add(inputalpha)
        anglesSizer.Add(inputbeta)
        anglesSizer.Add(inputgamma)

        MainSizer.Add(titleSizer)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt1Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(spcgrpSizer)
        MainSizer.Add(abcSizer)
        MainSizer.Add(anglesSizer)
        MainSizer.Add(wx.StaticLine(self))

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)

    def set_values(self, cfl_values):

        '''setting the values int the monocrystal panel'''
        if( cfl_values.title != None ):
            self.inputtitle.SetValue(cfl_values.title)
        if( cfl_values.spgr != None):
            self.input_spgr.SetValue(cfl_values.spgr)
        if( cfl_values.a !=None ):
            self.input_a.SetValue(cfl_values.a)
        if( cfl_values.b !=None ):
            self.input_b.SetValue(cfl_values.b)
        if( cfl_values.c !=None ):
            self.input_c.SetValue(cfl_values.c)
        if( cfl_values.alpha !=None ):
            self.input_alpha.SetValue(cfl_values.alpha)
        if( cfl_values.beta !=None ):
            self.input_beta.SetValue(cfl_values.beta)
        if( cfl_values.gamma !=None ):
            self.input_gamma.SetValue(cfl_values.gamma)


class PoliCrystalPanel(wx.Panel):
    '''creating the policrystal panel to display it in the gui'''
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(PoliCrystalPanel, self).__init__(parent)

        self.parent_widget = parent

        subt2 = wx.StaticText(self, wx.ID_ANY, '*** POLYCRYSTAL STRUCTURE ***')

        label_lg_size = wx.StaticText(self, wx.ID_ANY, 'Average Crystal size        ')
        self.input_lg_size = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_lg_size_units = wx.StaticText(self, wx.ID_ANY, ' (x100 A)')

        label_avg_strain = wx.StaticText(self, wx.ID_ANY, 'Average strain                  ')
        self.avg_strain = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_avg_strain_units = wx.StaticText(self, wx.ID_ANY, ' (%)')

        label_pref_orient = wx.StaticText(self, wx.ID_ANY, '                     Preferred orientation')
        label_h = wx.StaticText(self, wx.ID_ANY, '              H')
        self.input_h = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_k = wx.StaticText(self, wx.ID_ANY, '              K')
        self.input_k = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_l = wx.StaticText(self, wx.ID_ANY, '              L',style=wx.TE_CENTRE)
        self.input_l = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)

        label_hkl_wh = wx.StaticText(self, wx.ID_ANY, 'IPF Gaussian distribution FWHM ')
        self.input_hkl_wh = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)

        label_azm_ipf = wx.StaticText(self, wx.ID_ANY, 'Inverse pole figure azimuth         ')
        self.input_azm_ipf = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)

        label_ipf_res = wx.StaticText(self, wx.ID_ANY, 'IPF number of horizontal steps  ')
        self.input_ipf_res = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)

        MainSizer      = wx.BoxSizer(wx.VERTICAL)
        subt2Sizer     = wx.BoxSizer(wx.HORIZONTAL)
        avgeszSizer    = wx.BoxSizer(wx.HORIZONTAL)
        avgestrSizer   = wx.BoxSizer(wx.HORIZONTAL)
        hklSizer       = wx.BoxSizer(wx.VERTICAL)
        fwhmSizer      = wx.BoxSizer(wx.HORIZONTAL)
        ipfazSizer     = wx.BoxSizer(wx.HORIZONTAL)
        ipfnsSizer     = wx.BoxSizer(wx.HORIZONTAL)
        hSizer         = wx.BoxSizer(wx.VERTICAL)
        kSizer         = wx.BoxSizer(wx.VERTICAL)
        lSizer         = wx.BoxSizer(wx.VERTICAL)
        hklhSizer      = wx.BoxSizer(wx.HORIZONTAL)

        hSizer.Add(label_h)
        hSizer.Add(self.input_h)
        kSizer.Add(label_k)
        kSizer.Add(self.input_k)
        lSizer.Add(label_l)
        lSizer.Add(self.input_l)

        hklhSizer.Add(hSizer)
        hklhSizer.Add(kSizer)
        hklhSizer.Add(lSizer)

        subt2Sizer.Add(subt2, 0, wx.ALL, 2)

        avgeszSizer.Add(label_lg_size)
        avgeszSizer.Add(self.input_lg_size)
        avgeszSizer.Add(label_lg_size_units)

        avgestrSizer.Add(label_avg_strain)
        avgestrSizer.Add(self.avg_strain)
        avgestrSizer.Add(label_avg_strain_units)

        hklSizer.Add(label_pref_orient)
        hklSizer.Add(hklhSizer)

        fwhmSizer.Add(label_hkl_wh)
        fwhmSizer.Add(self.input_hkl_wh)

        ipfazSizer.Add(label_azm_ipf)
        ipfazSizer.Add(self.input_azm_ipf)

        ipfnsSizer.Add(label_ipf_res)
        ipfnsSizer.Add(self.input_ipf_res)

        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt2Sizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(avgeszSizer)
        MainSizer.Add(avgestrSizer)
        MainSizer.Add(wx.StaticLine(self))
        MainSizer.Add(hklSizer)
        MainSizer.Add(wx.StaticLine(self))
        MainSizer.Add(fwhmSizer)
        MainSizer.Add(ipfazSizer)
        MainSizer.Add(ipfnsSizer)
        #MainSizer.Add(self.maskminSizer)


        self.SetSizer(MainSizer)
        MainSizer.Fit(self)


    def set_values(self, cfl_values):

        '''setting the values int the policrystal panel'''
        if( cfl_values.size_lg != None ):
            self.input_lg_size.SetValue(cfl_values.size_lg)

        self.avg_strain.SetValue('none')

        if( cfl_values.h != None and cfl_values.k != None and cfl_values.l !=None ):
            self.input_h.SetValue(cfl_values.h)
            self.input_k.SetValue(cfl_values.k)
            self.input_l.SetValue(cfl_values.l)

        if(cfl_values.hklwh != None ):
            self.input_hkl_wh.SetValue(cfl_values.hklwh)

        if(cfl_values.azm_ipf != None ):
            self.input_azm_ipf.SetValue(cfl_values.azm_ipf)

        if(cfl_values.ipf_res != None ):
            self.input_ipf_res.SetValue(cfl_values.ipf_res)

