#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.import wx

import numpy as np
import wx
from looping import np2bmp_diff

def build_np_img(height = 64, width = 64):
  data2d = np.zeros( (height, width),'float')
  print "width, height =", width, height
  for x in range(0, width):
    for y in range(0, height):
      data2d[y,x] = np.sqrt(x*x + y*y)

  data2d[height * 1 / 2: height * 3 / 4, width / 4: width * 3 / 4] = data2d.max()
  data2d[height / 4:height*1 / 2, width / 4: width * 3 / 4] = 0
  return data2d

class ImgFrame(wx.Frame):
    def __init__(self):
        super(ImgFrame, self).__init__( None, -1, 'Input Data')

        data2d = build_np_img(width = 80, height = 50)
        bmp_dat = np2bmp_diff()
        my_bitmap = wx.BitmapFromImage(bmp_dat.img_2d_rgb(data2d = data2d,
                                 invert = False, sqrt_scale = False ) )

        print "\n bmp_dat.i_min_max = ", bmp_dat.i_min_max

        self.bitmap = wx.StaticBitmap(self, bitmap=my_bitmap)

        MainSizer = wx.BoxSizer(wx.VERTICAL)
        MainSizer.Add(self.bitmap, proportion = 1, flag = wx.EXPAND)
        self.SetSizer(MainSizer)

        self.Centre()

if __name__ == '__main__':
    app = wx.App()
    frame = ImgFrame().Show()
    app.MainLoop()

