#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

import wx
import os

from readdat import *


class ParamPanel(wx.Panel):


    '''creating the experiment parameters panel to display it in the gui'''
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(ParamPanel, self).__init__(parent)


        subt3 = wx.StaticText(self, wx.ID_ANY, '*** EXPERIMENTAL DATA ***')

        label_lambd= wx.StaticText(self, wx.ID_ANY, 'Wavelength                          ')
        self.input_lambd= wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_lambd_units = wx.StaticText(self, wx.ID_ANY, ' (Angstrom)')

        label_dst_det= wx.StaticText(self, wx.ID_ANY, 'Sample-detector distance')
        self.input_dst_det= wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_dst_det_units = wx.StaticText(self, wx.ID_ANY, ' (mm)')

        label_diam_det= wx.StaticText(self, wx.ID_ANY, 'Detector diameter              ')
        self.input_diam_det= wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_diam_det_units = wx.StaticText(self, wx.ID_ANY, ' (mm)')

        label_x_beam = wx.StaticText(self, wx.ID_ANY, ' X Beam centre')
        self.input_x_beam = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_y_beam = wx.StaticText(self, wx.ID_ANY, ' Y Beam centre')
        self.input_y_beam = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)

        label_x_res = wx.StaticText(self, wx.ID_ANY, '          X Max       ')
        self.input_x_res = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)
        label_y_res = wx.StaticText(self, wx.ID_ANY, '          Y Max        ')
        self.input_y_res = wx.TextCtrl(self, wx.ID_ANY, '',style=wx.TE_CENTRE)

        #TODO make the next CheckBox centered
        self.chk_up_hfl = wx.CheckBox(self, label=" Upper half only ")
        h_spc_box = wx.BoxSizer(wx.HORIZONTAL)
        h_spc_box.Add(wx.StaticText(self, wx.ID_ANY, '^         '), 3, wx.EXPAND)
        h_spc_box.Add(self.chk_up_hfl, 0, wx.EXPAND)
        h_spc_box.Add(wx.StaticText(self, wx.ID_ANY, '          ^'), 3, wx.EXPAND)

        MainSizer   = wx.BoxSizer(wx.VERTICAL)
        subt3Sizer  = wx.BoxSizer(wx.HORIZONTAL)
        wlSizer     = wx.BoxSizer(wx.HORIZONTAL)
        sddSizer    = wx.BoxSizer(wx.HORIZONTAL)
        ddSizer     = wx.BoxSizer(wx.HORIZONTAL)
        xySizer     = wx.BoxSizer(wx.VERTICAL)
        fwhmSizer   = wx.BoxSizer(wx.HORIZONTAL)


        xbSizer = wx.BoxSizer(wx.VERTICAL)
        ybSizer = wx.BoxSizer(wx.VERTICAL)
        xmSizer = wx.BoxSizer(wx.VERTICAL)
        ymSizer = wx.BoxSizer(wx.VERTICAL)

        xybSizer = wx.BoxSizer(wx.HORIZONTAL)
        xymSizer = wx.BoxSizer(wx.HORIZONTAL)

        ddhSizer = wx.BoxSizer(wx.HORIZONTAL)

        xbSizer.Add(label_x_beam, 0, wx.ALL, 2)
        xbSizer.Add(self.input_x_beam, 2, wx.ALL| wx.EXPAND, 2)
        ybSizer.Add(label_y_beam, 0, wx.ALL, 2)
        ybSizer.Add(self.input_y_beam, 2, wx.ALL| wx.EXPAND, 2)

        xmSizer.Add(label_x_res, 0, wx.ALL, 2)
        xmSizer.Add(self.input_x_res, 2, wx.ALL| wx.EXPAND, 2)
        ymSizer.Add(label_y_res, 0, wx.ALL, 2)
        ymSizer.Add(self.input_y_res, 2, wx.ALL| wx.EXPAND, 2)

        xybSizer.Add(xbSizer, 0, wx.ALL| wx.EXPAND, 2)
        xybSizer.Add(ybSizer, 0, wx.ALL| wx.EXPAND, 2)

        xymSizer.Add(xmSizer, 0, wx.ALL| wx.EXPAND, 2)
        xymSizer.Add(ymSizer, 0, wx.ALL| wx.EXPAND, 2)

        #TODO make the next CheckBox centered
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        #MainSizer.Add(self.chk_up_hfl, 0, wx.ALL,2)
        MainSizer.Add(h_spc_box, 0, wx.ALL,2)

        subt3Sizer.Add(subt3, 0, wx.ALL,2)

        wlSizer.Add(label_lambd, 0, wx.ALL, 2)
        wlSizer.Add(self.input_lambd, 2, wx.ALL| wx.EXPAND, 2)
        wlSizer.Add(label_lambd_units, 0, wx.ALL, 2)

        sddSizer.Add(label_dst_det, 0, wx.ALL, 2)
        sddSizer.Add(self.input_dst_det, 2, wx.ALL| wx.EXPAND, 2)
        sddSizer.Add(label_dst_det_units, 0, wx.ALL, 2)

        ddSizer.Add(label_diam_det, 0, wx.ALL, 2)
        ddSizer.Add(self.input_diam_det, 2, wx.ALL| wx.EXPAND, 2)
        ddSizer.Add(label_diam_det_units, 0, wx.ALL, 2)

        xySizer.Add(xybSizer, 0, wx.ALL| wx.EXPAND, 2)
        xySizer.Add(xymSizer, 0, wx.ALL| wx.EXPAND, 2)


        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(subt3Sizer, 0, wx.CENTER)


        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(wlSizer, 0, wx.LEFT)
        MainSizer.Add(sddSizer, 0, wx.LEFT)
        MainSizer.Add(ddSizer, 0, wx.LEFT)


        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)
        MainSizer.Add(xySizer, 0, wx.CENTER)
        MainSizer.Add(wx.StaticLine(self), 0, wx.ALL| wx.EXPAND, 2)

        self.SetSizer(MainSizer)
        MainSizer.Fit(self)
