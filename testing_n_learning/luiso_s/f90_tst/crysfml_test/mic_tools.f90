! This file is part of the Python - Fortran version of Anaelu project,
! a tool for the treatment of 2D - XRD patterns of textured samples
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this
! file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
Module mic_2D_tools
   ! -  -  -  -  Use Modules  -  -  -  - !

   use CFML_IO_Formats,                only: err_form_mess,err_form
   Use CFML_Math_General,              only : Cosd, Sind, Acosd, atand!, cotan
   use compilers_specific

   use global_types_and_dependencies

   ! -  -  -  -  Variables  -  -  -  - !
   implicit none
   private
    public   Get_Laue_Equiv, wr_img_file, wrt_pf, write_debye_prof

    character(len = 256)          :: fname
    integer                 :: i,  y     ! Reused control cycle variables
    !integer             :: log_uni

    contains

    subroutine Get_Laue_Equiv(tmpgrp,pnspgr)
        integer , intent(in)    :: tmpgrp
        integer , intent(out)   :: pnspgr

        If (tmpgrp >=  1 .And. tmpgrp <=  2) Then
            pnspgr = 2
            write(unit=*,fmt=*) "Point Group is Triclin"
        ElseIf (tmpgrp >=  3 .And. tmpgrp <=  15) Then
            pnspgr = 10
            write(unit=*,fmt=*) "Point Group is Monoclin"
        ElseIf (tmpgrp >=  16 .And. tmpgrp <= 74) Then
            pnspgr = 47
            write(unit=*,fmt=*) "Point Group is Ortoromb"
        ElseIf (tmpgrp >=  75 .And. tmpgrp <=  88) Then
            pnspgr = 83
            write(unit=*,fmt=*) "Point Group is Tetrag"
        ElseIf (tmpgrp >=  89 .And. tmpgrp <=  142) Then
            pnspgr = 123
            write(unit=*,fmt=*) "Point Group is Tetrag"
        ElseIf (tmpgrp >=  143 .And. tmpgrp <=  148) Then
            pnspgr = 147
            write(unit=*,fmt=*) "Point Group is Trigon"
        ElseIf (tmpgrp >=  149 .And. tmpgrp <=  167) Then
            pnspgr = 164
            write(unit=*,fmt=*) "Point Group is Trigon"
        ElseIf (tmpgrp >=  168 .And. tmpgrp <=  176) Then
            pnspgr = 175
            write(unit=*,fmt=*) "Point Group is Hexagn"
        ElseIf (tmpgrp >=  177 .And. tmpgrp <=  194) Then
            pnspgr = 191
            write(unit=*,fmt=*) "Point Group is Hexagn"
        ElseIf (tmpgrp >=  195 .And. tmpgrp <=  206) Then
            pnspgr = 200
            write(unit=*,fmt=*) "Point Group is Cubic"
        ElseIf (tmpgrp >=  207 .And. tmpgrp <=  230) Then
            pnspgr = 221
            write(unit=*,fmt=*) "Point Group is Cubic"
        Else
            write(unit=*,fmt=*) "No Sym"
            pnspgr = tmpgrp
        End If

    return
    end subroutine Get_Laue_Equiv

    subroutine wrt_pf()

        real, dimension(1:3)                :: hkl_vec
        integer                             :: pf_file_log_uni
        integer                             :: ysiz
        ysiz = ubound(texture_info%pf,1)
        !Call Get_Logunit(pf_file_log_uni)
        log_uni = log_uni + 1
        pf_file_log_uni = log_uni

        write(*,*) "pf_file_log_uni =" , pf_file_log_uni

        fname = '1d_pf.dat'
        OPEN(unit = pf_file_log_uni, file = fname, status = "replace", form = "formatted")
        do i = 1,pk_lst%npk,1
            hkl_vec(1:3) = real(pk_lst%pk(i)%H(1:3))

            write(unit = pf_file_log_uni,fmt=*) "___________________________________________________________________________"
            if( hkl_vec(1) == int(hkl_vec(1)) .and. hkl_vec(2) == int(hkl_vec(2)) .and. hkl_vec(3) == int(hkl_vec(3)))then
                write(unit = pf_file_log_uni,fmt=*) "(",int(hkl_vec),")  Pole Figure"
            else
                write(unit = pf_file_log_uni,fmt=*) "(",hkl_vec,")  Pole Figure"
            end if
            do y = 1,ysiz,1
                write(unit = pf_file_log_uni,fmt=*) (real(y) / real(ysiz)) * 180.0," , ", texture_info%pf(y,i)
            end do
            write(unit = pf_file_log_uni,fmt=*) " "

        end do
        close(unit = pf_file_log_uni)

        write(unit=*,fmt = "(a)") " 1D Pole figures in File: "//fname

    return
    end subroutine wrt_pf


    subroutine write_debye_prof()

        real, dimension(1:3)                :: hkl_vec
        integer                             :: debye__ring_file_log_uni

        !Call Get_Logunit(debye__ring_file_log_uni)
        log_uni = log_uni + 1
        debye__ring_file_log_uni = log_uni

        write(*,*) "debye__ring_file_log_uni =", debye__ring_file_log_uni

        fname = 'Debye_profile.dat'

        OPEN(unit = debye__ring_file_log_uni, file = fname, status = "replace", form = "formatted")

        write(*,*) "here tst 01"
        write(*,*) "sm_prt = ", sm_prt
        write(*,*) "pk_lst%npk = ", pk_lst%npk
        write(*,*) "pk_lst%"

        do i = 1, pk_lst%npk,1
            hkl_vec(1:3) = real(pk_lst%pk(i)%H(1:3))

            write(unit = debye__ring_file_log_uni,fmt=*) "_______________________________________________________"
            if( hkl_vec(1) == int(hkl_vec(1)) .and. hkl_vec(2) == int(hkl_vec(2)) .and. hkl_vec(3) == int(hkl_vec(3)))then
                write(unit = debye__ring_file_log_uni,fmt=*) "(",int(hkl_vec),")  Profile along Debye ring"
            else
                write(unit = debye__ring_file_log_uni,fmt=*) "(",hkl_vec,")  Profile along Debye ring"
            end if
            do y = 1,sm_prt%ipf_yres,1
                write(unit = debye__ring_file_log_uni,fmt=*) (real(y) / real(sm_prt%ipf_yres)) * 180.0," , ", Debye_prof(y,i)
            end do
            write(unit = debye__ring_file_log_uni,fmt=*) " "

        end do
        write(*,*) "here tst 02"

        close(unit = debye__ring_file_log_uni)

        write(unit=*,fmt = "(a)") " 1D Debye profile in File: "//fname

    return
    end subroutine write_debye_prof

    subroutine wr_img_file()
        character(len=255)                               :: f_name_ini = "tmp_name"

        real(kind = 8), dimension(:,:), allocatable     :: pat2d_raw
        real(kind = 4), dimension(:,:), allocatable     :: pat2d_edf
        ![bin_output] 0=ascii, 1=raw 2=edf 10=all_bin 11=all
        integer(kind = 2)                               :: bin_output=10
        character(len = 256)                            :: fname
        integer                                         :: x, y

        allocate(pat2d_raw(par_2d%xres,par_2d%yres))
        allocate(pat2d_edf(par_2d%xres,par_2d%yres))
        do y = 1,par_2d%yres,1
            do x = 1,par_2d%xres,1
                pat2d_raw(x,y) = pat2d(x,y) ! * 0.001
                pat2d_edf(x,y) = pat2d(x,y) ! * 0.001
            end do
        end do

        !write(*,*) "f_name_ini =", f_name_ini
        !write(unit = fname, fmt = *) f_name_ini // "tail test"
        !write(*,*) "fname (tst) =", fname

        if( bin_output == 0 .or.  bin_output == 11)then
            !Call Get_Logunit(log_uni)
            log_uni = log_uni + 1

            write(unit = fname, fmt = *) f_name_ini // "_1.asc"
            fname = adjustl(fname)
            write(*,*) "log_uni (wr_img_file) =", log_uni

            write(*,*) "Writing image into: ", fname, " this might take a while"
            OPEN(unit = log_uni, file = trim(fname), status = "replace", form = "formatted")

            write(unit = log_uni, fmt=*) 'Text output of image region:'
            write(unit = log_uni, fmt=*) 'Number of pixels in X direction  = ', par_2d%xres
            write(unit = log_uni, fmt=*) 'Number of pixels in Y direction  = ', par_2d%yres
            write(unit = log_uni, fmt=*) 'X starting pixel of ROI = 1'
            write(unit = log_uni, fmt=*) 'Y starting pixel of ROI = 1'
            write(unit = log_uni, fmt=*) 'Pixel values follow (X - direction changing fastest, bottom left first):'

            do y = 1,par_2d%yres,1
                call prog_bar(1,x,par_2d%xres,' writing IMG')
                do x = 1,par_2d%xres,1
                    write(unit = log_uni, fmt =*) pat2d(x,y)
                end do
            end do
            close(unit = log_uni)
            write(unit=*,advance = 'yes',fmt = '(A)') char(13)
            write(unit=*,fmt = "(a)") " Result image in File: "//fname
        endif

        if( bin_output == 1 .or.  bin_output == 10 )then
            !Call Get_Logunit(log_uni)
            log_uni = log_uni + 1
            fname = my_def%field(4)%value
            fname = adjustl(fname)

            write(*,*) "RAW fname", fname

            OPEN(unit = log_uni, file = trim(fname), status = "replace", access = "stream", form = "unformatted")
            write(unit = log_uni) pat2d_raw

            close(unit = log_uni)
            write(unit=*,advance = 'yes',fmt = '(A)') char(13)
            write(unit=*,fmt = "(a)") " Result image in File: "//fname
        end if

        if( bin_output == 2 .or. bin_output == 10 )then
            !Call Get_Logunit(log_uni)
            log_uni = log_uni + 1
            fname = my_def%field(3)%value
            fname = adjustl(fname)
            OPEN(unit = log_uni, file = trim(fname), status = "replace", form = "formatted")

            write(unit = log_uni, fmt = '(A)') "{"
            write(unit = log_uni, fmt = '(A)') "HeaderID = EH:000001:000000:000000;"
            write(unit = log_uni, fmt = '(A)') "Image = 1 ;"
            write(unit = log_uni, fmt = '(A)') "ByteOrder = LowByteFirst ;"
            write(unit = log_uni, fmt = '(A)') "DataType = FloatValue ;"
            write(unit = log_uni, fmt = 1010 ) par_2d%xres
            write(unit = log_uni, fmt = 1011 ) par_2d%yres
            write(unit = log_uni, fmt = 1012 ) 4 * par_2d%yres * par_2d%xres
            !write(unit = log_uni, fmt = '(A)') "Size = 2097152 ;"
            write(unit = log_uni, fmt = '(A)') "Title = EDF file by ANAELU;}"


1010        format('Dim_1 = ',I8,' ;')
1011        format('Dim_2 = ',I8,' ;')
1012        format('Size = ',I8,' ;')

            close(unit = log_uni)
            OPEN(unit = log_uni, file = trim(fname), position = "append", access = "stream", &
             form = "unformatted", convert="LITTLE_ENDIAN")

            write(unit = log_uni) pat2d_edf

            close(unit = log_uni)
            write(unit=*,advance = 'yes',fmt = '(A)') char(13)
            write(unit=*,fmt = "(a)") " Result image in File: "//fname

        end if

    return
    end subroutine wr_img_file



End Module mic_2D_tools

