#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
echo " Cleaning"
rm anaelu_calc_xrd                                #removing previous exe
rm *.raw                                          #removing leftover
echo " Building"
./make_anaelu_gfortran64.sh                       # calling compile script
echo "Running"
./anaelu_calc_xrd my_params.dat cfl_test.cfl      # running
python matplot_2d_bin.py                          # plotting
#
#./make_anaelu_gfortran.sh
#./anaelu_calc_xrd dat_in=my_params.dat cfl_in=cfl_test.cfl edf_out=img_file.edf raw_out=raw_img_file.raw
#echo "Plotting"
#python matplot_2d_bin.py
#python matplot_2d_pat_edf.py
#
