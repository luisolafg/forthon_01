module global_types_and_dependencies

    ! -  -  -  -  Use Modules  -  -  -  - !
    use input_arg_tools,                only: arg_def_list
    use CFML_crystallographic_symmetry, only: space_group_type, Write_SpaceGroup
    use CFML_Atom_TypeDef,              only: Atom_List_Type, Write_Atom_List
    use CFML_Crystal_Metrics,           only: Crystal_Cell_Type, Write_Crystal_Cell
    use CFML_Reflections_Utilities,     only: Reflection_List_Type, Hkl_Uni, get_maxnumref

    ! -  -  -  -  Variables  -  -  -  - !
    implicit none

    public
    real, dimension(:,:), allocatable , public , save  :: Debye_prof
    Type   :: area_det_intrum
        integer :: xres,yres             ! res (pixels)
        real    :: lambda                ! wavelenght
        real    :: Thmin, Thmax          ! Min and Max angles
        real    :: dst_det,diam_det      ! Distances (mm)
        real    :: x_dr,y_dr             ! position of direct beam in the detector (pixels)
    End Type area_det_intrum

    Type   :: sample_n_instrument_properties
        real                    :: crys_size = 0.45     ! crystal size
        real                    :: itm_broad = 0.02     ! instrumental broadening in deg
        integer                 :: ipf_xres
        integer                 :: ipf_yres
        real                    :: prf_width
        real                    :: ipf_ang = 0.0
        character(len = 256)      :: pref_hkl_string
    End Type sample_n_instrument_properties

    Type   :: peak_details
        integer, dimension(3)           :: H            ! H K L
        real                            :: Its          ! calculated intensity
        real                            :: bragg        ! bragg angle
        real                            :: fwhm         ! full width at half maximum
        integer                         :: wt_px        ! width in pixels
        integer                         :: dst_px_from  ! distance in pixels from the direct beam position  (start)
        integer                         :: dst_px_to    ! distance in pixels from the direct beam position    (end)
        integer                         :: dst_px_max_p ! distance in pixels from the direct beam position (max pos)
        real, dimension(:),allocatable  :: i_px         ! intensity in function of distance in pixels
    end Type peak_details

    Type   :: peak_details_list
        integer                                         :: npk  ! Number of peaks
        Type (peak_details), dimension(:), allocatable  :: pk   ! 1D data of the peak
    end Type peak_details_list

    Type texture_data
        real, dimension(:,:), allocatable               :: ipf
        real, dimension(:,:), allocatable               :: pf
        real, dimension(:),   allocatable               :: tmp_pf
    end Type texture_data

    type (area_det_intrum)            , public , save  :: par_2d   ! parameters of diffractometer with 2D detector
    type (peak_details_list)              , public , save   :: pk_lst   ! list of calculated peaks
    type (sample_n_instrument_properties) , public , save   :: sm_prt ! parameters that affect the peak width and azimut

    type (space_group_type)  , public , save  :: SpGroup
    type (Atom_list_Type)    , public , save  :: At_list
    type (Crystal_Cell_Type) , public , save  :: UnitCell
    type (Reflection_List_Type)  , public , save   :: hkl
    type (texture_data)     , public    , save  :: texture_info

    Type(arg_def_list)              :: my_def


    real, dimension(:,:), allocatable , public , save  :: pat2d
    integer                 , public, save      ::log_uni = 15

end module global_types_and_dependencies
