import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
#from PySide2.QtGui import *

from PySide2 import QtUiTools

class Form(QObject):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("launcher.ui")
        self.window.Run1Button.clicked.connect(self.btn_1_clk)
        self.window.Run2Button.clicked.connect(self.btn_2_clk)
        self.window.Run3Button.clicked.connect(self.btn_3_clk)
        self.window.show()

    def btn_1_clk(self):
        print("self.btn_1_clk")
        self.window.Run1Button.setEnabled(False)
        self.window.Run2Button.setEnabled(True)
        self.window.Run3Button.setEnabled(True)

        print(
            "self.window.ShowAllPatternsCheckBox.isChecked =",
            self.window.ShowAllPatternsCheckBox.isChecked()
        )


    def btn_2_clk(self):
        print("self.btn_2_clk")
        self.window.Run1Button.setEnabled(True)
        self.window.Run2Button.setEnabled(False)
        self.window.Run3Button.setEnabled(True)

    def btn_3_clk(self):
        print("self.btn_3_clk")
        self.window.Run1Button.setEnabled(True)
        self.window.Run2Button.setEnabled(True)
        self.window.Run3Button.setEnabled(False)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())

