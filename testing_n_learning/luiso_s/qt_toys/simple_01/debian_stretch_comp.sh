QT5_PATH="/usr/include/x86_64-linux-gnu/qt5"

g++ -c -m64 -pipe -O2 -Wall -W -D_REENTRANT \
    -fPIC -DQT_NO_DEBUG -DQT_WIDGETS_LIB \
    -DQT_GUI_LIB -DQT_CORE_LIB -I. -I. \
    -isystem $QT5_PATH \
    -isystem $QT5_PATH/QtWidgets \
    -isystem $QT5_PATH/QtGui -isystem $QT5_PATH/QtCore \
    -I. -I/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++-64 \
    -o main_gui.o main_gui.cpp

g++ -m64 -Wl,-O1 -o simple_01 main_gui.o \
    -L/usr/X11R6/lib64 -lQt5Widgets \
    -lQt5Gui -lQt5Core -lGL -lpthread
