echo " Program Compilation"
OPT1="-c -O2 "
gfortran  $OPT1             cmd_par.f90
gfortran  $OPT1            read_bin.f90

echo " Linking"
gfortran -o read_bin.r *.o

echo "\n\n Running test #1"
./read_bin.r APT73_d122_from_m02to02__01_41.mar2300.bin xres=1234 yres=5432 zres=0.000001 out_file.txt
echo "\n\n Running test #2"
./read_bin.r file_in=A.mar2300.bin xres    =1234 yres=   5432 zres   =    0.000001 niters   =  0 v = 2 file_out=B.txt
echo "\n\n Running test #3"
./read_bin.r A.mar2300.bin     xres=1234 yres=5432     zres=0.000001    wvar=3.14159265358   u=0  v=2    file=B.txt
echo "\n\n Running test #4"
./read_bin.r mar2300.bin
echo "\n\n Removing leftovers"
rm *.o *.mod *.r
