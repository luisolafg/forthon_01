module deps
    real vars

    contains

    subroutine sub_tst1
        write(*,*) "Hi"
    end subroutine sub_tst1


end module deps

program prog_main
    use deps
    use inside_one
    implicit none
    call primera_dep()
    write(*,*) "a =", a
    a = a + 2
    write(*,*) "a =", a

    call sub_tst1()
end program prog_main


