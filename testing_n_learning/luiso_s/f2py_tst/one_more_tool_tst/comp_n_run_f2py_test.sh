#  This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
#echo " "
#echo "removing previous builds"
#echo " "
#rm *.so *.pyc
echo "removing previously compiled module"
rm *.so
echo "building with f2py"
f2py -c -m f90_tools  tools.f90
echo "running python test/caller"
python test_call_to_f90_mod.py ../../../../../test_area/apt73ppp/img_xrd.edf

