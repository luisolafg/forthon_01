
module inner_tools
    implicit none

    real(kind=8),  dimension(:,:), allocatable :: img_out
    contains

    subroutine rotate_img(img_in)
        real(kind=8),  dimension(:,:), intent (in) :: img_in
        integer                     :: xmax, ymax, x, y

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        do x = 1, xmax
            do y = 1, ymax
                img_out(y, x) = img_in(xmax - x + 1, y)
            end do
        end do
    end subroutine rotate_img


end module inner_tools
