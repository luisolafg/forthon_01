import f90_tools
from matplotlib import pyplot as plt
import numpy as np
import fabio
import sys

if( __name__ == "__main__" ):

    arg_lst = sys.argv
    print "sys.argv =", arg_lst
    path_to_img = sys.argv[1]

    ini_img = fabio.open(path_to_img).data.astype(np.float64)
    xres, yres = np.shape(ini_img)

    plt.imshow( ini_img , interpolation = "nearest" )
    plt.show()

    f90_tools.inner_tools.rotate_img(ini_img)
    arr_out = f90_tools.inner_tools.img_out

    plt.imshow( arr_out , interpolation = "nearest" )
    plt.show()

    new_img = fabio.edfimage.edfimage()
    print "type(new_img) =", type(new_img)

    new_img.data = arr_out
    file_out = path_to_img + ".out"
    print "file_out =", file_out
    new_img.write(file_out)
