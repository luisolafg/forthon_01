        SUBROUTINE FIB(A,N)
            implicit none

            INTEGER N
            REAL*8 A(N)
cf2py intent(in) n
cf2py intent(out) a
cf2py depend(n) a
            INTEGER :: i
            DO I=1,N
                A(I) = I * 2
            ENDDO
        END SUBROUTINE FIB
