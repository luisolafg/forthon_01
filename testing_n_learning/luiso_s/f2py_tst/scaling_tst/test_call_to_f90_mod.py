import f90_tools
from matplotlib import pyplot as plt
import numpy as np
import fabio
import sys

def plot_scaled(img_arr):
    pre_val_min = img_arr[0, 0]
    pre_val_max = img_arr[1, 1]

    img_arr[1, 1] = 19946
    img_arr[0, 0] = -10

    plt.imshow( img_arr , interpolation = "nearest" )
    img_arr[0, 0] = pre_val_min
    img_arr[1, 1] = pre_val_max
    plt.show()

if( __name__ == "__main__" ):
    arg_lst = sys.argv
    print "sys.argv =", arg_lst

    path_to_img_1 = sys.argv[1]
    exp_img_arr = fabio.open(path_to_img_1).data.astype(np.float64)
    exp_img_arr[1020:1190,1125:1180] = -2
    #plot_scaled(exp_img_arr)

    path_to_img_2 = sys.argv[2]
    calc_img_arr = fabio.open(path_to_img_2).data.astype(np.float64)
    #plot_scaled(calc_img_arr)

    path_to_img_3 = sys.argv[3]
    mask_img_arr = fabio.open(path_to_img_3).data.astype(np.float64)
    mask_img_arr[1020:1190,1125:1180] = -2

    #plt.imshow( mask_img_arr , interpolation = "nearest" )
    #plt.show()

    path_to_img_4 = sys.argv[4]
    smth_img_arr = fabio.open(path_to_img_4).data.astype(np.float64)
    smth_img_arr[1020:1190,1125:1180] = -2
    #plot_scaled(smth_img_arr)


    not_needed_for_now = '''
    Nrows = np.size( exp_img_arr[0:1, :] )
    Ncols = np.size( exp_img_arr[:, 0:1] )
    print "Nrows, Ncols =", Nrows, Ncols

    f90_tools.inner_tools.ref_scale(exp_img_arr,
                                    calc_img_arr,
                                    mask_img_arr,
                                    smth_img_arr)

    arr_out = f90_tools.inner_tools.img_out

    print "plotting"

    plot_scaled(arr_out)

    new_img = fabio.edfimage.edfimage()

    new_img.data = arr_out
    file_out = path_to_img_1 + ".scaled.edf"
    print "file_out =", file_out
    new_img.write(file_out)
    '''

    Nrows = np.size( exp_img_arr[:, 0:1] )
    Ncols = np.size( exp_img_arr[0:1, :] )

    scaled_bkg = np.empty( (Nrows, Ncols), np.float64)
    tot_img_mod = np.empty( (Nrows, Ncols), np.float64)
    scaled_calc = np.empty( (Nrows, Ncols), np.float64)

    f90_tools.inner_tools.scale_n_shift_img(smth_img_arr, 0.5, 0.0)
    scaled_bkg[:,:] = f90_tools.inner_tools.img_out

    r_good = -1

    for scale_int_step in xrange(15):

        scale_float = scale_int_step * 0.003
        print "scale_float =", scale_float


        f90_tools.inner_tools.scale_n_shift_img(calc_img_arr, scale_float, 0.0)
        scaled_calc[:,:] = f90_tools.inner_tools.img_out

        f90_tools.inner_tools.add_2_imgs(scaled_bkg, scaled_calc)
        tot_img_mod[:,:] = f90_tools.inner_tools.img_out

        #plot_scaled(tot_img_mod)

        f90_tools.inner_tools.comp_imgs(tot_img_mod, exp_img_arr, mask_img_arr)

        r = f90_tools.inner_tools.r_fact
        print "r =", r

        if(r_good == -1 or r < r_good):
            print "<<"
            r_good = float(r)
            scale_good = scale_float

    print "\n best scale for 2d_calc = ", scale_good
