
module inner_tools
    implicit none
    real(kind=8),  dimension(:,:), allocatable :: img_out
    real(kind=8)                               :: tot_dif, tot_prop, r_fact
    real(kind=8)                               :: shif_fact, scal_fact
    integer(kind=8)                            :: xmax, ymax

    contains

    subroutine ref_scale(exp_img, calc_img, mask_img, bkgr_img)
        real(kind=8),  dimension(:,:), intent (in)  :: exp_img
        real(kind=8),  dimension(:,:), intent (in)  :: calc_img
        real(kind=8),  dimension(:,:), intent (in)  :: mask_img
        real(kind=8),  dimension(:,:), intent (in)  :: bkgr_img
        real(kind=8),  dimension(:,:), allocatable  :: temp_img
        real(kind=8)                                :: i_max = 0

        integer                                     :: times_loop, outer_loop
        integer                                     :: x, y

        xmax = ubound(exp_img, 1)
        ymax = ubound(exp_img, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        if( allocated(temp_img) )then
            deallocate(temp_img)
        end if
        allocate(temp_img(ymax, xmax))
        temp_img = bkgr_img

        call comp_imgs(temp_img, exp_img, mask_img)

        do outer_loop = 1, 2
            write(*,*) "____________________________________ scale"
            do times_loop = 1, 2
                write(*,*)
                write(*,*) "tot_prop =", tot_prop, "r_fact =", r_fact
                scal_fact = tot_prop
                temp_img = bkgr_img / scal_fact
                call comp_imgs(temp_img, exp_img, mask_img)
            end do
            write(*,*) "____________________________________ shift"
            do times_loop = 1, 2
                write(*,*)
                write(*,*) "tot_dif =", tot_dif, "r_fact =", r_fact
                temp_img = bkgr_img / scal_fact
                shif_fact = tot_dif
                temp_img = temp_img - shif_fact
                call comp_imgs(temp_img, exp_img, mask_img)
            end do
        end do

        img_out = temp_img

    end subroutine ref_scale

    subroutine comp_imgs(img1, img2, mask_img)
        real(kind=8),  dimension(:,:), intent (in)  :: img1, img2, mask_img
        integer                                     :: x, y
        integer(kind=8)                             :: n_px
        real(kind=8)                                :: dif, prop

        n_px = 0
        do x = 1, xmax
           do y = 1, ymax
                if(mask_img(y, x) /= -1)then
                    n_px = n_px + 1
                end if
            end do
        end do

        tot_dif = 0.0
        tot_prop = 0.0
        r_fact = 0.0
        do x = 1, xmax
           do y = 1, ymax
                if(mask_img(y, x) /= -1)then
                    dif = img1(y, x) - img2(y, x)
                    tot_dif = tot_dif + dif / real(n_px)
                    r_fact = r_fact + abs(dif)

                    if(img2(y,x) > 0.0 .and. img1(y,x) > 0 )then
                        prop = img1(y, x) / img2(y, x)
                        tot_prop = tot_prop + prop / real(n_px)
                    end if

                end if
            end do
        end do

    end subroutine comp_imgs


    subroutine add_2_imgs(img1, img2)
        real(kind=8),  dimension(:,:), intent (in)  :: img1, img2
        integer                                     :: x, y
        integer                                     :: xmax, ymax

        xmax = ubound(img1, 1)
        ymax = ubound(img1, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        if(xmax == ubound(img2, 1) .and. ymax == ubound(img2, 2))then
            img_out = img1 + img2
        else
            write(*,*) " - ERROR -"
            write(*,*) "arrays with different shapes"
            img_out = -1.0
        end if

    end subroutine add_2_imgs

    subroutine scale_n_shift_img(img_in, scale_in, shif_in)
        real(kind=8),  dimension(:,:), intent (in)  :: img_in
        real(kind=8),                  intent (in)  :: scale_in, shif_in

        xmax = ubound(img_in, 1)
        ymax = ubound(img_in, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        img_out(:,:) = img_in(:,:) * scale_in
        img_out(:,:) = img_out(:,:) + shif_in

    end subroutine scale_n_shift_img

end module inner_tools
