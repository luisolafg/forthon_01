//-*-c++-*-
/*
  Bisection method as test and learn
*/

import 'dart:math';

float func( float x ){
  float y;
  y = x * x + 5.0;
  return y;
}

void main() {
  float x, y, y_target;
  //var x_lst = [];
  //var y_lst = [];
  // begin test code

  x = 1;
  y = func(x);
  y_target = y;

  // [ equation ] -> if (x = 1) and ( y = x ** 2 + 5) y = 6 )
  // end test code

  float x1, x2, y1, y2;
  x1 = 0.2;
  x2 = 1.2;
  y1 = func(x1);
  y2 = func(x2);
  print("func( $x1 ) = $y1");
  print("func( $x2 ) = $y2");
  var found_sol = 0;
  for(int i = 0; i < 60 && found_sol == 0; i++){
    print (i);
    x = ( x1 + x2 ) / 2.0;
    y = func(x);

    print("func( $x1 ) = $y1");
    print("func( $x2 ) = $y2");

    if( y > y_target ){
      x2 = x;
      y2 = y;
    }else if( y < y_target ){
      x1 = x;
      y1 = y;
    }else{
      print( "solution, x = $x" );
      found_sol = 1;
    }
  }
  print("x, y = $x , $y");
}
