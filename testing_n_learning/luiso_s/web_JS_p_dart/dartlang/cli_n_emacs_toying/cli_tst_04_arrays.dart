//-*-java-*-
/*
emacs example with dart language use

the first line of this file is to tell
emacs that this file should be highlighted
with java syntax style

to open menu press [ M-x ]

then type "menu-bar-open"

[ M- ] = press "Alt" or "Meta" key
[ x ] = "x" letter key
 */

import 'dart:math';
class func{
    float a, b;
    //float x1[10,10], x2[5,5];
    
    func(this.a, this.b){
	print("entered:\n $a, $b");
    }

}

void main() {
    var fnc = new func(5, 5);
    print("accessing func.a, func.b = ${fnc.a}, ${fnc.b}");
}
