
/*
exersices with growable lists
here is tested the dinamic memory
vehaviour in dart


List<int> fixedLengthList = new List(5);
fixedLengthList.length = 0;  // Error
fixedLengthList.add(499);    // Error
fixedLengthList[0] = 87;
List<int> growableList = [1, 2];
growableList.length = 0;
growableList.add(499);
growableList[0] = 87;

*/
main(){
    var my_lst = [];
    print(my_lst);
    my_lst.add("cad1");
    print(my_lst);
    my_lst.add(1);
    print(my_lst);
    print("_____________________");

    var lst2 = ["x",666];
    /*
      if lst2 here declared as:

      var lst2 = my_lst;

      the behaviour would have been completelly different because
      lst_sum list would contain two pointers to the same list
    */

    print("my_lst =");
    print(my_lst);

    print("lst2 =");
    print(lst2);
    print("_____________________");
    var lst_sum = [my_lst,lst2];
    print("lst_sum = [my_lst,lst2] =");
    print(lst_sum);
    lst_sum[0][1] += 5;
    lst_sum[1][1] += 2;
    print("_____________________");
    print("lst_sum(after modif) =");
    print(lst_sum);
    lst2[0]="a";
    print("_____________________");
    print("lst_sum(after modif 2) =");
    print(lst_sum);
    my_lst[0]="b";
    print("_____________________");
    print("lst_sum(after modif 3) =");
    print(lst_sum);


    //List<int> my_lst = new List(5);

    /*
    for(int i = 0; i < 5; i++){
	//my_lst.add(0);
        my_lst[i] = i * 3;
        print("i =${i}");
        print("my_lst[${i}] =");
        print(my_lst[i]);
    }
    */

}
