import numpy as np
from matplotlib import pyplot as plt

with open('CE_RSM 10deg_Chi_Phi10_0001.txt', 'r') as f:
    # do things with your file
    data = f.read()

f.close()

split_data = data.split("\n")

print type(split_data)
print len(split_data)

num_data = split_data[14:len(split_data)]
xyi_lst = []

for line in num_data:
    listed_dat = line.split("\t")
    if(len(listed_dat) == 4):
        x = float(listed_dat[0])
        y = float(listed_dat[1])
        i = float(listed_dat[2])
        xyi_lst.append([x,y,i])

print "done reading"

xmin = xyi_lst[0][0]
ymin = xyi_lst[0][0]
xmax = xyi_lst[0][1]
ymax = xyi_lst[0][1]

print "xmin, ymin, xmax, ymax =", xmin, ymin, xmax, ymax

for listed_dat in xyi_lst:
    x = listed_dat[0]
    y = listed_dat[1]

    if(x < xmin):
        xmin = x

    if(x > xmax):
        xmax = x

    if(y < ymin):
        ymin = y

    if(y > ymax):
        ymax = y

print "xmin, ymin, xmax, ymax =", xmin, ymin, xmax, ymax

arr_x_size = int(xmax - xmin) + 1
arr_y_size = int(ymax - ymin) + 1

img_arr = np.zeros((arr_x_size, arr_y_size),'double')

print "arr size =", arr_x_size, arr_y_size

for listed_dat in xyi_lst:
    x = listed_dat[0]
    y = listed_dat[1]
    i = listed_dat[2]
    x_pos = int(x - xmin)
    y_pos = int(y - ymin)

    try:
        img_arr[x_pos, y_pos] = i

    except:
        print "failed in arr pos", x_pos, y_pos


plt.imshow( img_arr , interpolation = "nearest" )
plt.show()

