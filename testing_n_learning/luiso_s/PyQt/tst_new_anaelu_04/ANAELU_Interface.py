import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)

    def wheelEvent(self, event):
        int_delta = int(event.delta())
        self.m_scrolling.emit(int_delta)
        event.accept()

class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("ANAELU_interface.ui")

        self.gscene_list = []

        for n in range(4):
            new_gsecene = MultipleImgSene()
            new_gsecene.m_scrolling.connect(self.scale_all_views)
            self.gscene_list.append(new_gsecene)

        self.gview_list = [self.window.graphicsView_Exp, self.window.graphicsView_Mask_n_Diff,
                           self.window.graphicsView_Modl, self.window.graphicsView_Bakgr]

        for n, gview in enumerate(self.gview_list):
            gview.setScene(self.gscene_list[n])
            gview.setDragMode(QGraphicsView.ScrollHandDrag)

        self.window.pushButton_load_exp.clicked.connect(self.set_img1)
        self.window.pushButton_mask_model.clicked.connect(self.set_img2)
        self.window.run_model.clicked.connect(self.set_img3)
        self.window.background_smoothing.clicked.connect(self.set_img4)

        '''
        for gview in self.gview_list:
            gview.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            gview.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        '''

        self.real_time_zoom = False

        for gview in self.gview_list:
            gview.verticalScrollBar().valueChanged.connect(self.scroll_all_v_bars)
            gview.horizontalScrollBar().valueChanged.connect(self.scroll_all_h_bars)

        self.window.show()

    def scroll_all_v_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.verticalScrollBar().setValue(value)

    def scroll_all_h_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.horizontalScrollBar().setValue(value)

    def scale_all_views(self, scale_fact):
        self.real_time_zoom = True
        for gview in self.gview_list:
            gview.scale(1.0 + float(scale_fact) / 2500.0,
                        1.0 + float(scale_fact) / 2500.0)
        self.real_time_zoom = False

    def set_img_n(self, img_path, gscene_num):
        image1 = QImage(img_path)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.gscene_list[gscene_num].addPixmap(self.pixmap_1)

    def set_img1(self):
        self.set_img_n("../../../../miscellaneous/lena.jpeg", 0)

    def set_img2(self):
        self.set_img_n("../../../../miscellaneous/lena_gray.jpeg", 1)

    def set_img3(self):
        self.set_img_n("../../../../miscellaneous/lena_inverted.jpeg", 2)

    def set_img4(self):
        self.set_img_n("../../../../miscellaneous/lena_gray_inverted.jpeg", 3)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
