import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("ANAELU_interface.ui")
        #self.window = QtUiTools.QUiLoader().load("ANAELU_interface_tmp.ui")

        #image = "../../../miscellaneous/lena.jpeg" #path to your image file

        '''
        myscene = QGraphicsScene()
        self.window.graphicsView_2.setScene(myscene)
        self.window.graphicsView_3.setScene(myscene)
        self.window.graphicsView_4.setScene(myscene)
        self.window.graphicsView_5.setScene(myscene)
        '''

        self.window.show()



if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
