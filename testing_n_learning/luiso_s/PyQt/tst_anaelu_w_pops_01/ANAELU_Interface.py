import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class PopScene(QGraphicsScene):
    signalMousePos = Signal(int)
    def __init__ (self, parent=None):
        super(PopScene, self).__init__ (parent)

    def mousePressEvent(self, event):
        print("\n mousePressEvent")
        q_point = event.scenePos()
        self.ini_pos = q_point.x(), q_point.y()
        print("q_point =", self.ini_pos)

    def mouseMoveEvent(self, event):
        #print("\n mouseMoveEvent")
        q_point = event.scenePos()
        x2, y2 = q_point.x(), q_point.y()
        #print("q_point =", self.mov_pos)

        try:
            self.removeItem(self.my_rect)

        except AttributeError:
            print("first addRect")

        x1 = self.ini_pos[0]
        y1 = self.ini_pos[1]

        if x1 > x2:
            x2, x1 = x1, x2

        if y1 > y2:
            y2, y1 = y1, y2

        dx = x2 - x1
        dy = y2 - y1

        self.my_rect = self.addRect(x1, y1, dx, dy)

    def mouseReleaseEvent(self, event):
        print("\n mouseReleaseEvent")
        q_point = event.scenePos()
        self.rel_pos = q_point.x(), q_point.y()
        print("q_point =", self.rel_pos)


class MyPopupDialog(QObject):
    def __init__(self, parent=None):
        super(MyPopupDialog, self).__init__(parent)
        self.pop_dia = QtUiTools.QUiLoader().load("pop_dialog.ui")
        self.pop_dia.show()

        self.img_scene = PopScene()
        self.pop_dia.InerGraphicsView.setScene(self.img_scene)
        self.pop_dia.InerGraphicsView.setDragMode(QGraphicsView.NoDrag)

        fileName = "../../../../miscellaneous/lena_inverted.jpeg"
        image1 = QImage(fileName)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.img_scene.addPixmap(self.pixmap_1)


class Form(QObject):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("ANAELU_interface.ui")

        self.exp_scene = QGraphicsScene()
        self.window.graphicsView_Exp.setScene(self.exp_scene)
        self.window.graphicsView_Exp.setDragMode(
            QGraphicsView.ScrollHandDrag
        )
        self.mask_n_diff_scene = QGraphicsScene()
        self.window.graphicsView_Mask_n_Diff.setScene(
            self.mask_n_diff_scene
        )
        self.window.graphicsView_Mask_n_Diff.setDragMode(
            QGraphicsView.ScrollHandDrag
        )
        self.window.pushButton_load_exp.clicked.connect(self.set_img1)
        self.window.pushButton_mask_model.clicked.connect(self.set_img2)
        self.window.pop_mark_tool.clicked.connect(self.pop_mask)
        self.window.show()

    def pop_mask(self):
        print("pop_mask ...")
        self.dialog = MyPopupDialog(self)

    def set_img1(self):
        fileName = "../../../../miscellaneous/lena.jpeg"
        image1 = QImage(fileName)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.exp_scene.addPixmap(self.pixmap_1)

    def set_img2(self):
        fileName = "../../../../miscellaneous/lena_gray.jpeg"
        image2 = QImage(fileName)
        self.pixmap_2 = QPixmap.fromImage(image2)
        self.mask_n_diff_scene.addPixmap(self.pixmap_2)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
