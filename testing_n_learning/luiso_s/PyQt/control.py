#!/usr/bin/python
# -*- coding: utf-8 -*-

from subprocess import call as shell_func
import sys

from PyQt4.QtGui import *
from PyQt4.QtCore import *

class Example( QWidget):

    def __init__(self):
        super(Example, self).__init__()

        cb = QCheckBox('ECHO', self)
        cb.move(50, 40)
        cb.stateChanged.connect(self.changeCheck)

        size_double_box = 26
        box_dst = 80
        x_real_pos = 550
        x_label_pos = 350
        y_pos = 330


        self.lin_txt1 =  QDoubleSpinBox(self)
        y_pos = 50
        self.lin_txt1.setRange(-99999999, 99999999)
        self.lin_txt1.move(x_real_pos, y_pos)
        self.opt_box1 = QComboBox(self)
        self.opt_box1.lin_ed = self.lin_txt1
        self.opt_box1.lst_opt=[]
        self.opt_box1.lst_opt.append("10")
        self.opt_box1.lst_opt.append("15")
        self.opt_box1.lst_opt.append("20")
        self.opt_box1.lst_opt.append("25")
        self.opt_box1.lst_opt.append("30")
        for lst_itm in self.opt_box1.lst_opt:
            self.opt_box1.addItem(lst_itm)
        self.opt_box1.setCurrentIndex(0)
        self.opt_box1.currentIndexChanged.connect(self.combobox_changed)
        self.opt_box1.move(x_real_pos, y_pos + size_double_box)
        FreqLabel = QLabel(parent = self, text = "Frequency (MHz) ")
        FreqLabel.move(x_label_pos, y_pos)


        y_pos += box_dst
        self.lin_txt2 =  QDoubleSpinBox(self)
        self.lin_txt2.setRange(0, 630)
        self.lin_txt2.move(x_real_pos, y_pos)
        self.opt_box2 = QComboBox(self)
        self.opt_box2.lin_ed = self.lin_txt2
        self.opt_box2.lst_opt=[]
        self.opt_box2.lst_opt.append("630")
        self.opt_box2.lst_opt.append("500")
        self.opt_box2.lst_opt.append("10")
        for lst_itm in self.opt_box2.lst_opt:
            self.opt_box2.addItem(lst_itm)
        self.opt_box2.setCurrentIndex(0)
        self.opt_box2.currentIndexChanged.connect(self.combobox_changed)
        self.opt_box2.move(x_real_pos, y_pos + size_double_box)
        AmpLabel = QLabel(parent = self, text = "Amplitude (mV) ")
        AmpLabel.move(x_label_pos, y_pos)


        y_pos += box_dst
        self.lin_txt3 =  QDoubleSpinBox(self)
        self.lin_txt3.setRange(-99999999, 99999999)
        self.lin_txt3.move(x_real_pos, y_pos)
        self.opt_box3 = QComboBox(self)
        self.opt_box3.lin_ed = self.lin_txt3
        self.opt_box3.lst_opt=[]
        self.opt_box3.lst_opt.append("1")
        self.opt_box3.lst_opt.append("650")
        self.opt_box3.lst_opt.append("1000 ")
        for lst_itm in self.opt_box3.lst_opt:
            self.opt_box3.addItem(lst_itm)
        self.opt_box3.setCurrentIndex(0)
        self.opt_box3.currentIndexChanged.connect(self.combobox_changed)
        self.opt_box3.move(x_real_pos, y_pos + size_double_box)
        PulWLabel = QLabel(parent = self, text = "Pulse Width (ms) ")
        PulWLabel.move(x_label_pos, y_pos)


        y_pos += box_dst
        self.lin_txt4 =  QDoubleSpinBox(self)
        self.lin_txt4.move(x_real_pos, y_pos)
        self.lin_txt4.setRange(-99999999, 99999999)
        self.opt_box4 = QComboBox(self)
        self.opt_box4.lin_ed = self.lin_txt4
        self.opt_box4.lst_opt=[]
        self.opt_box4.lst_opt.append("1")
        self.opt_box4.lst_opt.append("10")
        self.opt_box4.lst_opt.append("2000")
        for lst_itm in self.opt_box4.lst_opt:
            self.opt_box4.addItem(lst_itm)
        self.opt_box4.setCurrentIndex(0)
        self.opt_box4.currentIndexChanged.connect(self.combobox_changed)
        self.opt_box4.move(x_real_pos, y_pos + size_double_box)
        PulFLabel = QLabel(parent = self, text = "Pulse Freq (Hz) ")
        PulFLabel.move(x_label_pos, y_pos)


        y_pos += box_dst
        self.lin_txt5 =  QDoubleSpinBox(self)
        self.lin_txt5.move(x_real_pos, y_pos)
        self.lin_txt5.setRange(-99999999, 99999999)
        self.opt_box5 = QComboBox(self)
        self.opt_box5.lin_ed = self.lin_txt5
        self.opt_box5.lst_opt=[]
        self.opt_box5.lst_opt.append("1")
        self.opt_box5.lst_opt.append("10")
        self.opt_box5.lst_opt.append("100")
        for lst_itm in self.opt_box5.lst_opt:
            self.opt_box5.addItem(lst_itm)
        self.opt_box5.setCurrentIndex(0)
        self.opt_box5.currentIndexChanged.connect(self.combobox_changed)
        self.opt_box5.move(x_real_pos, y_pos + size_double_box)
        NdropLabel = QLabel(parent = self, text = "# Drops ")
        NdropLabel.move(x_label_pos, y_pos)

        y_pos_down = y_pos + 85

        self.multi_choice = QComboBox(self)
        self.multi_choice.lst_opt=[]
        self.multi_choice.lst_opt.append("0")
        self.multi_choice.lst_opt.append("1")
        self.multi_choice.lst_opt.append("2")
        self.multi_choice.lst_opt.append("3")
        self.multi_choice.lst_opt.append("4")
        self.multi_choice.lst_opt.append("5")
        self.multi_choice.lst_opt.append("6")
        self.multi_choice.lst_opt.append("7")
        self.multi_choice.lst_opt.append("8")
        self.multi_choice.lst_opt.append("9")
        for lst_itm in self.multi_choice.lst_opt:
            self.multi_choice.addItem(lst_itm)
        self.multi_choice.setCurrentIndex(0)
        self.multi_choice.currentIndexChanged.connect(self.choice_changed)
        self.multi_choice.move(50, y_pos_down)
        NdropLabel = QLabel(parent = self, text = "Presets  ")
        NdropLabel.move(50, y_pos + 70)

        self.btn_load =  QPushButton(' Load ', self)
        self.btn_load.move(150, y_pos_down)
        self.btn_load.clicked.connect(self.clickload)

        self.btn_save =  QPushButton(' Save ', self)
        self.btn_save.move(250, y_pos_down)
        self.btn_save.clicked.connect(self.clicksave)

        self.btn_go =  QPushButton(' \nGo\n', self)
        self.btn_go.move(x_real_pos + 50, y_pos_down + 20)
        self.btn_go.clicked.connect(self.clickgo)

        # The next who QPushButton initialisations need to go
        # BEFORE self.setGeometry() and self.show() like
        # the rest of the widgets in the big widget

        self.btn_out1 =  QCheckBox(' Out 1 ', self)
        self.btn_out1.move(150, y_pos + 25)
        self.btn_out1.clicked.connect(self.clickout1)

        self.btn_out2 =  QCheckBox(' Out 2 ', self)
        self.btn_out2.move(250, y_pos + 25)
        self.btn_out2.clicked.connect(self.clickout2)
        
        
        #self.p_button = QCheckBox("check", self)
        #self.p_button.move(350, y_pos +25)        

        self.setGeometry(300, y_pos_down + 35, 800, 580)
        self.setWindowTitle('Shell dialog')
        self.show()

    def combobox_changed(self, value):
        sender = self.sender()
        print "combobox_changed to: ",
        str_value = str(sender.lst_opt[value])
        print "value =", float(str_value)
        sender.lin_ed.setValue(float(str_value))

    def choice_changed(self, value):
        sender = self.sender()
        print "choice_changed to: ",
        str_value = str(sender.lst_opt[value])
        print str_value

    def changeCheck(self):
        print "changeCheck"

    def clickload(self):
        print "clickload"

        num = self.lin_txt3.value()
        print "num =", num


    # Luiso replaced Tab with spaces in the next two definitions
    # if you make your code editor to put spaces instead of Tab
    # your Python coding will be a lot easier, Python relies
    # heavily on indentation

    def clickout1(self):
        print "output 1 on"

    def clickout2(self):
        print "output 2 on"

    def clicksave(self):
        print "clicksave"

    def clickgo(self):


        print "clickgo"
        
        #print "index =", self.multi_choice.currentIndex()


        import visa
        rm = visa.ResourceManager()
        
        instrument = rm.open_resource('USB::0x0699::0x0353::1544236::INSTR')
        mhz = self.lin_txt1.value()
        mvolt = self.lin_txt2.value()
        msec = self.lin_txt3.value()
        freq = self.lin_txt4.value()
        ndrops = self.lin_txt5.value()
        
        print 'mhz =', mhz
        print 'mvolt =', mvolt
        print 'msec =', msec
        print 'freq =', freq
        print 'ndrops =', ndrops
        
        
        
        preset = 0 #connect to Presets
        echosig = 0 #connect to Echo
        out1 = 0 #connect to Out 1
        out2 = 0 #connect to Out 2

        instrument.write('output1:state on')
        instrument.write('output2:state on')
        #instrument.write('source1:frequency %d'); %(mhz)
        #instrument.write()





def main():

    app =  QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()


#remove the next line to uncomment code
