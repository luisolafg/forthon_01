import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("test1.ui")

        self.window.pushButtonX.clicked.connect(self.add_x)
        self.window.pushButtonY.clicked.connect(self.add_y)
        #tableWidget_ini
        #pushButtonX
        #pushButtonY

        self.window.show()

    def add_x(self):
        print("Hi there X")
        self.window.tableWidget_ini.insertColumn(0)

    def add_y(self):
        print("Hi there Y")
        self.window.tableWidget_ini.insertRow(0)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())

