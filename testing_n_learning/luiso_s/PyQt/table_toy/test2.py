import sys
import os
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *
from grid_atoms_xyz import read_cfl_atoms,AtomData
from readcfl import read_cfl_file



class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("test2.ui")

        self.window.delButtonY.clicked.connect(self.remove_r)
        self.window.addButtonY.clicked.connect(self.add_r)
        self.window.readButton.clicked.connect(self.read_r)
        self.window.writeButton.clicked.connect(self.savefile)

        columns = ['z','Label','Xpos','Ypos','Zpos','DWF']
        self.window.tableWidget_ini.setColumnCount(len(columns))
        self.window.tableWidget_ini.setHorizontalHeaderLabels(columns)



        self.window.show()

        self.info = read_cfl_file(my_file_path = "My_Cfl.cfl")
        #self.info2 = read_cfl_atoms(my_file_path = "cfl_out.cfl")



    def remove_r(self):
        print("Row removed")
        self.window.tableWidget_ini.removeRow(self.window.tableWidget_ini.model().rowCount()-1)

    def add_r(self):
        print("Row Added")
        self.window.tableWidget_ini.insertRow(self.window.tableWidget_ini.model().rowCount())
        print(self.window.tableWidget_ini.model().rowCount())

    def read_r(self):
        #self.info2 = QFileDialog.getOpenFileName(self, 'Open File')
        self.info2 = read_cfl_atoms(my_file_path = "My_Cfl.cfl")
        self.window.tableWidget_ini.setRowCount(len(self.info2))
        for n,atm in enumerate(self.info2):

            self.window.tableWidget_ini.setItem(n,0,QTableWidgetItem(atm.Zn))
            self.window.tableWidget_ini.setItem(n,1,QTableWidgetItem(atm.Lbl))
            self.window.tableWidget_ini.setItem(n,2,QTableWidgetItem(atm.x))
            self.window.tableWidget_ini.setItem(n,3,QTableWidgetItem(atm.y))
            self.window.tableWidget_ini.setItem(n,4,QTableWidgetItem(atm.z))
            self.window.tableWidget_ini.setItem(n,5,QTableWidgetItem(atm.b))

    def savefile(self):


        save_atm = []
        print(range(self.window.tableWidget_ini.rowCount()))
        for d in range(self.window.tableWidget_ini.rowCount()):
            print(d)
            atm = AtomData()
            atm.Zn = self.window.tableWidget_ini.item(d, 0).text()
            atm.Lbl = self.window.tableWidget_ini.item(d, 1).text()
            atm.x = self.window.tableWidget_ini.item(d,2).text()
            atm.y = self.window.tableWidget_ini.item(d,3).text()
            atm.z = self.window.tableWidget_ini.item(d,4).text()
            atm.b = self.window.tableWidget_ini.item(d,5).text()

            save_atm.append(atm)

        for c in save_atm:
            print(c.Zn,c.Lbl,c.x,c.y,c.z,c.b)

        return save_atm

        
        '''
        name = QFileDialog.getSaveFileName(self, 'Save File', '', "CFL(.cfl)")
        file = open(name,'w')
        text = self.tableWidget_ini.toPlainText()
        file.close()
        '''

        #write_cfl_athom(dto_lts, myfile = None)


        '''
        filename,_ = QFileDialog.getSaveFileName(self, 'Save File', '', "CFL(.cfl)")
        wbk = xlwt.Workbook()
        sheet = wbk.add_sheet("sheet", cell_overwrite_ok=True)
        style = xlwt.XFStyle()
        font = xlwt.Font()
        font.bold = True
        style.font = font
        model = self.window.tableWidget_ini.model()
        for c in range(model.columnCount()):
            text = model.headerData(c, Qt.Horizontal)
            sheet.write(0, c+1, text, style=style)

        for c in range(model.columnCount()):
            for r in range(model.rowCount()):
                text = model.data(model.index(r, c))
                sheet.write(r+1, c+1, text)
                wbk.save(filename)
        '''
    #need to change for text extension

    #open dialog is wrong


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
    sys.getfilesystemencoding()

