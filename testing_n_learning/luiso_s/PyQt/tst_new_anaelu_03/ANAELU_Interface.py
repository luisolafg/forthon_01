import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)

    def wheelEvent(self, event):
        #print("event.delta", event.delta())
        int_delta = int(event.delta())
        if int_delta > 0:
            self.m_scrolling.emit(int_delta)

        else:
            self.m_scrolling.emit(int_delta)

        event.accept()

class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("ANAELU_interface.ui")

        self.exp_scene = MultipleImgSene()
        self.window.graphicsView_Exp.setScene(self.exp_scene)

        self.mask_n_diff_scene = MultipleImgSene()
        self.window.graphicsView_Mask_n_Diff.setScene(self.mask_n_diff_scene)

        self.modl_scene = MultipleImgSene()
        self.window.graphicsView_Modl.setScene(self.modl_scene)

        self.bakgr_scene = MultipleImgSene()
        self.window.graphicsView_Bakgr.setScene(self.bakgr_scene)

        self.window.pushButton_load_exp.clicked.connect(self.set_img1)
        self.window.pushButton_mask_model.clicked.connect(self.set_img2)
        self.window.run_model.clicked.connect(self.set_img3)
        self.window.background_smoothing.clicked.connect(self.set_img4)

        self.gscene_list = [self.exp_scene, self.mask_n_diff_scene,
                            self.modl_scene, self.bakgr_scene]

        for gscene in self.gscene_list:
            gscene.m_scrolling.connect(self.scale_all_views)
            #gscene.setSceneRect(gscene.sceneRect())

        self.gview_list = [self.window.graphicsView_Exp, self.window.graphicsView_Mask_n_Diff,
                           self.window.graphicsView_Modl, self.window.graphicsView_Bakgr]

        for gview in self.gview_list:
            #gview.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            #gview.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
            gview.setDragMode(QGraphicsView.ScrollHandDrag)

        self.inter_view_connects()
        self.window.show()

    def inter_view_connects(self):
        # connecting Scroll Bars of each view with its
        # surrounding other three
        for n_a, gview in enumerate(self.gview_list):
            for n_b, other_gview in enumerate(self.gview_list):
                if n_a != n_b:
                    gview.verticalScrollBar().valueChanged.connect(
                            other_gview.verticalScrollBar().setValue
                        )

                    gview.horizontalScrollBar().valueChanged.connect(
                        other_gview.horizontalScrollBar().setValue
                    )

    def inter_view_disconnects(self):
        # disconnecting Scroll Bars of each view with its
        # surrounding other three
        for n_a, gview in enumerate(self.gview_list):
            for n_b, other_gview in enumerate(self.gview_list):
                if n_a != n_b:
                    gview.verticalScrollBar().valueChanged.disconnect(
                            other_gview.verticalScrollBar().setValue
                        )

                    gview.horizontalScrollBar().valueChanged.disconnect(
                        other_gview.horizontalScrollBar().setValue
                    )

    def scale_all_views(self, scale_fact):
        self.inter_view_disconnects()
        for gview in self.gview_list:
            gview.scale(1.0 + float(scale_fact) / 2500.0,
                        1.0 + float(scale_fact) / 2500.0)

        self.inter_view_connects()

    def set_img1(self):
        fileName = "../../../../miscellaneous/lena.jpeg"
        image1 = QImage(fileName)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.exp_scene.addPixmap(self.pixmap_1)

    def set_img2(self):
        fileName = "../../../../miscellaneous/lena_gray.jpeg"
        image2 = QImage(fileName)
        self.pixmap_2 = QPixmap.fromImage(image2)
        self.mask_n_diff_scene.addPixmap(self.pixmap_2)

    def set_img3(self):
        fileName = "../../../../miscellaneous/lena_inverted.jpeg"
        image3 = QImage(fileName)
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.modl_scene.addPixmap(self.pixmap_3)

    def set_img4(self):
        fileName = "../../../../miscellaneous/lena_gray_inverted.jpeg"
        image4 = QImage(fileName)
        self.pixmap_4 = QPixmap.fromImage(image4)
        self.bakgr_scene.addPixmap(self.pixmap_4)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
