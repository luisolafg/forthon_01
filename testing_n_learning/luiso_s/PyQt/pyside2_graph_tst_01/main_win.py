import sys
#from PySide import QtUiTools
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

from PySide2 import QtUiTools

class Form(QObject):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("my_win.ui")
        self.window.pushButton.clicked.connect(self.btn_clk)

        print("QGraphicsScene test start")
        my_scene = QGraphicsScene()
        self.window.graphicsView.setScene(my_scene)
        self.window.graphicsView.setDragMode(QGraphicsView.ScrollHandDrag)

        print("QGraphicsScene test end")

        fileName = "../../../../miscellaneous/lena.jpeg"
        image = QImage(fileName)
        self.l_pixmap = QPixmap.fromImage(image)
        my_scene.addPixmap(self.l_pixmap)


        self.window.show()

    def btn_clk(self):
        print("self.btn_clk")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())

