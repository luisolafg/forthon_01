import sys

from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2 import QtUiTools

from img_rgb_gen import np2bmp_heat
import fabio

#from PIL import Image, ImageQt
import numpy as np


class Form(QObject):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("my_win.ui")
        self.window.pushButton.clicked.connect(self.btn_clk)

        print("QGraphicsScene test start")
        self.my_scene = QGraphicsScene()
        self.window.graphicsView.setScene(self.my_scene)
        self.window.graphicsView.setDragMode(QGraphicsView.ScrollHandDrag)

        print("QGraphicsScene test end")

        fileName = "../../../../miscellaneous/lena.jpeg"
        image = QImage(fileName)
        self.l_pixmap = QPixmap.fromImage(image)
        self.my_scene.addPixmap(self.l_pixmap)

        self.bmp_heat = np2bmp_heat()

        self.window.show()

    def btn_clk(self):
        print("self.btn_clk")
        self.my_scene.clear()

        rel_path = "../../../../miscellaneous/new_interface/code_sample_04/img_file.edf"

        img_arr = fabio.open(rel_path).data.astype("float64")
        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)

        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )

        self.l_pixmap = QPixmap.fromImage(q_img)
        self.my_scene.addPixmap(self.l_pixmap)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())

