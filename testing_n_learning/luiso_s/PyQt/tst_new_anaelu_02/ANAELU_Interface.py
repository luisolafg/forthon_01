import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *


class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("ANAELU_interface.ui")

        self.exp_scene = QGraphicsScene()
        self.window.graphicsView_Exp.setScene(self.exp_scene)
        self.window.graphicsView_Exp.setDragMode(QGraphicsView.ScrollHandDrag)

        self.mask_n_diff_scene = QGraphicsScene()
        self.window.graphicsView_Mask_n_Diff.setScene(self.mask_n_diff_scene)
        self.window.graphicsView_Mask_n_Diff.setDragMode(QGraphicsView.ScrollHandDrag)

        self.modl_scene = QGraphicsScene()
        self.window.graphicsView_Modl.setScene(self.modl_scene)
        self.window.graphicsView_Modl.setDragMode(QGraphicsView.ScrollHandDrag)

        self.bakgr_scene = QGraphicsScene()
        self.window.graphicsView_Bakgr.setScene(self.bakgr_scene)
        self.window.graphicsView_Bakgr.setDragMode(QGraphicsView.ScrollHandDrag)


        self.window.pushButton_load_exp.clicked.connect(self.set_img1)
        self.window.pushButton_mask_model.clicked.connect(self.set_img2)
        self.window.run_model.clicked.connect(self.set_img3)
        self.window.background_smoothing.clicked.connect(self.set_img4)



        self.gview_list = [self.window.graphicsView_Exp, self.window.graphicsView_Mask_n_Diff,
                           self.window.graphicsView_Modl, self.window.graphicsView_Bakgr]



        for n_a, gview in enumerate(self.gview_list):
            for n_b, other_gview in enumerate(self.gview_list):

                if n_a != n_b:
                    #debuging_code = '''
                    #it should NOT print the same number twice one next to another
                    print(n_a, " != ", n_b)
                    #'''

                    gview.verticalScrollBar().valueChanged.connect(
                            other_gview.verticalScrollBar().setValue
                        )

                    gview.horizontalScrollBar().valueChanged.connect(
                        other_gview.horizontalScrollBar().setValue
                    )


        self.window.show()


    def set_img1(self):
        fileName = "../../../../miscellaneous/lena.jpeg"
        image1 = QImage(fileName)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.exp_scene.addPixmap(self.pixmap_1)

    def set_img2(self):
        fileName = "../../../../miscellaneous/lena_gray.jpeg"
        image2 = QImage(fileName)
        self.pixmap_2 = QPixmap.fromImage(image2)
        self.mask_n_diff_scene.addPixmap(self.pixmap_2)

    def set_img3(self):
        fileName = "../../../../miscellaneous/lena_inverted.jpeg"
        image3 = QImage(fileName)
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.modl_scene.addPixmap(self.pixmap_3)

    def set_img4(self):
        fileName = "../../../../miscellaneous/lena_gray_inverted.jpeg"
        image4 = QImage(fileName)
        self.pixmap_4 = QPixmap.fromImage(image4)
        self.bakgr_scene.addPixmap(self.pixmap_4)



if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
