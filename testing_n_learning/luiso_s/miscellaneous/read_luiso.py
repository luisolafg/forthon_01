
def read_cfl_file(my_file_path = "My_Cfl.cfl"):

    myfile = open(my_file_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    lst_dat = []

    for lin_char in all_lines:
        print "_______________________________________________________________________________"

        print "lin_char =", lin_char
        commented = False

        for delim in ',;':
            lin_char = lin_char.replace(delim, ' ')

        for litle_block in lin_char.split():
            if( litle_block == "!" or litle_block == "#" ):
                commented = True
            if( not commented and litle_block != ","):
                lst_dat.append(litle_block)

    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"


    for blk_lst in lst_dat:
        print blk_lst


if( __name__ == "__main__" ):
    read_cfl_file("My_Cfl_tester.cfl")
