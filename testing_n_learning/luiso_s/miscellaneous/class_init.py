class printea():

    def __init__(self, a = 5):
        print "b_var =", a
        self.var = a

    def __call__(self, x):
        print "entered x =", x
        print "self var =", self.var

if(__name__ == "__main__"):
    v1 = printea()
    v2 = printea(4)

    v1(2)
    v1(1)

    v2(3)
    v2(4)
