precios =    {'manzana': 2.50, 'naranja': 1.50}
mi_compra =  {'manzana': 4   , 'naranja': 2}

old_way = '''
costo_total = sum(precios[fruta] * mi_compra[fruta]
                   for fruta in mi_compra)
'''
costo_total = 0
for fruta in mi_compra:
    print "fruta = ", fruta
    print "precios[fruta] =", precios[fruta]
    print "mi_compra[fruta] =", mi_compra[fruta]
    print "costo_total (before add) =", costo_total
    costo_total += precios[fruta] * mi_compra[fruta]
    print "costo_total (after add) =", costo_total

print 'le debo al cajero $%.2f' % costo_total

# puedes usar el simbolo % o simplemente poner una coma y luego el costo_total