from __future__ import division
class radians():
    def __init__(self):
        self.pi = 3.141593
    def get_pi(self):
        return self.pi
    def get_length(self, rad):
        lgt = 2 * self.pi * rad
        return lgt


class circle(radians):
    def get_area(self, rad):
        a = self.pi * rad ** 2.0
        return a


if( __name__ == "__main__" ):
    circ = circle()
    print "pi =", circ.get_pi()
    print "l =", circ.get_length(1)
    print "a =", circ.get_area(2)