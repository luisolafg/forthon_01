#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from setuptools import setup
import os

setup(
    name="dui",
    description="Dials User Interface",
    author="Luis Fuentes-Montero (Luiso) Et al",
    author_email="luicub@gmail.com",
    url="https://gitlab.com/luisolafg/anaelu3",
    platforms="GNU/Linux & Windows",
    packages=["dui", "dui.cli"],
    package_dir={"": "src"},
    data_files=[
        (
            "dui/resources",
            [
                "src/dui/resources/img_dummy1.png",
                "src/dui/resources/img_dummy2.png",
            ],
        )
    ],
    include_package_data=True,
    entry_points={"console_scripts": ["dui=dui.main_dui:main"]},
)
