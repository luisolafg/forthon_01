from setuptools import setup, find_packages, find_namespace_packages
'''
setup(
    name="Hi_tst01",
    version="0.1",

    package_dir={"": "src"},
    entry_points={
        "console_scripts": [
            "hi = src.hi:main",
        ]
    }
)
'''
setup(name="hi",
      version="0.1",
      package_dir={"": "src"},
      packages=find_namespace_packages(where="src"))
