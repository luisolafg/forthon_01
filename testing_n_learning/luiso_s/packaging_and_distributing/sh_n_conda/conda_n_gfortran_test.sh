#!/bin/bash

TMP_ROOT=$(pwd)

wget https://repo.anaconda.com/miniconda/Miniconda2-4.3.31-Linux-x86_64.sh
bash Miniconda2-4.3.31-Linux-x86_64.sh -p "$TMP_ROOT/conda_bundle" -b
export PATH="$TMP_ROOT/conda_bundle/bin:$PATH"
conda install conda=4.3.34 -y
conda install -c anaconda gfortran_linux-64 -y   # still needs testing
#alias gfortran=x86_64-conda_cos6-linux-gnu-gfortran.bin

