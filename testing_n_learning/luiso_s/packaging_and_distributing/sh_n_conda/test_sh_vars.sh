echo " ################################### "
INI_DIR_PATH=$(pwd)
FURTHER_PATH="$INI_DIR_PATH/dir_extra"
printf "FURTHER_PATH=$FURTHER_PATH\n"


SET_DIALS_ENV="source $INI_DIR_PATH/dials-v3-8-3/dials_env.sh"
CLIENT_EXE_CMD="dials.python $INI_DIR_PATH/DUI2/src/client/main_client.py"

echo $SET_DIALS_ENV > dui_client
echo $CLIENT_EXE_CMD >> dui_client
