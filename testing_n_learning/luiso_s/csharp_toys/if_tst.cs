using System;
class HelloWorld {
  static void Main() {
    double a = 8;
    double b = 8;
    double m_val = 14;
    string resp = a.ToString() + " + " + b.ToString();
    if (a + b > m_val)
    {
        resp = resp + " is greater than ";
    }
    else if (a + b < m_val)
    {
        resp = resp + " is smaller than ";
    }
    else
    {
        resp = resp + " is equal to ";
    }
    Console.WriteLine(resp + m_val);
  }
}
