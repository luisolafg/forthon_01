
using Gtk;
using System;

class SharpApp : Window {
    public SharpApp() : base ("Two Buttons App") {
        SetDefaultSize(350, 400);
        SetPosition(WindowPosition.Center);

        Fixed fix = new Fixed();

        Button oneb = new Button("Button One");
        oneb.Clicked += OnOneClick;
        oneb.SetSizeRequest(120, 35);
        fix.Put(oneb, 30, 30);

        Button quit = new Button("Quit");
        quit.Clicked += OnQuitClick;
        quit.SetSizeRequest(80, 35);
        fix.Put(quit, 250, 350);

        Add(fix);
        ShowAll();
    }
    void OnQuitClick(object sender, EventArgs args) {
        Application.Quit();
    }
    void OnOneClick(object sender, EventArgs args) {
        Console.WriteLine("time to do << One >>");
    }
}

class HelloTwoButts {
    public static void Main() {
        Application.Init();
        new SharpApp();
        Application.Run();
    }
}
