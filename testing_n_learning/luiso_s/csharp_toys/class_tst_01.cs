using System;

class HelloWorld {
    int x;
    public HelloWorld() {
        Console.WriteLine ("Hello World class");
    }
    public void InData(int num_01) {
        Console.WriteLine ("entered: " + num_01.ToString());
        x = num_01;
    }
    public int Out_Y_Data() {
        Console.WriteLine ("outing: " + x.ToString());
        return x;
    }
}

class HiThere {
    static void Main() {
        int y = 0;
        HelloWorld obj_01 = new HelloWorld();
        obj_01.InData(3);
        y = obj_01.Out_Y_Data();
        Console.WriteLine ("y =" + y.ToString());
    }
}
