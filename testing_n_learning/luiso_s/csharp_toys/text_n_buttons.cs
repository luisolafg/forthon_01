
using Gtk;
using System;

class SharpApp : Window {
    Entry CmdOne = new Entry("...");
    Button RunBut = new Button("Button One");
    Button QuitBut = new Button("Quit");

    public SharpApp() : base ("Two Buttons App") {
        SetDefaultSize(350, 400);
        SetPosition(WindowPosition.Center);

        Fixed fix = new Fixed();

        RunBut.Clicked += OnOneClick;
        RunBut.SetSizeRequest(120, 35);
        fix.Put(RunBut, 30, 300);

        CmdOne.SetSizeRequest(120, 35);
        fix.Put(CmdOne, 30, 200);

        QuitBut.Clicked += OnQuitClick;
        QuitBut.SetSizeRequest(80, 35);
        fix.Put(QuitBut, 250, 350);

        Add(fix);
        ShowAll();
    }
    void OnQuitClick(object sender, EventArgs args) {
        Application.Quit();
    }
    void OnOneClick(object sender, EventArgs args) {
        Console.WriteLine("cmd = " + CmdOne.Text);
    }
}

class HelloTwoButts {
    public static void Main() {
        Application.Init();
        new SharpApp();
        Application.Run();
    }
}
