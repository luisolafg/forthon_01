using System;
using Gtk;

public class GtkHelloWorld {

    public static int Main() {
        Application.Init();

        Window myWin = new Window("My first GTK# App! ");
        myWin.Resize(200,200);

        Fixed fix = new Fixed();

        Label myLabel = new Label();
        myLabel.Text = "Hello World!!!!";
        fix.Put(myLabel, 50, 30);

        Button OneBtn = new Button("One func");
        OneBtn.Clicked += OnClickOne;
        OneBtn.SetSizeRequest(80, 35);
        fix.Put(OneBtn, 50, 60);

        Button QuitBtn = new Button("Quit");
        QuitBtn.Clicked += OnClickQuit;
        QuitBtn.SetSizeRequest(80, 35);
        fix.Put(QuitBtn, 150, 210);

        myWin.Add(fix);
        myWin.ShowAll();

        Application.Run();
        return 0;
    }

    private static void OnClickQuit(object sender, EventArgs args) {
        Application.Quit();
    }

    private static void OnClickOne(object sender, EventArgs args) {
        Console.WriteLine("time to do << One >>");
        Console.WriteLine("object sender, EventArgs args =", sender, args);
    }

}
