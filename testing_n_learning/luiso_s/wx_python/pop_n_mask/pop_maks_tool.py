import wx
import numpy as np

#import bitmap_from_numpy_hot_colour_palete

#from np_nditer_RGB.looping import np2bmp_heat, np2bmp_diff
from np_nditer_RGB.looping import np2bmp_heat
from np_nditer_RGB.bitmap_from_numpy_hot_colour_palete import build_np_img

class PaintAreaPanel(wx.Panel):

    c1 = None
    c2 = None
    lst_rec = []

    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)

        self.Bind(wx.EVT_MOTION, self.OnMouseMove)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnMouseDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnMouseUp)
        self.Bind(wx.EVT_PAINT, self.Drawing)

    def intro_n_show(self, data2d_in, fr_scale_in):
        bmp_dat = np2bmp_heat()

        tmp_img = bmp_dat.img_2d_rgb(
            data2d = data2d_in,
            invert = False, sqrt_scale = False
        )

        n_y = np.size( data2d_in[0:1, :] )
        n_x = np.size( data2d_in[:, 0:1] )

        my_img = tmp_img.Scale(
            n_y * fr_scale_in, n_x * fr_scale_in,
            wx.IMAGE_QUALITY_NORMAL
        )

        self.my_bmp = wx.BitmapFromImage(my_img)

        self.Show(True)
        self.Layout()
        self.Drawing()

    def Drawing(self, event = None):
        self.DevCont = wx.PaintDC(self)

        self.DevCont.Clear()
        self.DevCont.DrawBitmap(self.my_bmp, 1,1)

        dc = wx.PaintDC(self)
        dc.SetPen(wx.Pen('BLUE', 3))
        dc.SetBrush(wx.Brush("BLACK", wx.TRANSPARENT))

        for rect_wx_tup in self.lst_rec:
            dc.DrawRectangle(rect_wx_tup[0].x, rect_wx_tup[0].y,
                             rect_wx_tup[1].x - rect_wx_tup[0].x, rect_wx_tup[1].y - rect_wx_tup[0].y)


        if(self.c1 != None and self.c2 != None):
            dc.DrawRectangle(self.c1.x, self.c1.y,
                             self.c2.x - self.c1.x, self.c2.y - self.c1.y)

        self.Layout()

    def OnMouseMove(self, event):
        self.c2 = event.GetPosition()
        self.Refresh()

    def OnMouseDown(self, event):
        self.c1 = event.GetPosition()
        self.SetCursor(wx.StockCursor(wx.CURSOR_CROSS))

    def OnMouseUp(self, event):

        self.lst_rec.append((self.c1, self.c2))

        self.c1 = None
        self.c2 = None
        self.SetCursor(wx.StockCursor(wx.CURSOR_ARROW))


class MyFrame(wx.Frame):
    def __init__(self,(width, height)):
        #super(MyFrame, self).__init__( None, -1, "ANAELU 2.0")
        #self.SetAutoLayout(True)
        super(MyFrame, self).__init__( None, -1, "ANAELU 2.0", size = (width,height))

    def build_in(self, data2d_in, fr_scale_in):
        self.mPanel = PaintAreaPanel(self)
        self.mPanel.intro_n_show(data2d_in, fr_scale_in)

        ApplyBtn  = wx.Button(self, wx.ID_ANY, 'Apply Mask\n')
        self.Bind(wx.EVT_BUTTON, self.Apply, ApplyBtn)

        CancelBtn  = wx.Button(self, wx.ID_ANY, 'Cancel Mask\n')
        self.Bind(wx.EVT_BUTTON, self.Cancel, CancelBtn)


        vsizer = wx.BoxSizer(wx.VERTICAL)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        vsizer.Add(ApplyBtn, 1, wx.EXPAND)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        vsizer.Add(CancelBtn, 1, wx.EXPAND)
        vsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)

        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        hsizer.Add(vsizer, 1, wx.EXPAND)
        hsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        hsizer.Add(self.mPanel, 3, wx.EXPAND)
        hsizer.Add(wx.StaticText(self, wx.ID_ANY, '    '), 0, wx.EXPAND)
        self.SetSizer(hsizer)
        #print dir(self)

        #print "self.GetBestSize()", self.GetBestSize()
        #print "self.GetBestFittingSize()", self.GetBestFittingSize() # deprecated
        #print "self.GetBestVirtualSize()", self.GetBestVirtualSize()
        #print "self.GetBestSizeTuple()", self.GetBestSizeTuple()
        #self.SetSize(self.GetBestVirtualSize())
    def Apply(self, event):
        print "Apply clicked"
        print "( mPanel.lst_rec )=", self.mPanel.lst_rec

        lst_out = []

        for rect_wx_tup in self.mPanel.lst_rec:
            rect_out = (rect_wx_tup[0].x, rect_wx_tup[0].y, rect_wx_tup[1].x, rect_wx_tup[1].y)
            print "rect_out =", rect_out
            lst_out.append(rect_out)

    def Cancel(self, event):
        print "Cancel clicked"


if __name__ == "__main__":

    wxapp = wx.App(redirect = False)

    #following Fortran convention [Ny, Nx], names width & height are wrong
    data2d = build_np_img(height = 60, width = 130)

    n_y = np.size( data2d[0:1, :] )
    n_x = np.size( data2d[:, 0:1] )

    #asp_rat = float(n_y) / float(n_x)
    arr_avgr = float(n_y + n_x)
    my_avgr = 850.0
    print "arr_avgr =", arr_avgr
    print "my_avgr =", my_avgr

    fr_scale = my_avgr / arr_avgr
    print "fr_scale =", fr_scale
    fr_dim = (int(n_y * fr_scale) + 200, int(n_x * fr_scale) + 50)
    print "fr_dim =", fr_dim
    frame = MyFrame(fr_dim)
    frame.build_in(data2d, fr_scale)
    frame.Show()
    wxapp.MainLoop()


