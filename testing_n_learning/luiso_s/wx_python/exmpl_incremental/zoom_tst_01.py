import wx
import wx.lib.scrolledpanel as scroll_pan

class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)

        self.my_sizer = wx.BoxSizer(wx.VERTICAL)
        img = wx.Image('../../../eduardovp/wxpython/cute.jpg', wx.BITMAP_TYPE_JPEG)

        self.glob_bmp = img.ConvertToBitmap()
        stat_bmp = wx.StaticBitmap(self, bitmap=self.glob_bmp)

        self.my_sizer.Add(stat_bmp)
        self.SetSizer(self.my_sizer)
        self.SetupScrolling()
        self.Layout()

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(180, 120))

        self.scrolled_panel_01 = Scrolled_Img(self)
        sz = wx.BoxSizer(wx.HORIZONTAL)
        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)

        self.SetSizer(sz)
if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
