import wx
import wx.lib.scrolledpanel as scroll_pan
import math
import sys
my_op_sys = sys.platform
print "sys.platform =", my_op_sys


class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)

        self.parent_widget = parent
        self.Bdwn = False

        self.my_sizer = wx.BoxSizer(wx.VERTICAL)
        self.glob_img = wx.Image('../../../eduardovp/wxpython/cute.jpg', wx.BITMAP_TYPE_JPEG)
        self.i_width = self.glob_img.GetWidth()
        self.i_height = self.glob_img.GetHeight()

        self.set_my_content()
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)


    def set_my_content(self):
        print "self.parent_widget.glob_scal =", self.parent_widget.glob_scal

        my_img = self.glob_img.Scale(self.i_width * self.parent_widget.glob_scal,
                                self.i_height * self.parent_widget.glob_scal,
                                wx.IMAGE_QUALITY_NORMAL)

        my_bitmap = wx.BitmapFromImage(my_img)
        my_img.Destroy()
        self.glob_bmp = wx.StaticBitmap(self, bitmap = my_bitmap)
        my_bitmap.Destroy()


        self.glob_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.glob_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.glob_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.my_sizer.Clear(True)
        self.my_sizer.Add(self.glob_bmp)
        self.SetSizer(self.my_sizer)
        self.SetupScrolling()
        self.SetScrollRate(1, 1)
        self.Layout()

    def OnMouseWheel(self, event):
        sn_mov = math.copysign(1, float(event.GetWheelRotation()))

        if( sn_mov > 0 ):
            self.parent_widget.glob_scal *= 1.05
            if( self.parent_widget.glob_scal > 20.0 ):
                self.parent_widget.glob_scal = 20.0

        else:
            self.parent_widget.glob_scal *= 0.95
            if( self.parent_widget.glob_scal < 0.2 ):
                self.parent_widget.glob_scal = 0.2

        wx.CallAfter(self.parent_widget.set_all_contents)

        '''
    Here starts the inseted code
        '''

    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.pos_x, self.pos_y = event.GetPosition()
        print "OnLeftButDown, GetViewStart() =", self.GetViewStart()

    def OnLeftButUp(self, event):
        self.Bdwn = False
        self.pos_x, self.pos_y = event.GetPosition()
        print "OnLeftButUp(x,y) =", self.pos_x, self.pos_y

    def OnMouseMotion(self, event):
        new_x, new_y = event.GetPosition()
        if( self.Bdwn == True ):
            dx = new_x - self.pos_x
            dy = new_y - self.pos_y
            bar_x, bar_y = self.GetViewStart()
            self.parent_widget.scroll_all(bar_x - dx, bar_y - dy)

            if my_op_sys[0:5] == "linux" :
                self.pos_x, self.pos_y = event.GetPosition()

    def scroll_me(self, scroll_x_new, scroll_y_new):
        wx.CallAfter(self.Scroll, scroll_x_new, scroll_y_new)

        '''
    Here ends the inseted code
        '''


class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(180, 120))

        self.glob_scal = 1.0
        num_of_pan = 2

        self.lst_panels = []
        self.n_panels = 4
        sz = wx.BoxSizer(wx.HORIZONTAL)
        for n_times in xrange(self.n_panels):
            new_panel = Scrolled_Img(self)
            sz.Add(new_panel, 1, wx.EXPAND)
            self.lst_panels.append(new_panel)

        self.SetSizer(sz)


    def set_all_contents(self):
        for panl in self.lst_panels:
            panl.set_my_content()

    def scroll_all(self, scroll_x_new, scroll_y_new):
        for panel in self.lst_panels:
            panel.scroll_me(scroll_x_new, scroll_y_new)

if(__name__ == "__main__"):

    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
