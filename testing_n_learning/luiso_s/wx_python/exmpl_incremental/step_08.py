import wx
import wx.lib.scrolledpanel as scroll_pan
class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)
        self.p_widg = parent
        img_sizer = wx.BoxSizer(wx.VERTICAL)
        img = wx.Image('../../../eduardovp/wxpython/cute.jpg', wx.BITMAP_TYPE_JPEG)
        bmp = img.ConvertToBitmap()
        my_bmp = wx.StaticBitmap(self, bitmap=bmp)
        my_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        my_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        my_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        my_bmp.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bdwn = False
        self.dx = 0
        self.dy = 0
        img_sizer.Add(my_bmp)
        self.SetSizer(img_sizer)
        self.SetupScrolling()
        self.SetScrollRate(1, 1)
        self.Layout()
    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.pos_x, self.pos_y = event.GetPosition()
        print "OnLeftButDown, GetViewStart() =", self.GetViewStart()
    def OnLeftButUp(self, event):
        self.Bdwn = False
        self.pos_x, self.pos_y = event.GetPosition()
        print "OnLeftButUp(x,y) =", self.pos_x, self.pos_y
    def OnMouseMotion(self, event):
        new_x, new_y = event.GetPosition()
        if( self.Bdwn == True ):
            self.dx += new_x - self.pos_x
            self.dy += new_y - self.pos_y
            self.bar_x, self.bar_y = self.GetViewStart()
        self.pos_x, self.pos_y = new_x, new_y
    def OnIdle(self, event):
        if( self.dx != 0 or self.dy != 0 ):
            self.p_widg.scroll_all(self.bar_x - self.dx, self.bar_y - self.dy)
        self.dx = 0
        self.dy = 0

    def scroll_me(self, scroll_x_new, scroll_y_new):
        #self.Scroll(scroll_x_new, scroll_y_new)
        wx.CallAfter(self.Scroll, scroll_x_new, scroll_y_new)

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(180, 120))

        self.panel_lst = []
        self.n_panels = 5
        sz = wx.BoxSizer(wx.HORIZONTAL)
        for n_times in xrange(self.n_panels):
            new_panel = Scrolled_Img(self)
            self.panel_lst.append(new_panel)
            sz.Add(new_panel, 1, wx.EXPAND)

        self.SetSizer(sz)

    def scroll_all(self, scroll_x_new, scroll_y_new):
        for panel in self.panel_lst:
            panel.scroll_me(scroll_x_new, scroll_y_new)

if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
