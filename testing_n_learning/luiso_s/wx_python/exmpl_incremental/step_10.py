import wx
import wx.lib.scrolledpanel as scroll_pan
import sys
my_op_sys = sys.platform
print "sys.platform =", my_op_sys
class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)
        self.p_widg = parent
        img_sizer = wx.BoxSizer(wx.VERTICAL)
        img = wx.Image('../../../eduardovp/wxpython/cute.jpg', wx.BITMAP_TYPE_JPEG)
        bmp = img.ConvertToBitmap()
        my_bmp = wx.StaticBitmap(self, bitmap=bmp)
        my_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        my_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        my_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.Bdwn = False
        img_sizer.Add(my_bmp)
        self.SetSizer(img_sizer)
        self.SetupScrolling()
        self.SetScrollRate(1, 1)
        self.Layout()

    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.pos_x, self.pos_y = event.GetPosition()
        print "OnLeftButDown, GetViewStart() =", self.GetViewStart()

    def OnLeftButUp(self, event):
        self.Bdwn = False
        self.pos_x, self.pos_y = event.GetPosition()
        print "OnLeftButUp(x,y) =", self.pos_x, self.pos_y

    def OnMouseMotion(self, event):
        new_x, new_y = event.GetPosition()
        if( self.Bdwn == True ):
            dx = new_x - self.pos_x
            dy = new_y - self.pos_y
            bar_x, bar_y = self.GetViewStart()
            self.p_widg.scroll_all(bar_x - dx, bar_y - dy)
            if my_op_sys[0:5] == "linux" :
                self.pos_x, self.pos_y = event.GetPosition()

    def scroll_me(self, scroll_x_new, scroll_y_new):
        wx.CallAfter(self.Scroll, scroll_x_new, scroll_y_new)

class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(180, 120))
        self.panel_lst = []
        self.n_panels = 4
        sz = wx.BoxSizer(wx.HORIZONTAL)
        for n_times in xrange(self.n_panels):
            new_panel = Scrolled_Img(self)
            self.panel_lst.append(new_panel)
            sz.Add(new_panel, 1, wx.EXPAND)

        self.SetSizer(sz)

    def scroll_all(self, scroll_x_new, scroll_y_new):
        for panel in self.panel_lst:
            panel.scroll_me(scroll_x_new, scroll_y_new)

if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
