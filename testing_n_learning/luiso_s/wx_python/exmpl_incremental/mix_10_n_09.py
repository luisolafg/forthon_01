import wx
import wx.lib.scrolledpanel as scroll_pan
import sys
import math

my_op_sys = sys.platform
print "sys.platform =", my_op_sys
class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)
        self.p_widg = parent

        #self.SetBackgroundStyle(wx.BG_STYLE_PAINT) #TODO study this line
        self.img_sizer = wx.BoxSizer(wx.VERTICAL)
        self.p_widg.scale = 1.0
        self.x_scroll_pos = 0
        self.y_scroll_pos = 0
        self.Bdwn = False
        self.img = wx.Image('../../../eduardovp/wxpython/cute.jpg', wx.BITMAP_TYPE_JPEG)
        self.img_w, self.img_h = self.img.GetSize()
        self.put_bmp()

    def put_bmp(self):
        tmp_img = self.img.Scale(self.img_w * self.p_widg.scale, self.img_h * self.p_widg.scale, wx.IMAGE_QUALITY_HIGH)
        self.DestroyChildren()

        tmp_bmp = wx.BitmapFromImage(tmp_img)
        tmp_img.Destroy()
        self.my_bmp = wx.StaticBitmap(self, bitmap = tmp_bmp)
        tmp_bmp.Destroy()

        self.img_sizer.Clear(True)
        self.img_sizer.Add(self.my_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)

        self.my_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.my_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.my_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)

        self.SetSizer(self.img_sizer)
        self.SetupScrolling()
        self.Layout()

    def scroll_ext(self, scroll_x_new, scroll_y_new):
        self.x_scroll_pos = scroll_x_new
        self.y_scroll_pos = scroll_y_new
        self.scroll_me()

    def scroll_me(self):
        self.SetScrollRate(1, 1)
        self.Scroll(self.x_scroll_pos, self.y_scroll_pos)
        #wx.CallAfter(self.Scroll, self.x_scroll_pos, self.y_scroll_pos)

    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.pos_x, self.pos_y = event.GetPosition()

    def OnLeftButUp(self, event):
        self.Bdwn = False
        self.pos_x, self.pos_y = event.GetPosition()

    def OnMouseMotion(self, event):
        new_x, new_y = event.GetPosition()
        if( self.Bdwn == True ):
            dx = new_x - self.pos_x
            dy = new_y - self.pos_y
            bar_x, bar_y = self.GetViewStart()
            self.p_widg.scroll_all(bar_x - dx, bar_y - dy)
            if my_op_sys[0:5] == "linux" :
                self.pos_x, self.pos_y = event.GetPosition()

    def OnMouseWheel(self, event):
        self.Bdwn = False
        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        if( sn_mov == 1.0 ):
            self.p_widg.scale *=1.01

        else:
            self.p_widg.scale *=0.99

        self.p_widg.scale_all_bmp()


class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(180, 120))
        self.panel_lst = []
        self.n_panels = 4
        sz = wx.BoxSizer(wx.HORIZONTAL)
        for n_times in xrange(self.n_panels):
            new_panel = Scrolled_Img(self)
            self.panel_lst.append(new_panel)
            sz.Add(new_panel, 1, wx.EXPAND)

        self.SetSizer(sz)

    def scroll_all(self, scroll_x_new, scroll_y_new):
        for panel in self.panel_lst:
            panel.scroll_ext(scroll_x_new, scroll_y_new)

    def scale_all_bmp(self):
        for panel in self.panel_lst:
            panel.put_bmp()



if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
