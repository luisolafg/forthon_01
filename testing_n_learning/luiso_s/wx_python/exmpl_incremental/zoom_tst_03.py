import wx
import wx.lib.scrolledpanel as scroll_pan
import math


class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)

        self.parent_widget = parent

        self.my_sizer = wx.BoxSizer(wx.VERTICAL)
        self.glob_img = wx.Image('../../../eduardovp/wxpython/cute.jpg', wx.BITMAP_TYPE_JPEG)
        self.i_width = self.glob_img.GetWidth()
        self.i_height = self.glob_img.GetHeight()

        self.set_my_content()

        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)

    def set_my_content(self):
        print "self.parent_widget.glob_scal =", self.parent_widget.glob_scal

        my_img = self.glob_img.Scale(self.i_width * self.parent_widget.glob_scal,
                                self.i_height * self.parent_widget.glob_scal,
                                wx.IMAGE_QUALITY_NORMAL)

        my_bitmap = wx.BitmapFromImage(my_img)
        my_img.Destroy()
        self.glob_bmp = wx.StaticBitmap(self, bitmap = my_bitmap)
        my_bitmap.Destroy()

        self.my_sizer.Clear(True)
        self.my_sizer.Add(self.glob_bmp)
        self.SetSizer(self.my_sizer)
        self.SetupScrolling()
        self.Layout()

    def OnMouseWheel(self, event):
        sn_mov = math.copysign(1, float(event.GetWheelRotation()))

        if( sn_mov > 0 ):
            self.parent_widget.glob_scal *= 1.05
            if( self.parent_widget.glob_scal > 20.0 ):
                self.parent_widget.glob_scal = 20.0

        else:
            self.parent_widget.glob_scal *= 0.95
            if( self.parent_widget.glob_scal < 0.2 ):
                self.parent_widget.glob_scal = 0.2

        wx.CallAfter(self.set_my_content)



class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(180, 120))

        self.glob_scal = 1.0


        self.scrolled_panel_01 = Scrolled_Img(self)
        sz = wx.BoxSizer(wx.HORIZONTAL)
        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)

        self.SetSizer(sz)
if(__name__ == "__main__"):

    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
