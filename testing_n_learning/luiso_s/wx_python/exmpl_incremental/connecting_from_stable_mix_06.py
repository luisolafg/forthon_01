import math, wx
import wx, wx.lib.scrolledpanel as scroll_pan

class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)

        self.parent_widget = parent
        self.Bdwn = False

        self.my_sizer = wx.BoxSizer(wx.VERTICAL)
        self.glob_img = wx.Image('../../../../miscellaneous/lena.jpeg', wx.BITMAP_TYPE_JPEG)
        self.i_width = self.glob_img.GetWidth()
        self.i_height = self.glob_img.GetHeight()

        self.set_my_content()
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)

    def set_my_content(self):
        print "self.parent_widget.glob_scal =", self.parent_widget.glob_scal

        my_img = self.glob_img.Scale(self.i_width * self.parent_widget.glob_scal,
                                self.i_height * self.parent_widget.glob_scal,
                                wx.IMAGE_QUALITY_NORMAL)

        my_bitmap = wx.BitmapFromImage(my_img)
        my_img.Destroy()
        self.glob_bmp = wx.StaticBitmap(self, bitmap = my_bitmap)
        my_bitmap.Destroy()

        self.my_sizer.Clear(True)
        self.my_sizer.Add(self.glob_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)
        self.glob_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.glob_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.glob_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)
        self.SetSizer(self.my_sizer)
        self.SetupScrolling()
        self.Layout()

    def OnMouseWheel(self, event):
        sn_mov = math.copysign(1, float(event.GetWheelRotation()))

        if( sn_mov > 0 ):
            self.parent_widget.glob_scal *= 1.01
            if( self.parent_widget.glob_scal > 6.0 ):
                self.parent_widget.glob_scal = 6.0

        else:
            self.parent_widget.glob_scal *= 0.99
            if( self.parent_widget.glob_scal < 0.4 ):
                self.parent_widget.glob_scal = 0.4

        self.parent_widget.do_zoom = True


    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.pos_x, self.pos_y = event.GetEventObject().ScreenToClient(wx.GetMousePosition())
        print "OnLeftButDown, GetViewStart() =", self.GetViewStart()

    def OnLeftButUp(self, event):
        self.Bdwn = False
        self.pos_x, self.pos_y = event.GetEventObject().ScreenToClient(wx.GetMousePosition())
        print "OnLeftButUp(x,y) =", self.pos_x, self.pos_y

    def OnMouseMotion(self, event):
        new_x, new_y = event.GetEventObject().ScreenToClient(wx.GetMousePosition())
        print "new_x, new_y =", new_x, new_y
        #TODO use "LeftIsDown" instead for next if
        if( self.Bdwn == True ):
            dx = new_x - self.pos_x
            dy = new_y - self.pos_y
            bar_x, bar_y = self.GetViewStart()
            self.parent_widget.scroll_all_to(bar_x - dx, bar_y - dy)

    def scroll_me(self, scroll_x_new, scroll_y_new):
        self.SetScrollRate(1, 1)

        wx.CallAfter(self.Scroll, scroll_x_new, scroll_y_new)


class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels", size=(580, 150))

        self.glob_scal = 1.0
        self.do_zoom = False

        self.x_pos = 0
        self.y_pos = 0

        self.lst_panels = []
        self.n_panels = 2
        sz = wx.BoxSizer(wx.HORIZONTAL)
        for n_times in xrange(self.n_panels):
            new_panel = Scrolled_Img(self)
            sz.Add(new_panel, 1, wx.EXPAND)
            self.lst_panels.append(new_panel)

        self.SetSizer(sz)
        self.Bind(wx.EVT_IDLE, self.OnIdle)

    def set_all_contents(self):
        for panl in self.lst_panels:
            panl.set_my_content()

        self.scroll_all()

    def scroll_all(self):
        for panel in self.lst_panels:
            panel.scroll_me(self.x_pos, self.y_pos)

    def scroll_all_to(self, scroll_x_new, scroll_y_new):
        self.x_pos = scroll_x_new
        self.y_pos = scroll_y_new
        self.scroll_all()

    def OnIdle(self, event):
        if( self.do_zoom == True ):
            self.set_all_contents()
            self.do_zoom = False


if(__name__ == "__main__"):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()
