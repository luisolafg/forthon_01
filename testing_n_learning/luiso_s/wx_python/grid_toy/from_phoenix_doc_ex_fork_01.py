import wx
import wx.grid

class MyForm(wx.Frame):
    def __init__(self, parent):
        super(MyForm, self).__init__(parent, title="Atom Grid")
        grid = wx.grid.Grid(self, -1)
        grid.CreateGrid(10, 5)
        self.Show()


if __name__ == '__main__':
    app = wx.App(0)
    #frame = GridFrame(None)
    frame = MyForm(None)
    app.MainLoop()
