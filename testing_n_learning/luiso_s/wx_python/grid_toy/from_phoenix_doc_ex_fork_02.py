import wx
import wx.grid
import wx.lib.scrolledpanel as scroll_pan


class MyForm(wx.Frame):
    def __init__(self, parent):
        super(MyForm, self).__init__(parent, title="Atom Grid")

        grid = wx.grid.Grid(self, -1)
        grid.CreateGrid(10, 5)
        self.Show()


        my_panel = Scrolled_Img(self)
        self.box = wx.BoxSizer(wx.VERTICAL)
        self.box.Add(my_panel)
        self.SetSizer(self.box)



class Scrolled_Img(scroll_pan.ScrolledPanel):
    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)

        self.my_box = wx.BoxSizer(wx.VERTICAL)
        grid = wx.grid.Grid(self, -1)
        grid.CreateGrid(10, 5)
        self.my_box.Add(grid)
        self.SetSizer(self.my_box)
        self.SetupScrolling()


if( __name__ == '__main__' ):
    app = wx.App(0)
    frame = MyForm(None)
    app.MainLoop()
