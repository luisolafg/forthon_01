import wx
import math
import wx.lib.scrolledpanel as scroll_pan

from subprocess import call as shell_func
from read_2d_numpy import read_bin_file
from np_nditer_RGB.looping import np_to_bmp

class Scrolled_Img(scroll_pan.ScrolledPanel):

    def __init__(self, parent):

        super(Scrolled_Img, self).__init__(parent)
        self.Prnt_Panel = parent
        self.SetScrollRate(1, 1)
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.old_Mouse_Pos_x = -1
        self.old_Mouse_Pos_y = -1
        self.scroll_rot = 0
        self.Bdwn = False

    def intro_img_path(self, path_to_img, real_yn):

        data2d = read_bin_file(path_to_img, real_yn)
        bmp_dat = np_to_bmp()
        my_tmp_bmp = bmp_dat.img_2d_rgb(data2d)

        self.img = wx.ImageFromBitmap(my_tmp_bmp)
        self.i_scale = 0.3333
        self.i_width = self.img.GetWidth()
        self.i_height = self.img.GetHeight()
        self.img_vert_sizer = wx.BoxSizer(wx.VERTICAL)
        self.set_img_n_bmp()


    def set_img_n_bmp(self):

        my_img = self.img.Scale(self.i_width * self.i_scale,
                                self.i_height * self.i_scale,
                                wx.IMAGE_QUALITY_HIGH)

        my_bitmap = wx.BitmapFromImage(my_img)
        my_img.Destroy()
        self.my_st_bmp = wx.StaticBitmap(self, bitmap = my_bitmap)

        self.img_vert_sizer.Clear(True)
        self.img_vert_sizer.Add(self.my_st_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 6)
        self.SetSizer(self.img_vert_sizer)

        self.my_bindings()
        self.SetupScrolling()
        self.Layout()
        self.Prnt_Panel.Layout()

    def my_bindings(self):

        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.my_st_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.my_st_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.my_st_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)

    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.old_Mouse_Pos_x, self.old_Mouse_Pos_y = event.GetPosition()

    def OnLeftButUp(self, event):
        self.Bdwn = False
        Mouse_Pos_x, Mouse_Pos_y = event.GetPosition()

    def OnMouseMotion(self, event):
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()

    def OnMouseWheel(self, event):

        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        self.scroll_rot += sn_mov

        v_size_x, v_size_y = self.GetVirtualSize()
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()

        View_start_x, View_start_y = self.GetViewStart()

        self.x_uni = float( View_start_x + self.Mouse_Pos_x ) / float(v_size_x)
        self.y_uni = float( View_start_y + self.Mouse_Pos_y ) / float(v_size_y)

    def OnIdle(self, event):

        if( self.scroll_rot != 0 ):

              self.SetScrollRate(1, 1)


              self.i_scale = self.i_scale * ( 1.0 + self.scroll_rot * 0.05 )
              if( self.i_scale > 3.0 ):
                  self.i_scale = 3.0
              elif( self.i_scale < 0.2 ):
                  self.i_scale = 0.2

              self.scroll_rot = 0
              v_size_x, v_size_y = self.GetVirtualSize()
              new_scroll_pos_x = float(self.x_uni * v_size_x - self.Mouse_Pos_x)
              new_scroll_pos_y = float(self.y_uni * v_size_y - self.Mouse_Pos_y)
              self.set_img_n_bmp()

        elif( (self.old_Mouse_Pos_x != -self.Mouse_Pos_x or
               self.old_Mouse_Pos_y != -self.Mouse_Pos_y )and self.Bdwn == True):

            dx = self.old_Mouse_Pos_x - self.Mouse_Pos_x
            dy = self.old_Mouse_Pos_y - self.Mouse_Pos_y
            self.old_Mouse_Pos_x = self.Mouse_Pos_x
            self.old_Mouse_Pos_y = self.Mouse_Pos_y


class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels")
        sz = wx.BoxSizer(wx.HORIZONTAL)

        self.scrolled_panel_01 = Scrolled_Img(self)
        self.scrolled_panel_01.intro_img_path("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/2d_pat_1.raw", real_yn = True)

        self.scrolled_panel_02 = Scrolled_Img(self)
        self.scrolled_panel_02.intro_img_path("../../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.mar2300.bin", real_yn = False)

        show_but_1 = wx.Button(self, -1, "Butt One ...")
        show_but_2 = wx.Button(self, -1, "Butt Two ...")
        sz.Add(show_but_1, 0, wx.LEFT | wx.ALL,8)
        sz.Add(show_but_2, 0, wx.LEFT | wx.ALL,8)

        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)
        sz.Add(self.scrolled_panel_02, 1, wx.EXPAND)

        self.SetSizer(sz)

        show_but_1.Bind(wx.EVT_BUTTON, self.OnBut_1)
        show_but_2.Bind(wx.EVT_BUTTON, self.OnBut_2)



    def OnBut_1(self, event):
        print "OnBut_1"
        shell_func("../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/Anaelu_gfortran.r")

        print "Done running Anaelu\n\nUpdating Img"
        self.scrolled_panel_01.intro_img_path("2d_pat_1.asc")


    def OnBut_2(self, event):
        print "OnBut_2"


if( __name__ == '__main__' ):
    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()

