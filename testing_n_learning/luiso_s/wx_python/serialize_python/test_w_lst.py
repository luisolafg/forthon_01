a = [1, 2, 3, 4, 5]

print ("a =")
print (a)

b = [0, 0, 0, 0, 0]

print ("b =")
print (b)

traditional_way = '''
for col in range(5):
    b[col] = a[col]
#'''

b = [ a[col] for col in range(5) ]

print ("b(after) =")
print (b)
