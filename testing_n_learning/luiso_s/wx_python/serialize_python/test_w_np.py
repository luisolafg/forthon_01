import numpy as np

a = np.arange(25).reshape((5,5))
print ("a =")
print (a)

#b = np.arange(25).reshape((5,5))
#b[:,:] = 0
#print ("b =")
#print (b)

traditional_way = '''
for col in range(5):
    for row in range(5):
        b[row, col] = a[row, col]
#'''

b = ( a[row, col] for col in range(5) for row in range(5) )
#b = ( a[row, col] for col in np.nditer(5) for row in np.nditer(5) )

print "b(after) =", b
