#!/usr/bin/env python
import wx

class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos = wx.DefaultPosition, size = wx.DefaultSize,
                 style = wx.DEFAULT_FRAME_STYLE, name = "MyFrame"):

        super(MyFrame, self).__init__(parent = None, title = title)

        sz = wx.BoxSizer(wx.VERTICAL)
        collpane = wx.CollapsiblePane(self, wx.ID_ANY, "Details:")

        # add the pane with a zero proportion value to the 'sz' sizer which contains it
        sz.Add(collpane, 0, wx.GROW | wx.ALL, 5)

        # now add a test label in the collapsible pane using a sizer to layout it:
        win = collpane.GetPane()
        paneSz = wx.BoxSizer(wx.VERTICAL)
        paneSz.Add(wx.StaticText(win, wx.ID_ANY, "test!"), 1, wx.GROW | wx.ALL, 2)
        win.SetSizer(paneSz)
        paneSz.SetSizeHints(win)


class MyApp(wx.App):
    def OnInit(self):
        self.frame = MyFrame(None, title = "tst interfs", pos = (150, 150))
        self.SetTopWindow(self.frame)
        self.frame.Show()
        return True


if(__name__ == "__main__"):
    app = MyApp(redirect = False)
    app.MainLoop()

