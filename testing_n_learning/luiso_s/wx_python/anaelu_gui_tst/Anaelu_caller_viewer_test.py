#!/usr/bin/env python
#
#  flex_3d_array_viewer_test.py
#
#  test for multi_3D_slice_viewer.py
#
#  Copyright (C) 2014 Cimav
#
#  Author: Luis Fuentes-Montero (Luiso)
#
#  This code is distributed under the BSD license, a copy of which is
#  included in the root directory of this package.

import numpy as np
from Anaelu_viewer import show_3d_wx_app

if(__name__ == "__main__"):
    width = 20
    height = 20
    data_xyz = np.arange(width * height).reshape(width, height)

    app = show_3d_wx_app(redirect=False)
    app.in_lst(data_xyz)

    app.MainLoop()
