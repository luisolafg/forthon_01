#   Read 2D numpy
#   reader of 2D ASC diffraction files
#
#   This file is part of the Python-Fortran version of Anaelu project,
#   a tool for the treatment of 2D-XRD patterns of textured samples
#   Copyright (C) 2014
#   Luis Fuentes Montero (Luiso)
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Lesser General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import numpy
from matplotlib import pyplot as plt
import math

def read_bin_file(path_to_img = "my_bin_img.bin", real_dat = False):

    #print
    print "reading ", path_to_img, " file"
    #print "real_dat =", real_dat
    if(real_dat == True):
        data_in = numpy.fromfile(path_to_img, dtype=numpy.float32)

    else:
        data_in = numpy.fromfile(path_to_img, dtype=numpy.uint16)

    lng_fl = len(data_in)
    #print "len(data_in) =", lng_fl
    cuad_lng = int(math.sqrt(len(data_in)))
    #print "math.sqrt(len(data_in)) =", cuad_lng
    xres = cuad_lng
    yres = cuad_lng

    read_data = data_in.reshape((xres, yres)).T
    if(real_dat == False):
        read_data = numpy.transpose(read_data)

    print "Done reading 2D data"
    print

    return read_data

def read_file(path_to_img = "../../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.txt"):

    print "readin ", path_to_img, " file"

    myfile = open(path_to_img, "r")

    all_lines = myfile.readlines()
    myfile.close()
    n=0

    #copied_from_eduardos_code = '''
    xres=2300
    yres=2300

    n_line = 0

    for line in all_lines:
        n_line += 1
        if( n_line < 10 ):
            print "____________________________________________________"

            print line
            print "comparing:"
            print line[:32].lower()
            print "with:"
            print " number of pixels in x direction"
        if( line[:32].lower() == " number of pixels in x direction" ):
            print "Found the X resolution"
            print "asc to convert =", line[36:]
            xres = int(line[36:])
            print " X =", xres

        elif( line[:32].lower() == " number of pixels in y direction" ):
            print "Found the Y resolution"
            print "asc to convert =", line[36:]
            yres = int(line[36:])
            print " Y =", yres


    #'''


    a=numpy.zeros((xres,yres))

    x=0
    y=yres - 1
    for line in all_lines:
        n+=1
        if n>6:
            a[x,y]=float(line)
            if x>=xres-1:
                x = -1
                y -= 1
            x += 1
        else:
            print n, line
    return a


def plott_img(arr):
    print "Plotting arr"
    plt.imshow(  numpy.transpose(arr) , interpolation = "nearest" )
    #plt.imshow(  arr , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):
    img_arr = read_file()
    plott_img(img_arr)
