import wx
import math
import wx.lib.scrolledpanel as scroll_pan

from subprocess import call as shell_func
from read_2d_numpy import read_bin_file
from np_nditer_RGB.looping import np_to_bmp

class Scrolled_Img(scroll_pan.ScrolledPanel):

    def __init__(self, parent):
        super(Scrolled_Img, self).__init__(parent)
        self.Prnt_Wg = parent
        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.old_Mouse_Pos_x = -1
        self.old_Mouse_Pos_y = -1
        self.scroll_rot = 0
        self.Bdwn = False
        self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y = 0, 0
        self.Prnt_Wg.img_scale = 0.1


    def intro_img_path(self, path_to_img, real_yn):
        data2d = read_bin_file(path_to_img, real_yn)
        tmp_img = np_to_bmp()
        self.img = tmp_img.img_2d_rgb(data2d)

        self.i_width = self.img.GetWidth()
        self.i_height = self.img.GetHeight()
        self.img_vert_sizer = wx.BoxSizer(wx.VERTICAL)
        self.set_img_n_bmp()
        self.SetupScrolling()

    def set_img_n_bmp(self):

        my_img = self.img.Scale(self.i_width * self.Prnt_Wg.img_scale,
                                self.i_height * self.Prnt_Wg.img_scale, wx.IMAGE_QUALITY_NORMAL)

        my_bitmap = wx.BitmapFromImage(my_img)
        my_img.Destroy()
        self.my_st_bmp = wx.StaticBitmap(self, bitmap = my_bitmap)
        my_bitmap.Destroy()

        self.img_vert_sizer.Clear(True)
        self.img_vert_sizer.Add(self.my_st_bmp, flag = wx.ALIGN_CENTER | wx.TOP, border = 1)
        self.SetSizer(self.img_vert_sizer)

        self.my_bindings()
        self.Scrolling_to()


    def my_bindings(self):
        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_MOUSEWHEEL, self.OnMouseWheel)
        self.my_st_bmp.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.my_st_bmp.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)
        self.my_st_bmp.Bind(wx.EVT_MOTION, self.OnMouseMotion)

    def Scrolling_to(self):

        VwStart = self.Prnt_Wg.scroll_pos_x, self.Prnt_Wg.scroll_pos_y
        ClSize = self.GetClientSize()
        Vsize = self.GetVirtualSize()
        x_pos_start, y_pos_start = (VwStart[0] + ClSize[0], VwStart[1] + ClSize[1])

        if(self.Prnt_Wg.scroll_pos_x < 0):
            self.Prnt_Wg.scroll_pos_x = 0

        if(self.Prnt_Wg.scroll_pos_y < 0):
            self.Prnt_Wg.scroll_pos_y = 0

        if(x_pos_start > Vsize[0]):
            x_pos_start = Vsize[0]
            self.Prnt_Wg.scroll_pos_x = x_pos_start - ClSize[0]

        if(y_pos_start > Vsize[1]):
            y_pos_start = Vsize[1]
            self.Prnt_Wg.scroll_pos_y = y_pos_start - ClSize[1]

        self.SetScrollRate(1, 1)
        self.Scroll(int(self.Prnt_Wg.scroll_pos_x), int(self.Prnt_Wg.scroll_pos_y))
        self.Layout()
        self.Prnt_Wg.Layout()

    def OnLeftButDown(self, event):
        self.Bdwn = True
        self.old_Mouse_Pos_x, self.old_Mouse_Pos_y = event.GetPosition()

    def OnLeftButUp(self, event):
        self.Bdwn = False

    def OnMouseMotion(self, event):
        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()

    def OnMouseWheel(self, event):
        sn_mov = math.copysign(1, float(event.GetWheelRotation()))
        self.scroll_rot += sn_mov
        v_size_x, v_size_y = self.GetVirtualSize()
        self.x_uni = float( self.Prnt_Wg.scroll_pos_x ) / float(v_size_x)
        self.y_uni = float( self.Prnt_Wg.scroll_pos_y ) / float(v_size_y)

    def OnIdle(self, event):
        if( self.scroll_rot != 0 ):
            new_scale = self.Prnt_Wg.img_scale * ( 1.0 + self.scroll_rot * 0.01 )
            if( new_scale > 0.3 ):
                new_scale = 0.3
            elif( new_scale < 0.033333 ):
                new_scale = 0.033333

            if( new_scale != self.Prnt_Wg.img_scale ):
                self.Prnt_Wg.img_scale = new_scale
                self.Prnt_Wg.update_scale()

                v_size_x, v_size_y = self.GetVirtualSize()
                self.Prnt_Wg.scroll_pos_x = float(self.x_uni * v_size_x)
                self.Prnt_Wg.scroll_pos_y = float(self.y_uni * v_size_y)
                self.Prnt_Wg.update_scroll_pos()

            self.scroll_rot = 0

        elif( (self.old_Mouse_Pos_x != -self.Mouse_Pos_x or
               self.old_Mouse_Pos_y != -self.Mouse_Pos_y )and self.Bdwn == True):

            dx = self.Mouse_Pos_x - self.old_Mouse_Pos_x
            dy = self.Mouse_Pos_y - self.old_Mouse_Pos_y
            self.Prnt_Wg.scroll_pos_x = self.Prnt_Wg.scroll_pos_x - dx
            self.Prnt_Wg.scroll_pos_y = self.Prnt_Wg.scroll_pos_y - dy

            self.Prnt_Wg.update_scroll_pos()
            self.old_Mouse_Pos_x = self.Mouse_Pos_x
            self.old_Mouse_Pos_y = self.Mouse_Pos_y
        else:
            self.scroll_rot = 0
            self.old_Mouse_Pos_x = self.Mouse_Pos_x
            self.old_Mouse_Pos_y = self.Mouse_Pos_y
            self.Prnt_Wg.update_scroll_pos()


class main_frame(wx.Frame):
    def __init__(self):
        super(main_frame, self).__init__( None, -1, "Two scrolled panels")
        sz = wx.BoxSizer(wx.HORIZONTAL)

        self.scrolled_panel_01 = Scrolled_Img(self)
        calc_img_path = "../../../../subset_sxtalsoft/toys_by_luiso/2d_txtr/2d_pat_1.raw"
        self.scrolled_panel_01.intro_img_path(calc_img_path, real_yn = True)

        self.scrolled_panel_02 = Scrolled_Img(self)
        exp_img_path = "../../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.mar2300.bin"

        self.scrolled_panel_02.intro_img_path(exp_img_path, real_yn = False)

        show_but_1 = wx.Button(self, -1, "Butt One ...")
        show_but_2 = wx.Button(self, -1, "Butt Two ...")
        sz.Add(show_but_1, 0, wx.LEFT | wx.ALL,8)
        sz.Add(show_but_2, 0, wx.LEFT | wx.ALL,8)

        sz.Add(self.scrolled_panel_01, 1, wx.EXPAND)
        sz.Add(self.scrolled_panel_02, 1, wx.EXPAND)

        self.SetSizer(sz)

        show_but_1.Bind(wx.EVT_BUTTON, self.OnBut_1)
        show_but_2.Bind(wx.EVT_BUTTON, self.OnBut_2)

    def OnBut_1(self, event):
        shell_func("Anaelu_gfortran.r", shell=True)
        self.scrolled_panel_01.intro_img_path("2d_pat_1.raw", real_yn = True)

    def OnBut_2(self, event):
        print "OnBut_2"

    def update_scroll_pos(self):
        self.scrolled_panel_01.Scrolling_to()
        self.scrolled_panel_02.Scrolling_to()

    def update_scale(self):
        self.scrolled_panel_01.set_img_n_bmp()
        self.scrolled_panel_02.set_img_n_bmp()

if __name__ == "__main__":

    wxapp = wx.App(redirect = False)
    fr = main_frame()
    fr.Show()
    wxapp.MainLoop()

