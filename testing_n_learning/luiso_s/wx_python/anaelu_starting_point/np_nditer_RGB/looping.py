import wx
import numpy as np
import time

class np_to_bmp(object):
    def __init__(self):

        self.red_byte = np.empty( (255 * 3), 'int')
        self.green_byte = np.empty( (255 * 3), 'int')
        self.blue_byte = np.empty( (255 * 3), 'int')

        for i in xrange(255):
            self.red_byte[i] = i
            self.green_byte[i + 255] = i
            self.blue_byte[i + 255 * 2 ] = i


        self.red_byte[255:255 * 3] = 255
        self.green_byte[0:255] = 0
        self.green_byte[255 * 2 : 255 * 3] = 255
        self.blue_byte[0:255 * 2] = 0

        self.blue_byte[764] = 255
        self.red_byte[764] = 255
        self.green_byte[764] = 255


    def img_2d_rgb(self, data2d):
        self.width = np.size( data2d[0:1, :] )
        self.height = np.size( data2d[:, 0:1] )

        div_scale = 764.0 / data2d.max()
        data2d_scale = np.multiply(data2d, div_scale)

        img_array = np.empty( (self.height ,self.width, 3),'uint8')

        img_array_r = np.empty( (self.height, self.width), 'int')
        img_array_g = np.empty( (self.height, self.width), 'int')
        img_array_b = np.empty( (self.height, self.width), 'int')

        scaled_i = np.empty( (self.height, self.width), 'int')
        scaled_i[:,:] = data2d_scale[:,:]

        img_array_r[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_r[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.red_byte[x]

        img_array_g[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_g[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.green_byte[x]

        img_array_b[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_b[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.blue_byte[x]

        img_array[:, :, 0] = img_array_r[:,:]
        img_array[:, :, 1] = img_array_g[:,:]
        img_array[:, :, 2] = img_array_b[:,:]

        image = wx.EmptyImage(self.width, self.height)
        image.SetData( img_array.tostring())

        scale = 5.0
        NewW = int(self.width * scale)
        NewH = int(self.height * scale)
        image = image.Scale(NewW, NewH, wx.IMAGE_QUALITY_NORMAL)


        return image
