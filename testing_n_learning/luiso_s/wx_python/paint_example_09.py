import wx

import wx.lib.scrolledpanel as scroll_pan
from bitmap_from_numpy_w_matplotlib import GetBitmap_from_np_array, build_np_img

class PaintAreaPanel(wx.Panel):
    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)

        #old_ver = '''
        self.data2d = build_np_img(width=18, height=18)
        self.my_bmp = GetBitmap_from_np_array(self.data2d)
        #'''

        new_ver = '''
        self.img = wx.Image("/home/luisolafg/Downloads/test.png", wx.BITMAP_TYPE_PNG)
        self.bitmap = wx.BitmapFromImage(self.img)
        self.my_bmp = wx.StaticBitmap(self, wx.ID_ANY, self.bitmap)
        '''


        self.Mouse_Pos_x = -1
        self.Mouse_Pos_y = -1
        self.Mouse_Left_clk = False
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_MOTION, self.OnMouseMotion)

        self.Bind(wx.EVT_LEFT_DOWN, self.OnLeftButDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnLeftButUp)

        self.Bind(wx.EVT_IDLE, self.OnIdle)

        self.Layout()

    def OnPaint(self, event):
        #print "OnPaint"
        if(self.Mouse_Pos_x > 0 and self.Mouse_Pos_y > 0):
            self.Drawing()

    def Drawing(self):
        #print "Drawing"
        self.DevCont = wx.PaintDC(self)

        self.DevCont.Clear()
        self.DevCont.DrawBitmap(self.my_bmp, 1,1)

        self.DevCont.SetPen(wx.Pen("BLACK", 4))
        self.DevCont.DrawLine(55, 55, self.Mouse_Pos_x, self.Mouse_Pos_y)

        self.Layout()


    def OnLeftButDown(self, event):
        #print "OnLeftButDown"
        self.Mouse_Left_clk = True


    def OnLeftButUp(self, event):
        #print "OnLeftButUp"
        self.Mouse_Left_clk = False

    def OnIdle(self, event):
        #print "OnIdle"
        if( self.Mouse_Left_clk == True):
            #print "self.Mouse_Pos_x, self.Mouse_Pos_y =", self.Mouse_Pos_x, self.Mouse_Pos_y
            self.Drawing()

    def OnMouseMotion(self, event):

        self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetEventObject().ScreenToClient(wx.GetMousePosition())

        #self.Mouse_Pos_x, self.Mouse_Pos_y = event.GetPosition()


class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        panel = PaintAreaPanel(self)


        self.Show(True)


class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="DrawLine")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
