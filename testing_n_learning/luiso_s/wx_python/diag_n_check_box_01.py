import wx

class ExamplePanel(wx.Panel):
    def __init__(self, parent):
        super(ExamplePanel, self).__init__(parent)

        self.cb1 = wx.CheckBox(self, label="Go, #1", pos=(50, 100))
        self.cb2 = wx.CheckBox(self, label="Go, #2", pos=(50, 200))
        self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox1, self.cb1)
        self.Bind(wx.EVT_CHECKBOX, self.EvtCheckBox2, self.cb2)

    def EvtCheckBox1(self, event):
        print "Clicked CheckBox 1"
        self.print_stat()

    def EvtCheckBox2(self, event):
        print "Clicked CheckBox 2"
        self.print_stat()

    def print_stat(self):
        print "self.cb1 =", self.cb1.GetValue()
        print "self.cb2 =", self.cb2.GetValue()

app = wx.App(False)
frame = wx.Frame(None)
panel = ExamplePanel(frame)
frame.Show()
app.MainLoop()


