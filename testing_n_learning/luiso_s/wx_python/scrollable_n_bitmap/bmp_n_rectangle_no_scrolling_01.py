import wx

class PaintAreaPanel(wx.Panel):

    c1 = None
    c2 = None

    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)

        self.img = wx.Image('../../../../miscellaneous/lena.jpeg', wx.BITMAP_TYPE_JPEG)
        self.my_bmp = wx.BitmapFromImage(self.img)

        self.Bind(wx.EVT_MOTION, self.OnMouseMove)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnMouseDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnMouseUp)
        self.Bind(wx.EVT_PAINT, self.Drawing)

        self.Show(True)
        self.Layout()
        self.Drawing()

    def Drawing(self, event = None):
        self.DevCont = wx.PaintDC(self)

        self.DevCont.Clear()
        self.DevCont.DrawBitmap(self.my_bmp, 1,1)

        dc = wx.PaintDC(self)
        dc.SetPen(wx.Pen('BLUE', 3))
        dc.SetBrush(wx.Brush("BLACK", wx.TRANSPARENT))
        if(self.c1 != None and self.c2 != None):
            dc.DrawRectangle(self.c1.x, self.c1.y,
                             self.c2.x - self.c1.x, self.c2.y - self.c1.y)

        self.Layout()

    def OnMouseMove(self, event):
        self.c2 = event.GetPosition()
        self.Refresh()

    def OnMouseDown(self, event):
        self.c1 = event.GetPosition()
        self.SetCursor(wx.StockCursor(wx.CURSOR_CROSS))

    def OnMouseUp(self, event):
        self.c1 = None
        self.c2 = None
        self.SetCursor(wx.StockCursor(wx.CURSOR_ARROW))


class MyFrame(wx.Frame):
    def __init__(self):
        super(MyFrame, self).__init__( None, -1, "ANAELU 2.0", size = (800,600))
        self.mPanel = PaintAreaPanel(self)

        RunBtn  = wx.Button(self, wx.ID_ANY, 'Run Model\n')
        self.Bind(wx.EVT_BUTTON, self.Run, RunBtn)

        CallBtn  = wx.Button(self, wx.ID_ANY, 'Call Model\n')
        self.Bind(wx.EVT_BUTTON, self.Call, CallBtn)


        vsizer = wx.BoxSizer(wx.VERTICAL)
        vsizer.Add(RunBtn, 1, wx.EXPAND)
        vsizer.Add(CallBtn, 1, wx.EXPAND)

        hsizer = wx.BoxSizer(wx.HORIZONTAL)
        hsizer.Add(vsizer, 1, wx.EXPAND)
        hsizer.Add(self.mPanel, 3, wx.EXPAND)
        self.SetSizer(hsizer)

    def Run(self, event):
        print "Run clicked"

    def Call(self, event):
        print "Call clicked"


if __name__ == "__main__":

    wxapp = wx.App(redirect = False)
    frame = MyFrame()
    frame.Show()
    wxapp.MainLoop()


