import wx
import wx.lib.scrolledpanel as scroll_pan

class PaintAreaPanel(scroll_pan.ScrolledPanel):

    c1 = None
    c2 = None

    def __init__(self, outer_panel):
        super(PaintAreaPanel, self).__init__(outer_panel)

        self.img = wx.Image('../../../../miscellaneous/lena.jpeg', wx.BITMAP_TYPE_JPEG)
        self.my_bmp = wx.BitmapFromImage(self.img)


        self.SetupScrolling(self)
        self.Layout()

        self.Bind(wx.EVT_IDLE, self.OnIdle)
        self.Bind(wx.EVT_MOTION, self.OnMouseMove)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnMouseDown)
        #self.Bind(wx.EVT_RIGHT_DOWN, self.OnMouseRDown)
        self.Bind(wx.EVT_LEFT_UP, self.OnMouseUp)
        self.Bind(wx.EVT_PAINT, self.OnPaint)

        self.SetCursor(wx.StockCursor(wx.CURSOR_CROSS))

    def OnPaint(self, event):
        self.Drawing()

    def Drawing(self):
        #print "Drawing"
        self.DevCont = wx.PaintDC(self)

        self.DevCont.Clear()
        self.DevCont.DrawBitmap(self.my_bmp, 1,1)

        dc = wx.PaintDC(self)
        dc.SetPen(wx.Pen('BLUE', 3))
        dc.SetBrush(wx.Brush("BLACK", wx.TRANSPARENT))
        try:
            dc.DrawRectangle(self.c1.x, self.c1.y,
                             self.c2.x - self.c1.x, self.c2.y - self.c1.y)

        except:
            print"self.c1 or self.c2 NOT set yet"

        self.Layout()

    def OnMouseMove(self, event):
        self.c2 = event.GetPosition()
        self.Refresh()

    def OnMouseDown(self, event):
        self.c1 = event.GetPosition()

    def OnMouseUp(self, event):
        self.SetCursor(wx.StockCursor(wx.CURSOR_ARROW))

    def OnIdle(self, event):
        if(self.OnMouseDown == True):
            print "Drawing"
            self.Drawing()


class MyFrame(wx.Frame):
    def __init__(self, parent, id=wx.ID_ANY, title="",
                 pos=wx.DefaultPosition, size=wx.DefaultSize,
                 style=wx.DEFAULT_FRAME_STYLE,
                 name="MyFrame"):
        super(MyFrame, self).__init__(parent, id, title,
                                      pos, size, style, name)

        panel = PaintAreaPanel(self)

        self.Show(True)


class MyApp(wx.App):
  def OnInit(self):
    self.frame = MyFrame(None, title="Eduardo's Rectangle")
    return True

if(__name__ == "__main__"):
    app = MyApp(False)
    app.MainLoop()
