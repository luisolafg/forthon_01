import wx

class MultiImagePanel(wx.Panel):
    def __init__(self, parent, style=wx.SIMPLE_BORDER):
        super(MultiImagePanel, self).__init__(parent)
        self.parent_widget = parent

class RandomPanel(wx.Panel):
    def __init__(self, parent):
        super(RandomPanel, self).__init__(parent)
        self.SetBackgroundColour("blue")

class MainFrame(wx.Frame):
    def __init__(self):
        super(MainFrame, self).__init__( None, -1, "ANAELU 2.0", size = (1200,600))

        splitter = wx.SplitterWindow(self)
        leftP = RandomPanel(splitter)
        rightP = MultiImagePanel(splitter)

        splitter.SplitVertically(leftP, rightP)
        splitter.SetMinimumPaneSize(20)

        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add(splitter, 1, wx.EXPAND)
        self.SetSizer(sizer)

if __name__ == "__main__":
    app = wx.App(False)
    frame = MainFrame()
    frame.Show()
    app.MainLoop()
