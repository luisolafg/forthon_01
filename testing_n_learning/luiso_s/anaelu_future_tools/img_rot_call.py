import rotate
from matplotlib import pyplot as plt
import numpy as np
import fabio

if( __name__ == "__main__" ):
    path_to_img ="../../../../test_data/APT73_d122_from_m02to02__01_41.mar2300"
    #path_to_img ="../../../../test_data/ECAE_S1P_scan_0004.edf"
    #path_to_img ="APT73_d122_from_m02to02__01_41.mar2300"
    #path_to_img ="../../../miscellaneous/ECAE_S1P_scan_0004.edf"
    ini_img = fabio.open(path_to_img).data.astype(np.float64)

    xres, yres = np.shape(ini_img)
    cut_img = ini_img[0:xres,0:yres / 2]

    cut_img[100:500, 100:250] = 500000.0

    plt.imshow( cut_img , interpolation = "nearest" , cmap="gist_stern")
    plt.show()
    print "\n\n xres, yres =", xres, yres

    rotate.rot_inner.rotate_img(cut_img)
    arr_out = rotate.rot_inner.img_out

    plt.imshow( arr_out , interpolation = "nearest" , cmap="gist_stern")
    plt.show()
