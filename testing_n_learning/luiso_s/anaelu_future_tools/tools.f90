
module inner_tools
    implicit none

    real(kind=8),  dimension(:,:), allocatable :: img_out
    contains

    subroutine rotate_img(img_in)
        real(kind=8),  dimension(:,:), intent (in) :: img_in

        integer                     :: xmax, ymax, x, y
        real(kind=8)                :: e=0.00000000001

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        do x = 1, xmax
            do y = 1, ymax
                img_out(y, x) = img_in(xmax - x + 1, y)
            end do
        end do

    end subroutine rotate_img


    subroutine smooth_img(img_in)
        real(kind=8),  dimension(:,:), intent (in) :: img_in
        real(kind=8),  dimension(:,:), allocatable :: img_tmp
        integer                     :: xmax, ymax, x, y, times
        real(kind=8)                :: e=0.00000000001

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)
        write(*,*) "Allocating img_out"
        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(xmax, ymax))

        write(*,*) "Allocating img_tmp"
        if( allocated(img_tmp) )then
            deallocate(img_tmp)
        end if
        allocate(img_tmp(xmax, ymax))
        img_tmp = img_in
        write(*,*) "looping"
        do times = 1, 25
!            do x = 2, xmax - 1
!                do y = 2, ymax - 1

            forall (x=2:xmax - 1, y=2:ymax - 1) img_out(x, y) = &
             (sum(img_tmp(x - 1: x + 1, y - 1: y + 1)) &
              - img_tmp(x, y) ) / 8.0

!                    img_out(x,y) = sum(img_tmp(x - 1: x + 1, y - 1: y + 1)) - img_tmp(x, y)  / 8.0
!                end do
!            end do
            img_tmp = img_out
        end do
        write(*,*) "results in img_out"


    end subroutine smooth_img


end module inner_tools
