import cuts
import tools
from matplotlib import pyplot as plt
import numpy as np
import fabio

if( __name__ == "__main__" ):
    #path_to_img ="../../../../test_data/2d_pat_1.edf"
    path_to_img ="../../../../test_data/APT73_d122_from_m02to02__01_41.mar2300"
    #path_to_img ="APT73_d122_from_m02to02__01_41.mar2300"
    #path_to_img ="../../../miscellaneous/ECAE_S1P_scan_0004.edf"
    ini_img = fabio.open(path_to_img).data.astype(np.float64)

    xres, yres = np.shape(ini_img)
    xc = float(xres)/2.0
    yc = float(yres)/2.0

    #erasing correcting direct beam peak
    ini_img[int(xc-xc/10.0):int(xc+xc/10.0),int(yc-yc/10.0):int(yc+yc/10.0)] = 0.0

    plt.imshow( ini_img , interpolation = "nearest" )
    plt.show()

    print "\n\n xres, yres =", xres, yres

    data_in = np.fromfile("../../../../test_data/2d_mask.raw", dtype=np.float64)
    ini_mask = data_in.reshape((yres, xres))

    plt.imshow( ini_mask , interpolation = "nearest" )
    plt.show()

    cuts.cuts_inner.cut_peaks_w_mask(ini_mask, ini_img, xc, yc)
    arr_cuted = cuts.cuts_inner.img_out

    plt.imshow( arr_cuted , interpolation = "nearest" )
    plt.show()

    print "__________________________Hi 01"
    tools.inner_tools.smooth_img(arr_cuted)
    arr_out = tools.inner_tools.img_out
    print "__________________________Hi 02"

    plt.imshow( arr_out , interpolation = "nearest" )
    plt.show()
