if(__name__ == "__main__"):
    tup_str2write = (
        "echo \" \"",
        "echo \"Running autogenerated Anaelu install script ...\"",
        "echo \" \"",
        "echo \"step 1:\"",
        "echo \" \"",
        "echo \"Working on Python packaged with Conda \"",
        "cd conda_bundle/bin/",
        "echo \" \"",
        "echo \"path to Conda binary = $(pwd)\"",
        "echo \" \"",
        "echo \" \"",
        "export PATH=$(pwd):$PATH",
        "echo \" \"",
        "echo \"step 2:\"",
        "echo \"Working on Anaelu source root: \"",
        "cd ../../anaelu_main/",
        "echo \" \"",
        "echo \"path to Anaelu source = $(pwd)\"",
        "echo \" \"",
        "python gen_path.py $(pwd) add_py",
        "echo \" \"",
        "echo \" ... Done\"",
        "echo \" \"",
        "echo \" \"",
        "echo \"Anaelu bundle was succesfully installed\"",
        "echo \" \"",
        "echo \" \"",
        "echo \"To get Anaelu ready to run, just add the following line:\"",
        "echo \" \"",
        "echo \"source $(pwd)/anaelu_main/setpath.sh\"",
        "echo \" \"",
        "echo \"to either ~/.bashrc   or  ~/.profile start scripts, depending \"",
        "echo \"on your distribution. This will override your Python \"",
        "echo \"installation. If you prefer to get Anaelu ready to run\"",
        "echo \"on a temporal state just type:\"",
        "echo \" \"",
        "echo \"source $(pwd)/anaelu_main/setpath.sh\"",
        "echo \" \"",
        "echo \"and the configuration will last only the current session\"",
        "echo \" \"",
        "echo \" \""
    )

    myfile = open("install_anaelu.sh", "w")
    for single_str in tup_str2write:
        str2write = single_str + "\n"
        myfile.write(str2write)

    myfile.close()

