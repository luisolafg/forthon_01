import numpy as np

path = "../xas_n02/Sigma11.dat"

myfile = open(path, "r")
all_lines = myfile.readlines()
myfile.close()

head_lines = all_lines[:5]

for hd_lin in head_lines:
    print("<<", hd_lin, ">>")

print("\n ----------- \n")

spec_lines = all_lines[5:]

lenght = len(spec_lines)
print("len(spec_lines) =", lenght)
spec_en = np.zeros(lenght)
spec_re = np.zeros(lenght)
spec_im = np.zeros(lenght)

for num, sp_lin in enumerate(spec_lines):
    lst_dat = sp_lin[:-1].split(" ")
    #print("lst_dat =", lst_dat)
    lst_num = []
    for uni_dat in lst_dat:
        try:
            lst_num.append(float(uni_dat))

        except ValueError:
            pass

    if len(lst_num) != 3:
        print("error reading spectra")

    else:
        [spec_en[num], spec_re[num], spec_im[num]] = lst_num

print("spec_en = ", spec_en)
print("spec_re = ", spec_re)
print("spec_im = ", spec_im)
