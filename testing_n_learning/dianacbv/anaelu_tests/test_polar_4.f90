PROGRAM test_polar_4

IMPLICIT NONE

REAL (KIND=8), DIMENSION (2300, 2300) :: MV
REAL (KIND=8), DIMENSION (2300, 2300) :: R
REAL (KIND=8), DIMENSION (2300, 2300) :: DTh
REAL (KIND=8), DIMENSION (2300, 2300) :: Alfa
REAL (KIND=8), DIMENSION (30, 181) :: MN
REAL (KIND=8), DIMENSION (30) :: dthetaN
!INTEGER, DIMENSION (30) :: i
REAL (KIND=8), DIMENSION (181) :: alphaN
!INTEGER, DIMENSION (181) :: j
REAL (KIND=8), DIMENSION (30, 181) :: RN
!REAL (KIND=8), DIMENSION (30, 181) :: x
!REAL (KIND=8), DIMENSION (30, 181) :: y
INTEGER :: k, l, m, n, i, j
INTEGER :: IOstatus
REAL (KIND=8) :: p, pi


OPEN(unit= 10, status='old',file = 'convert.raw',form='unformatted', access='stream', action='read')

Do m=1, 2300
    Do n=1, 2300
        read(10, IOSTAT=IOstatus) MV(m,n)
    End Do
End Do

WRITE (*,*) "IOstatus=", IOstatus

CLOSE(10)

p=0.15   !! dimension en mm del pixcel para el detector MAR 2300
pi=3.141592653589793238462643383279502884197169399375105820974944592307816406286

DO m=1,2300
   Do n=1,2300
       R(m,n)=sqrt((((n-1150)*p)*((n-1150)*p)) + (((1150-m)*p)*((1150-m)*p)))
       DTh(m,n)=  ((((atan(R(m,n)/150))))*180)/pi !! distancia muestra-detector=150mm
       Alfa(m,n)= ((atan(((n-1150)*p)/((1150-m)*p)))*180)/pi
   End Do
End Do

WRITE (*,*) 'STEP 1 DONE'

Do k = 1, 30
    dthetaN(k) = k
        Do l = 1, 181
            RN(k,l) =  (tan (dthetaN(k)))*150 !150= distancia muestra-detector
            alphaN(l) = l-(90-1)
            !x = RN*((sin(alphaN))*180/pi)
            j=NINT((1150*p+(RN(k,l)*((sin(alphaN(l)))*180/pi)))/p)
            !y = RN*((cos(alphaN))*180/pi)
            i=NINT((1150*p-(RN(k,l)*((cos(alphaN(l)))*180/pi)))/p)
            MN=MV(i,j)
        End Do
End Do

END PROGRAM test_polar_4
