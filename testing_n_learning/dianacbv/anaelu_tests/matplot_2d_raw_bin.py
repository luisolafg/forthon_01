
import numpy
from matplotlib import pyplot as plt
#from scipy.io.numpyio import fread

def read_file(path_to_img = "2d_mask.raw"):

    print "reading ", path_to_img, " file"

    xres = 2048
    yres = 2048

    #data_in = numpy.fromfile(path_to_img, dtype=numpy.uint16)
    data_in = numpy.fromfile(path_to_img, dtype=numpy.float64)
    #read_data = read_data.reshape((n, m), order="FORTRAN")
    print len(data_in)
    read_data = data_in.reshape((xres, yres))

    return read_data

def plott_img(arr):
    print "Plotting arr"
    plt.imshow(  numpy.transpose(arr) , interpolation = "nearest" )
    #plt.imshow(  arr , interpolation = "nearest" )
    plt.show()


if(__name__ == "__main__"):
    img_arr = read_file("2d_mask.raw")
    #img_arr = read_file("../../../mar_convert/mar_img/APT73_d122_from_m02to02__01_41.bin")
    plott_img(img_arr)
