!Este programa escibe en un nuevo fichero la sumatoria 
! de x(i) que lee de un fichero.dat
PROGRAM Leer_y_escribir
IMPLICIT NONE
    INTEGER :: i
    REAL, DIMENSION(21) :: x, y
    REAL :: x_tot=0.0, y_tot=0.0
    
    OPEN (unit = 2,file = "di_poli4.dat", status = 'old', action = 'read')
       DO i=1,21
           READ (2,*) x(i), y(i)
       END DO
    CLOSE (unit=2)
       
       DO i=1,21
           WRITE (*,*) x(i), y(i)
       END DO
       
       DO i=1,21
           x_tot=x_tot+x(i)
           y_tot=y_tot+y(i)
       END DO
       
       WRITE (*,*)"La sumatoria de las x(i) es:", x_tot, "la sumatoria de y(i) es:", y_tot
    
END PROGRAM Leer_y_escribir