!Resolucion de ecuaciones cuadraticas 2
PROGRAM cuadratica
    REAL :: x1, x2, a, b, c
    COMPLEX :: SOL1, SOL2, a1, b1, c1, RAIZ1, RAIZ2, IMAG1, IMAG2
    SOL1(RAIZ1, IMAG1)
    SOL2(RAIZ2, IMAG2)
    a1(a, 0)
    b1(b, 0)
    c1(c, 0)
    WRITE (*,*) "Cuales son los valores de a, b y c?"
    READ (*,*) a, b, c
    D = b**2 - 4*a*c
    IF (D>0.0) THEN
        x1 = (-b + D**1/2)/(2*a)
        x2 = (-b - D**1/2)/(2*a)
        WRITE (*,*) "Las soluciones son x1:", x1, "x2:", x2
    ELSE IF (D == 0) THEN
        x1 = -(b/2*a)
        x2 = -(b/2*a)
        WRITE (*,*) "Las soluciones son x1:", x1, "x2:", x2
    ELSE IF (D<0.0) THEN
        RAIZ1((-b1/(2*a1)), 0)
        IMAG1((((-b1)**1/2)/(2*a1)), 0)
        RAIZ2((-b1/(2*a1)), 0)
        IMAG2(-(((-b1)**1/2)/(2*a1)), 0)
        WRITE (*,*) "Las soluciones son x1:", SOL1, "x2:", SOL2
END IF
END PROGRAM cuadratica
