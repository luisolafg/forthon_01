Module mod_escala_2
    IMPLICIT NONE

    REAL, PUBLIC :: r

    CONTAINS
     SUBROUTINE resta(arr, arr_2, s)
         REAL, INTENT(IN), DIMENSION(2300,2300) :: arr, arr_2
         REAL, INTENT(IN) :: s
         INTEGER :: i, j
         r = 0
         Do i = 1, 2300
            Do j = 1, 2300
                r = r + abs(arr(i, j) - arr_2(i, j)) / s
            End Do
        End Do
        WRITE (*,*) 'r =', r
    END SUBROUTINE resta

END MODULE mod_escala_2
