Module mod_escala
    IMPLICIT NONE

    REAL(KIND=8), PUBLIC, DIMENSION(:,:), ALLOCATABLE   :: img_arr_out
    REAL(KIND=8), PUBLIC                                :: s, r, r_2
    INTEGER(KIND=8) :: xmax, ymax
    CONTAINS

    SUBROUTINE tot_i(arr)
        INTEGER(KIND=8) :: i, j
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:) :: arr

        xmax = ubound(arr, 1)
        ymax = ubound(arr, 2)

        s = 0
        Do i = 1, xmax
            Do j = 1, ymax
                s = s + arr(i, j)
            End Do
        End Do
        WRITE (*,*) 's =', s

    END SUBROUTINE tot_i


    SUBROUTINE resid(arr_1, arr_2, mask)
        REAL, INTENT(IN), DIMENSION(:,:) :: arr_1, arr_2, mask
        REAL(KIND=8)        :: r_pics
        INTEGER(KIND=8)     :: i, j, xmax, ymax

        xmax = ubound(arr_1, 1)
        ymax = ubound(arr_1, 2)

        r = 0
        r_2 = 0

        Do i = 1, xmax
            Do j = 1, ymax
                if(mask( i,j) .ne. -1.0 )then
                    r_pics = abs(arr_1(i, j) - arr_2(i, j))
                    r = r + r_pics
                    r_2 = r_2 + r_pics * r_pics
                end if
            End Do
        End Do

        WRITE(*,*) 'r =', r
        WRITE(*,*) 'r_2 =', r_2

    END SUBROUTINE resid


    SUBROUTINE multi(img_in, nb)
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:)    :: img_in
        REAL(KIND=8), INTENT(IN)                    :: nb
        INTEGER(KIND=8)                             :: i, j, xmax, ymax

        xmax = ubound(img_in, 1)
        ymax = ubound(img_in, 2)

        if( allocated(img_arr_out) )then
            deallocate(img_arr_out)
        end if
        allocate(img_arr_out(ymax, xmax))

        WRITE (*,*), "nb =", nb

        img_arr_out(:,:) = 0

        Do i = 1, xmax
            Do j = 1, ymax
                img_arr_out(i,j) = img_in(i,j)*nb
            End Do
        End Do

        !img_arr_out(:,:) = img_in(:,:)*nb

        WRITE (*,*) 'done multiplying'

    END SUBROUTINE multi

    SUBROUTINE img_suma(modl, bkg)
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:)    :: modl, bkg
        INTEGER(KIND=8)                             :: i, j, xmax, ymax

        xmax = ubound(modl, 1)
        ymax = ubound(modl, 2)

        if( allocated(img_arr_out) )then
            deallocate(img_arr_out)
        end if
        allocate(img_arr_out(ymax, xmax))

        img_arr_out(:,:) = 0

        Do i = 1, xmax
            Do j = 1, ymax
                img_arr_out(i,j) = modl(i,j) + bkg(i,j)
            End Do
        End Do

        write(*,*) "two images added"

    END SUBROUTINE img_suma

END MODULE mod_escala
