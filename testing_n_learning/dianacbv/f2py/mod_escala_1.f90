Module mod_escala_1
    IMPLICIT NONE

    REAL, PUBLIC :: s

    CONTAINS

    SUBROUTINE suma(arr)
        INTEGER :: i, j
        REAL, INTENT(IN), DIMENSION(2300,2300) :: arr
        s = 0
        !WRITE(*,*) 'estoy corriendo'
        Do i = 1, 2300
            Do j = 1, 2300
                s = s + arr(i, j)
            End Do
        End Do
        WRITE (*,*) 's =', s
     END SUBROUTINE suma

END MODULE mod_escala_1
