Module mod_escala
    IMPLICIT NONE

    REAL, PUBLIC, DIMENSION(:,:), ALLOCATABLE :: arr_2b
    REAL, PUBLIC :: s, r, r_2
    INTEGER(KIND=8) :: xmax, ymax
    CONTAINS

    SUBROUTINE suma(arr)
        INTEGER(KIND=8) :: i, j
        REAL, INTENT(IN), DIMENSION(:,:) :: arr

        xmax = ubound(arr, 1)
        ymax = ubound(arr, 2)

        s = 0
        Do i = 1, xmax
            Do j = 1, ymax
                s = s + arr(i, j)
            End Do
        End Do
        WRITE (*,*) 's =', s

    END SUBROUTINE suma


    SUBROUTINE resta(arr, arr_2, my_s)
        REAL, INTENT(IN), DIMENSION(:,:) :: arr, arr_2
        REAL, INTENT(IN) :: my_s
        INTEGER(KIND=8) :: i, j, xmax, ymax

        xmax = ubound(arr, 1)
        ymax = ubound(arr, 2)

        r = 0
        Do i = 1, xmax
            Do j = 1, ymax
                r = r + abs(arr(i, j) - arr_2(i, j)) / my_s
            End Do
        End Do
        WRITE (*,*) 'r =', r

    END SUBROUTINE resta


    SUBROUTINE multi(arr_2, nb)
        REAL, INTENT(IN), DIMENSION(:,:) :: arr_2
        REAL, INTENT(IN) :: nb
        INTEGER(KIND=8) :: i, j, xmax, ymax

        xmax = ubound(arr_2, 1)
        ymax = ubound(arr_2, 2)


        if( allocated(arr_2b) )then
            deallocate(arr_2b)
        end if
        allocate(arr_2b(ymax, xmax))

        WRITE (*,*), nb

        arr_2b(:,:) = 0

        Do i = 1, xmax
            Do j = 1, ymax
                arr_2b(i,j) = arr_2(i,j)*nb
            End Do
        End Do

        !arr_2b(:,:) = arr_2(:,:)*nb

        WRITE (*,*) 'done multiplying'

    END SUBROUTINE multi

    SUBROUTINE resta_2(arr, new_model, my_s)
        REAL, INTENT(IN), DIMENSION(:,:) :: arr, new_model
        REAL, INTENT(IN) :: my_s
        INTEGER(KIND=8) :: i, j, xmax, ymax

        xmax = ubound(arr, 1)
        ymax = ubound(arr, 2)

        r_2 = 0
        Do i = 1, xmax
            Do j = 1, ymax
                r_2 = r_2 + abs(arr(i, j) - new_model(i, j)) / my_s
            End Do
        End Do
        WRITE (*,*) 'r_2 =', r_2

    END SUBROUTINE resta_2

    SUBROUTINE img_suma(arr, bkg)
        REAL, INTENT(IN), DIMENSION(:,:) :: arr, bkg
        INTEGER(KIND=8) :: i, j, xmax, ymax

        xmax = ubound(arr, 1)
        ymax = ubound(arr, 2)

        Do i = 1, xmax
            Do j = 1, ymax
                arr_2b(i,j) = arr(i,j) + bkg(i,j)
            End Do
        End Do

    END SUBROUTINE img_suma

END MODULE mod_escala
