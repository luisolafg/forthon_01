import numpy as np
from matplotlib import pyplot as plt
import fabio
import mod_escala


def plott_img(arr):
    print ("Plotting arr")
    plt.imshow(  arr , interpolation = "nearest", cmap="gist_stern" )
    plt.show()

expr_img = fabio.open("../../img_data/APT73_d122_from_m02to02__01_41.mar2300").data.astype(np.float64)

model_img = fabio.open("../../img_data/APT73_d122_from_m02to02_01_41_model.edf").data.astype(np.float64)

img_cut = fabio.open("../../img_data/APT73_d122_from_m02to02__01_41.mar2300-cut.edf").data.astype(np.float64)

#plott_img(arr)
#plott_img(model_img)

Nrows = np.size( expr_img[:, 0:1] )
Ncols = np.size( expr_img[0:1, :] )

scaled_bkg = np.zeros( (Nrows, Ncols), np.float64)
img_suma = np.zeros( (Nrows, Ncols), np.float64)

tmp_mask = np.zeros( (Nrows, Ncols), np.float64)
tmp_mask[1000:1300,1000:1300] = -1.0
plott_img(tmp_mask)

mod_escala.mod_escala.tot_i(expr_img)
s = mod_escala.mod_escala.s
print s

mod_escala.mod_escala.resid(expr_img, model_img, tmp_mask)
r = mod_escala.mod_escala.r
print r

#nb = raw_input('Enter the number to multiply the model by: ')
#print nb
nb = 0.2


mod_escala.mod_escala.multi(model_img, nb)

scaled_bkg[:,:] =  mod_escala.mod_escala.img_arr_out[:,:]

#plott_img(scaled_bkg)

mod_escala.mod_escala.img_suma(scaled_bkg, img_cut)
img_suma[:,:] = mod_escala.mod_escala.img_arr_out[:,:]
#plott_img(img_suma)

