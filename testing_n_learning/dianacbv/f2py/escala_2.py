import numpy as np
from matplotlib import pyplot as plt
import fabio
import mod_escala


def plott_img(arr):
    print ("Plotting arr")
    plt.imshow(  arr , interpolation = "nearest", cmap="gist_stern" )
    plt.show()

arr = fabio.open("../../img_data/APT73_d122_from_m02to02__01_41.mar2300").data.astype(np.float64)

arr_2 = fabio.open("../../img_data/APT73_d122_from_m02to02_01_41_model.edf").data.astype(np.float64)

bkg = fabio.open("/home/diana/Data_files/APT73_d122_from_m02to02__01_41.mar2300-cut.edf.sm.edf").data.astype(np.float64)

print mod_escala.mod_escala.suma.__doc__
print mod_escala.mod_escala.resta.__doc__
print mod_escala.mod_escala.multi.__doc__
print mod_escala.mod_escala.resta_2.__doc__
print mod_escala.mod_escala.img_suma.__doc__

mod_escala.mod_escala.suma(arr)
s = mod_escala.mod_escala.s
print s
my_s = s
print my_s

mod_escala.mod_escala.suma(arr)
xmax = mod_escala.mod_escala.xmax
print xmax


mod_escala.mod_escala.suma(arr)
ymax = mod_escala.mod_escala.ymax
print ymax


mod_escala.mod_escala.resta(arr, arr_2, my_s)
r = mod_escala.mod_escala.r
print r

nb = raw_input('Enter the number to multiply the model by: ')
print nb

mod_escala.mod_escala.multi(arr_2, nb)
arr_2b = mod_escala.mod_escala.arr_2b
plott_img(arr_2b)
new_model = arr_2b

mod_escala.mod_escala.resta_2(arr, new_model, my_s)
r_2 = mod_escala.mod_escala.r_2
print r_2

mod_escala.mod_escala.img_suma(arr_2,bkg)
arr_2b[:,:] = mod_escala.mod_escala.arr_2b
plott_img(arr_2b)

