import numpy as np
from matplotlib import pyplot as plt
import fabio
import mod_escala_1
import mod_escala_2

def plott_img(arr):
    print ("Plotting arr")
    plt.imshow(  arr , interpolation = "nearest", cmap="gist_stern" )
    plt.show()

arr = fabio.open("/home/diana/Data_files/APT73_d122_from_m02to02__01_41.mar2300").data.astype(np.float64)

arr_2 = fabio.open("/home/diana/Data_files/APT73_d122_from_m02to02__01_41.mar2300-cut.edf").data.astype(np.float64)


#plott_img(arr)

#plott_img(arr_2)


#comp = np.array_equal(arr, arr_2)
#print comp

print mod_escala_1.mod_escala_1.suma.__doc__
print mod_escala_2.mod_escala_2.resta.__doc__

mod_escala_1.mod_escala_1.suma(arr)
s = mod_escala_1.mod_escala_1.s
print s

mod_escala_2.mod_escala_2.resta(arr, arr_2, s)
r = mod_escala_2.mod_escala_2.r
print r
