Module escala
    IMPLICIT NONE

    REAL, DIMENSION(2300,2300), PUBLIC :: arr, arr_2
    REAL, PUBLIC :: s, r

    CONTAINS

    SUBROUTINE suma(arr, s)
        INTEGER :: i, j
        s = 0
        Do i = 1, 2300
            Do j = 1, 2300
                s = s + arr(i, j)
            End Do
        End Do
        WRITE (*,*) 's =', s
     END SUBROUTINE suma

     SUBROUTINE resta(arr, arr_2, s, r)
         INTEGER :: i, j
         r = 0
         Do i = 1, 2300
            Do j = 1, 2300
                r = r + abs(arr(i, j) - arr_2(i, j)) / s
            End Do
        End Do
        WRITE (*,*) 'r =', r
    END SUBROUTINE resta

END MODULE escala



