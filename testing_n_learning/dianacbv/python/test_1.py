import numpy as np
from matplotlib import pyplot as plt
import fabio

def plott_img(arr):
    print ("Plotting arr")
    plt.imshow(  arr , interpolation = "nearest", cmap="gist_stern" )
    plt.show()

arr = fabio.open("/home/diana/Data_files/APT73_d122_from_m02to02__01_41.mar2300").data.astype(np.float64)
arr_new = np.multiply(10, arr)

arr_2 = fabio.open("/home/diana/Data_files/APT73_d122_from_m02to02__01_41.mar2300-cut.edf").data.astype(np.float64)
arr_2_new = np.multiply(10, arr_2)

plott_img(arr)
plott_img(arr_new)
plott_img(arr_2)
plott_img(arr_2_new)

#comp = np.array_equal(arr, arr_new)
#print comp

new_img = fabio.edfimage.edfimage()
print "type(new_img) =", type(new_img)

new_img.data = arr_new
path_to_img_out = "salida.edf"
new_img.write(path_to_img_out)


