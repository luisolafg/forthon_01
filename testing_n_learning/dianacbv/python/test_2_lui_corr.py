import numpy as np
from matplotlib import pyplot as plt
import fabio

def plott_img(arr):
    print ("Plotting arr")
    plt.imshow(  arr , interpolation = "nearest", cmap="gist_stern" )
    plt.show()

arr = fabio.open("/home/luisolafg/anaelu_git/test_data/APT73_d122_from_m02to02__01_41.mar2300").data.astype(np.float64)
#arr_new = np.multiply(10, arr)

arr_2 = fabio.open("/home/luisolafg/anaelu_git/test_data/2d_pat_1.edf").data.astype(np.float64)
#arr_2_new = np.multiply(10, arr_2)

plott_img(arr)
#plott_img(arr_new)
plott_img(arr_2)
#plott_img(arr_2_new)

comp = np.array_equal(arr, arr_2)
print comp

len_x = len(arr[:,1])
len_y = len(arr[1,:])
print "len_x, len_y =", len_x, ", ", len_y

old_way = '''
arr_3 = arr - arr_2
arr_3_new = np.sum(arr_3)
print arr_3_new
'''

s = 0

for x in xrange(len_x):
    for y in xrange(len_y):
        s = s + arr[x, y]


r = 0
for x in xrange(len_x):
    for y in xrange(len_y):
        r = r + abs(arr[x, y] - arr_2[x, y]) / s


print "r =", r


#new_img = fabio.edfimage.edfimage()
#print "type(new_img) =", type(new_img)

#new_img.data = arr_new
#path_to_img_out = "salida.edf"
#new_img.write(path_to_img_out)

