!Fibonacci sequence module that gives "n" number of the sequence.
MODULE mod_fibonacci
    IMPLICIT NONE

    INTEGER :: F
    INTEGER :: Fi
    INTEGER :: Fn, fa, fb
    INTEGER :: n

    CONTAINS

    SUBROUTINE F_n_0(n,F)
           INTEGER, INTENT(OUT) :: F
           INTEGER :: n
           F=0
           WRITE(*,*) 'Fib',n,'=',F
           
        RETURN
    END SUBROUTINE F_n_0

    SUBROUTINE F_n_1(n,F)
           INTEGER, INTENT(OUT) :: F
           INTEGER :: n
           F=1
           WRITE(*,*)'Fib',n,'=',F
           
        RETURN
    END SUBROUTINE F_n_1

    SUBROUTINE Fib(Fi,Fn)
              
        INTEGER :: Fi
        INTEGER :: Fn, fa, fb
        INTEGER :: n
        
           Do n=3, Fi
               fa=n-1
               fb=n-2
               Fn=fa+fb
               WRITE(*,*) 'Fib',n,'=',Fn
           END DO
       
    END SUBROUTINE 

END MODULE mod_fibonacci
