PROGRAM Bidimensional
IMPLICIT NONE

INTEGER :: i, j, l
REAL (KIND=8):: C, local_dos_theta, Ibid1, Ibid2
REAL (KIND=8), DIMENSION(1501) :: dos_theta, Iuni
REAL (KIND=8), DIMENSION(345,345) :: Inten

integer                     :: log_uni
character(len = 256)        :: fname


OPEN(UNIT=3,FILE='element.dat',STATUS='old',ACTION='read')

!Calculating the theta angle for each pixel to the sample position

    DO i=1,1501
        READ(3,*) dos_theta(i), Iuni(i)
    END DO
    CLOSE(UNIT=3)

    DO i=1,345
        DO j=1,345
            C=SQRT(((i-172.5)**2)+(((345-j)-172.5)**2))
            local_dos_theta=(atan(C/86.25))*57.2957795
                DO l=1,1501
                    IF (local_dos_theta>=dos_theta(l) .and. local_dos_theta<=dos_theta(l+1)) THEN
                        Ibid1=Iuni(l)*(sin(dos_theta(l)))*57.2957795
                        Ibid2=Iuni(l+1)*(sin(dos_theta(l+1)))*57.2957795
                        Inten(i,j)=(Ibid1+Ibid2)/2
                    END IF
                END DO

        END DO
    END DO

    log_uni = 4
    fname = '2d_pat_1.raw'

    OPEN(unit = log_uni, file = fname, status = "replace", access = "stream", form = "unformatted")
        write(unit = log_uni) Inten
    close(unit = log_uni)

    write(unit=*,fmt = "(a)") " Result image in File: "//trim(fname)

    

END PROGRAM
