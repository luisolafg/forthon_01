program tictactoe
    integer :: p
    integer :: t
    character :: s1, s2, s3, s4, s5, s6, s7, s8, s9
    s1 = "_"
    s2 = "_"
    s3 = "_"
    s4 = "_"
    s5 = "_"
    s6 = "_"
    s7 = "_"
    s8 = "_"
    s9 = "_"
    do t = 1, 4
    write(*,*) "P1's turn"
    read(*,*) p
    if(p==1)then
        s1 = "X"
    else if(p==2)then
        s2 = "X"
    else if(p==3)then
        s3 = "X"
    else if(p==4)then
        s4 = "X"
    else if(p==5)then
        s5 = "X"
    else if(p==6)then
        s6 = "X"
    else if(p==7)then
        s7 = "X"
    else if(p==8)then
        s8 = "X"
    else if(p==9)then
        s9 = "X"
    end if
    write(*,*) s7, s8, s9
    write(*,*) s4, s5, s6
    write(*,*) s1, s2, s3
    if(s5=="X")then
        if(s5==s4 .and. s4==s6)then
            write(*,*) "P1 wins"
            stop
        else if(s5==s7 .and. s7==s3)then
            write(*,*) "P1 wins"
            stop
        else if(s5==s8 .and. s8==s2)then
            write(*,*) "P1 wins"
            stop
        else if(s5==s9 .and. s9==s1)then
            write(*,*) "P1 wins"
            stop
        end if
    else if(s4=="X")then
        if(s4==s7 .and. s7==s1)then
            write(*,*) "P1 wins"
            stop
        end if
    else if(s8=="X")then
        if(s8==s7 .and. s7==s9)then
            write(*,*) "P1 wins"
            stop
        end if
    else if(s6=="X")then
        if(s6==s9 .and. s9==s3)then
            write(*,*) "P1 wins"
            stop
        end if
    else if(s2=="X")then
        if(s2==s1 .and. s1==s3)then
            write(*,*) "P1 wins"
            stop
        end if
    end if
    write(*,*) "P2's turn"
    read(*,*) p
    if(p==1)then
        s1 = "O"
    else if(p==2)then
        s2 = "O"
    else if(p==3)then
        s3 = "O"
    else if(p==4)then
        s4 = "O"
    else if(p==5)then
        s5 = "O"
    else if(p==6)then
        s6 = "O"
    else if(p==7)then
        s7 = "O"
    else if(p==8)then
        s8 = "O"
    else if(p==9)then
        s9 = "O"
    end if
    write(*,*) s7, s8, s9
    write(*,*) s4, s5, s6
    write(*,*) s1, s2, s3
    if(s5=="O")then
        if(s5==s4 .and. s4==s6)then
            write(*,*) "P2 wins"
            stop
        else if(s5==s7 .and. s7==s3)then
            write(*,*) "P2 wins"
            stop
        else if(s5==s8 .and. s8==s2)then
            write(*,*) "P2 wins"
            stop
        else if(s5==s9 .and. s9==s1)then
            write(*,*) "P2 wins"
            stop
        end if
    else if(s4=="O")then
        if(s4==s7 .and. s7==s1)then
            write(*,*) "P2 wins"
            stop
        end if
    else if(s8=="O")then
        if(s8==s7 .and. s7==s9)then
            write(*,*) "P2 wins"
            stop
        end if
    else if(s6=="O")then
        if(s6==s9 .and. s9==s3)then
            write(*,*) "P2 wins"
            stop
        end if
    else if(s2=="O")then
        if(s2==s1 .and. s1==s3)then
            write(*,*) "P2 wins"
            stop
        end if
    end if
    end do
    write(*,*) "P1's turn"
    read(*,*) p
    if(p==1)then
        s1 = "X"
    else if(p==2)then
        s2 = "X"
    else if(p==3)then
        s3 = "X"
    else if(p==4)then
        s4 = "X"
    else if(p==5)then
        s5 = "X"
    else if(p==6)then
        s6 = "X"
    else if(p==7)then
        s7 = "X"
    else if(p==8)then
        s8 = "X"
    else if(p==9)then
        s9 = "X"
    end if
    write(*,*) s7, s8, s9
    write(*,*) s4, s5, s6
    write(*,*) s1, s2, s3
    if(s5=="X")then
        if(s5==s4 .and. s4==s6)then
            write(*,*) "P1 wins"
            stop
        else if(s5==s7 .and. s7==s3)then
            write(*,*) "P1 wins"
            stop
        else if(s5==s8 .and. s8==s2)then
            write(*,*) "P1 wins"
            stop
        else if(s5==s9 .and. s9==s1)then
            write(*,*) "P1 wins"
            stop
        end if
    else if(s4=="X")then
        if(s4==s7 .and. s7==s1)then
            write(*,*) "P1 wins"
            stop
        end if
    else if(s8=="X")then
        if(s8==s7 .and. s7==s9)then
            write(*,*) "P1 wins"
            stop
        end if
    else if(s6=="X")then
        if(s6==s9 .and. s9==s3)then
            write(*,*) "P1 wins"
            stop
        end if
    else if(s2=="X")then
        if(s2==s1 .and. s1==s3)then
            write(*,*) "P1 wins"
            stop
        end if
    else
        write(*,*) "It's a tie"
    end if
end program tictactoe
