# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 18:37:44 2020

@author: alesk
"""
print("Modeling pole figure (PF) from given inverse pole figure (IPF)")
import numpy as np
import scipy.integrate as sq
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D
from scipy import integrate
import numpy as np
import pandas as pd
import collections
import numpy as np ; import math
import os

def Ortorrombic(a,b,c,alpha,beta,gamma,hkl,hkl_ipf): #in this function we input the values of the DPF and the IPF an we obtain a group of angles that will be used for the final calculations 
    intensity,fideg,hkl123 = Arrays()
    rtg = 180.0/math.pi
    #Vectors
    a1 = np.array([[abs(a)],[0],[0]])
    a2 = np.array([[abs(a)*cos(gamma/rtg)],[abs(a)*sin(gamma/rtg)],[0]])
    a3 = np.array([[abs(a)*sin(0)],[abs(b)*sin(0)],[c]])

    print("a1=", a1, "\n")
    print("a2=", a2, "\n")
    print("a3=", a3, "\n")

    #Volume and vectors
    cross_prod = np.cross(a2,a3, axisa=0, axis= 0)
    print("cross_prod=",cross_prod,"\n")
    volume = a1.T.dot(cross_prod)
    print("volume, dot prod=",volume,"\n")

    b1 = (1/volume)*(np.cross(a2,a3, axisa=0, axis= 0))
    b2 = (1/volume)*(np.cross(a3,a1, axisa=0, axis= 0))
    b3 = (1/volume)*(np.cross(a1,a2, axisa=0, axis= 0))

    print("b1=",b1,"\n")
    print("b2=",b2,"\n")
    print("b3=",b3,"\n")

    #angles tau and ganma for 
    #hkl_ipf = [1,1,1]
    dir_crys = [[1,1,-1]]

    Bh = hkl_ipf[0]*b1 + hkl_ipf[1]*b2 + hkl_ipf[2]*b3
    print("Bh=", Bh, "\n")

    ModBh = math.sqrt(Bh[0]**2 + Bh[1]**2 + Bh[2]**2)
    print("modBh", ModBh, "\n")

    theta = arccos(Bh[2]/ModBh)
    theta = [l.tolist() for l in theta]
    theta = float(theta[0])
    thetadeg = theta*rtg
    if thetadeg == 0: thetadeg = 0.01

    print("theta (rad,degree)=", theta, thetadeg , "\n")
    g = sin(theta)*ModBh
    print("vector g=", g, "\n")
    if g >= 1: g = 1
    if g <= -1 : g = -1
    phi = arccos(Bh[0]/g)
    phi = [l.tolist() for l in phi]
    phi = float(phi[0])
    phideg = phi*rtg
    if math.isnan(phideg): phideg = 0
    print("phi (rad,degree)=", phi, phideg, "\n")

    #module for dircryst
    Bh_crys = []
    ModBh_crys = []
    for i in dir_crys:
        Bh_crys.append(i[0]*b1 + i[1]*b2 + i[2]*b3)

    for i in Bh_crys:
        ModBh_crys.append(math.sqrt(i[0]**2 + i[1]**2 + i[2]**2))
    print("Bh_crys=", Bh_crys, "\n")
    print("ModBh_crys=", ModBh_crys, "\n")

    #tau and gamma for hkl's:
    deltarad = [arccos(i[2]/j) for i,j in zip(Bh_crys,ModBh_crys)]
    deltarad = [l.tolist() for l in deltarad]
    deltarad = [item for sublist in deltarad for item in sublist]
    deltadeg = [x * rtg for x in deltarad]
    deltadeg = [i if i != 0 else 0.01 for i in deltadeg]
    print("deltarad=", deltarad , "\n")
    print("deltadeg=", deltadeg , "\n")


    g_crys = [sin(i)*j for i,j in zip(deltarad,ModBh_crys)]
    print("vector g_crys=", g_crys, "\n")
    iotarad = [arccos(i[0]/j) for i,j in zip(Bh_crys,g_crys)]
    iotarad = [l.tolist() for l in iotarad]
    iotarad = [item for sublist in iotarad for item in sublist]
    iotarad = [(0.0) if math.isnan(i) else i for i in iotarad]
    print("iotarad", iotarad, "\n")
    iotadeg = [x*rtg for x in iotarad]
    iotadeg = [(0.0) if math.isnan(i) else i for i in iotadeg] 
    print("iotadeg=", iotadeg, "\n")


    #modules for list of hkl
    Bh_hkl = []
    ModBh_hkl = []
    for i in hkl:
        Bh_hkl.append(i[0]*b1 + i[1]*b2 + i[2]*b3)

    for i in Bh_hkl:
        ModBh_hkl.append(math.sqrt(i[0]**2 + i[1]**2 + i[2]**2))
    print("Bh_hkl=", Bh_hkl, "\n")
    print("ModBh_hkl=", ModBh_hkl, "\n")

    #tau and gamma for hkl's:
    taurad = [arccos(i[2]/j) for i,j in zip(Bh_hkl,ModBh_hkl)]
    taurad = [l.tolist() for l in taurad]
    taurad = [item for sublist in taurad for item in sublist]
    taudeg = [x * rtg for x in taurad]
    taudeg = [i if i != 0 else 0.01 for i in taudeg]
    print("taurad=", taurad , "\n")
    print("tau=", taudeg , "\n")


    g_hkl = [sin(i)*j for i,j in zip(taurad,ModBh_hkl)]
    print("vector g_hkl=", g_hkl, "\n")
    gammarad = [arccos(i[0]/j) for i,j in zip(Bh_hkl,g_hkl)]
    gammarad = [l.tolist() for l in gammarad]
    gammarad = [item for sublist in gammarad for item in sublist]
    gammarad = [(0.0) if math.isnan(i) else i for i in gammarad]
    print("gamarad", gammarad, "\n")
    gammadeg = [x*rtg for x in gammarad]
    gammadeg = [(0.0) if math.isnan(i) else i for i in gammadeg] 
    print("gamma=", gammadeg, "\n")
    
    return (a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg)
def Arrays():

    df1 = pd.read_csv('BaTiO3.csv', encoding= 'latin-1')
    print ("dataframe=",df1,"\n")

    print ("test1,", list(df1.columns.values),"\n")
    print ("data list", df1[['      2d','        I']],"\n")

    df2 = df1.groupby('      2d')['        I'].sum()
    np.set_printoptions(suppress=True)
    intensity = df2.to_numpy()
    print ("same intensity", df2,"\n")
    print ("Intensity array", intensity,"\n")

    df3 = df1.drop_duplicates(['      2d'])
    fideg = df3['      2d'].to_numpy()
    print("dropping duplicates", df3, "\n" )
    print ("array 2d",fideg,"\n")

    df4 = df1[(df1[' l'] >= 0)]
    df4 = df4.drop_duplicates(['      2d'])
    print("new list for l", df4, "\n")
    dfh = df4['h'].to_numpy()
    dfk = df4['k'].to_numpy()
    dfl = df4[' l'].to_numpy()

    print("new list for h", dfh, "\n")
    print("new list for k", dfk, "\n")
    print("new list for l", dfl, "\n")

    hkl = [list(x) for x in zip(dfh , dfk , dfl)]
    print("hkl nest list", hkl,"\n")

    return (intensity,fideg,hkl)
a = 3.98 ; alpha = 90
b = 3.98 ; beta = 90
c = 4.04 ; gamma = 90
h12=int(input("input value h for inverse pole figure (h,k.l):"))
k12=int(input("input value k for inverse pole figure (h,k.l):"))
l12=int(input("input value l for inverse pole figure (h,k.l):"))
hkl_ipf=[h12,k12,l12]
widthdeg = int(input("input value of fWHM:"))
h12=int(input("input value h for Direct pole figure (h,k.l):"))
k12=int(input("input value k for Direct pole figure (h,k.l):"))
l12=int(input("input value l for Direct pole figure (h,k.l):"))
hkl=[[h12,k12,l12]]#list of values to be used of the direct pole figute
print(hkl)
a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = Ortorrombic(a,b,c,alpha,beta,gamma,hkl,hkl_ipf)#At this point we enter the values of te PF and IPF to obtain the respective angles that will be used later
#print("valores",thetadeg,phideg,taudeg,gammadeg)/#angles to be used in the calculations of IPF and PF
# Defining parameters:
rtg = 180.0 / pi
thetadeg = int(thetadeg); phideg = int(phideg);
taudeg = taudeg[0] ; gammadeg = gammadeg[0]
if thetadeg == 0.0: thetadeg = 0.01
if taudeg == 0.0: taudeg = 0.01
theta = thetadeg/rtg; phi = phideg/rtg; distwidth = widthdeg/rtg
tau = taudeg/rtg; gamma = gammadeg/rtg
print (" Max of IPF at theta, phi (degrees) = ", thetadeg, phideg)
print (" Max of IPF at theta, phi (radians) = ", theta, phi)
print (" IPF width (degrees and radians) = ", widthdeg, distwidth)
print (" Pole figure tau, gamma (degrees) = ", taudeg, gammadeg)
print (" Pole figure tau, gamma (radians) = ", tau, gamma)

# Defining functions:
def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    if d >= 1.0: d = 1.0
    elif -1.0 < d < 1.0 : d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    elif d <= -1.0 : d = -1.0
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnorma(eta, chi):
    return (4.0*pi/R0)*R1(eta, chi)
def Rvar(psi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if B >= 1.0: B = 1.0
    elif -1.0 < B < 1.0 : B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    elif B <= -1.0 : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))

def pfg(fi):
    pfgr = sq.quad(Rvar, 0, 2.0*pi)
    return pfgr[0]


# Calculating R surface integral:
R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization:
Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)

# Maximum of normalized IPF, R:
print (" R_normalized(max) =", Rnorma(theta, phi)/sin(theta))
print ()

# Calculating the inverse pole figure surface:
eta, chi = np.meshgrid(np.linspace (0, pi, 36) , np.linspace(0, 2.0*pi,72))
#calculating R
p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
p1 = arccos(p)
R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))
#plotting inverse pole figure
fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface(eta*rtg, chi*rtg, R, cmap='viridis', linewidths=0.2)
ax.set_xlim(0, 360); ax.set_ylim(0, 360); ax.set_zlim(0, 11)
plt.show()

# Calculating and plotting the direct pole figure curve:
file = open("polefig.dat", "w")
fidg = np.zeros(180); pfg = np.zeros(180)
fideg = 0.0; delfideg = 5.0
while fideg <= 180.0:
    n = int(fideg/5)
    fidg[n] = fideg
    fi = fideg/rtg
    pf = sq.quad(Rvar, 0, 2.0*pi)
    pfg[n] = pf[0]/(2.0*pi)
    print (" n, fidg[n], Pole Figure[n] = ", n, fidg[n], pfg[n])
    file.write(str(fidg[n]/rtg) + " " + str(pfg[n])+"\n")
    fideg = fideg + delfideg
print(fidg, pfg)
plt.plot(fidg, pfg)
plt.show()
file.close()
file=open('figpolos111.txt','w')
for i in range(37):
   file.write("%d\t" % (fidg[i]))
   file.write("%f\r\n" % (pfg[i]))
file.close()

# g=int(input("ingrese algo:"))

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
 
# def sphere(r):
#     u = np.linspace(0, 2 * np.pi, 100)
#     v = np.linspace(0, np.pi, 100)
#     x = r * np.outer(np.cos(u), np.sin(v))
#     y = r * np.outer(np.sin(u), np.sin(v))
#     z = r * np.outer(np.ones(np.size(u)), np.cos(v))
#     return x,y,z
 
# x,y,z = sphere(2)
# ax.plot_surface(x, y, z, rstride=4, cstride=4, color='b')
 
# plt.show()
# como hacer una esfera en python,
# de ahi tengo que hacer una variable con los maximos asigrnando con los angulos 
# multiplicar esos maximos por las diferentes direcciones x,y,z
# 3d figures con un ciclo for para ir sumando los maximos 