# -*- coding: utf-8 -*-
"""
Created on Mon Oct 26 15:56:59 2020

@author: alesk
"""
from numpy import sin,cos,pi,abs, sqrt, max # makes the code more readable
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import scipy as sci
import scipy.special as sp


from numpy import sin,pi
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
import scipy.integrate as sq
data=pd.read_csv('polefigure110.txt',header=1,delim_whitespace=True)
df=pd.DataFrame(data)
x=data.iloc[:,0]
x1=x
y1=data.iloc[:,1]
plt.plot(x1,y1,"r-",label='110')
plt.xlabel("x")
plt.ylabel("y")

# problem= sq.quad(lambda x: sin(x),0,pi)
# result=problem[0]
# print(result)
x2=np.linspace(0,pi/2.0,89)
y2=sin(x2)
# result1=sq.cumtrapz(y,x)
# print(result1)
plt.plot(x2,y2,"m-",label='sin')
l = int(input("l = "))
m = int(input("m = "))

# Defining functions:
def pleg(polar):
    K1 = sqrt((2*l+1)/(4*pi) * sp.factorial(l-m)/sp.factorial(l+m))
    delkron = 0
    if m == 0: delkron = 1
    K = sqrt(2-delkron) * K1
    y =sp.lpmv(m,l,cos(polar))
    return K * y

# Generating and Plotting selected Plm curve
theta = np.linspace(0, pi/2.0, 89)
p = pleg(theta)
q = - cos(theta)*(sin(theta))**3
plt.plot(theta, p)
# plt.plot(theta, q)
plt.show()

npl = y1*y2*p
print(npl)
plt.plot(x2,npl,"g-")
result1=sq.cumtrapz(npl,x2)
print(result1)