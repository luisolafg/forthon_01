# -*- coding: utf-8 -*-
"""
Created on Mon Mar  1 17:12:18 2021

@author: alesk
"""
import os
import pandas 
from os import listdir
import numpy as np
import numpy
from numpy import sin,cos,pi,abs, sqrt, max # makes the code more readable
import matplotlib.pyplot as plt
import scipy.special as sp
from scipy.special import legendre
import scipy.integrate as sq
def pleg(polar):
   K1 = sqrt((2*l+1)/(4*pi) * sp.factorial(l-m)/sp.factorial(l+m))
   delkron = 0
   if m == 0: delkron = 1
   K = sqrt(2-delkron) * K1
   y =sp.lpmv(m,l,cos(polar))
   return K * y
#print(os.getcwd())
os.chdir('archivos')
archivos = listdir()
#print(os.getcwd())
lmax = int(input("\n Enter lmax: "))
MCf=[]
t=0
for archivo in archivos:
    data=pandas.read_csv(str(archivo),header=1,delim_whitespace=True)
    df=pandas.DataFrame(data)
    x=data.iloc[:,0]
    y1 = data.iloc[:,1]
    y2 = sin(x)
    plt.plot(x,y1,"b-")
    plt.xlabel("polar angle (radians)")
    plt.ylabel("observed pole figure")
    plt.show()
    x3=[]
    for l in range(0, lmax+1):
        x3.append(l)
        j=numpy.linspace(0, l, l + 1)
        j=numpy.ndarray.tolist(j)
        x3[l]=j
    YR = 0.0
    Cf=[]
    for l in range(0, lmax + 1):
       for m in range(0, 1):
           p = sqrt(2.0*pi) * pleg(x)
           npl = y1 * y2 * p
           r=sq.cumtrapz(npl,x)
           x3[l][m] = r[-1]
           YR=x3[l][m] * p + YR
           Cf.append(r[-1])
           print(" l, coef = ", l, "{:.6f}".format(x3[l][m]))
           plt.plot(x,YR,"r-")
           plt.plot(x,y1,"b-")
           plt.xlabel("polar angle (radians)")
           plt.ylabel("observed and calculated pole figures")
           plt.show()
    MCf.append(Cf)