import sys
from PySide2.QtWidgets import QApplication, QLabel, QLineEdit
from PySide2.QtWidgets import QDialog, QPushButton, QVBoxLayout
from PySide2 import QtUiTools

class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("copytextexercise.ui")


        self.window.pushButton.clicked.connect(self.greetings)

        self.window.show()


    def greetings(self):

        text = self.window.lineEdit.text()
        print('Contents of QLineEdit widget: {}'.format(text))
        self.window.lineEdit_2.setText(str(text))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
