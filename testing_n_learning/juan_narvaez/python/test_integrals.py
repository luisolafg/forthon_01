import numpy as np ; import math
import scipy.integrate as sq
import scipy.special as special
from numpy import sin,cos,pi,abs, arccos, exp, tan
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def R(psi,tau,gamma,fi):
    return np.exp(-tau*gamma*fi**2)

tau = np.mgrid[0:5:5j]
gamma = np.mgrid[0:10:5j]
fi = np.mgrid[0:1:5j]

print ("tau=",tau,"\n")
print ("gamma=",gamma,"\n")
print ("fi=",fi,"\n")


value_integrals = []
for i,j,k in zip(tau,gamma,fi):    
    Valint,error = sq.quad(R,0,1,args=(i,j,k))
    value_integrals.append(Valint)
    print(i,j,k,Valint,"\n")
print("value of integrals",value_integrals)

#primer argumento es la variable de integracion

