def cuadratica():
    import math
    print("Se resolvera la ecuacion  cuadratica")
    a = float(raw_input("Dame coeficiente a:"))
    b = float(raw_input("Dame coeficiente b:"))
    c = float(raw_input("Dame coeficiente c:"))
    print()

    discriminante = b**2 - 4*a*c

    if discriminante < 0 or a == 0 :
        print("No tiene solucion")

    elif discriminante == 0 :
        val1 = (-b) / (2*a)
        print("La unica solucion es:", val1)

    else :
        disc = float(discriminante)**0.5
        val1 = (-b + disc) / (2*a)
        val2 = (-b - disc) / (2*a)

        print(val1,val2)

    while True :
        reply = raw_input('Desea resolver otra ecuacion?,[Escriba "si" para resolver otra, Escriba "no" para usar la consola]')

        if reply == "si" :
            cuadratica()

        if reply == "no" :
            break

#Ejecuta programa principal
if(__name__ == "__main__"):
    #Llama mi funcion
    cuadratica()




