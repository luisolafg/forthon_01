import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *


class Form(QDialog):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("ANAELU_interface.ui")

        image = "../../../../miscellaneous/lena.jpeg" #path to your image file
        self.exp_scene = QGraphicsScene()
        self.window.graphicsView_Exp.setScene(self.exp_scene)
        self.window.graphicsView_Exp.setDragMode(QGraphicsView.ScrollHandDrag)

        self.mask_n_diff_scene = QGraphicsScene()
        self.window.graphicsView_Mask_n_Diff.setScene(self.mask_n_diff_scene)
        self.window.graphicsView_Mask_n_Diff.setDragMode(QGraphicsView.ScrollHandDrag)

        self.window.pushButton_load_exp.clicked.connect(self.set_img1)
        self.window.pushButton_mask_model.clicked.connect(self.set_img2)

        self.window.show()


    def set_img1(self):
        fileName = "../../../../miscellaneous/lena.jpeg"
        image1 = QImage(fileName)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.exp_scene.addPixmap(self.pixmap_1)

    def set_img2(self):
        fileName = "../../../../miscellaneous/lena_gray.jpeg"
        image2 = QImage(fileName)
        self.pixmap_2 = QPixmap.fromImage(image2)
        self.mask_n_diff_scene.addPixmap(self.pixmap_2)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
