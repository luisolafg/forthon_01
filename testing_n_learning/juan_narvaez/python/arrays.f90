program hola
    integer, allocatable , dimension(:)  :: array1 
    integer                              :: array_size
    array_size = 5
    allocate(array1(array_size))
    write (*,*) "Hola arrays"
    array1 =  (/ (I, I = 1, array_size) /)
    write (*,*) array1
    array1 = (/1, 3,4,0,7 /)
    write (*,*) array1
    do i = 1, array_size, 1
        write(*,*) i
        array1(i) = i
    end do
    write(*,*) array1
end program hola


