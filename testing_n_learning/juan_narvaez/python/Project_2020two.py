import sys , time
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("project2020.ui")

        self.window.lcdNumber.setDigitCount(30)
        self.number = ''

        #LCD Counter calc buttons
        self.window.Start_Button_0.clicked.connect(self.set_0)

        self.window.Start_Button_1.clicked.connect(self.set1)
        self.window.Start_Button_2.clicked.connect(self.set2)
        self.window.Start_Button_3.clicked.connect(self.set3)
        self.window.Start_Button_4.clicked.connect(self.set4)
        self.window.Start_Button_5.clicked.connect(self.set5)
        self.window.Start_Button_6.clicked.connect(self.set6)
        self.window.Start_Button_7.clicked.connect(self.set7)
        self.window.Start_Button_8.clicked.connect(self.set8)
        self.window.Start_Button_9.clicked.connect(self.set9)

        self.window.pushButton_dot.clicked.connect(self.setdot)

        self.window.show()

        #LCD basic operations
        self.window.Start_Button_plus.clicked.connect(self.add)
        self.window.Start_Button_minus.clicked.connect(self.substract)
        self.window.Start_Button_x.clicked.connect(self.multiply)
        self.window.Start_Button_div.clicked.connect(self.divide)

        #Storing/clearing/showing memory buttons

        self.window.MemoryButton.clicked.connect(self.store1)
        self.window.MemoryButton2.clicked.connect(self.store2)
        self.window.Start_Button_clear.clicked.connect(self.reset)
        self.window.Start_Button_equal.clicked.connect(self.equal)

        #display on lcdNumber
    def set_0(self):
        self.number += '0'
        self.window.lcdNumber.display(self.number)

    def set1(self):
        self.number += '1'
        self.window.lcdNumber.display(self.number)

    def set2(self):
        self.number += '2'
        self.window.lcdNumber.display(self.number)

    def set3(self):
        self.number += '3'
        self.window.lcdNumber.display(self.number)

    def set4(self):
        self.number += '4'
        self.window.lcdNumber.display(self.number)

    def set5(self):
        self.number += '5'
        self.window.lcdNumber.display(self.number)

    def set6(self):
        self.number += '6'
        self.window.lcdNumber.display(self.number)

    def set7(self):
        self.number += '7'
        self.window.lcdNumber.display(self.number)

    def set8(self):
        self.number += '8'
        self.window.lcdNumber.display(self.number)

    def set9(self):
        self.number += '9'
        self.window.lcdNumber.display(self.number)

    def setdot(self):
        self.number += '.'
        self.window.lcdNumber.display(self.number)

    #operations for calculator

    def add(self):
        self.value = float(self.number)
        self.number = ''
        self.operator_use = '+'

    def substract(self):
        if self.number == '':
            self.number = '-'
            self.window.lcdNumber.display(self.number)
        else:
            self.value = float(self.number)
            self.number = ''
            self.operator_use = '-'

    def multiply(self):
        self.value = float(self.number)
        self.number = ''
        self.operator_use = '*'

    def divide(self):
        self.value = float(self.number)
        self.number = ''
        self.operator_use = '/'

    #stroring memory

    def store1(self):
        self.memory1 = self.window.lcdNumber.value()

    def store2(self):
        self.memory2 = self.window.lcdNumber.value()

    def reset(self):
        self.value   = ''
        self.number = ''
        self.window.lcdNumber.display(0)

    def equal(self):
       if self.operator_use == '*':
           self.LCDscreen = self.value.__mul__(float(self.number))
       elif self.operator_use == '+':
           self.LCDscreen =  self.value.__add__(float(self.number))
       elif self.operator_use == '-':
           self.LCDscreen = self.value.__sub__(float(self.number))
       elif self.operator_use == '/':
           self.LCDscreen = self.value.__truediv__(float(self.number))

       self.number = ''
       self.value = ''

       self.window.lcdNumber.display(self.LCDscreen)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())


#test2
