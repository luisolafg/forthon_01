import sys , time
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("project2020.ui")

        #LCD Counter calc buttons
        self.window.Start_Button_0.clicked.connect(self.set_0)

        self.window.Start_Button_1.clicked.connect(self.set1)
        self.window.Start_Button_2.clicked.connect(self.set2)
        self.window.Start_Button_3.clicked.connect(self.set3)
        self.window.Start_Button_4.clicked.connect(self.set4)
        self.window.Start_Button_5.clicked.connect(self.set5)
        self.window.Start_Button_6.clicked.connect(self.set6)
        self.window.Start_Button_7.clicked.connect(self.set7)
        self.window.Start_Button_8.clicked.connect(self.set8)

        self.window.Start_Button_9.clicked.connect(self.set9)
        self.window.show()

        #LCD basic operations
        self.window.Start_Button_plus.clicked.connect(self.add)
        self.window.Start_Button_minus.clicked.connect(self.substract)
        self.window.Start_Button_x.clicked.connect(self.multiply)
        self.window.Start_Button_div.clicked.connect(self.divide)

        #display on lcdNumber
    def set_0(self):
        self.window.lcdNumber.display(0)
        self.memory = self.window.lcdNumber.value()

    def set1(self):
        self.window.lcdNumber.display(1)
        self.memory1 = self.window.lcdNumber.value()

    def set2(self):
        self.window.lcdNumber.display(2)
        self.memory2 = self.window.lcdNumber.value()

    def set3(self):
        self.window.lcdNumber.display(3)
        self.memory3 = self.window.lcdNumber.value()

    def set4(self):
        self.window.lcdNumber.display(4)
        self.memory4 = self.window.lcdNumber.value()

    def set5(self):
        self.window.lcdNumber.display(5)
        self.memory5 = self.window.lcdNumber.value()

    def set6(self):
        self.window.lcdNumber.display(6)
        self.memory6 = self.window.lcdNumber.value()

    def set7(self):
        self.window.lcdNumber.display(7)
        self.memory7 = self.window.lcdNumber.value()

    def set8(self):
        self.window.lcdNumber.display(8)
        self.memory8 = self.window.lcdNumber.value()

    def set9(self):
        self.window.lcdNumber.display(9)
        self.memory9 = self.window.lcdNumber.value()

    def add(self):
        self.window.lcdNumber.display(self.memory4 + self.memory3)

    def substract(self):
        self.window.lcdNumber.display(self.memory4 - self.memory3)

    def multiply(self):
        self.window.lcdNumber.display(self.memory3 * self.memory4)

    def divide(self):
        self.window.lcdNumber.display(self.memory8 / self.memory4)

    def clear(self):
        self.window.lcdNumber.display(0)





if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
