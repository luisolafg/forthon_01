#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# test Juan

from setuptools import setup
from setup_qt import build_qt
import os

setup(
    name="ANAELU",
    version = "3.0.0",
    description="ANAlytical Emulator Laue Utility",
    author="Luis Fuentes-Montero (Luiso) Et al",
    author_email="luicub@gmail.com",
    url="https://gitlab.com/luisolafg/anaelu3",
    platforms="GNU/Linux & Windows",
    packages=["ANAELU", "ANAELU.operation", "ANAELU.GUI", "pckg"],
    package_dir={"": "command_center",},
    data_files=[
        (
            "ANAELU/resources",
            [
                "command_center/ANAELU/imgdata/anaelu.png",
                "command_center/ANAELU/imgdata/lena.jpeg",
                "command_center/ANAELU/imgdata/lena_gray.jpeg",
                "command_center/ANAELU/imgdata/lena_gray_inverted.jpeg",
                "command_center/ANAELU/imgdata/lena_inverted.jpeg",
                "command_center/ANAELU/imgdata/a.exe",

                "command_center/ANAELU/tgui/ANAELU_Interface.py",
                "command_center/ANAELU/tgui/ANAELU_interface.ui",
                "command_center/ANAELU/tgui/static_linkin.exe",
                "command_center/ANAELU/tgui/anaelu_calc_xrd.exe",
                "command_center/ANAELU/tgui/img_file.edf",
                "command_center/ANAELU/tgui/img_file.RAW",
                "command_center/ANAELU/tgui/My_Cfl.cfl",
                "command_center/ANAELU/tgui/my_params.dat",
            ],
        )
    ],
    include_package_data=True,
    entry_points={"console_scripts": ["ANAELU=ANAELU.main_ANAELU:main"]},
)
