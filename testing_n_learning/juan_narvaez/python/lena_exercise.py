import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *

from PySide2.QtGui import QPixmap, QImage

from PySide2 import QtUiTools

class Form(QObject):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("GUIexercise.ui")

        fileName = "../../../miscellaneous/lena.jpeg"
        image = QImage(fileName)
        """
        self.label = QLabel()
        self.label.setPixmap(QPixmap.fromImage(image))
        self.window.scrollArea.setWidget(self.label)

        image_2 = QImage(fileName)
        self.label_2 = QLabel()
        self.label_2.setPixmap(QPixmap.fromImage(image_2))
        self.window.scrollArea_2.setWidget(self.label_2)
        """
        self.window.graphicsView.setPixmap(QPixmap.fromImage(image))

        self.window.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())

