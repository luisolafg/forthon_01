import sys
from PySide2.QtWidgets import QApplication, QLabel, QLineEdit
from PySide2.QtWidgets import QDialog, QPushButton, QVBoxLayout

class Form(QDialog):
    """"""

    def __init__(self, parent=None):
        #window function
        super(Form, self).__init__(parent)

        self.edit = QLineEdit("")
        self.button = QPushButton("Copy text")
        self.edit2 = QLineEdit("hola")

        #window layout
        layout = QVBoxLayout()
        layout.addWidget(self.edit)
        layout.addWidget(self.button)
        layout.addWidget(self.edit2)

        self.setLayout(layout)

        self.button.clicked.connect(self.greetings)


    def greetings(self):
        #copy text
        text = self.edit.text()
        print('Contents of QLineEdit widget: {}'.format(text))
        self.edit2.setText(str(text))

if __name__ == "__main__":
    app = QApplication([])
    form = Form()
    form.show()
    sys.exit(app.exec_())
