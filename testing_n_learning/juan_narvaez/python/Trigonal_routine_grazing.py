import numpy as np ; import math
from numpy import sin,cos,pi,abs, arccos, exp, tan, arctan2
from Pandas import Arrays

def Trigonal(a,b,c,alpha,beta,gamma):
    intensity,fideg,hkl = Arrays()
    rtg = 180.0/math.pi

    #Vectors
    a1 = np.array([[abs(a)],[0],[0]])
    a2 = np.array([[abs(a)*cos(gamma/rtg)],[abs(a)*sin(gamma/rtg)],[0]])
    a3 = np.array([[abs(a)*sin(0)],[abs(b)*sin(0)],[c]])
    print("a1=", a1, "\n")
    print("a2=", a2, "\n")
    print("a3=", a3, "\n")
    #Volume and vectors
    cross_prod = np.cross(a2,a3, axisa=0, axis= 0)
    print("cross_prod=",cross_prod,"\n")
    volume = a1.T.dot(cross_prod)
    print("volume, dot prod=",volume,"\n")
    b1 = (1/volume)*(np.cross(a2,a3, axisa=0, axis= 0))
    b2 = (1/volume)*(np.cross(a3,a1, axisa=0, axis= 0))
    b3 = (1/volume)*(np.cross(a1,a2, axisa=0, axis= 0))
    print("b1=",b1,"\n")
    print("b2=",b2,"\n")
    print("b3=",b3,"\n")

    ###############################################
    '''Tau and phi for a list of hkl (crystalographic directions-Eduardo)'''
    #angles tau and ganma for 
    #hkl_list = [[1,0,0],[1,1,1],[0,1,2],[-1,2,3],[-1,1,0],[2,-1,0],[1,-1,0],[1,-2,0],[0,-1,0],[-1,-1,0],[-1,0,0]]
    hkl_list = [[0,0,1],[0,0,-1],[0,0,1],[0,0,-1]]
    Bh_list = [i[0]*b1 + i[1]*b2 + i[2]*b3 for i in hkl_list]
    print("Bh_list=", Bh_list, "\n")
    ModBh_list = [math.sqrt(i[0]**2 + i[1]**2 + i[2]**2) for i in Bh_list]
    print("modBh_list", ModBh_list, "\n")
    theta_list = [np.arctan2(math.sqrt(i[0]**2 + i[1]**2), i[2]) for i in Bh_list]
    print("check_bug theta=", theta_list,"\n")
    print("check_bug2 theta=", theta_list,"\n")
    theta_list = [float(item) for item in theta_list]
    print("theta_list=", theta_list,"\n")
    thetadeg_list = [rtg*x for x in theta_list]
    thetadeg_list = [0.01 if a_ == 0 else a_ for a_ in thetadeg_list]
    g_list = sin(thetadeg_list)*ModBh_list
    print("g_list=", g_list,"\n")
    g_list = [1 if a_ >= 1 else a_ for a_ in g_list]
    g_list = [-1 if a_ <= -1 else a_ for a_ in g_list]
    print("g_list=", g_list,"\n")
    phi_list =  [np.arctan2(i[1],i[0]) for i in Bh_list]
    phi_list = [l.tolist() for l in phi_list]
    phi_list = [item for sublist in phi_list for item in sublist]
    phi_list = [(0.0) if math.isnan(i) else i for i in phi_list]
    print("phi_list=", phi_list,"\n")
    phideg_list = [x*rtg for x in phi_list]
    phideg_list = [(0.0) if math.isnan(i) else i for i in phideg_list] 
    print("thetadeg_list=", thetadeg_list,"\n")
    print("phideg_list=", phideg_list,"\n")

    ###############################################
    '''Theta and phi for one hkl'''
    ###############################################
    hkl_ipf = [0,-1,0]
    #dir_crys = [[1,1,-1]]
    Bh = hkl_ipf[0]*b1 + hkl_ipf[1]*b2 + hkl_ipf[2]*b3
    print("Bh=", Bh, "\n")
    ModBh = math.sqrt(Bh[0]**2 + Bh[1]**2 + Bh[2]**2)
    print("modBh", ModBh, "\n")
    #theta = arccos(Bh[2]/ModBh)
    theta = np.arctan2(math.sqrt(Bh[0]**2 + Bh[1]**2), Bh[2])
    theta = [l.tolist() for l in theta]
    theta = float(theta[0])
    thetadeg = theta*rtg
    if thetadeg == 0: thetadeg = 0.01
    print("theta (rad,degree)=", theta, thetadeg , "\n")
    g = sin(theta)*ModBh
    #print("vector g=", g, "\n")
    if g >= 1: g = 1
    if g <= -1 : g = -1
    #phi = arccos(Bh[0]/g)
    phi = np.arctan2(Bh[1],Bh[0])
    phi = [l.tolist() for l in phi]
    phi = float(phi[0])
    phideg = phi*rtg
    if math.isnan(phideg): phideg = 0
    print("phi (rad,degree)=", phi, phideg, "\n")



    #modules for list of hkl
    Bh_hkl = []
    ModBh_hkl = []
    for i in hkl:
        Bh_hkl.append(i[0]*b1 + i[1]*b2 + i[2]*b3)
    for i in Bh_hkl:
        ModBh_hkl.append(math.sqrt(i[0]**2 + i[1]**2 + i[2]**2))
    print("Bh_hkl=", Bh_hkl, "\n")
    print("ModBh_hkl=", ModBh_hkl, "\n")
    #tau and gamma for hkl's:
    taurad = [arccos(i[2]/j) for i,j in zip(Bh_hkl,ModBh_hkl)]
    taurad = [l.tolist() for l in taurad]
    taurad = [item for sublist in taurad for item in sublist]
    taudeg = [x * rtg for x in taurad]
    taudeg = [i if i != 0 else 0.01 for i in taudeg]
    print("taurad=", taurad , "\n")
    print("tau=", taudeg , "\n")
    g_hkl = [sin(i)*j for i,j in zip(taurad,ModBh_hkl)]
    print("vector g_hkl=", g_hkl, "\n")
    gammarad = [arccos(i[0]/j) for i,j in zip(Bh_hkl,g_hkl)]
    gammarad = [l.tolist() for l in gammarad]
    gammarad = [item for sublist in gammarad for item in sublist]
    gammarad = [(0.0) if math.isnan(i) else i for i in gammarad]
    print("gamarad", gammarad, "\n")
    gammadeg = [x*rtg for x in gammarad]
    gammadeg = [(0.0) if math.isnan(i) else i for i in gammadeg] 
    print("gamma=", gammadeg, "\n")

    return (a,b,c,alpha,beta,gamma,thetadeg,phideg,taudeg,gammadeg,theta_list,thetadeg_list, \
        phi_list,phideg_list)

if __name__ == "__main__":
    #thetadeg,phideg,taudeg,gammadeg = Ortorrombic()
   a,b,c,alpha,beta,gamma,thetadeg,phideg,taudeg,gammadeg,theta_list,thetadeg_list, \
        phi_list,phideg_list = Trigonal()