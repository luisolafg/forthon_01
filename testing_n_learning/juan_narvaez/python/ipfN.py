import numpy as np
import math
import scipy.integrate as sq
import scipy.special as special
from numpy import sin,cos,pi,abs, arccos, exp
from mpl_toolkits.mplot3d import Axes3D

def R1(chi, eta):
    d = cos(eta)*cos(alpha)+sin(eta)*sin(alpha)*cos(chi-beta)
    dr = arccos(d)
    return exp(-0.693*((dr/distwidth)**2))*sin(eta)

#north pole and desired planes
hkl_np = [0,0,1]
hkl = [1,1,1]
rtg = 180/math.pi

alpha = pi/2.0; beta = pi/2.0; distwidth = 0.5
print (" Max of IPF at alpha, beta = \n", alpha*rtg, beta*rtg)
print (" IPF width = \n", distwidth)

R1suma = sq.dblquad(R1,0,pi,lambda eta:0, lambda eta: 2*pi)
R0 = R1suma[0]
print ("Integral de R1, 4*pi = \n", R0, 4.0*pi)

#create 2D arrays of new angles
eta, chi = np.meshgrid(np.linspace (0, math.pi, 18) , np.linspace(0, 2.0*math.pi,36))

#calculating R
p = cos(eta)*cos(alpha)+sin(eta)*sin(alpha)*cos(chi-beta)
p1 = arccos(p)
R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))

#plotting figure
fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface( eta , chi , R , rstride=1, cstride=1)
plt.show()

