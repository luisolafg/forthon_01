#garbage program for dumb tests
from scipy import integrate
import numpy as np
import pandas as pd
import collections

intensity = [x.split()[8] for x in open('PZT.txt').readlines()[1:]]
print("intensity value", intensity, "\n")

angle = [x.split()[7] for x in open('PZT.txt').readlines()[1:]]
print("angle value" , angle , "\n")

h = [x.split()[0] for x in open('PZT.txt').readlines()[1:]]
k = [x.split()[1] for x in open('PZT.txt').readlines()[1:]]
l = [x.split()[2] for x in open('PZT.txt').readlines()[1:]]

lst_difract = [list(x) for x in zip(intensity , angle )]
hkl = [list(x) for x in zip(h , k , l)]

print("nested list for difract", lst_difract)
print("nested list for difract", hkl)

#sum with pandas at same degree
df = pd.read_csv('test1.CSV', encoding= 'utf-8')
df1 = df.groupby('      2d')['         I'].sum()
df2 = df.groupby('         I')['      2d'].sum()

print ("columns = ", df.columns)
print ("same degree and summe intensity=", df1 ,"\n")

dfI = df1.to_numpy()
df2d = df2.to_numpy()
print ("Intensity array", dfI,"\n")
print ("Intensity array", df2d,"\n")

#get list from metadata
#difract_list = df['      2d'].values.tolist()
#intensity_list = df['         I'].values.tolist()

#print ("list angles 2d = ", difract_list, "\n")
#print (";ist of intensity = ", difract_list, "\n")

#get nested list from metadata
nested_dif_int = df.values.tolist()
print("list of list=" , nested_dif_int, "\n")

#same HKLS
df1 = pd.read_csv('test2.CSV', encoding= 'utf-8')
print("hkls", df1.columns,"\n")

 

df_new = df1.drop(df1[(df1['l'] < 0)].index)

print("newhkls =", df_new)
#df2 = df2.groupby(['a', 'b']).sum(['i']).reset_index()



