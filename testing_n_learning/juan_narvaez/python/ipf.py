import numpy as np
import math
import scipy.integrate as integrate
import scipy.special as special
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos
from mpl_toolkits.mplot3d import Axes3D

#north pole and desired planes
hkl_np = [0,0,1]
hkl = [1,1,1]
rtg = 180/math.pi

alpha = pi/2.0; beta = pi/2.0
Ro = 1.0; distwidth = 0.5
print (" Max of IPF at alpha, beta = \n", alpha*rtg, beta*rtg)
print ( "IPF amplitude and width = \n", Ro, distwidth)

#create 2D arrays of new angles
eta, chi = np.meshgrid(np.linspace (0, math.pi, 18) , np.linspace(0, 2.0*math.pi,36))

#calculating R
p = cos(eta)*cos(alpha)+sin(eta)*sin(alpha)*cos(chi-beta)
p1 = arccos(p)
R = Ro*np.exp(-0.693*((p1/distwidth)**2))

print ( "ro values = \n", p,)
print ( "R values = \n", R)

#plotting figure
fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface( eta , chi , R , rstride=1, cstride=1)
plt.show()


#integral, inverse pole figure 0 - 2pi
# def R1(eta,chi):
#     return np.exp(-0.693*((p1/distwidth)**2))*sin(eta)*eta*chi
# r2 = integrate.dblquad(R1,0,pi/2.0,lambda polar: 0, lambda polar: 2.0*pi)
# print ("R2 integral:\n",r2 , "\n")


# #whole integral value
# r3 = r2[0]*R
# R0 = (4*math.pi)/(r3)

# fourpi = R0*r3

# print ("R3 value to get Ro:\n ",r3 , "\n")
# print (" Ro value:\n ",R0 , "\n")
# print (" if 4pi, correct:\n ", fourpi , "\n")




