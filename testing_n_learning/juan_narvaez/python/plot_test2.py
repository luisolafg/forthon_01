#test plots
import numpy as np
from Cobre_Difractograma_texture import do_all

#FWHM Gaussian Equation
def gaussian_dist(x, a , mean, sd):
    return a/(sd*np.sqrt(2.0*np.pi))*np.exp(-(x - mean)**2.0/(2.0*sd**2.0))

if __name__ == "__main__":

    lst = do_all()

    x = np.linspace(30,150,1000)
    y1 = np.zeros((1000))

    fwhm = 0.7
    std = fwhm/2.35

    for i,xgeom in enumerate(x):
        for j in lst:
            y1[i] += gaussian_dist(xgeom , j[0] , j[1] , std)

    #plotting of peaks
    import pylab as pl
    pl.plot(x, y1, '-k', label='1,1,1')
    pl.xlabel('2θ')
    pl.ylabel('Intensity')
    pl.show()
