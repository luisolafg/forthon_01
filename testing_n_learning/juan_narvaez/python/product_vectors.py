import numpy as np ; import math
from numpy import sin,cos,pi,abs, arccos, exp, tan
from Pandas import Arrays


def Trigonal(a,b,c,alpha,beta,gamma):
    intensity,fideg,hkl = Arrays()
    rtg = 180.0/math.pi
 
    #Vectors
    a1 = np.array([[abs(a)],[0],[0]])
    a2 = np.array([[abs(a)*cos(gamma/rtg)],[abs(a)*sin(gamma/rtg)],[0]])
    a3 = np.array([[abs(a)*sin(0)],[abs(b)*sin(0)],[c]])

    print("a1=", a1, "\n")
    print("a2=", a2, "\n")
    print("a3=", a3, "\n")

    #Volume and vectors
    cross_prod = np.cross(a2,a3, axisa=0, axis= 0)
    print("cross_prod=",cross_prod,"\n")
    volume = a1.T.dot(cross_prod)
    print("volume, dot prod=",volume,"\n")

    b1 = (1/volume)*(np.cross(a2,a3, axisa=0, axis= 0))
    b2 = (1/volume)*(np.cross(a3,a1, axisa=0, axis= 0))
    b3 = (1/volume)*(np.cross(a1,a2, axisa=0, axis= 0))

    print("b1=",b1,"\n")
    print("b2=",b2,"\n")
    print("b3=",b3,"\n")

    #angles tau and ganma for 
    hkl_ipf = [0,2,0]
    dir_crys = [[0,-2,0]]

    Bh = hkl_ipf[0]*b1 + hkl_ipf[1]*b2 + hkl_ipf[2]*b3
    print("Bh=", Bh, "\n")

    ModBh = math.sqrt(Bh[0]**2 + Bh[1]**2 + Bh[2]**2)
    print("modBh", ModBh, "\n")

    theta = arccos(Bh[2]/ModBh)
    theta = [l.tolist() for l in theta]
    theta = float(theta[0])
    thetadeg = theta*rtg
    if thetadeg == 0: thetadeg = 0.01

    print("theta (rad,degree)=", theta, thetadeg , "\n")
    g = sin(theta)*ModBh
    print("vector g=", g, "\n")
    if g >= 1: g = 1
    if g <= -1 : g = -1
    phi = arccos(Bh[0]/g)
    phi = [l.tolist() for l in phi]
    phi = float(phi[0])
    phideg = phi*rtg
    if math.isnan(phideg): phideg = 0
    print("phi (rad,degree)=", phi, phideg, "\n")

    #module for dircryst
    Bh_crys = []
    ModBh_crys = []
    for i in dir_crys:
        Bh_crys.append(i[0]*b1 + i[1]*b2 + i[2]*b3)

    for i in Bh_crys:
        ModBh_crys.append(math.sqrt(i[0]**2 + i[1]**2 + i[2]**2))
    print("Bh_crys=", Bh_crys, "\n")
    print("ModBh_crys=", ModBh_crys, "\n")

    #tau and gamma for hkl's:
    deltarad = [arccos(i[2]/j) for i,j in zip(Bh_crys,ModBh_crys)]
    deltarad = [l.tolist() for l in deltarad]
    deltarad = [item for sublist in deltarad for item in sublist]
    deltadeg = [x * rtg for x in deltarad]
    deltadeg = [i if i != 0 else 0.01 for i in deltadeg]
    print("deltarad=", deltarad , "\n")
    print("deltadeg=", deltadeg , "\n")


    g_crys = [sin(i)*j for i,j in zip(deltarad,ModBh_crys)]
    print("vector g_crys=", g_crys, "\n")
    iotarad = [arccos(i[0]/j) for i,j in zip(Bh_crys,g_crys)]
    iotarad = [l.tolist() for l in iotarad]
    iotarad = [item for sublist in iotarad for item in sublist]
    iotarad = [(0.0) if math.isnan(i) else i for i in iotarad]
    print("iotarad", iotarad, "\n")
    iotadeg = [x*rtg for x in iotarad]
    iotadeg = [(0.0) if math.isnan(i) else i for i in iotadeg] 
    print("iotadeg=", iotadeg, "\n")

    #modules for list of hkl
    Bh_hkl = []
    ModBh_hkl = []
    for i in hkl:
        Bh_hkl.append(i[0]*b1 + i[1]*b2 + i[2]*b3)

    for i in Bh_hkl:
        ModBh_hkl.append(math.sqrt(i[0]**2 + i[1]**2 + i[2]**2))
    print("Bh_hkl=", Bh_hkl, "\n")
    print("ModBh_hkl=", ModBh_hkl, "\n")

    #tau and gamma for hkl's:
    taurad = [arccos(i[2]/j) for i,j in zip(Bh_hkl,ModBh_hkl)]
    taurad = [l.tolist() for l in taurad]
    taurad = [item for sublist in taurad for item in sublist]
    taudeg = [x * rtg for x in taurad]
    taudeg = [i if i != 0 else 0.01 for i in taudeg]
    print("taurad=", taurad , "\n")
    print("tau=", taudeg , "\n")


    g_hkl = [sin(i)*j for i,j in zip(taurad,ModBh_hkl)]
    print("vector g_hkl=", g_hkl, "\n")
    gammarad = [arccos(i[0]/j) for i,j in zip(Bh_hkl,g_hkl)]
    gammarad = [l.tolist() for l in gammarad]
    gammarad = [item for sublist in gammarad for item in sublist]
    gammarad = [(0.0) if math.isnan(i) else i for i in gammarad]
    print("gamarad", gammarad, "\n")
    gammadeg = [x*rtg for x in gammarad]
    gammadeg = [(0.0) if math.isnan(i) else i for i in gammadeg] 
    print("gamma=", gammadeg, "\n")
    
    return (a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg)

if __name__ == "__main__":
    #thetadeg,phideg,taudeg,gammadeg = Ortorrombic()
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = Trigonal()
