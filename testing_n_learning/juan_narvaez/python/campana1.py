import numpy as np
import math
from numpy import sin,cos,pi,abs,arccos # makes the code more readable
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#from mayavi import mlab
eta, chi = np.mgrid[0:np.pi:5j,0:2*np.pi:9j]
r = cos(eta)*cos(pi/4.0)+sin(eta)*sin(pi/4.0)*cos(chi-pi/4.0)
ro = arccos(r)
z = np.exp(-ro**2)
print (r)
print (ro)
#mlab.figure()
#mlab.mesh(eta,chi,z)
#mlab.scalarbar(orientation="vertical")
#mlab.show()

fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface( eta , chi , z , rstride=1, cstride=1)
plt.show()
