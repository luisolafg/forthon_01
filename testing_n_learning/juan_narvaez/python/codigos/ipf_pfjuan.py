import numpy as np; import math
import scipy.integrate as sq
import scipy.special as special
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D



def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)

def Rnorma(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))*sin(eta)

def Rvar(psi):
    A = cos(tau)*cos(PHI) + sin(tau)*sin(PHI)*cos(psi)
    eta = arccos(A)
    ctgeta = 1.0 / tan(eta) ; ctgtau = 1.0 / tan(tau)
    B = cos(PHI)/(sin(eta)*sin(tau)) - ctgeta*ctgtau
    if psi < pi:
        chi = -arccos(B)
    else:
        chi = arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-PHI)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))

#north pole and desired planes
hkl_np = [0,0,1]
hkl = [1,1,1]
rtg = 180.0/math.pi

#theta = pi/2.0; phi = pi; distwidth = 0.5
theta = 0.95531662 ; phi = pi/4.0; distwidth = 0.5
#theta = 0.0 ; phi = 0.0; distwidth = 0.5
print (" Max of IPF at theta, phi (degrees) = \n", theta*rtg, phi*rtg)
print (" Max of IPF at theta, phi (radians) = \n", theta, phi)
print (" IPF width (radians) = \n", distwidth)
#tau = pi/2.0 ; gamma = pi
tau = 0.95531662 ; gamma = pi/4.0
#tau = 0.0 ; gamma = 0.0
print (" PF vector at tau, gamma (degrees) = \n", tau*rtg, gamma*rtg)
print (" PF vector at tau, gamma (radians) = \n", tau, gamma)

R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]

print ("R0 values: \n" , R0 , "\n")


while tau < 2:
    Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
    Rnorsu = Rnormsum[0]
    print (" Integral of the normalized IPF = \n", tau, Rnormsum[0])
    print (" 4*pi =\n ", 4.0*pi)
    tau = tau + 0.05

#print " R_normalized(max) =", Rnorma(theta, phi)/sin(theta)

#Calculation of pole figure
PHI = 0
while PHI < 2:
    pf = sq.quad(Rvar, 0, 2.0*pi)
    print (" PHI and Pole Figure at PHI:\n", PHI, pf[0]/(2.0*pi))
    #print("PF values por unidimensional PF \n" , pf , "\n")
    PHI = PHI + 0.05


#pf1 =np.zeros(13)
#for i in pf:
 #   pf1 += pf[i]

#pf1 = (0.05, 0.1, 0.15, 0.2, 0.25, 0.3 , 0.35 , 0.40 , 0.45, 0.50 , 0.55 , 0.60, 0.65)

#PHI = np.linspace(0,0.65,13)
#pf = sq.quad(Rvar, 0, 2.0*pi)

#print("PHI values por unidimensional PF \n" , PHI , "\n")
#print("pf1 values for unidimensional PF \n" , pf1 , "\n")
#print("PF values por unidimensional PF \n" , pf , "\n")

#plt.plot(PHI, pf1)
#plt.show()



#create 2D arrays of new angles inverse pole figure
#eta, chi = np.meshgrid(np.linspace (0, math.pi, 36) , np.linspace(0, 2.0*math.pi,72))

#calculating R for inverse pole figura
#p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
#p1 = arccos(p)
#R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))
#print ("R values for plotting \n", R , "\n")


#calculating P for pole figure
#d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-PHI)
#d1 = arccos(d)
#P = (4.0*pi/R0)*exp(-0.693*((d1/distwidth)**2))

#plotting inverse pole figure
# fig1= plt.figure()
# ax1 = Axes3D(fig1)

# ax1.plot_surface(eta, chi, R,cmap='viridis', linewidths=0.2)
# ax1.set_xlim(0, 6); ax1.set_ylim(0, 6); ax1.set_zlim(0, 12)
# plt.show()

# #plotting pole figure
# fig2 = plt.figure()
# ax2 = Axes3D(fig2)

# ax2.plot_surface(eta, chi, P,cmap='viridis', linewidths=0.2)
# ax2.set_xlim(0, 6); ax2.set_ylim(0, 6); ax2.set_zlim(0, 12)
# plt.show()



#checking values
# print
# theta = 45 ; phi = 0; distwidth = 0.5
# print (" theta, phi (degrees) = ", theta, phi)
# theta=theta/rtg; phi=phi/rtg

# tau = 45 ; gamma = 0
# print (" tau, gamma (degrees) = ", tau, gamma)
# tau=tau/rtg; gamma = gamma/rtg

# PHI = 20.0; psi = 270
# print (" PHI, psi (degrees) = ", PHI, psi)
# PHI=PHI/rtg; psi=psi/rtg

# A = cos(tau)*cos(PHI) + sin(tau)*sin(PHI)*cos(psi)
# eta = arccos(A)
# ctgeta = 1.0 / tan(eta) ; ctgtau = 1.0 / tan(tau)
# B = cos(PHI)/(sin(eta)*sin(tau)) - ctgeta*ctgtau
# if psi < pi:
#     chi = -arccos(B)
# else:
#     chi = arccos(B)
# print (" eta, chi = ", eta*rtg, chi*rtg)

# d = (cos(eta)*cos(tau)) + (sin(eta)*sin(tau)*cos(chi-gamma))
# d = arccos(d)
# print("ortodromic distance ", d*rtg)

#print("theta value \n", theta , "\n")
#print("PHI value \n", PHI , "\n")
#print("Psi value \n", psi , "\n")
#print("tau value \n", tau , "\n")
#print("gamma value \n", gamma/2 , "\n")
