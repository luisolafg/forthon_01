import numpy as np; import math
import scipy.integrate as sq
import scipy.special as special
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D

def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)

def Rnorma(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))*sin(eta)

def Rvar(psi):
    A = cos(tau)*cos(PHI) + sin(tau)*sin(PHI)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(PHI)/(sin(eta)*sin(tau)) - cteta*cttau
    chi = gamma - arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-PHI)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))

#north pole and desired planes
hkl_np = [0,0,1]
hkl = [1,1,1]
rtg = 180.0/math.pi

#theta = pi/2.0; phi = pi; distwidth = 0.5
#theta = 0.8 ; phi = 0.8; distwidth = 0.5
#theta = 0.0 ; phi = 0.0; distwidth = 0.5
theta = 90 ; phi = 0; distwidth = 0.5
theta = theta/rtg; phi = phi/rtg
print (" Max of IPF at theta, phi (degrees) = \n", theta*rtg, phi*rtg)
print (" Max of IPF at theta, phi (radians) = \n", theta, phi)
print (" IPF width (radians) = \n", distwidth)
#tau = pi/2.0 ; gamma = pi
tau = 45 ; gamma = 0
tau = tau/rtg; gamma = gamma/rtg
#tau = 0.0 ; gamma = 0.0
print (" PF vector at tau, gamma (degrees) = ", tau*rtg, gamma*rtg)
print (" PF vector at tau, gamma (radians) = ", tau, gamma)

R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]

Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
#print " Integral of the normalized IPF = ", Rnorsu
#print " 4*pi = ", 4.0*pi
print
#print " R_normalized(max) =", Rnorma(theta, phi)/sin(theta)

# Calculation of pole figure

PHI = 20.0; psi = 270
PHI = PHI/rtg; psi = psi/rtg

print (" PHI, psi (degrees) = \n", PHI*rtg, psi*rtg)
#print " PHI, psi (radians) = ", PHI, psi

A = cos(tau)*cos(PHI) + sin(tau)*sin(PHI)*cos(psi)
eta = arccos(A)
cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
B = cos(PHI)/(sin(eta)*sin(tau)) - cteta*cttau
chi = gamma - arccos(B)
#d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-PHI)
#rho = arccos(d)

print (" eta, chi (degrees) = \n", eta*rtg, chi*rtg)
#print " eta, chi (radians) = ", eta, chi
print
#print " cteta, cttau, d, rho = ", cteta, cttau, d, rho

#while PHI < 0.6:
#pf = sq.quad(Rvar, 0, 2.0*pi)
#print " PHI and Pole Figure at PHI:", PHI, pf[0]/(2.0*pi)
#PHI = PHI + 0.05

#create 2D arrays of new angles
#eta, chi = np.meshgrid(np.linspace (0, math.pi, 18) , np.linspace(0, 2.0*math.pi,36))

#calculating R
#p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
#p1 = arccos(p)
#R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))

#plotting inverse pole figure
#fig= plt.figure()
#ax = Axes3D(fig)
#ax.plot_surface(eta, chi, R)
#plt.show()

