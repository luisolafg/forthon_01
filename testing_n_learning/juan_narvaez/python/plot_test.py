#test plots
import numpy as np
from scipy.interpolate import UnivariateSpline

#FWHM Gaussian Equation
def gaussian_dist(x, mean, sd):
    return a/(sd*np.sqrt(2.0*np.pi))*np.exp(-(x - mean)**2.0/(2.0*sd**2.0))

if __name__ == "__main__":

    lst = []
    lst.append([752047.6694553357, 2.0 * 21.679133113542612])
    lst.append([351902.24538205995, 2.0 * 25.249194001900577])
    lst.append([200133.4154032288, 2.0 * 37.102393351455085])
    lst.append([237224.29910165028, 2.0 * 45.02077528331917])
    lst.append([71090.86806223867, 2.0 * 47.63071329351087])
    lst.append([189685.47189566024, 2.0 * 58.55178205261272])
    lst.append([60601.67896067204, 2.0 * 68.38145487935331])
    lst.append([293063.08918513544, 2.0 * 72.51740420108986])

    x = np.linspace( 30 , 150 , 452 )
    fwhm = 1
    std = fwhm/2.35

    y1 = []

    for y2 in lst:
        a = y2[0]
        y1.append(gaussian_dist( x, y2[1], std ))


    #plotting of peaks
    import pylab as pl
    pl.plot(x, y1, '-k', label='1,1,1')
    pl.xlabel('2θ')
    pl.ylabel('Intensity')
    pl.show()
