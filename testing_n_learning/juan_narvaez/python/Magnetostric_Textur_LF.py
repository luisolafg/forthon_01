# -*- coding: utf-8 -*-
# Magnetostriccion en policristales cubicos texturados
import numpy as np
from numpy import sin,cos,pi    # makes the code more readable
from mayavi import mlab
import scipy.integrate as sq

def R1(azimut, polar):
    return np.exp(-(polar/omr)**2)*sin(polar)
    
def R(azimut, polar):
    return (4.0*pi/R0)*np.exp(-(polar/omr)**2)*sin(polar)

def Rnorm(azimut, polar):
    return (4.0*pi/R0)*np.exp(-(polar/omr)**2)
    
def R4(azimut, polar):
    R41=(4.0*pi/R0)*np.exp(-(polar/omr)**2)*sin(polar)
    k41=0.2423851-2.423851*cos(polar)**2+2.827827*cos(polar)**4+0.33799*sin(polar)**4*cos(4.0*azimut)
    return R41*k41

# INITIAL DATA
n4=-0.646360; FWHM=35; omgr=0.6*FWHM; omr= omgr*pi/180.0
#Co0.8 Fe2.2 O4
L100 = -590; L111=120
#Terfenol-D
#L100=90.0; L111=1640.0
# Cristina's sample
#L100=318*2/3; L111=-20*2/3
# galfenol 15
#L100=173; L111=-15
#L100=173; L111=-35

phi, beta = np.mgrid[0:pi:180j,0:2*pi:360j]

# SINGLE CRYSTAL

# Armonicos esfericos para simetria cubica
h1 = sin(phi)*cos(beta)
h2 = sin(phi)*sin(beta)
h3 = cos(phi)
Fi4 = h1*h1*h2*h2+h2*h2*h3*h3+h1*h1*h3*h3
k41=n4*(5.0*Fi4-1.0)      # Armónico esférico simetrizado cúbico  

# Coeficientes del desarrollo de la magnetostricción
em01=2.0*L100/5.0+3.0*L111/5.0; ep01 = em01
em41=3.0*(L111-L100)/(5.0*n4)     #monocristal
print "em01,em41 =", em01, em41 

# Magnetostriccion del monocristal
E = L100+3.0*(L111-L100)*Fi4    # Cálculo por Cosenos Directores
#E= em01+em41*k41               # Cálculo a la manera de Bunge

# Representacion grafica
x1 = E*sin(phi)*cos(beta)
y1 = E*sin(phi)*sin(beta)
z1 = E*cos(phi)
mlab.figure()
mlab.mesh(x1,y1,z1, scalars=E)
mlab.scalarbar(orientation="vertical")
mlab.show()

# POLICRISTAL

# Figura inversa de polos. Orientacion Preferida = 0,0,1

RR = sq.dblquad(R1,0,pi/2.0,lambda polar: 0, lambda polar: 2.0*pi)
R0=6.0*RR[0]
print "FWHM, Integral de R1 =", FWHM, R0

# Figura inversa de polos normalizada
RR = sq.dblquad(R,0,pi/2.0,lambda polar: 0, lambda polar: 2.0*pi)
RR0 = 6.0*RR[0]
print "Integral de R, 4*pi, Rnorm(0,0) =", RR0, 4.0*pi, Rnorm(0,0)

# Coeficiente de textura C41:

C411 = sq.dblquad(R4,0,pi/2.0,lambda polar: 0, lambda polar: 2.0*pi)
C41 = 6.0*C411[0]
print "C41 = ", C41

# Coeficientes del desarrollo de la magnetostriccion - policristles
ep41=(1.0/(4.0*pi))*np.sqrt(2.0/9.0)*em41*C41
#ep41=0
print "ep01, ep41 = ", ep01, ep41

# Polinomios de Legendre:
P4= (3.0*np.sqrt(2.0)/16.0)*(35.0*cos(phi)**4-30.0*cos(phi)**2+3.0)

# Magnetostriccion del policristal
E = ep01 + ep41*P4

# Representacion grafica
x1 = E*sin(phi)*cos(beta)
y1 = E*sin(phi)*sin(beta)
z1 = E*cos(phi)
mlab.figure()
mlab.mesh(x1,y1,z1, scalars=E)
mlab.scalarbar(orientation="vertical")
mlab.show()
