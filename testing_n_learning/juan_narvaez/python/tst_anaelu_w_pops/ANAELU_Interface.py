import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *

class PopScene(QGraphicsScene):
    signalMousePos = Signal(int)
    def __init__ (self, parent=None):
        super(PopScene, self).__init__ (parent)

    def mousePressEvent(self, event):
<<<<<<< HEAD
        ### ScenePos() argument ???
        pos = QGraphicsSceneMouseEvent.scenePos(event)
        self.signalMousePos.emit(pos)
        print ("pressed here: " + str(pos.x()) + ", " + str(pos.y()))
        self.update()


    def mouseMoveEvent(self, event):
        self.pos = QGraphicsSceneMouseEvent.scenePos(event)
        self.signalMousePos.emit(self.pos)
        print ("moved here: " + str(self.pos.x()) + ", " + str(self.pos.y()))
        self.update()

    def mouseReleaseEvent(self, event):
        pos_ition = QGraphicsSceneMouseEvent.scenePos(event)
        self.signalMousePos.emit(pos_ition)
        print ("released here: " + str(self.pos.x()) + ", " + str(self.pos.y()))
        self.update()
=======
        print("\n mousePressEvent")
        q_point = event.scenePos()
        self.ini_pos = q_point.x(), q_point.y()
        print("q_point =", self.ini_pos)

    def mouseMoveEvent(self, event):
        print("\n mouseMoveEvent")
        q_point = event.scenePos()
        self.mov_pos = q_point.x(), q_point.y()
        print("q_point =", self.mov_pos)

        ''''Line''''
        #new_line = QLine(self.ini_pos[0], self.ini_pos[1], self.mov_pos[0], self.mov_pos[1])
        #rect_item = self.addLine(new_line)
        #self.old_line = rect_item
        #self.removeItem(self.old_line)

        ''''Rectangle''''
        self.new_rect = QRect(self.ini_pos[0], self.ini_pos[1], self.mov_pos[0]-self.ini_pos[0], self.mov_pos[1]-self.ini_pos[1])
        self.rect_item = self.addRect(self.new_rect)
        #self.rect_item.update()
        #self.removeItem(self.rect_item)
        


        '''
        try:
            self.removeItem(self.old_line)

        except AttributeError:
            print("first line")
        '''

        # new_line = QLineF(self.ini_pos[0], self.ini_pos[1], self.mov_pos[0], self.mov_pos[1])
        # self.addLine(new_line)
        # self.old_line = new_line


    def mouseReleaseEvent(self, event):
        print("\n mouseReleaseEvent")
        q_point = event.scenePos()
        self.rel_pos = q_point.x(), q_point.y()
        print("q_point =", self.rel_pos)

<<<<<<< HEAD
        #### Line
        # new_line = QLine(self.ini_pos[0], self.ini_pos[1], self.rel_pos[0], self.rel_pos[1])
        # rect_item = self.addLine(new_line)

        #### rectangle
        new_rect_release = QRect(self.ini_pos[0], self.ini_pos[1], self.rel_pos[0]-self.ini_pos[0], self.rel_pos[1]-self.ini_pos[1])
        rect_item_release = self.addRect(new_rect_release)

        #### Ellipse
        #new_ellipse = self.addEllipse(self.ini_pos[0], self.ini_pos[1], self.rel_pos[0], self.rel_pos[1])
=======
>>>>>>> 5e3d85cd5659b9bc23043e050e986783800bce0e
>>>>>>> c39729d8ceb2d6ba686e76dc1f3ee695ea94385d


class MyPopupDialog(QObject):
    def __init__(self, parent=None):
        super(MyPopupDialog, self).__init__(parent)
        self.pop_dia = QtUiTools.QUiLoader().load("pop_dialog.ui")
        self.pop_dia.show()

        self.img_scene = PopScene()
        self.pop_dia.InerGraphicsView.setScene(self.img_scene)
        self.pop_dia.InerGraphicsView.setDragMode(QGraphicsView.NoDrag)

        fileName = "../../lena_inverted.jpeg"
        image1 = QImage(fileName)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.img_scene.addPixmap(self.pixmap_1)


class Form(QObject):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("ANAELU_interface.ui")

        image = "../../lena.jpeg" #path to your image file
        self.exp_scene = QGraphicsScene()
        self.window.graphicsView_Exp.setScene(self.exp_scene)
        self.window.graphicsView_Exp.setDragMode(QGraphicsView.ScrollHandDrag)

        self.mask_n_diff_scene = QGraphicsScene()
        self.window.graphicsView_Mask_n_Diff.setScene(self.mask_n_diff_scene)
        self.window.graphicsView_Mask_n_Diff.setDragMode(QGraphicsView.ScrollHandDrag)

        self.window.pushButton_load_exp.clicked.connect(self.set_img1)
        self.window.pushButton_mask_model.clicked.connect(self.set_img2)
        self.window.pop_mark_tool.clicked.connect(self.pop_mask)
        self.window.show()

    def pop_mask(self):
        print("pop_mask ...")
        self.dialog = MyPopupDialog(self)

    def set_img1(self):
        fileName = "../../lena.jpeg"
        image1 = QImage(fileName)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.exp_scene.addPixmap(self.pixmap_1)

    def set_img2(self):
        fileName = "../../lena_gray.jpeg"
        image2 = QImage(fileName)
        self.pixmap_2 = QPixmap.fromImage(image2)
        self.mask_n_diff_scene.addPixmap(self.pixmap_2)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
