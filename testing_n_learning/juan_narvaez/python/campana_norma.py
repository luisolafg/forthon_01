# Campana y su integral
import numpy as np
from numpy import pi, mgrid, sin,cos, arccos, exp
import scipy.integrate as sq

def R0(chi, eta):
    return sin(eta)

def R1(chi, eta):
    d = cos(eta)*cos(pi/4.0)+sin(eta)*sin(pi/4.0)*cos(chi-pi/4.0)
    dr = arccos(d)
    return exp(-dr**2)*sin(eta)

def R(chi, eta):
    d = cos(eta)*cos(pi/4.0)+sin(eta)*sin(pi/4.0)*cos(chi-pi/4.0)
    dr = arccos(d)
    return (4.0*pi/R11)* exp(-dr**2)*sin(eta)


# eta, chi = mgrid[0:np.pi:18j,0:2*np.pi:36j]
# Generation of Gaussian (not normalized) inverse pole figure (IPF):
# Maximum of IPF at (pi/4, pi/4), width of IPF = 1.

R0suma = sq.dblquad(R0,0,pi,lambda eta:0, lambda eta: 2*pi)
R01 = R0suma[0]
print ("Integral de R0, 4*pi = \n ", R01, 4.0*pi)

R1suma = sq.dblquad(R1,0,pi,lambda eta:0, lambda eta: 2*pi)
R11 = R1suma[0]
print ("Integral de R1, 4*pi = \n", R11, 4.0*pi)

Rsuma = sq.dblquad(R,0,pi,lambda eta:0, lambda eta: 2*pi)
RR = Rsuma[0]
print ("Integral de  R, 4*pi = \n", RR, 4.0*pi)

