#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

from setuptools import setup
import os

setup(
    name="pckg_dir",
    description="ANAELU TEST",
    author="Juan Narvaez",
    author_email="juan.narvaez@cimav.edu.mx",
    url="https://gitlab.com/luisolafg/anaelu3",
    platforms="GNU/Linux & Windows",
    packages=["pckg_dir", "pckg_dir.cli"],
    package_dir={"": "anaelu"},
    data_files=[
        (
            "pckg_dir/img",
            [
                "pckg_dir/img/img_dummy1.png",
                "pckg_dir/img/img_dummy2.png",
            ],
        )
    ],
    include_package_data=True,
    entry_points={"console_scripts": ["pckg_dir=pckg_dir.main_anaelu:main"]},
)
