print("Modeling pole figure (PF) from given inverse pole figure (IPF)")
import numpy as np ; import math
import scipy.integrate as sq
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D
from product_vectors_abel import Ortorrombic
from product_vectors import Trigonal
from Pandas import Arrays

#pzt
a = 3.98 ; alpha = 90
b = 3.98 ; beta = 90
c = 4.04 ; gamma = 90

#calling function
if a == b and b == c and c == a and alpha == beta and beta ==gamma and gamma == alpha \
or a == b and b < c and c > a and alpha == beta and beta ==gamma and gamma == alpha:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = \
        Ortorrombic(a,b,c,alpha,beta,gamma)
if a == b and b < c and c > a and alpha == beta and beta < gamma and gamma > alpha \
or b > c and c > a and alpha == gamma and beta > alpha and beta > gamma:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = \
        Trigonal(a,b,c,alpha,beta,gamma)

# Defining parameters:
rtg = 180.0 / pi ; widthdeg = 30.0
#thetadeg = 90.0; phideg = 0.00; 
#taudeg = 90.0 ; gammadeg = 0.0
if thetadeg == 0.0: thetadeg = 0.01
if taudeg == 0.0: taudeg = 0.01
PFtau=np.zeros(12); PFgamma=np.zeros(12)
for Npeak in range (0, 12):
    PFtau[Npeak] = taudeg[Npeak] * pi / 180.0
    PFgamma[Npeak] = gammadeg[Npeak] * pi / 180.0
    nombre = "PoleFig" + str(Npeak) + ".dat"
    print(nombre)
theta = thetadeg/rtg; phi = phideg/rtg; distwidth = widthdeg/rtg
#tau = [x/rtg for x in taudeg] ; gamma = [x/rtg for x in gammadeg] 
print (" Max of IPF at theta, phi (degrees) = ", thetadeg, phideg)
#print (" Max of IPF at theta, phi (radians) = ", theta, phi)
print (" IPF width (degrees and radians) = ", widthdeg, distwidth)
print (" Pole figure tau, gamma (degrees) = ", taudeg, gammadeg)
#print (" Pole figure tau, gamma (radians) = ", tau, gamma)

# Defining functions:
def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    if d >= 1.0: d = 1.0
    elif -1.0 < d < 1.0 : d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    elif d <= -1.0 : d = -1.0
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnorma(eta, chi):
    return (4.0*pi/R0)*R1(eta, chi)
def Rvar(psi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if np.all(B >= 1.0): B = 1.0
    elif np.all(-1.0 < B < 1.0) : B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    elif np.all(B <= -1.0) : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))

def pfg(fi):
    pfgr = sq.quad(Rvar, 0, 2.0*pi)
    return pfgr[0]

# Calculating R surface integral:
R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization:
Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)

# Maximum of normalized IPF, R:
print (" R_normalized(max) =", Rnorma(theta, phi)/sin(theta))
print ()

thetan =[0.9703849663517776, 0.9703849663517776, 0.9703849663517776, 0.9703849663517776, 2.1712076872380157, 2.1712076872380157, 2.1712076872380157, 2.1712076872380157]
phin = [0.7853981633974483, -0.7853981633974483, 2.356194490192345, -2.356194490192345, 0.7853981633974483, -0.7853981633974483, 2.356194490192345, -2.356194490192345]
Rsum = 0.0
for Nchip in range(0,8):
    thetph = thetan[Nchip]
    phiph = phin[Nchip]
    eta, chi = np.meshgrid(np.linspace (0, pi, 36) , np.linspace(0, 2.0*pi,72))
    p = cos(eta)*cos(thetph)+sin(eta)*sin(thetph)*cos(chi-phiph)
    p1 = arccos(p)
    R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))
    Rsum = Rsum + R
    

    #plotting inverse pole figure Rsum = 0
fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface(eta*rtg, chi*rtg, Rsum, cmap='viridis', linewidths=0.2)
ax.set_xlim(0, 180); ax.set_ylim(0, 360); ax.set_zlim(0, np.max(Rsum))
plt.show()

# # Calculating the inverse pole figure surface:
# eta, chi = np.meshgrid(np.linspace (0, pi, 36) , np.linspace(0, 2.0*pi,72))
# #calculating R
# p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
# p1 = arccos(p)
# R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))
# #plotting inverse pole figure
# fig= plt.figure()
# ax = Axes3D(fig)
# ax.plot_surface(eta*rtg, chi*rtg, R, cmap='viridis', linewidths=0.2)
# ax.set_xlim(0, 360); ax.set_ylim(0, 360); ax.set_zlim(0, 11)
# plt.show()

for Npeak in range(0, 12):
    file = open("polefig" + str(Npeak) + ".dat", "w")
    tau = PFtau[Npeak]; gamma = PFgamma[Npeak]
    fidg = np.zeros(180); pfg = np.zeros(180)
    fideg = 0.0
    for fideg in range(0, 181, 5):
        n = int(fideg/5)
        fidg[n] = fideg
        fi = fideg/rtg
        pf = sq.quad(Rvar, 0, 2.0*pi)
        pfg[n] = pf[0]/(2.0*pi)
        print (" n, fidg[n], Pole Figure[n] = ", n, fidg[n], pfg[n])
        file.write(str(fidg[n])+" "+ str(pfg[n])+"\n")
    plt.plot(fidg, pfg)
    plt.show()
    file.close()


plt.plot(fidg, pfg)
print("fidg=" , fidg,"\n")
#plt.show()

#### pf for difractograms
def Rvar(psi,tau,gamma,fi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if np.all(B >= 1.0): B = 1.0
    elif np.all(-1.0 < B < 1.0) : B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    elif np.all(B <= -1.0) : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))
#######################################################
intensity,fideg,hkl = Arrays()
########################################################
fidiv = [x/2 for x in fideg]
fi = [x/rtg for x in fidiv]

print ("fi values=" , fi, "\n")
print("fi div by 2 on degrees =" , fidiv, "\n")
print("Bragg angles on radians =" , fi, "\n")


tau = [x/rtg for x in taudeg] ; gamma = [x/rtg for x in gammadeg]
delta = [x/rtg for x in deltadeg]; iota =  [x/rtg for x in iotadeg]
delta = delta[0] ; iota = float(0)
print("delta, iota= ", delta, iota , "\n")

dBb = []
for i,j in zip(tau,gamma):
    dBb.append(cos(i)*cos(theta)+sin(i)*sin(theta)*cos(j-phi))
print (" distance bragbrentano = ", dBb, "\n")

for i in dBb: rhoBb = arccos(dBb)      
print (" Rho brag brentano = ", rhoBb, "\n")

d1Bb = []
for i,j in zip(tau,gamma):
    d1Bb.append(cos(i)*cos(delta)+sin(i)*sin(delta)*cos(j-iota))
print (" distance bragbrentano2 = ", d1Bb, "\n")

for i in d1Bb : rho1Bb = arccos(d1Bb)
print (" Rho bragg brentano2 = ", rho1Bb, "\n")

for i,j in zip(rhoBb,rho1Bb):
    Rhkl = (exp(-0.693*((rhoBb/distwidth)**2)) + exp(-0.693*((rho1Bb/distwidth)**2))) 
print (" Rhkl brag brentano = ", Rhkl, "\n")

#Integral of pf
pf = []
for i,j,k in zip(tau,gamma,fi):
    pfvalue, error = sq.quad(Rvar, 0, 2.0*pi, args=(i,j,k))
    pf.append(pfvalue)
pf = np.array(pf)
pf1 = [x/(2.0*pi) for x in pf]
print("pf value =", pf1, "\n")

####
Bragg_int = [x*y for x,y in zip(Rhkl,intensity)]
print ("intensity bragg brentano = ", Bragg_int, "\n")
Bragg_dif = [list(x) for x in zip(Bragg_int, fideg)]
print ("nested list bragg brentano = ", Bragg_dif, "\n") 
####
text_int = [x*y for x,y in zip(pf1,intensity)]
print ("new integral intensity = ", text_int, "\n")
Difract_nl = [list(x) for x in zip(text_int , fideg)]
print("new nested list for difractogram" , Difract_nl , "\n")
print("hkl nest list", hkl,"\n")

#FWHM Gaussian Equation
def gaussian_dist(x, a , mean, sd):
    return a/(sd*np.sqrt(2.0*np.pi))*np.exp(-(x - mean)**2.0/(2.0*sd**2.0))

if __name__ == "__main__":

    lst = Difract_nl

    x = np.linspace(5,80,10000)
    y1 = np.zeros((10000))

    fwhm = 0.1
    std = fwhm/2.35

    for i,xrdgeom in enumerate(x):
        for j in lst:
            y1[i] += gaussian_dist(xrdgeom , j[0] , j[1] , std)

    import pylab as pl
    pl.figure(1)
    pl.plot(x, y1, '-k', label='1,1,1')
    pl.xlabel('2θ(degrees)')
    pl.ylabel('Intensity(a.u)')
    pl.title('BaTiO3 Grazing incidence')
    

    lst1 = Bragg_dif

    x = np.linspace(5,80,10000)
    y1 = np.zeros((10000))

    fwhm = 0.1
    std = fwhm/2.35

    for i,xrdgeom in enumerate(x):
        for j in lst1:
            y1[i] += gaussian_dist(xrdgeom , j[0] , j[1] , std)

    pl.figure(2)
    pl.plot(x, y1, '-k', label='0,0,1')
    pl.xlabel('2θ(degrees)')
    pl.ylabel('Intensity(a.u)')
    pl.title('BaTiO3 Bragg-Brentano')
    pl.show()

