#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# test Juan

from setuptools import setup, find_packages
import os

setup(
    name="ANAELU",
    version = "3.0.0",
    description="ANAlytical Emulator Laue Utility",
    author="Luis Fuentes-Montero (Luiso) Et al",
    author_email="luicub@gmail.com",
    url="https://gitlab.com/luisolafg/anaelu3",
    platforms="GNU/Linux & Windows",
    packages=["ANAELU"],
    package_dir={"": "command_center",},
    package_data={"": ["ANAELU_interface.ui"]},
    install_requires=["PySide2 == 5.14.1" , "fabio == 0.9.0"],
    test_suite="ANAELU_interface",
    data_files=[
        (
            "ANAELU/resources",
            [
                "command_center/ANAELU/imgdata/anaelu.png",
                "command_center/ANAELU/imgdata/lena.jpeg",
                "command_center/ANAELU/imgdata/lena_gray.jpeg",
                "command_center/ANAELU/imgdata/lena_gray_inverted.jpeg",
                "command_center/ANAELU/imgdata/lena_inverted.jpeg",
                "command_center/ANAELU/imgdata/a.exe",
            ],
        )
    ],
    classifiers = [
            "Intended Audience :: Science/Research",
            "Development Status :: 3 - Alpha",
            "Operating System :: Microsoft :: Windows :: Windows 10 : LINUX",
        ],
    include_package_data=True,
    entry_points={"console_scripts": ["ANAELU=ANAELU.main_ANAELU:main"]},
)

