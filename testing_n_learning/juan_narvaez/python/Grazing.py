import numpy as np ; import math
import scipy.integrate as sq
import scipy.special as special
from numpy import sin,cos,pi,abs, arccos, exp, tan
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from product_vectors_abel import Ortorrombic
from product_vectors import Trigonal
from Pandas import Arrays

#cell parameter
#quartz
# a = 4.91; alpha = 90
# b = 4.91; beta = 90
# c = 5.40; gamma = 120

#pzt
a = 3.98 ; alpha = 90
b = 3.98 ; beta = 90
c = 4.04 ; gamma = 90


#calling function
if a == b and b == c and c == a and alpha == beta and beta ==gamma and gamma == alpha \
or a == b and b < c and c > a and alpha == beta and beta ==gamma and gamma == alpha:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = \
        Ortorrombic(a,b,c,alpha,beta,gamma)
if a == b and b < c and c > a and alpha == beta and beta < gamma and gamma > alpha \
or b > c and c > a and alpha == gamma and beta > alpha and beta > gamma:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = \
        Trigonal(a,b,c,alpha,beta,gamma)
 

#original routine IPF,PF
rtg = 180.0/math.pi ; widthdeg =  10
theta = thetadeg/rtg; phi = phideg/rtg; distwidth = widthdeg/rtg
delta = [x/rtg for x in deltadeg]; iota =  [x/rtg for x in iotadeg] #float(0)
delta = delta[0] ; iota = float(0)
tau = [x/rtg for x in taudeg] ; gamma = [x/rtg for x in gammadeg]  

print (" Max of IPF at tau(rads) = ", tau, "\n")
print (" Max of IPF at gamma(rads) = ",  gamma,"\n")
print (" Max of IPF at theta(rads) = ", theta,"\n")
print (" Max of IPF at  phi (rads) = ", phi,"\n")
print (" Max of IPF at delta(rads) = ", delta,"\n")
print (" Max of IPF at  iota (rads) = ", iota,"\n")
print (" IPF width (degrees and radians) = ", widthdeg, distwidth, "\n")

# Defining functions:
def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnorma(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rcrys(eta, chi,delta,iota):
    d = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnew(eta,chi): 
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    d1 = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho1 = arccos(d1)
    return (exp(-0.693*((rho/distwidth)**2)) + exp(-0.693*((rho1/distwidth)**2)))*sin(eta)
def Rnewnorma(eta,chi): 
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    d1 = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho1 = arccos(d1)
    return factor*(exp(-0.693*((rho/distwidth)**2)) + exp(-0.693*((rho1/distwidth)**2)))*sin(eta)
def Rvar(psi,tau,gamma,fi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if np.any(B > 1.0): B = 1.0
    if np.any(B < -1.0) : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    d1 = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho1 = arccos(d1)
    return factor*(exp(-0.693*((rho/distwidth)**2)) + exp(-0.693*((rho1/distwidth)**2)))*sin(eta)

  

# Calculating R surface integral:
R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization:
Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)

# Maximum of normalized IPF, R:
print
if theta != 0.0:
    print (" R_normalized(max) =", Rnorma(theta, phi)/sin(theta))
else:
    print (" R_normalized(max) =", 4.0*pi/R0, "\n")
print

# Arrays obtained from Pandas.py
intensity,fideg,hkl = Arrays()
fidiv = [x/2 for x in fideg]
fi = [x/rtg for x in fidiv]

print ("fi values=" , fi, "\n")
print("fi div by 2 on degrees =" , fidiv, "\n")
print("Bragg angles on radians =" , fi, "\n")

# Integral of IPF and normalized IPF
ipfnewval, error = sq.dblquad(Rnew, 0, 2.0*pi, 0, pi)
print("new sum RC integral sum", ipfnewval)
factor = 4*pi/ipfnewval
print("4pi / integral", factor,"\n")

ipfnewvalnorma, error = sq.dblquad(Rnewnorma, 0, 2.0*pi, 0, pi)
print("ipfnewvalnorma=", ipfnewvalnorma)

#Integral of pf bragg angles.
pf = []
for i,j,k in zip(tau,gamma,fi):
    pfvalue, error = sq.quad(Rvar, 0, 2.0*pi, args=(i,j,k))
    pf.append(pfvalue)
pf = np.array(pf)
pf1 = [x/(2.0*pi) for x in pf]
print("pf value =", pf1, "\n")
#################
####################
def Rvar2(epsilon,psi,tau,gamma,fi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if np.any(B > 1.0): B = 1.0
    if np.any(B < -1.0) : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    d1 = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho1 = arccos(d1)
    return factor*(exp(-0.693*((rho/distwidth)**2)) + exp(-0.693*((rho1/distwidth)**2)))*sin(eta)
####################

#Integral needed for onedimensional PF
psi = np.mgrid[0:180:19j]
psi = [x/rtg for x in psi]
fi= np.mgrid[0:90:19j]
fi = [x/rtg for x in fi]
tau = tau[0]
tau = np.tile(tau,19)
gamma = gamma[0]
gamma = np.tile(gamma,19)
print("new fi values for pf figures",fi, "\n")
print("new psi values for pf figures",psi, "\n")
print("new tau values for pf figures",tau, "\n")
print("new gammas values for pf figures",gamma, "\n")

print ("rvalues", Rvar(0,0,0,0), "\n")
onedim_pf = []
for i,j,k,l in zip(psi,tau,gamma,fi):
    oned_pf, error = sq.quad(Rvar2,0, 2.0*pi, args=(i,j,k,l))
    print("i,j,k,integrando values=", i,j,k,oned_pf,"\n")
    
    #print("j values=", j,"\n")
    #print("k values=", k,"\n")
    onedim_pf.append(oned_pf)
onedim_pf = np.array(onedim_pf)
onedim_pf1 = [x/(2.0*pi) for x in onedim_pf]
onedim_pf1 = [(0.0) if math.isnan(i) else i for i in onedim_pf1]
print("pf value for graphs =", onedim_pf1, "\n")

plt.plot(fi,onedim_pf1)
plt.show()

#tryintegral
tau = 0
gamma = 0
fi = 5
test_pf, error = sq.quad(Rvar,0, 2.0*pi,args=(0.0001,0,(5*pi)/180))
test_pf = test_pf/(2.0*pi)
print("testpf=", test_pf, "\n")



#fi= np.linspace(0,90,91)


#Writing files of IPF
# file = open("polefigure001.txt", "w")
# for index in range(len(fi)):
#     file.write(str(fi[index]) + " " + str(onedim_pf1[index]) + "\n")
# file.close()


#hkl_label = ["001","010","011","110","111","002","020","012","021","120",\
#            "112","121","022","220","003","122","221","030","013","031","130","113","131"]

# Calculating and plotting the inverse pole figure surface:
eta, chi = np.meshgrid(np.linspace (0, math.pi, 36) , np.linspace(0, 2.0*math.pi,72))
#calculating R
pA = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
p1 = arccos(pA)
RA = (factor)*exp(-0.693*((p1/distwidth)**2))

pB = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
p2 = arccos(pB)
RB = (factor)*exp(-0.693*((p2/distwidth)**2))

RC = RA + RB
#print ("Ra =", RA,"\n")
#print ("Rb =", RB,"\n")
#print ("Rc =", RC,"\n")



delta = [x/rtg for x in deltadeg]; iota = [x/rtg for x in iotadeg]
ipf_sum = []
for i,j in zip(delta,iota):
    ipfval, error = sq.dblquad(Rcrys, 0, 2.0*pi, 0, pi, args=(i,j))
    ipf_sum.append(ipfval)
#print("ipf integral sum", ipfval)


#plotting inverse pole figure
fig= plt.figure(2)
ax = Axes3D(fig,azim=113, elev=30)

ax.plot_surface(eta*rtg, chi*rtg, RC)
#print ("value axis x", eta*rtg)
#print ("value axis y", chi*rtg)
#print ("value axis z", RC)
plt.show()
