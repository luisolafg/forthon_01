#Second rank tensors
import numpy as np
from numpy import sin,cos,pi,abs # makes the code more readable
#from mayavi import mlab

x, y = np.mgrid[0:np.pi:5j,0:2*np.pi:9j]
print (" x:", x, "\n")
print (" y:", y, "\n")
d2 = (x-pi/4)**2 + (y-pi/4)**2
print ("d2:" , d2 , "\n")
z = np.exp(-d2)
print (" z:", z, "\n")

#mlab.figure()
#mlab.mesh(x,y,z)
#mlab.scalarbar(orientation="vertical")
#mlab.show()
