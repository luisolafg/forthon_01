import numpy as np 
import math
import scipy.integrate as integrate
import scipy.special as special
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos
from mpl_toolkits.mplot3d import Axes3D

#north pole and desired planes
hkl_np = [0,0,1]
hkl = [1,1,1]
rtg = 180/math.pi

#theta_angles = []
alpha_ang =(math.acos( (hkl_np[0]*hkl[0]) + (hkl_np[1]*hkl[1]) + (hkl_np[2]*hkl[2]) \
          /(math.sqrt((hkl_np[0]**2 + hkl_np[1]**2 + hkl_np[2]**2)*(hkl[0]**2 + hkl[1]**2 + hkl[2]**2))))*rtg)
print("alpha_angles:\n" , alpha_ang , "\n" )

alpha_angle = pi/4

#theta angles
theta = (math.acos(1/math.sqrt(3)))
print("alpha angle:\n" , theta , "\n")

#create 2D arrays of new angles
chi , eta = np.meshgrid( np.linspace (0, 2*math.pi, 9) , np.linspace(0 , math.pi , 5))
print("chi :\n", chi, "\n")
print("eta:\n", eta, "\n")

#calculating Ro
p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-pi/4.0)
p1 = arccos(p)
print ("Ro:\n", p , "\n")
print ("Rop1:\n", p1 , "\n")


#R value, inverse pole figure
Ro = 1
distwidth = 20

R = Ro*np.exp(-0.693*((p1/distwidth)**2))
print("R inverse pole figure:\n" , R , "\n")

#plotting figure
fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface( eta , chi , R , rstride=1, cstride=1)
plt.show()


#integral, inverse pole figure 0 - 2pi
# def R1(eta,chi):
#     return np.exp(-0.693*((p1/distwidth)**2))*sin(eta)*eta*chi
# r2 = integrate.dblquad(R1,0,pi/2.0,lambda polar: 0, lambda polar: 2.0*pi)
# print ("R2 integral:\n",r2 , "\n")


# #whole integral value
# r3 = r2[0]*R
# R0 = (4*math.pi)/(r3)

# fourpi = R0*r3

# print ("R3 value to get Ro:\n ",r3 , "\n")
# print (" Ro value:\n ",R0 , "\n")
# print (" if 4pi, correct:\n ", fourpi , "\n")




