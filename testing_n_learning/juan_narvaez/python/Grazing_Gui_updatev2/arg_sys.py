import sys
 
# total arguments used 
n = len(sys.argv)
print("Total arguments passed:", n)
 
# Arguments for the script
print("\nName of Python script:", sys.argv[0])

# How to use arguments
a = sys.argv[1]
b = sys.argv[2]
c = sys.argv[3]
d = sys.argv[4]

y = int(a) + int(b) + int(c) +int(d) 
print("\noutput",y)