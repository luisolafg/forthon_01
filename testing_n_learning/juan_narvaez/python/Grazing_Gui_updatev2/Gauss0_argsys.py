import numpy as np ; import math
import scipy.integrate as sq
from scipy import special
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
from matplotlib import cm, colors
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D
from ortorrombic_routine_grazing import Ortorrombic
from Trigonal_routine_grazing import Trigonal
from Pandas import Arrays
import sys

n = len(sys.argv)
print("Total arguments passed:", n)

#checking program
print("\nName of Python script:", sys.argv[0])

#Arg_sys_list 9 
a,b,c,alpha,gamma,beta,widthdeg,FWHMdeg,fwhm2 = sys.argv[1:]

#update
list1 = []
for i in [a,b,c,alpha,gamma,beta,widthdeg,FWHMdeg,fwhm2]:
    list1.append(i)
list1 = [float(s) for s in list1]
a,b,c,alpha,gamma,beta,widthdeg,FWHMdeg,fwhm2 = list1

#params
#a = 3.9430 ; alpha = 90.0
#b =  3.9430 ; beta = 90.0
#c = 3.9300 ; gamma = 90.0
#widthdeg = 30.0 ; FWHMdeg = 15.0 

#calling function
if a == b and b == c and c == a and alpha == beta and beta ==gamma and gamma == alpha \
or a == b and b < c and c > a and alpha == beta and beta ==gamma and gamma == alpha:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,taudeg,gammadeg,theta_list,thetadeg_list, \
    phi_list,phideg_list = Ortorrombic(a,b,c,alpha,beta,gamma)
elif a == b and b < c and c > a and alpha == beta and beta < gamma and gamma > alpha \
or b > c and c > a and alpha == gamma and beta > alpha and beta > gamma:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,taudeg,gammadeg,theta_list,thetadeg_list, \
    phi_list,phideg_list = Trigonal(a,b,c,alpha,beta,gamma)
else:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,taudeg,gammadeg,theta_list,thetadeg_list, \
    phi_list,phideg_list = Ortorrombic(a,b,c,alpha,beta,gamma)

'''Defining Gaussian distribution parameters:'''
grad_rad = pi / 180.0 ; rtg = 180.0 / pi

FWHM = FWHMdeg * grad_rad
print (" IPF FWHM (degrees and radians) = ", FWHMdeg, "{:.4f}".format(FWHM))
Q = 0.7   # Radius of the reference sphere
distwidth = widthdeg/rtg

'''functions'''
def R1(eta, chi):
    RR = 0.0
    for test in range(0,len(theta_list)):
        theta = theta_list[test]
        phi = phi_list[test]
        d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
        if d >= 1.0: d = 1.0
        elif -1.0 < d < 1.0 : d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
        elif d <= -1.0 : d = -1.0
        rho = arccos(d)
        RR = RR + exp(-2.7725887*(rho/FWHM)**2)*sin(eta) 
    return RR
def Rnorma(eta, chi):
    return (4.0*pi/R0)*R1(eta, chi)
def Rvar(psi):
    RRR = 0.0
    for test in range(0,len(theta_list)):
        theta = theta_list[test]
        phi = phi_list[test]
        A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
        eta = arccos(A)
        cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
        B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
        if np.all(B >= 1.0): B = 1.0
        elif np.all(-1.0 < B < 1.0) : B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
        elif np.all(B <= -1.0) : B = -1.0
        if psi <= pi:
            chi = gamma - arccos(B)
        else:
            chi = gamma + arccos(B)
        d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
        rho = arccos(d)
        RRR = RRR + (4.0*pi/R0)*exp(-2.7725887*(rho/FWHM)**2)
    return RRR

def pfg(fi):
    pfgr = sq.quad(Rvar, 0, 2.0*pi)
    return pfgr[0]


# Calculating R surface integral:
R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization:
Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)

print (" R_normalized(max) =", Rnorma(theta_list[0], phi_list[0])/sin(theta_list[0]))

'''Inverse Pole Figure'''
eta, chi = np.mgrid[0:pi:51j, 0:2*pi:101j]
Rsum = 0.0
Rsum2 = 0.0
for Nchip in range(0,len(theta_list)):
    theta = theta_list[Nchip]
    phi = phi_list[Nchip]
    p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    p1 = arccos(p)
    R = exp(-2.7725887*(p1/FWHM)**2) 
    R1 = R + Q
    Rsum = Rsum + R1
    Rsum2 = Rsum2 + R
    print("Rsum=", Rsum)
    print("Rsum2", Rsum2)
    print("maxvalrsum=",np.max(Rsum))
    print("maxvalrsum2=",np.max(Rsum2))
    x = Rsum * sin(eta) * cos(chi)
    y = Rsum * sin(eta) * sin(chi)
    z = Rsum * cos(eta)

###



### draw pf
PFtau=np.zeros(4); PFgamma=np.zeros(4)
for Npeak in range (0, 4):
    PFtau[Npeak] = taudeg[Npeak] * pi / 180.0
    PFgamma[Npeak] = gammadeg[Npeak] * pi / 180.0
    nombre = "PoleFig" + str(Npeak) + ".dat"
    print(nombre)

for Npeak in range(0, 4):
    file = open("polefig" + str(Npeak) + ".dat", "w")
    tau = PFtau[Npeak]; gamma = PFgamma[Npeak]
    fidg = np.zeros(180); pfg = np.zeros(180)
    fideg = 0.0
    for fideg in range(0, 181, 5):
        n = int(fideg/5)
        fidg[n] = fideg
        fi = fideg/rtg
        pf = sq.quad(Rvar, 0, 2.0*pi)
        pfg[n] = pf[0]/(2.0*pi)
        print (" n, fidg[n], Pole Figure[n] = ", n, fidg[n], pfg[n])
        file.write(str(fidg[n])+" "+ str(pfg[n])+"\n")
    plt.plot(fidg, pfg)
    plt.savefig("pole_figure.png")
    #plt.show()
    file.close()


plt.plot(fidg, pfg)
print("fidg=" , fidg,"\n")



#### pf for difractograms
def Rvar(psi,tau,gamma,fi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if np.all(B >= 1.0): B = 1.0
    elif np.all(-1.0 < B < 1.0) : B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    elif np.all(B <= -1.0) : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))
#######################################################
intensity,fideg,hkl = Arrays()
########################################################
fidiv = [x/2 for x in fideg]
fi = [x/rtg for x in fidiv]

print ("fi values=" , fi, "\n")
print("fi div by 2 on degrees =" , fidiv, "\n")
print("Bragg angles on radians =" , fi, "\n")

tau = [x/rtg for x in taudeg] ; gamma = [x/rtg for x in gammadeg]
#Integral of pf
pf = []
for i,j,k in zip(tau,gamma,fi):
    pfvalue, error = sq.quad(Rvar, 0, 2.0*pi, args=(i,j,k))
    pf.append(pfvalue)
pf = np.array(pf)
pf1 = [x/(2.0*pi) for x in pf]
print("pf value =", pf1, "\n")

dBb = []
for i,j in zip(tau,gamma):
    dBb.append(cos(i)*cos(theta)+sin(i)*sin(theta)*cos(j-phi))
print (" distance bragbrentano = ", dBb, "\n")

for i in dBb: rhoBb = arccos(dBb)      
print (" Rho brag brentano = ", rhoBb, "\n")

for i in rhoBb:
    Rhkl = (exp(-0.693*((rhoBb/distwidth)**2))) 
print (" Rhkl brag brentano = ", Rhkl, "\n")

Bragg_int = [x*y for x,y in zip(Rhkl,intensity)]
print ("intensity bragg brentano = ", Bragg_int, "\n")
Bragg_dif = [list(x) for x in zip(Bragg_int, fideg)]
print ("nested list bragg brentano = ", Bragg_dif, "\n") 

text_int = [x*y for x,y in zip(pf1,intensity)]
print ("new integral intensity = ", text_int, "\n")
Difract_nl = [list(x) for x in zip(text_int , fideg)]
print("new nested list for difractogram" , Difract_nl , "\n")
print("hkl nest list", hkl,"\n")

#FWHM Gaussian Equation
def gaussian_dist(x, a , mean, sd):
    return a/(sd*np.sqrt(2.0*np.pi))*np.exp(-(x - mean)**2.0/(2.0*sd**2.0))

if __name__ == "__main__":

    lst = Difract_nl

    xgrazing = np.linspace(5,80,10000)
    ygrazing = np.zeros((10000))

    #fwhm2 = 0.7
    std = fwhm2/2.35

    for i,xrdgeom in enumerate(xgrazing):
        for j in lst:
            ygrazing[i] += gaussian_dist(xrdgeom , j[0] , j[1] , std)

    import pylab as pl
    pl.figure(2)
    pl.plot(xgrazing, ygrazing, '-k', label='1,1,1')
    pl.xlabel('2θ(degrees)')
    pl.ylabel('Intensity(a.u)')
    pl.title('$BiMnO_{3}$ after annealing')
    pl.savefig('grazing_fig.png')
    

    lst1 = Bragg_dif

    xbragg = np.linspace(5,80,10000)
    ybragg = np.zeros((10000))

    #fwhm2 = 0.7
    std = fwhm2/2.35

    for i,xrdgeom in enumerate(xbragg):
        for j in lst1:
            ybragg[i] += gaussian_dist(xrdgeom , j[0] , j[1] , std)

    pl.figure(3)
    pl.plot(xbragg, ybragg, '-k', label='0,0,1')
    pl.xlabel('2θ(degrees)')
    pl.ylabel('Intensity(a.u)')
    pl.title('$BiMnO_{3}$ aftr annealing')
    pl.savefig('bragg_fig.png')
    #pl.show()

### draw ipf
colorfunction= Rsum
norm = colors.Normalize()
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x, y, z)
ax.plot_surface(x, y, z,  rstride=1, cstride=1 , facecolors=cm.jet(norm(colorfunction)))
val = 1.2198085261644226024779405283314
ax.set_xlim(-val*np.max(Rsum), val*np.max(Rsum)); ax.set_ylim(-val*np.max(Rsum),val*np.max(Rsum))
plt.savefig("inverse pole figure.png")
#plt.show()