import sys
import os
import time
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *
from matplotlib.backends.qt_compat import QtCore, QtWidgets
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from mpl_toolkits.mplot3d import Axes3D
from readcfl import read_cfl_file, CflData
from grid_atoms_xyz import read_cfl_atoms, AtomData
import numpy as np
import subprocess

class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)

    def wheelEvent(self, event):
        #print("event.delta", event.delta())
        int_delta = int(event.delta())
        if int_delta > 0:
            self.m_scrolling.emit(int_delta)

        else:
            self.m_scrolling.emit(int_delta)

        event.accept()



class MyPopupDialog(QObject):
    def __init__(self, parent=None):
        super(MyPopupDialog, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("Widget.ui")
        

        self.bragg = MultipleImgSene()
        self.grazing = MultipleImgSene()
        self.window.graphicsView_7.setScene(self.bragg)
        self.window.graphicsView_8.setScene(self.grazing)

        fileName3 = "bragg_fig.png"
        image3 = QImage(fileName3) 
        self.pixmap_3 = QPixmap.fromImage(image3)
        self.bragg.addPixmap(self.pixmap_3)

        fileName4 = "grazing_fig.png"
        image4 = QImage(fileName4) 
        self.pixmap_4 = QPixmap.fromImage(image4)
        self.grazing.addPixmap(self.pixmap_4)


        self.window.show()
        


class Form(QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("grazing.ui")
        self.window.setWindowTitle("Grazing")
        appIcon = QIcon("grazing_icon.png")
        self.window.setWindowIcon(appIcon)

        #Buttons
        self.window.pushButton.clicked.connect(self.popwindow)
        self.window.Run_Button.clicked.connect(self.running_grazing)
        

        #Table Widget
        self.window.del_row_button.clicked.connect(self.remove_r)
        self.window.add_row_button.clicked.connect(self.add_r)
        columns = ['z','Label','X pos','Y pos','Z pos', 'Occ']
        self.window.tableWidget_ini.setColumnCount(len(columns))
        self.window.tableWidget_ini.setHorizontalHeaderLabels(columns)

        #controller
        self.window.h_edit.setText("0")
        self.window.k_edit.setText("0")
        self.window.l_edit.setText("0")

        self.window.a_edit.setText("5.0")
        self.window.b_edit.setText("5.0")
        self.window.c_edit.setText("5.0")

        self.window.alpha_edit.setText("90.0")
        self.window.beta_edit.setText("90.0")
        self.window.gamma_edit.setText("90.0")

        self.window.distwidth_edit.setText("60")
        self.window.fwhm_edit.setText("15")


        #images
        
        self.modl_scene = MultipleImgSene()
        self.ipf_scene = MultipleImgSene()
        self.window.graphicsView_3.setScene(self.modl_scene)
        self.window.graphicsView_2.setScene(self.ipf_scene)
        self.window.show()

        #open read cfl Tabs
        self.window.actionOpen_CFL.triggered.connect(self.actionOpen_Cfl_connect)
        self.window.actionSave_CFL.triggered.connect(self.savefile_cfl)

    def actionOpen_Cfl_connect(self):
        self.read_cfl()

    def popwindow(self):
        self.dialog = MyPopupDialog(self)

    def set_img(self):
        fileName1 = "inverse pole figure.png"
        image1 = QImage(fileName1) 
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.modl_scene.addPixmap(self.pixmap_1)



    def remove_r(self):
        print("Row removed")
        self.window.tableWidget_ini.removeRow(self.window.tableWidget_ini.model().rowCount()-1)

    def add_r(self):
        print("Row Added")
        self.window.tableWidget_ini.insertRow(self.window.tableWidget_ini.model().rowCount())
        print(self.window.tableWidget_ini.model().rowCount())

    def running_grazing(self):
        print("\nrunnign grazing")
        a_arg = self.window.a_edit.text()
        b_arg = self.window.b_edit.text()
        c_arg = self.window.c_edit.text()
        alpha_arg = self.window.alpha_edit.text()
        beta_arg = self.window.beta_edit.text()
        gamma_arg = self.window.gamma_edit.text()
        dist_width_arg = self.window.distwidth_edit.text()
        fwhm_arg = self.window.fwhm_edit.text()

        print("\ngetting text from GUI",a_arg)
        print("calling grazing")
        command = 'python Gauss0_argsys.py ' + a_arg + ' ' + b_arg + ' ' + c_arg + ' ' + alpha_arg \
                                      + ' ' + beta_arg + ' ' + gamma_arg + ' ' + dist_width_arg \
                                      + ' ' +fwhm_arg + ' ' + str(0.7)
        print("\ncommand = ", command)
        os.system(command)
        
        fileName1 = "inverse pole figure.png"
        image1 = QImage(fileName1) 
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.modl_scene.addPixmap(self.pixmap_1)

        fileName2 = "pole_figure.png"
        image2 = QImage(fileName2) 
        self.pixmap_2 = QPixmap.fromImage(image2)
        self.ipf_scene.addPixmap(self.pixmap_2)

    def read_cfl(self):
        self.info1 = QFileDialog.getOpenFileName(
            self.window, 'Open CFL File', ".", "Files (*.cfl)")[0]
        print("info from clf", self.info1)

        self.data = read_cfl_file(my_file_path = self.info1)

        self.window.spgr_edit.setText(str(self.data.spgr))
        self.window.a_edit.setText(str(self.data.a))
        self.window.b_edit.setText(str(self.data.b))
        self.window.c_edit.setText(str(self.data.c))
        self.window.alpha_edit.setText(str(self.data.alpha))
        self.window.beta_edit.setText(str(self.data.beta))
        self.window.gamma_edit.setText(str(self.data.gamma))
        self.window.h_edit.setText(str(self.data.h))
        self.window.k_edit.setText(str(self.data.k))
        self.window.l_edit.setText(str(self.data.l))


        self.info2 = read_cfl_atoms(my_file_path = self.info1)
        self.window.tableWidget_ini.setRowCount(len(self.info2))
        for n,atm in enumerate(self.info2):

            self.window.tableWidget_ini.setItem(n,0,QTableWidgetItem(atm.Zn))
            self.window.tableWidget_ini.setItem(n,1,QTableWidgetItem(atm.Lbl))
            self.window.tableWidget_ini.setItem(n,2,QTableWidgetItem(atm.x))
            self.window.tableWidget_ini.setItem(n,3,QTableWidgetItem(atm.y))
            self.window.tableWidget_ini.setItem(n,4,QTableWidgetItem(atm.z))
            self.window.tableWidget_ini.setItem(n,5,QTableWidgetItem(atm.b))

    def savefile_cfl(self):
        self.cfl_file_name = QFileDialog.getSaveFileName(
            self.window, 'Save File', os.getenv('HOME'), filter='*.cfl'
        )[0]

        print("save from cfl1=", self.cfl_file_name,"\n")
        self.write_out_cfl()

    def write_out_cfl(self):
        save_atm = []
        print("range of table=",range(self.window.tableWidget_ini.rowCount()),"\n")
        for d in range(self.window.tableWidget_ini.rowCount()):
            print("print d= ", d)
            atm = AtomData()
            atm.Zn = str(self.window.tableWidget_ini.item(d, 0).text())
            atm.Lbl = str(self.window.tableWidget_ini.item(d, 1).text())
            atm.x = float(self.window.tableWidget_ini.item(d,2).text())
            atm.y = float(self.window.tableWidget_ini.item(d,3).text())
            atm.z = float(self.window.tableWidget_ini.item(d,4).text())
            atm.b = float(self.window.tableWidget_ini.item(d,5).text())

            save_atm.append(atm)
        print("data save_atom",save_atm)

        data = CflData()
        data.spgr = self.window.spgr_edit.text()
        data.a = self.window.a_edit.text()
        data.b = self.window.b_edit.text()
        data.c = self.window.c_edit.text()
        data.alpha = self.window.alpha_edit.text()
        data.beta = self.window.beta_edit.text()
        data.gamma = self.window.gamma_edit.text()
        data.h = self.window.h_edit.text()
        data.k = self.window.k_edit.text()
        data.l = self.window.l_edit.text()


        crystal = "Title  "
        crystal += "placeholder title" + " \n"
        crystal += "!" + "        a" + "        b" + "        c" + \
                "     alpha" + "    beta" + "   gamma" + "\n"
        crystal += "Cell    " + str(data.a) + "     " + str(data.b) + "     " +\
                str(data.c) + "  " + str(data.alpha) + "  " + str(data.beta) + \
                "  " + str(data.gamma) + "\n"
        crystal += "!     " + "Space Group" + "\n"
        crystal += "Spgr  " + str(data.spgr) + "\n"
        crystal += "SIZE_LG " + "1.05 placeholder value" + "\n"
        crystal += "IPF_RES " + "1440 placeholder" + "\n"
        crystal += "HKL_PREF " + str(data.h) + " " + str(data.k) + " " + str(data.l) + "\n"
        crystal += "HKL_PREF_WH " + "6 placeholder" + "\n"
        crystal += "AZM_IPF " + "place holder" + "\n"
        crystal += "MASK_MIN " + "place holder" + "\n"
        crystal += "!" + "\n"

        cfl_data = str(crystal)

        self.info2 = open(self.cfl_file_name, 'w')
        for c in save_atm:
            print("data of table=",c.Zn,c.Lbl,c.x,c.y,c.z,c.b,"\n")
            var = "Atom "
            var += str(c.Zn) + "  "
            var += str(c.Lbl) + "  "
            var += str(c.x) + "  "
            var += str(c.y) + "  "
            var += str(c.z) + "  "
            var += str(c.b) + " \n"
            cfl_data += str(var)

        self.info2.write(cfl_data)
        self.info2.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
