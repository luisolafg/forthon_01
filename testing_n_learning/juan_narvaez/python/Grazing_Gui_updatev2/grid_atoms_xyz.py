#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


class AtomData(object):


    '''functionless data class'''
    def __init__(self):

        self.Zn  = None
        self.Lbl = None
        self.x   = None
        self.y   = None
        self.z   = None
        self.b   = None

def read_cfl_atoms(my_file_path = "cfl_out.cfl"):

    '''opening the .cfl file to read the atoms data'''
    myfile = open(my_file_path, "r")
    all_lines = myfile.readlines()
    myfile.close()

    lst_dat = []

    for lin_char in all_lines:

        commented = False

        for delim in ',;=:':    # filtering unwanted data in the .cfl file as ,:=;
            lin_char = lin_char.replace(delim, ' ')

        for litle_block in lin_char.split():    # filtering the commented lines
            if( litle_block == "!" or litle_block == "#" ):
                commented = True
            if( not commented and litle_block != ","):
                lst_dat.append(litle_block)


    lst_pos = []
    for pos, dat in enumerate(lst_dat):

        if(dat[0:4] == "Atom"):

            lst_pos.append(pos)


        atm_lst = []
        for n_i, pos in enumerate(lst_pos):
            atm = AtomData()
            atm.Zn = lst_dat[pos + 1]
            atm.Lbl = lst_dat[pos + 2]
            atm.x = lst_dat[pos + 3]
            atm.y = lst_dat[pos + 4]
            atm.z = lst_dat[pos + 5]
            try:
                atm.b = lst_dat[pos + 6]
                if(atm.b == "Atom"):
                    atm.b = None
            except:
                atm.b = None
            atm_lst.append(atm)

    return atm_lst

