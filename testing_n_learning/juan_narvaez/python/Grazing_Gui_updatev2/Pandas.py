from scipy import integrate
import numpy as np
import pandas as pd
import collections

def Arrays():

    df1 = pd.read_csv('BiMnO3.csv', encoding= 'latin-1')
    df2 = df1.groupby('      2d')['        I'].sum()
    np.set_printoptions(suppress=True)
    intensity = df2.to_numpy()
    df3 = df1.drop_duplicates(['      2d'])
    fideg = df3['      2d'].to_numpy()
    df4 = df1[(df1[' l'] >= 0)]
    df4 = df4.drop_duplicates(['      2d'])
    dfh = df4['h'].to_numpy()
    dfk = df4['k'].to_numpy()
    dfl = df4[' l'].to_numpy()
    hkl = [list(x) for x in zip(dfh , dfk , dfl)]
    print("hkls=",hkl)
    return (intensity,fideg,hkl)

if __name__ == "__main__":
    intensity,fideg,hkl = Arrays()



