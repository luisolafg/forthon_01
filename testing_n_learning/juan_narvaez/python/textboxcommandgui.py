import sys
import os
from subprocess import call as shell_func
from PySide2.QtWidgets import QApplication, QLabel, QLineEdit
from PySide2.QtWidgets import QDialog, QPushButton, QVBoxLayout


class Form(QDialog):
    """"""

    def __init__(self, parent=None):
        #window function
        super(Form, self).__init__(parent)

        self.edit = QLineEdit("")
        self.button = QPushButton("Run command")


        #window layout
        layout = QVBoxLayout()
        layout.addWidget(self.edit)
        layout.addWidget(self.button)


        self.setLayout(layout)

        self.button.clicked.connect(self.run_command)


    def run_command(self):
        text_to_command = str(self.edit.text())
        print('Contents of QLineEdit widget: ', text_to_command)
        shell_func(text_to_command, shell=True)


if __name__ == "__main__":
    app = QApplication([])
    form = Form()
    form.show()
    sys.exit(app.exec_())
