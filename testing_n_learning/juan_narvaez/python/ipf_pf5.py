import numpy as np; import math
import scipy.integrate as sq
import scipy.special as special
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D

# Defining parameters:
rtg = 180.0/math.pi
thetadeg = 0.0; phideg = 0.0; widthdeg = 30.0
taudeg = 0.001; gammadeg = 0.0
theta = thetadeg/rtg; phi = phideg/rtg; distwidth = widthdeg/rtg
tau = taudeg/rtg; gamma = gammadeg/rtg
limfideg = 180; delfideg = 10
print (" Max of IPF at theta, phi (degrees) = ", thetadeg, phideg)
print (" Max of IPF at theta, phi (radians) = ", theta, phi)
print (" IPF width (degrees and radians) = ", widthdeg, distwidth)
print (" tau, gamma (degrees) = ", taudeg, gammadeg)
print (" tau, gamma (radians) = ", tau, gamma)
print (" limfi, delfi (degrees = ", limfideg, delfideg)

# Defining functions:
def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnorma(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rvar(psi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if B > 1.0: B = 1.0
    if B < -1.0 : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))

def pfg(fi):
    pfgr = sq.quad(Rvar, 0, 2.0*pi)
    return pfgr[0]

# Calculating R surface integral:
R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization:
Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)

# Maximum of normalized IPF, R:
print
if theta != 0.0:
    print (" R_normalized(max) =", Rnorma(theta, phi)/sin(theta))
else:
    print (" R_normalized(max) =", 4.0*pi/R0)
print

# Calculating and plotting the direct pole figure curve:
fidg = np.zeros(38); pfg = np.zeros(38)
fideg = 0.0; delfideg = 5.0
while fideg <= limfideg:
    n = int(fideg/10)
    fidg[n] = fideg
    fi = fideg/rtg
    pf = sq.quad(Rvar, 0, 2.0*pi)
    pfg[n] = pf[0]/(2.0*pi)
    print (" n, fidg[n], Pole Figure[n] = ", n, fidg[n], pfg[n])
    fideg = fideg + delfideg
plt.plot(fidg, pfg)

# Calculating and plotting the inverse pole figure surface:
eta, chi = np.meshgrid(np.linspace (0, math.pi, 36) , np.linspace(0, 2.0*math.pi,72))
#calculating R
p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
p1 = arccos(p)
R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))
#plotting inverse pole figure
fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface(eta*rtg, chi*rtg, R)
plt.show()

