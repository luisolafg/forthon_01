import numpy as np 
import math
import scipy.integrate as integrate
import scipy.special as special
from numpy import sin,cos,pi,abs, arccos, exp, tan

#Polar angle (TAU) Angle , Cullity formula, cubic case
hkl_np = [0,0,1]
hkl = [[0,0,1],[1,0,0],[1,0,1] ,[1,1,0],[1,1,1],[0,0,2],[2,0,0],[1,0,2],[2,0,1],[2,1,0],[1,1,2],[2,1,1]]
hkI = [1,1,1]

print(hkl)

rtg = 180.0/math.pi

ang = []

for i in hkl:
    ang.append(float((hkl_np[0]*i[0])+(hkl_np[1]*i[1])+(hkl_np[2]*i[2]))/ \
               (math.sqrt((hkl_np[0]**2 + hkl_np[1]**2 + hkl_np[2]**2)*(i[0]**2 \
               + i[1]**2 + i[2]**2))))
  
taurad = arccos(ang)
tau = taurad*rtg

#Azimuth angle (Gamma),
a = 4.0400
b = 4.0400
c = 4.0400
Volume = a*b*c

#Reciprocal vectors
b1 = (b*c) / Volume 
b2 = (a*c) / Volume 
b3 = (a*b) / Volume 

#Bh Vector and module
Bh = []
MBh = []

for i in hkl:
    Bh.append(float( i[0]*b1 + i[1]*b2 +i[2]*b3 ))
    MBh.append(float(math.sqrt((i[0]**2)+(i[1]**2)+(i[2]**2))))

#value of G and calculus of gamma
g = (sin(taurad))*MBh
h1 = [i[0] for i in hkl]
hg = [i/j for i,j in zip(h1,g)]

gammarad = (arccos(hg))
gammadeg = [x * rtg for x in gammarad]
gammadeg = [0.001 if math.isnan(i) else i for i in gammadeg]   


print("Tau degree", tau, "\n")
print("gamma degree", gammadeg , "\n")



    # #pole figure calculation
    # #value of angles
    # alpha = (math.pi) / 2
    # beta = (math.pi) / 2
    # bragg_angle = 0.314159 #36
    # psi = (math.pi) / 4   #np.linspace( 0 ,2**math.pi,1000)
    # #theta
    # distwidth = 0.349066 #20 

    # #1st equations for angles
    # costheta = (math.cos(alpha)**math.cos(bragg_angle)) + \
    #            (math.cos(alpha)**math.sin(bragg_angle)**  \
    #            math.cos(psi)) 
    # print ("value of first equation",costheta , "\n")

    # #2nd equation for angles
    # cos_betaminus = (math.cos(bragg_angle)/(math.cos(costheta)**math.sin(alpha))) \
    #                 - (1/math.tan(costheta))**(1/math.tan(alpha))
    # print ("value of second equation",cos_betaminus , "\n")

    # # R equation
        
    # R = math.exp(-0.693**(bragg_angle/distwidth))
    # print ("inverse pole figure",R , "\n")

    # # pole figure
    # def integrand(R):
    #     return R
    
    # Ph = integrate.quad(integrand, 0, 2*math.pi)
    # P = (1/2**math.pi)**38.94
    # print ("Pole figure",P , "\n")

    #val1 = math.cos(3.1415926**rf)
    #print ("cos val", val1 , "\n")

    #cosp = ((math.cos(54.7**rf))**(math.cos(20**rf))) + (math.sin(54.7**rf)**(math.sin(20**rf))**(math.cos(25**rf)))
    #print ("Ro", cosp , "\n")