import numpy as np ; import math
import scipy.integrate as sq
import scipy.special as special
from numpy import sin,cos,pi,abs, arccos, exp, tan
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from product_vectors_abel import Ortorrombic
from product_vectors import Trigonal
from Pandas import Arrays

#cell parameter
#quartz
# a = 4.91; alpha = 90
# b = 4.91; beta = 90
# c = 5.40; gamma = 120

#pzt
a = 3.98 ; alpha = 90
b = 3.98 ; beta = 90
c = 4.04 ; gamma = 90


#calling function
if a == b and b == c and c == a and alpha == beta and beta ==gamma and gamma == alpha \
or a == b and b < c and c > a and alpha == beta and beta ==gamma and gamma == alpha:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = \
        Ortorrombic(a,b,c,alpha,beta,gamma)
if a == b and b < c and c > a and alpha == beta and beta < gamma and gamma > alpha \
or b > c and c > a and alpha == gamma and beta > alpha and beta > gamma:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = \
        Trigonal(a,b,c,alpha,beta,gamma)
 

#original routine IPF,PF
rtg = 180.0/math.pi ; widthdeg =  10
theta = thetadeg/rtg; phi = phideg/rtg; distwidth = widthdeg/rtg
delta = [x/rtg for x in deltadeg]; iota =  [x/rtg for x in iotadeg] #float(0)
delta = delta[0] ; iota = float(0)
tau = [x/rtg for x in taudeg] ; gamma = [x/rtg for x in gammadeg]  

print (" Max of IPF at tau(rads) = ", tau, "\n")
print (" Max of IPF at gamma(rads) = ",  gamma,"\n")
print (" Max of IPF at theta(rads) = ", theta,"\n")
print (" Max of IPF at  phi (rads) = ", phi,"\n")
print (" Max of IPF at delta(rads) = ", delta,"\n")
print (" Max of IPF at  iota (rads) = ", iota,"\n")
print (" IPF width (degrees and radians) = ", widthdeg, distwidth, "\n")

# Defining functions:
def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnorma(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rcrys(eta, chi,delta,iota):
    d = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnew(eta,chi): 
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    d1 = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho1 = arccos(d1)
    return (exp(-0.693*((rho/distwidth)**2)) + exp(-0.693*((rho1/distwidth)**2)))*sin(eta)
def Rnewnorma(eta,chi): 
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    d1 = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho1 = arccos(d1)
    return factor*(exp(-0.693*((rho/distwidth)**2)) + exp(-0.693*((rho1/distwidth)**2)))*sin(eta)
def Rvar(psi,tau,gamma,fi):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if np.any(B > 1.0): B = 1.0
    if np.any(B < -1.0) : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    d1 = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
    rho1 = arccos(d1)
    return factor*(exp(-0.693*((rho/distwidth)**2)) + exp(-0.693*((rho1/distwidth)**2)))*sin(eta)
    
def pfg(fi):
    pfgr = sq.quad(Rvar, 0, 2.0*pi)
    return pfgr[0]

# Calculating R surface integral:
R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization:
Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)

# Maximum of normalized IPF, R:
print
if theta != 0.0:
    print (" R_normalized(max) =", Rnorma(theta, phi)/sin(theta))
else:
    print (" R_normalized(max) =", 4.0*pi/R0, "\n")
print

#######################################################
intensity,fideg,hkl = Arrays()
########################################################
fidiv = [x/2 for x in fideg]
fi = [x/rtg for x in fidiv]

print ("fi values=" , fi, "\n")
print("fi div by 2 on degrees =" , fidiv, "\n")
print("Bragg angles on radians =" , fi, "\n")

###
#integral pfnew
ipfnewval, error = sq.dblquad(Rnew, 0, 2.0*pi, 0, pi)
print("new sum RC integral sum", ipfnewval)
factor = 4*pi/ipfnewval
print("4pi / integral", factor,"\n")

ipfnewvalnorma, error = sq.dblquad(Rnewnorma, 0, 2.0*pi, 0, pi)
print("ipfnewvalnorma=", ipfnewvalnorma)
###############
#Bragg Brentano
#widthdegBb =  10; distwidthBb = widthdegBb/rtg
dBb = []
for i,j in zip(tau,gamma):
    dBb.append(cos(i)*cos(theta)+sin(i)*sin(theta)*cos(j-phi))
    print (" distance bragbrentano = ", dBb, "\n")

for i in dBb: rhoBb = arccos(dBb)      
print (" Rho brag brentano = ", rhoBb, "\n")

d1Bb = []
for i,j in zip(tau,gamma):
    d1Bb.append(cos(i)*cos(delta)+sin(i)*sin(delta)*cos(j-iota))
    print (" distance bragbrentano2 = ", d1Bb, "\n")

for i in d1Bb : rho1Bb = arccos(d1Bb)
print (" Rho bragg brentano2 = ", rho1Bb, "\n")

for i,j in zip(rhoBb,rho1Bb):
    Rhkl = factor*(exp(-0.693*((rhoBb/distwidth)**2)) + exp(-0.693*((rho1Bb/distwidth)**2))) 
    print (" Rhkl brag brentano = ", Rhkl, "\n")
################
#Integral of pf
pf = []
for i,j,k in zip(tau,gamma,fi):
    pfvalue, error = sq.quad(Rvar, 0, 2.0*pi, args=(i,j,k))
    pf.append(pfvalue)
pf = np.array(pf)
pf1 = [x/(2.0*pi) for x in pf]
print("pf value =", pf1, "\n")
#################

fi= np.linspace(0,90,91)
fi = [x/rtg for x in fi]
tau = tau[0]
tau = np.tile(tau,91)
gamma = gamma[0]
gamma = np.tile(gamma,91)
print("new fi values for pf figures",fi, "\n")
print("new tau values for pf figures",tau, "\n")
print("new gammas values for pf figures",gamma, "\n")

onedim_pf = []
for i,j,k in zip(tau,gamma,fi):
    oned_pf, error = sq.quad(Rvar,0, 2.0*pi, args=(i,j,k))
    print("i values=", i,"\n")
    print("j values=", j,"\n")
    print("k values=", k,"\n")
    onedim_pf.append(oned_pf)
onedim_pf = np.array(onedim_pf)
onedim_pf1 = [x/(2.0*pi) for x in onedim_pf]
onedim_pf1 = [(0.0) if math.isnan(i) else i for i in onedim_pf1]
print("pf value for graphs =", onedim_pf1, "\n")


file = open("polefigure001.txt", "w")
for index in range(len(fi)):
    file.write(str(fi[index]) + " " + str(onedim_pf1[index]) + "\n")
file.close()



fig, ax = plt.subplots()
ax.plot(fideg, pf1)
#AA
#hkl_label = [101,110,111,102,212]
#BA
hkl_label = ["001","010","011","110","111","002","020","012","021","120",\
            "112","121","022","220","003","122","221","030","013","031","130","113","131"]
for i, txt in enumerate(hkl_label):
    ax.annotate(txt, (fideg[i], pf1[i]))

plt.show()
####
Bragg_int = [x*y for x,y in zip(Rhkl,intensity)]
print ("intensity bragg brentano = ", Bragg_int, "\n")
Bragg_dif = [list(x) for x in zip(Bragg_int, fideg)]
print ("nested list bragg brentano = ", Bragg_dif, "\n") 
####
text_int = [x*y for x,y in zip(pf1,intensity)]
print ("new integral intensity = ", text_int, "\n")
Difract_nl = [list(x) for x in zip(text_int , fideg)]
print("new nested list for difractogram" , Difract_nl , "\n")
print("hkl nest list", hkl,"\n")

#FWHM Gaussian Equation
def gaussian_dist(x, a , mean, sd):
    return a/(sd*np.sqrt(2.0*np.pi))*np.exp(-(x - mean)**2.0/(2.0*sd**2.0))

if __name__ == "__main__":

    lst = Difract_nl

    x = np.linspace(5,80,10000)
    y1 = np.zeros((10000))

    fwhm = 0.1
    std = fwhm/2.35

    for i,xrdgeom in enumerate(x):
        for j in lst:
            y1[i] += gaussian_dist(xrdgeom , j[0] , j[1] , std)

    import pylab as pl
    pl.figure(1)
    pl.plot(x, y1, '-k', label='1,1,1')
    pl.xlabel('2θ(degrees)')
    pl.ylabel('Intensity(a.u)')
    pl.title('BaTiO3 Grazing incidence')
    

    lst1 = Bragg_dif

    x = np.linspace(5,80,10000)
    y1 = np.zeros((10000))

    fwhm = 0.1
    std = fwhm/2.35

    for i,xrdgeom in enumerate(x):
        for j in lst1:
            y1[i] += gaussian_dist(xrdgeom , j[0] , j[1] , std)

    pl.figure(2)
    pl.plot(x, y1, '-k', label='0,0,1')
    pl.xlabel('2θ(degrees)')
    pl.ylabel('Intensity(a.u)')
    pl.title('BaTiO3 Bragg-Brentano')
    pl.show()

# Calculating and plotting the inverse pole figure surface:
eta, chi = np.meshgrid(np.linspace (0, math.pi, 36) , np.linspace(0, 2.0*math.pi,72))
#calculating R
pA = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
p1 = arccos(pA)
RA = (factor)*exp(-0.693*((p1/distwidth)**2))

pB = cos(eta)*cos(delta)+sin(eta)*sin(delta)*cos(chi-iota)
p2 = arccos(pB)
RB = (factor)*exp(-0.693*((p2/distwidth)**2))

RC = RA + RB
print ("Ra =", RA,"\n")
print ("Rb =", RB,"\n")
print ("Rc =", RC,"\n")



delta = [x/rtg for x in deltadeg]; iota = [x/rtg for x in iotadeg]
ipf_sum = []
for i,j in zip(delta,iota):
    ipfval, error = sq.dblquad(Rcrys, 0, 2.0*pi, 0, pi, args=(i,j))
    ipf_sum.append(ipfval)
print("ipf integral sum", ipfval)


#plotting inverse pole figure
fig= plt.figure(2)
ax = Axes3D(fig,azim=113, elev=30)
#ax.set_xlim3d(left=0,right=350)
#ax.set_zlim3d(bottom=0.0,top=505)
#ax.set_title("azimuth",loc='left')
ax.plot_surface(eta*rtg, chi*rtg, RC)
print ("value axis x", eta*rtg)
print ("value axis y", chi*rtg)
print ("value axis z", RC)
plt.show()

#creation of sphere
#rad = 1
#x = rad*sin(eta)*cos(chi)
#y = rad*sin(eta)*sin(chi)
#z = rad*cos(eta)

#zz = cos(R)


#fig= plt.figure()
#ax = Axes3D(fig)
#ax.plot_surface(x*rtg, y*rtg, z*rtg)

