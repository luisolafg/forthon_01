import sys
import time
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *
from matplotlib.backends.qt_compat import QtCore, QtWidgets
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from mpl_toolkits.mplot3d import Axes3D
import numpy as np

class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)

    def wheelEvent(self, event):
        #print("event.delta", event.delta())
        int_delta = int(event.delta())
        if int_delta > 0:
            self.m_scrolling.emit(int_delta)

        else:
            self.m_scrolling.emit(int_delta)

        event.accept()

class Widget(QWidget):
    def __init__(self, parent=None):
        super(Widget,self).__init__(parent)
        self.fig = Figure()
        self.canvas = FigureCanvas(self.fig)
        self.axes = self.fig.add_subplot(111, projection='3d')

        layout = QtWidgets.QVBoxLayout(self)
        layout.addWidget(self.canvas)

        X = np.arange(-5, 5, 0.25)
        Y = np.arange(-5, 5, 0.25)
        X, Y = np.meshgrid(X, Y)
        R = np.sqrt(X ** 2 + Y ** 2)
        Z = np.sin(R)

        self.axes.plot_surface(X, Y, Z)

class MyPopupDialog(QObject):
    def __init__(self, parent=None):
        super(MyPopupDialog, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("Widget.ui")
        self.window.show()

        


class Form(QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("grazing.ui")
        self.window.setWindowTitle("Grazing")
        appIcon = QIcon("grazing_icon.png")
        self.window.setWindowIcon(appIcon)
        self.window.pushButton.clicked.connect(self.popwindow)

        #Table Widget
        self.window.del_row_button.clicked.connect(self.remove_r)
        self.window.add_row_button.clicked.connect(self.add_r)
        columns = ['z','Label','X pos','Y pos','Z pos', 'Occ']
        self.window.tableWidget_ini.setColumnCount(len(columns))
        self.window.tableWidget_ini.setHorizontalHeaderLabels(columns)

        #controller
        self.window.h_edit.setText("0")
        self.window.k_edit.setText("0")
        self.window.l_edit.setText("0")

        self.window.a_edit.setText("5.0")
        self.window.b_edit.setText("5.0")
        self.window.c_edit.setText("5.0")

        self.window.alpha_edit.setText("90.0")
        self.window.beta_edit.setText("90.0")
        self.window.gamma_edit.setText("90.0")

        self.window.distwidth_edit.setText("60")
        self.window.fwhm_edit.setText("15")


        #images
        self.window.pushButton_3.clicked.connect(self.set_img)
        self.modl_scene = MultipleImgSene()
        self.window.graphicsView_3.setScene(self.modl_scene)
        self.window.show()

    def popwindow(self):
        self.dialog = MyPopupDialog(self)

    def set_img(self):
        fileName = "pffigure_grazing.png"
        image1 = QImage(fileName) 
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.modl_scene.addPixmap(self.pixmap_1)


    def remove_r(self):
        print("Row removed")
        self.window.tableWidget_ini.removeRow(self.window.tableWidget_ini.model().rowCount()-1)

    def add_r(self):
        print("Row Added")
        self.window.tableWidget_ini.insertRow(self.window.tableWidget_ini.model().rowCount())
        print(self.window.tableWidget_ini.model().rowCount())

    def running_grazing(self):
        print("runnign grazing")
        os.system('python Grazing.py')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
