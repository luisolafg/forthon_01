!  This file is part of the Anaelu Project
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this
! file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
module cutting
    implicit none

    real(kind=8),  dimension(:,:), allocatable :: img_out
    contains

    subroutine cut_img(mask, img_in, xc, yc)
        real(kind=8),  dimension(:,:), intent (in) ::  mask
        real(kind=8),  dimension(:,:), intent (in) :: img_in
        real(kind=8)                 , intent (in) :: xc, yc

        real(kind=8)                :: dx, dy, tot_in, avg_in, cont_px
        integer                     :: x, y, xmax, ymax
        integer                     :: lx, ly, xb1, yb1, xb2, yb2, y_end, x_end
        real(kind=8)                :: xstp, ystp, d1, d2, dt, e=0.00000000001

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)

        write(*,*) "xmax =", xmax
        write(*,*) "ymax =", ymax

        tot_in = 0

        if( allocated(img_out) )then
            deallocate(img_out)
        end if

        allocate(img_out(xmax, ymax))
        img_out = img_in

        cont_px = 0
        do x = 1, xmax
            do y = 1, ymax
                if( mask(x, y) /= 1.0 )then
                    tot_in = tot_in + img_out(x, y)
                    cont_px = cont_px + 1
                end if
            end do
        end do
        write(*, *) "tot_in =", tot_in
        write(*, *) "cont_px =", cont_px
        avg_in = tot_in / cont_px
        write(*,*) "avg_in =", avg_in
        write(*,*) "x(center), y(center) =", xc, yc
        do x = 1, xmax, 1
            do y = 1, ymax, 1
                if( mask(x, y) == 1.0 .and. img_in(x,y) >= 0)then

                    xb1 = -1
                    yb1 = -1
                    xb2 = -1
                    yb2 = -1

                    dx = x - xc
                    dy = y - yc
                    if( abs(dx) < abs(dy) )then
                    ! case of y advancing pixel per pixel and x slower

                        dx = dx / abs(dy)
                        ystp = (yc - y) / abs(yc - y)
                        if(ystp /= 0)then

                            ! looking from x,y to the center
                            xstp = -1.0
                            do ly = y, int(yc), int(ystp)
                                xstp = xstp + 1.0
                                if( abs(dx) > e .and. abs(xstp) > e )then
                                    lx = x - int(dx * xstp)
                                else
                                    !write(*,*), "step < e found; defaulting to 0"
                                    lx = x
                                end if
                                if( mask(lx, ly) == -1 )then
                                    xb1 = lx
                                    yb1 = ly
                                    !uncomment the next line for debugging only
                                    !img_out(lx, ly) = 59999
                                    exit
                                end if
                            end do

                            ! looking from x,y to the borders
                            ystp = -ystp
                            if( ystp > 0 )then
                                y_end = ymax
                            else
                                y_end = 1
                            end if
                            xstp = -1.0
                            do ly = y, y_end, int(ystp)
                                xstp = xstp + 1.0
                                if( abs(dx) > e .and. abs(xstp) > e )then
                                    lx = x + int(dx * xstp)
                                else
                                    !write(*,*), "step < e found; defaulting to 0"
                                    lx = x
                                end if
                                if( lx > 0 .and. ly > 0 .and. lx < xmax .and. ly < ymax )then
                                    if( mask(lx, ly) == -1 )then
                                        xb2 = lx
                                        yb2 = ly
                                        !uncomment the next line for debugging only
                                        !img_out(lx, ly) = 99999
                                        exit
                                    end if
                                else
                                    exit
                                end if
                            end do
                        end if

                    else
                    ! case of x advancing pixel per pixel and y slower

                        dy = dy / abs(dx)
                        xstp = (xc - x) / abs(xc - x)
                        if(xstp /= 0)then

                            ! looking from x,y to the center
                            ystp = -1.0
                            do lx = x, int(xc), int(xstp)
                                ystp = ystp + 1.0
                                if( abs(dy) > e .and. abs(ystp) > e )then
                                    ly = y - int(dy * ystp)
                                else
                                    !write(*,*), "step < e found; defaulting to 0"
                                    ly = y
                                end if
                                if( mask(lx, ly) == -1 )then
                                    xb1 = lx
                                    yb1 = ly
                                    !uncomment the next line for debugging only
                                    !img_out(lx, ly) = 59999
                                    exit
                                end if
                            end do


                            ! looking from x,y to the borders
                            xstp = -xstp
                            if( xstp > 0 )then
                                x_end = xmax
                            else
                                x_end = 1
                            end if
                            ystp = -1.0
                            do lx = x, x_end, int(xstp)
                                ystp = ystp + 1.0
                                if( abs(dy) > e .and. abs(ystp) > e )then
                                    ly = y + int(dy * ystp)
                                else
                                    !write(*,*), "step < e found; defaulting to 0"
                                    ly = y
                                end if
                                if( lx > 0 .and. ly > 0 .and. lx < xmax .and. ly < ymax )then
                                    if( mask(lx, ly) == -1 )then
                                        xb2 = lx
                                        yb2 = ly
                                        !uncomment the next line for debugging only
                                        !img_out(lx, ly) = 99999
                                        exit
                                    end if
                                else
                                    exit
                                end if
                            end do
                        end if
                    end if

                    if( xb1 == -1 .or. yb1 == -1 )then
                        img_out(x,y) = img_in(xb2, yb2)
                    elseif( xb2 == -1 .or. yb2 == -1 )then
                        img_out(x,y) = img_in(xb1, yb1)
                    else
                        d1 = sqrt(float(x - xb1) ** 2.0 + float(y - yb1) ** 2.0)
                        d2 = sqrt(float(x - xb2) ** 2.0 + float(y - yb2) ** 2.0)
                        dt = d1 + d2
                        img_out(x,y) = (img_in(xb1,yb1) * (d2/dt) + img_in(xb2, yb2) * (d1/dt))
                    end if
                end if
            end do
        end do

    end subroutine cut_img


    !end subroutine cut_img
end module cutting
