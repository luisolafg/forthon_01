
module inner_tools
    implicit none

    real(kind=8),  dimension(:,:), allocatable  :: img_out
    real(kind=8)                                :: r, r_2

    contains

    subroutine rotate_img(img_in)
        real(kind=8),  dimension(:,:), intent (in) :: img_in
        integer                     :: xmax, ymax, x, y

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        do x = 1, xmax
            do y = 1, ymax
                img_out(y, x) = img_in(xmax - x + 1, y)
            end do
        end do
    end subroutine rotate_img

    subroutine smooth_img(img_in, times_in)
        real(kind=8),    dimension(:,:), intent (in) :: img_in
        integer(kind=8),                 intent (in) :: times_in
        real(kind=8),    dimension(:,:), allocatable :: img_tmp

        integer                     :: xmax, ymax, x, y, times, x_sc, y_sc, ex_flag

        times = times_in

        xmax = ubound(img_in,1)
        ymax = ubound(img_in,2)
        write(*,*) "Allocating img_out"
        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(xmax, ymax))

        write(*,*) "Allocating img_tmp"
        if( allocated(img_tmp) )then
            deallocate(img_tmp)
        end if
        allocate(img_tmp(xmax, ymax))
        img_tmp = img_in
        write(*,*) "# of iterations =", times_in
        do times = 1, times_in

            do x=2, xmax - 1
                 do y=2, ymax - 1
                     ex_flag = 1
                     do x_sc = x - 1, x + 1
                         do y_sc = y - 1, y + 1
                             if(img_tmp(x_sc, y_sc) < 0)then
                                 ex_flag = -1
                             end if
                        end do
                    end do
                    if(ex_flag == 1)then
                        img_out(x, y) = (sum( &
                                        img_tmp(x - 1: x + 1, y - 1: y + 1)) - &
                                        img_tmp(x, y) ) / 8.0
                    else
                        img_out(x, y) = img_tmp(x, y)
                    end if
                end do
            end do

            img_tmp = img_out

            write(*,*) "iteration:", times, "done"
        end do

    end subroutine smooth_img

    SUBROUTINE resid(arr_1, arr_2)
        REAL, INTENT(IN), DIMENSION(:,:) :: arr_1, arr_2
        REAL(KIND=8)        :: r_pics, sum_tot
        INTEGER(KIND=8)     :: i, j, xmax, ymax

        xmax = ubound(arr_1, 1)
        ymax = ubound(arr_1, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        r = 0.0
        r_2 = 0.0
        sum_tot = 0.0

        Do i = 1, xmax
            Do j = 1, ymax
                if(arr_1(i, j) >= 0.0 .and. arr_2(i, j) >= 0.0 )then
                    sum_tot = sum_tot + arr_2(i, j)
                end if
            End Do
        End Do

        Do i = 1, xmax
            Do j = 1, ymax
                if(arr_1(i, j) >= 0.0 .and. arr_2(i, j) >= 0.0 )then
                    r_pics = abs(arr_1(i, j) - arr_2(i, j))
                    r = r + r_pics
                    r_2 = r_2 + r_pics * r_pics
                    img_out(i, j) = arr_1(i,j) - arr_2(i, j)
                end if
            End Do
        End Do

        r = 100.0 * (r / sum_tot)
        r_2 = 100.0 * (r_2 / sum_tot)


        WRITE(*,*) 'r =', r
        WRITE(*,*) 'r_2 =', r_2

    END SUBROUTINE resid

    SUBROUTINE multi(img_in, nb)
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:)    :: img_in
        REAL(KIND=8), INTENT(IN)                    :: nb
        INTEGER(KIND=8)                             :: i, j, xmax, ymax

        xmax = ubound(img_in, 1)
        ymax = ubound(img_in, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        write(*,*) "nb =", nb

        img_out(:,:) = 0

        Do i = 1, xmax
            Do j = 1, ymax
                if(img_in(i, j) >= 0.0)then
                    img_out(i,j) = img_in(i,j) * nb
                else
                    img_out(i,j) = img_in(i,j)
                end if
            End Do
        End Do

        !img_out(:,:) = img_in(:,:)*nb

        WRITE (*,*) 'done multiplying'

    END SUBROUTINE multi

    SUBROUTINE suma_img(arr_1, arr_2)
        REAL(KIND=8), INTENT(IN), DIMENSION(:,:)    :: arr_1, arr_2
        INTEGER(KIND=8)                             :: i, j, xmax, ymax

        xmax = ubound(arr_1, 1)
        ymax = ubound(arr_1, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        img_out(:,:) = 0

        Do i = 1, xmax
            Do j = 1, ymax
                if(arr_1(i, j) >= 0.0 .and. arr_2(i, j) >= 0.0 )then
                    img_out(i,j) = arr_1(i,j) + arr_2(i,j)
                else
                    if(arr_1(i,j) < arr_2(i,j))then
                        img_out(i,j) = arr_1(i,j)
                    else
                        img_out(i,j) = arr_2(i,j)
                    end if

                end if
            End Do
        End Do

        write(*,*) "two images added"

    END SUBROUTINE suma_img

    subroutine mark_mask(img_in, x1, x2, y1, y2)
        real(kind=8),    dimension(:,:), intent (in)    :: img_in
        integer(kind=8)                , intent (in)    :: x1, x2, y1, y2
        integer(kind=8)                                 :: xmax, ymax, row, col

        xmax = ubound(img_in, 1)
        ymax = ubound(img_in, 2)

        if( allocated(img_out) )then
            deallocate(img_out)
        end if
        allocate(img_out(ymax, xmax))

        Do row = 1, xmax
            Do col = 1, ymax
                if(col > x1 .and. col < x2 .and. row > y1 .and. row < y2)then
                    img_out(row, col) = -1.0
                else
                    img_out(row, col) = img_in(row, col)
                end if
            End Do
        End Do


    end subroutine mark_mask


end module inner_tools
