import tools
#from matplotlib import pyplot as plt
import numpy as np
import fabio
import sys
from arg_interface import get_par

if( __name__ == "__main__" ):

    par_def =(("img_in", "my_img_in.edf"),
              ("img_out", "scaled_img.edf"),
              ("scale", "0.5"))

    #print "\n sys.argv =", sys.argv, "\n"
    in_par = get_par(par_def, sys.argv[1:])

    path_to_img = in_par[0][1]
    file_out =    in_par[1][1]
    scale_fac = np.float64(in_par[2][1])

    #print "\n scale =", scale_fac, "\n"

    ini_img = fabio.open(path_to_img).data.astype(np.float64)
    xres, yres = np.shape(ini_img)

    tools.inner_tools.multi(ini_img, scale_fac)
    arr_out = tools.inner_tools.img_out

    new_img = fabio.edfimage.edfimage()
    print "type(new_img) =", type(new_img)

    new_img.data = arr_out

    new_img.write(file_out)


