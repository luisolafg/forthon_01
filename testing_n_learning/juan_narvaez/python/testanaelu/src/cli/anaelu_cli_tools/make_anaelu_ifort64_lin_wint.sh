#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
INC="-I$CRYSFML/ifort/LibC"
LIB="-L$CRYSFML/ifort/LibC"
LAUESUI="$SXTALSOFT/Laue_Suite_Project"
LIBSTATIC="-lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"
OPT1="-c -O2 -vec-report0"

echo " Program Compilation"

 ifort $OPT1       $LAUESUI/Laue_Modules/Src/ifort_specific.f90
 ifort $OPT1 $INC                                   calc_2d.f90
 ifort $OPT1                                   Anaelu_resid.f90
 ifort $OPT1 $INC -I$WINTER/lib.i64             ed_cell_ane.f90
 ifort $OPT1 $INC -I$WINTER/lib.i64             anaelu_wint.f90

echo " Resource Compilation"
 $WINTER/bin/rc -cif8 -i$WINTER/include anaelu_resource.rc

echo " Linking"
 ifort -o Anaelu_Wint.r *.o -static-intel $LIB $LIBSTATIC -L$WINTER/lib.i64  -lwint -lXm -lXt -lX11


rm *.o *.mod

echo " Runing"
./Anaelu_Wint.r
rm Anaelu_Wint.r


