echo
echo This Source Code Form is subject to the terms of the Mozilla Public
echo License, v. 2.0. If a copy of the MPL was not distributed with this
echo file, You can obtain one at http://mozilla.org/MPL/2.0/.
echo



echo "make_2d_mask  Program Compilation -static"

 gfortran -c -O -ffree-line-length-none -funroll-loops       ..\cli_f90_deps\gfortran_pbar.f90
 gfortran -c -O -ffree-line-length-none -funroll-loops         ..\cli_f90_deps\input_args.f90

 gfortran -c -O -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC  global_types_n_deps.f90
 gfortran -c -O -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC            mic_tools.f90
 gfortran -c -O -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC              calc_2d.f90
 gfortran -c -O -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC      sample_CLI_data.f90

 gfortran -c -O -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC         make_2d_mask.f90

echo " Linking"

 gfortran -o anaelu_calc_mask *.o  -L%CRYSFML%\GFortran\LibC -static -lcrysfml

del *.o *.mod

