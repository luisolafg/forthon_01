! This file is part of the Python - Fortran version of Anaelu project,
! a tool for the treatment of 2D - XRD patterns of textured samples
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this
! file, You can obtain one at http://mozilla.org/MPL/2.0/.
!
Module Calc_2D_pat
   ! -  -  -  -  Use Modules  -  -  -  - !
   use CFML_IO_Formats,                only: err_form_mess,err_form

   use CFML_Structure_Factors,         only: Structure_Factors, Write_Structure_Factors, &
                                             Init_Structure_Factors,Calc_StrFactor
   use CFML_PowderProfiles_CW,         only: PseudoVoigt, Gaussian, Lorentzian
   use CFML_Reflections_Utilities,     only: Hkl_Equiv_List, Unit_Cart_Hkl
   use CFML_Crystallographic_Symmetry, only: Get_Laue_Pg, Set_Spacegroup
   use CFML_Math_3D,                   only: Get_Cart_from_Spher, Get_Spheric_Coord
   use CFML_Geometry_Calc,             only: Angle_Uv, Matrix_Ry, Matrix_Rz, Matrix_Rx
   Use CFML_Math_General,              only: Cosd, Sind, Acosd, atand!, cotan

   use mic_2D_tools,                   only: Get_Laue_Equiv, wrt_pf

   use global_types_and_dependencies

   use compilers_specific

   ! -  -  -  -  Variables  -  -  -  - !
   implicit none
   private
    public ini_2d_det, ini_intens, ipf_ini, ipf_calc, calc_debye_prof, & !wr_img_file, &
           Calc_2D_img, Calc_2D_mask, pf_calc, area_det_intrum, &
           Crystal_Cell_Type, space_group_type, Atom_list_Type

! it is not clear how gfortran imlpements public + save variables
    real                                  , public , save   :: pi = 3.14159265358979323

!    character(len = 256)          :: fname
    real                :: stlmax           ! Maximum Sin(Theta) / Lambda
    real                :: to_deg, to_rad   ! for converting radians to degrees and vise versa

    real                    :: pixel_siz    ! pixel size (mm)
    integer                 :: MaxNumRef
    integer                 :: j, i        ! Reused control cycle variables
    integer                 :: x, y

    contains

    subroutine ini_2d_det()
        to_rad = pi / 180.0
        sm_prt%crys_size = 0.95     ! crystal size
        sm_prt%itm_broad = 0.02     ! instrumental broadening in deg

        pixel_siz =   par_2d%diam_det / ((real(par_2d%xres) + real(par_2d%xres)) / 2.0)

        ! 2D XRD calculated image
        allocate(pat2d(par_2d%xres,par_2d%yres))

        stlmax = sin(par_2d%Thmax * to_rad)  /  par_2d%lambda
        write(unit=*,fmt = "(/,/,6(a,/))")                                                  &
              "          ---------  PROGRAM Anaelu CLI   --------- "                      , &
              "     ******************************************************************"   , &
              "     * Calculates XRD - 2D pattern reading a *.CFL or a *.CIF file      *" , &
              "     ******************************************************************"   , &
              "                      (Luiso - JRC 2014 )"
        to_deg = 180.0 / pi
    return
    end subroutine ini_2d_det


    subroutine ini_intens
        real                :: LorentzF, Bragg, Intens, ang
        real                :: ss,cs
        real                :: I_max = 0, I_scale = 1.0
        real                :: px_deg                         ! local amount of pixels per degree

        if (err_form) then
            write(unit=*,fmt = "(a)") trim(err_form_mess)
        else

            MaxNumRef = get_maxnumref(stlmax,UnitCell%CellVol,mult = SpGroup%NumOps)
            call Hkl_Uni(UnitCell,SpGroup,.true.,0.0,stlmax,"s",MaxNumRef,hkl)

            call Init_Structure_Factors(hkl,At_list,SpGroup,mode = "XRA",lambda = par_2d%lambda)
            call Structure_Factors(At_list,SpGroup,hkl,mode = "XRA",lambda = par_2d%lambda)

            !call Init_Structure_Factors(hkl,At_list,SpGroup,mode = "XRA",lambda = .98)
            !call Structure_Factors(At_list,SpGroup,hkl,mode = "XRA",lambda = .98)

            call Write_Structure_Factors(6,hkl,mode = "XRA")

            pk_lst%npk = hkl%nref

            allocate(pk_lst%pk(hkl%nref))

            do i = 1,hkl%nref
                ss = par_2d%lambda * hkl%ref(i)%S
                cs = sqrt(abs(1.0 - ss * ss))
                !tt = ss / cs
                LorentzF = 0.5 / (ss * ss * cs)
                Bragg = 2.0 * asin(ss)  *  to_deg
                Intens =  (LorentzF * hkl%ref(i)%mult * hkl%ref(i)%Fc ** 2.0)
                !Intens =  LorentzF * hkl%ref(i)%Fc ** 2.0

                pk_lst%pk(i)%H = hkl%ref(i)%H
                pk_lst%pk(i)%Its = Intens

                if( Intens > I_max )then
                    I_max = Intens
                end if
                pk_lst%pk(i)%bragg = Bragg
            end do

            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! start TMP hack
            if( I_max > 10 )then
                I_scale = 10000.0 / I_max
                do i = 1, hkl%nref
                    pk_lst%pk(i)%Its = I_scale * pk_lst%pk(i)%Its
                end do
            end if
            !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! end TMP hack
            do i = 1,hkl%nref

                pk_lst%pk(i)%fwhm = ( 0.9 * par_2d%lambda ) / ( cos(to_rad * pk_lst%pk(i)%bragg / 2.0) * sm_prt%crys_size )

!               average of the Pythagorean sum and linear sum of peak broadening
                pk_lst%pk(i)%fwhm = ( sqrt( pk_lst%pk(i)%fwhm ** 2.0 + sm_prt%itm_broad ** 2.0 ) &
                                        + ( pk_lst%pk(i)%fwhm + sm_prt%itm_broad ) ) &
                                                                                 / 2.0

                pk_lst%pk(i)%dst_px_max_p = par_2d%dst_det * tan((pk_lst%pk(i)%bragg) * to_rad) / pixel_siz
                px_deg = (par_2d%dst_det * tan((pk_lst%pk(i)%bragg + 0.5) * to_rad) -   &
                       par_2d%dst_det * tan((pk_lst%pk(i)%bragg - 0.5) * to_rad)) / pixel_siz

                pk_lst%pk(i)%wt_px = nint(pk_lst%pk(i)%fwhm * px_deg * 60.0)

                pk_lst%pk(i)%dst_px_from = pk_lst%pk(i)%dst_px_max_p - pk_lst%pk(i)%wt_px / 2.0
                pk_lst%pk(i)%dst_px_to = pk_lst%pk(i)%dst_px_max_p + pk_lst%pk(i)%wt_px / 2.0

                if( pk_lst%pk(i)%dst_px_from<0 )then
                    pk_lst%pk(i)%dst_px_from = 0
                end if
                allocate(pk_lst%pk(i)%i_px(1:pk_lst%pk(i)%wt_px))
                do j = 1, pk_lst%pk(i)%wt_px
                    ang = real(j) / real(px_deg)
                    pk_lst%pk(i)%i_px(j) = PseudoVoigt( ang, (/pk_lst%pk(i)%fwhm,0.5 /))
                end do
            end do

            write(unit=*,fmt=*) '____________________________________________________________________________________'
            write(unit=*,fmt=*) '  H   K   L             Angle       Intensities   fwhm(deg)   fwhm(pix)  '//&
           ' from(pix)     to(pix)   max_pos(pix)  '
            write(unit=*,fmt=*) ' '
            do i = 1,hkl%Nref
            write(unit=*,fmt = "(3i4, 2f18.5, f12.5, 4i12 )") hkl%ref(i)%h,pk_lst%pk(i)%bragg , &
                                 pk_lst%pk(i)%Its, pk_lst%pk(i)%fwhm, pk_lst%pk(i)%wt_px,  &
                                 pk_lst%pk(i)%dst_px_from, pk_lst%pk(i)%dst_px_to, &
                                 pk_lst%pk(i)%dst_px_max_p
            end do

        end if
    return
    end subroutine ini_intens

    subroutine ipf_ini()
        integer                             :: n_pn_spgr
        type (space_group_type)             :: L_Grp
        character(len = 10)                   :: l_gr_shar

        sm_prt%ipf_yres =  nint(sm_prt%ipf_xres / 2.0 )
        if( allocated(texture_info%ipf) )then
            deallocate(texture_info%ipf)
        end if
        if( allocated(texture_info%pf) )then
            deallocate(texture_info%pf)
        end if
        if( allocated(texture_info%tmp_pf) )then
            deallocate(texture_info%tmp_pf)
        end if
        allocate(texture_info%ipf(1:sm_prt%ipf_yres,1:sm_prt%ipf_xres))
        allocate(texture_info%pf(1:sm_prt%ipf_yres,1:hkl%nref))
        allocate(texture_info%tmp_pf(1:sm_prt%ipf_yres))
        texture_info%ipf = 0.0
        texture_info%pf = 0.0
        call Get_Laue_Equiv(SpGroup%NumSpg,n_pn_spgr)
        write(unit = l_gr_shar,fmt = '(i3)') n_pn_spgr
        call Set_Spacegroup(SpaceGen = l_gr_shar, SpaceGroup = L_Grp)
        write(unit=*,fmt=*) 'Laue Group       ', L_Grp%CrystalSys
        !sys_crys = L_Grp%CrystalSys

    return
    end subroutine ipf_ini

    subroutine ipf_calc()
        ! prf_max might work as well if is it is a constant
        real                                :: prf_max = 10
        real,dimension(1:3)                 :: prf_hkl ! preferred H K L
        real,dimension(1:3)                 :: uni_vec
        integer                             :: n_op         ! n of  H K L generated
        character(len = 10)                 :: l_gr_shar
        integer                             :: n_pn_spgr,Multip
        real, dimension(:,:), allocatable   :: hkl_lst,tmp_lst
        real, dimension(:,:), allocatable   :: uni_vec_lst
        real                                :: lat, lon
        real                                :: ints_to_add, ang_dst
        type (space_group_type)             :: L_Grp
        read(unit = sm_prt%pref_hkl_string,fmt=*) prf_hkl
        write(unit=*,fmt=*) 'preferred   H  K  L = ', prf_hkl
        n_op = SpGroup%numops * 2
        write(unit=*,fmt=*) 'num ops (SpGroup)', n_op

        call Get_Laue_Equiv(SpGroup%NumSpg,n_pn_spgr)
        write(unit = l_gr_shar,fmt = '(i3)') n_pn_spgr
        call Set_Spacegroup(SpaceGen = l_gr_shar, SpaceGroup = L_Grp)

        write(unit=*,fmt=*) 'Laue Group       ', L_Grp%SPG_Symb,' num ', L_Grp%NumSpg
        n_op = L_Grp%numops * 2
        write(unit=*,fmt=*) 'num ops (Laue Group) ', n_op

        allocate(tmp_lst(1:3,1:n_op))
        call Hkl_Equiv_List(prf_hkl,L_Grp,.false.,Multip,tmp_lst)
        allocate(hkl_lst(1:3,1:Multip))
        write(unit=*,fmt=*) 'Multip  = ', Multip
        hkl_lst = tmp_lst(1:3,1:Multip)

        allocate(uni_vec_lst(1:3,1:Multip))
        do i = 1,Multip,1
            uni_vec_lst(1:3,i) = Unit_Cart_Hkl(hkl_lst(1:3,i), UnitCell)
        end do


        do y = 1, sm_prt%ipf_yres, 1
            call prog_bar(1,y,sm_prt%ipf_yres,' Calculating IPF')
            do x = 1, sm_prt%ipf_xres, 1
                lat = pi * (real(y) / real(sm_prt%ipf_yres))
                lon = pi * (real(x) / real(sm_prt%ipf_xres / 2))
                call Get_Cart_from_Spher(1.0,lat,lon,uni_vec,'rad')
                do i = 1,Multip,1
                    ang_dst = Angle_Uv(uni_vec_lst(1:3,i),uni_vec)
                    ints_to_add = Gaussian(ang_dst,(/sm_prt%prf_width/)) * prf_max
                    texture_info%ipf(y,x) = texture_info%ipf(y,x) + ints_to_add
                end do
            end do
        end do
        write(unit=*,advance = 'yes',fmt = '(A)') char(13)

    return
    end subroutine ipf_calc


    subroutine pf_calc(h_vec, ref_num)
        real, dimension(1:3)               , intent(in) :: h_vec
        integer                            , intent(in) :: ref_num
        real, dimension(1:3)                            :: uni_vec
        integer                             :: p_ang
        integer                             :: pol_res
        real                                :: Ss,pl_ltt,pl_lng, sn_lt, cs_lt, ct_lt
        real                                :: lat, lon, p_ang_rad, cs_p_an, sn_p_an
        real                                :: t,psi_rad,subtot
        integer                             :: lat_i, lon_i, lt_pos, ln_pos

        pol_res =  sm_prt%ipf_yres
        uni_vec = Unit_Cart_Hkl(h_vec, UnitCell)
        call Get_Spheric_Coord(uni_vec,Ss,lat,lon,'rad')

     !!   call from_Cart_to_Spher(uni_vec,lat,lon)

        sn_lt = sin(lat)
        cs_lt = cos(lat)
        ct_lt = 1.0 / tan(abs(lat))
        do p_ang = 1,pol_res,1
            p_ang_rad = pi * (real(p_ang) / real(pol_res))
            cs_p_an = cos(p_ang_rad)
            sn_p_an = sin(p_ang_rad)
            t = 0.0
            do i = 1,pol_res * 2,1
                psi_rad = (real(i) / real(pol_res)) * pi
                pl_ltt = acos( cs_lt * cs_p_an + sn_lt * sn_p_an * cos(psi_rad) )
                pl_lng = lon - acos( (cs_p_an / (sin(pl_ltt) * sn_lt) ) - (1.0 / tan(pl_ltt)) * ct_lt )

                if(pl_ltt<0)then
                    pl_ltt = pl_ltt + pi
                elseif(pl_ltt>pi)then
                    pl_ltt = pl_ltt - pi
                end if

                if(pl_lng<0)then
                    pl_lng = pl_lng + 2.0 * pi
                elseif(pl_lng>2.0 * pi)then
                    pl_lng = pl_lng - 2.0 * pi
                end if
                lat_i = nint((pl_ltt / pi) * real(sm_prt%ipf_yres))
                lon_i = nint((pl_lng / (pi * 2.0)) * real(sm_prt%ipf_xres))

                if (lat_i<1 )then
                    lat_i = 1
                elseif( lat_i>sm_prt%ipf_yres )then
                    lat_i = sm_prt%ipf_yres
                end if

                if( lon_i<1 )then
                    lon_i = 1
                elseif( lon_i>sm_prt%ipf_xres )then
                    lon_i = sm_prt%ipf_xres
                end if
                if(lat_i == 1 .or. lat_i == sm_prt%ipf_yres .or. lon_i == 1 .or. lon_i == sm_prt%ipf_xres )then
                    t = t + texture_info%ipf(lat_i,lon_i)
                else
                    subtot = 0
                    do lt_pos = lat_i - 1,lat_i + 1,1
                        do ln_pos = lon_i - 1,lon_i + 1,1
                            subtot = subtot + texture_info%ipf(lt_pos,ln_pos)
                        end do
                    end do
                    t = t + subtot / 9.0
                end if
            end do
            texture_info%pf(p_ang,ref_num) = real(t) / real(pol_res)
        end do

        do p_ang = 1,pol_res,1
            texture_info%tmp_pf(p_ang) = (texture_info%pf(p_ang,ref_num) + texture_info%pf(pol_res - p_ang + 1,ref_num)) / 2.0
        end do
        texture_info%pf(:,ref_num) = texture_info%tmp_pf

    return
    end subroutine pf_calc

    subroutine calc_debye_prof()

        integer                 :: i, i_az
        integer                 :: i_pol_ang
        real                    :: r_pol_ang
        real , dimension(1:3)   :: h_vec
        real                    :: prom, tot, lat

        do y = 1, sm_prt%ipf_yres, 1
            do x = 1, sm_prt%ipf_xres, 1
                lat = pi * (real(y) / real(sm_prt%ipf_yres))
                tot = tot + texture_info%ipf(y,x) * sin(lat)
            end do
        end do
        write(*,*) "vefore avg: IPF(tot)  = ", tot
        prom = tot / (sm_prt%ipf_yres * sm_prt%ipf_xres)
        write(*,*) "IPF(avg)  = ", prom
        texture_info%ipf = texture_info%ipf / prom
        write(*,*) "after avg: IPF(tot)  = ", tot
        if( allocated(Debye_prof) )then
            deallocate(Debye_prof)
        end if

        allocate(Debye_prof(1:sm_prt%ipf_yres,1:pk_lst%npk))

        do i = 1,pk_lst%npk,1
            call prog_bar(1,i,pk_lst%npk,' Calculating PF')
            h_vec(1:3) = real(pk_lst%pk(i)%H(1:3))
            call pf_calc(h_vec, i)
            do i_az = 1,sm_prt%ipf_yres,1
                r_pol_ang = acos(cosd(pk_lst%pk(i)%bragg / 2.0) * cos(pi * real(i_az) / real(sm_prt%ipf_yres)))
                i_pol_ang = int(sm_prt%ipf_yres * (r_pol_ang / pi) + 1)
                Debye_prof(i_az,i) = texture_info%pf(i_pol_ang,i)
            end do
        end do
        write(unit=*,advance = 'yes',fmt = '(A)') char(13)


    end subroutine calc_debye_prof


    subroutine coord2pos(dst, i_az)
        real     , intent(out)  :: dst        !  distance
        integer  , intent(out)  :: i_az

        real                    :: az
        real                    :: dx, dy     ! two legs of the triangle

        dx = real(x) - par_2d%x_dr
        dy = real(y) - par_2d%y_dr
        dst = sqrt(dx * dx + dy * dy)
        az = atan2(dx,dy)

        az = az + (sm_prt%ipf_ang / 180.0) * 3.14159
        if(az >  3.14159)then
            az = az -  3.14159
        end if
        if(az < 0)then
            az = az +  3.14159
        end if
        ! end Rotating
        i_az = abs(int(real(sm_prt%ipf_yres) * (az / pi)))

        if (i_az > sm_prt%ipf_yres) then
            i_az = sm_prt%ipf_yres
        elseif (i_az < 1) then
            i_az = 1
        end if
    end subroutine coord2pos



    subroutine Calc_2D_img

        integer                 :: i, i_az
        real                    :: dst        !  distance
        integer                 :: pos        ! position in pixels
        !real(kind=8)            :: tmp_ints

        pat2d = 0.0
        !do x = 200, par_2d%xres, 1
        do x = 1, par_2d%xres, 1

            !TODO make sure this is the fastest way to navigate tru data
            !test if inner loop should be arr(x, ...) or arr(... , y)

            call prog_bar(1,x,par_2d%xres,' Calculating IMG')

            do y = 1,par_2d%yres,1

                call coord2pos(dst, i_az)

                do i = 1,pk_lst%npk
                    if(dst>pk_lst%pk(i)%dst_px_from .and. dst<pk_lst%pk(i)%dst_px_to)then
                        pos = abs(dst - pk_lst%pk(i)%dst_px_max_p) + 1
                        if(pos<1)then
                            pos = 1
                        else if (pos>pk_lst%pk(i)%wt_px)then
                            pos = pk_lst%pk(i)%wt_px
                        end if
                        if( (y < par_2d%y_dr .and. (par_2d%up_hlf .eqv. .True.) ) .or. (par_2d%up_hlf .eqv. .False.))then
                            pat2d(x,y) = pat2d(x,y) + pk_lst%pk(i)%i_px(pos) * pk_lst%pk(i)%Its * Debye_prof(i_az,i)
                        end if
                    end if
                end do
                if( pat2d(x,y) == 0 )then
                     pat2d(x,y) = 0.00001
                end if
            end do
        end do
        write(unit=*,advance = 'yes',fmt = '(A)') char(13)

        !TODO the next line should be called from main program
        call wrt_pf()

    return
    end subroutine Calc_2D_img



    subroutine Calc_2D_mask

        integer                 :: i, i_az
        real                    :: dst        !  distance
        integer                 :: pos        ! position in pixels

        pat2d = -1.0
        do x = 1,par_2d%xres,1
            call prog_bar(1,x,par_2d%xres,' Calculating MASK')
            do y = 1,par_2d%yres,1
                call coord2pos(dst, i_az)
                do i = 1,pk_lst%npk
                    if(dst>pk_lst%pk(i)%dst_px_from .and. dst<pk_lst%pk(i)%dst_px_to)then
                        pos = abs(dst - pk_lst%pk(i)%dst_px_max_p) + 1
                        if(pos<1)then
                            pos = 1
                        else if (pos>pk_lst%pk(i)%wt_px)then
                            pos = pk_lst%pk(i)%wt_px
                        end if


                        if( pk_lst%pk(i)%i_px(pos) * Debye_prof(i_az,i) > sm_prt%mask_min .and. &
                          ( (y < par_2d%y_dr .and. (par_2d%up_hlf .eqv. .True.) ) .or. &
                           (par_2d%up_hlf .eqv. .False.) ) )then
                            pat2d(x,y) = 1.0
                        end if

                        ! if we replace the previous << if >> with the next
                        ! commented one it will calculate a leaped stepped mask

                        !if( pk_lst%pk(i)%i_px(pos) > 0.1 &
                        !  .and. Debye_prof(i_az,i) > 0.5 )then
                        !    pat2d(x,y) = 1.0
                        !end if
                    end if
                end do

            end do
        end do
        write(unit=*,advance = 'yes',fmt = '(A)') char(13)


    return
    end subroutine Calc_2D_mask



End Module Calc_2D_pat

