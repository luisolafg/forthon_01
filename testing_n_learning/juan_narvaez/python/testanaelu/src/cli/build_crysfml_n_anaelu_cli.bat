echo
echo This Source Code Form is subject to the terms of the Mozilla Public
echo License, v. 2.0. If a copy of the MPL was not distributed with this
echo file, You can obtain one at http://mozilla.org/MPL/2.0/.
echo


set CLI_MAIN=%cd%

cd crysfml_snapshot_nov_2017
set CRYSFML=%cd%
cd Scripts

call makecrys.bat gfortran

cd %CLI_MAIN%\anaelu_cli_tools

echo my_dir:

cd

dir

call make_anaelu_gfortran.bat
call make_2d_mask_builder.bat

cd %CLI_MAIN%\format_tools

call make_cif2cfl.bat

cd %CLI_MAIN%\..\

if exist build anaelu_tools rd /s /q build anaelu_tools

cd %CLI_MAIN%\anaelu_cli_tools

xcopy /S /Q /Y /F anaelu_calc_mask.exe ..\..\anaelu_tools\
xcopy /S /Q /Y /F anaelu_calc_xrd.exe ..\..\anaelu_tools\

cd %CLI_MAIN%\..\gui

python path.py
python install_ANAELU.py

cd %CLI_MAIN%\..\gui\dist

move ANAELU_run.exe ..\gui
move ANAELU_unins.exe ..\gui




cd %CLI_MAIN%
