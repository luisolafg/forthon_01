import sys
import os
from PySide2 import QtUiTools
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *
import subprocess
import fabio
import numpy as np
from img_rgb_gen import np2bmp_heat, np2bmp_diff
from grid_atoms_xyz import read_cfl_atoms,AtomData
from readcfl import read_cfl_file,CflData
from readdat import read_dat_file,InstrumentData
import time

def tmp_fix_edf(edf_in, edf_out):
    img_arr = fabio.open(edf_in).data.astype(np.float64)
    res_img = fabio.edfimage.edfimage()
    res_img.data = img_arr
    res_img.write(edf_out)

class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)

    def wheelEvent(self, event):
        int_delta = int(event.delta())
        self.m_scrolling.emit(int_delta)
        event.accept()


class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("Anaelu_gui.ui")
        self.window.setWindowTitle("ANAELU 2.0")
        self.window.setWindowIcon(QIcon("anaelu.png"))

        self.gscene_list = []

        for n in range(4):
            new_gsecene = MultipleImgSene()
            new_gsecene.m_scrolling.connect(self.scale_all_views)
            self.gscene_list.append(new_gsecene)

        self.gview_list = [self.window.graphicsView_Exp, self.window.graphicsView_Mask_n_Diff,
                           self.window.graphicsView_Modl, self.window.graphicsView_Bakgr]

        for n, gview in enumerate(self.gview_list):
            gview.setScene(self.gscene_list[n])
            gview.setDragMode(QGraphicsView.ScrollHandDrag)

        #utilities for buttons bottom part
        self.window.run_model.clicked.connect(self.run_model_connect)

        self.window.remodel_comparison.clicked.connect(self.on_remodel)

        self.window.pushButton_mask_model.clicked.connect(self.onmask_run)
        self.window.cut_mask_peaks.clicked.connect(self.cut_mask_run)
        self.window.background_smoothing.clicked.connect(self.smooth_run)

        self.window.scale_sum_back_diff.clicked.connect(self.scale_sum_back_diff_connect)

        #Add row delete row Table Widget
        self.window.del_row_button.clicked.connect(self.remove_r)
        self.window.add_row_button.clicked.connect(self.add_r)

        #open read cfl Tabs
        self.window.actionOpen_Cfl.triggered.connect(self.actionOpen_Cfl_connect)

        self.window.actionSave_Cfl.triggered.connect(self.savefile_cfl)

        #open read dat And write Dat
        self.window.actionOpen_Dat.triggered.connect(self.actionOpen_Dat_connect)

        self.window.actionSave_Dat.triggered.connect(self.savefile_dat)

        #Load image bottom
        self.window.pushButton_load_exp.clicked.connect(self.load_exp_img)
        #self.window.pushButton_load_exp_top_right.clicked.connect(self.new_value_cfl)
        #self.window.pushButton_load_exp_bottom_right.clicked.connect(self.new_value_dat)

        #plain text to modify for the user
        self.window.mask_treshold_edit.setText("0.5")
        self.window.times_smooth_edit.setText("15")
        self.window.scale_factor_edit.setText("1.0")
        self.window.residual_edit.setText("None")

        #colums table widget
        columns = ['z','Label','Xpos','Ypos','Zpos','DWF']
        self.window.tableWidget_ini.setColumnCount(len(columns))
        self.window.tableWidget_ini.setHorizontalHeaderLabels(columns)

        #Tooltip add
        self.window.pushButton_load_exp.setToolTip("Load exp tooltip")
        self.window.pushButton_mask_model.setToolTip("Draw mask tooltip")
        self.window.run_model.setToolTip("Run model tooltip")
        self.window.background_smoothing.setToolTip("Smooth tooltipgit")

        for gview in self.gview_list:
            gview.verticalScrollBar().valueChanged.connect(self.scroll_all_v_bars)
            gview.horizontalScrollBar().valueChanged.connect(self.scroll_all_h_bars)

        self.real_time_zoom = False
        self.value_backup_h = 0
        self.value_backup_v = 0

        self.bmp_heat = np2bmp_heat()
        self.bmp_diff = np2bmp_diff()
        self.add = 1

        self.window.show()

    def run_model_connect(self):
        self.on_run()

    def actionOpen_Dat_connect(self):
        self.read_dat()

    def scale_sum_back_diff_connect(self):
        self.scale_run()
        self.imgadd_run()
        self.R_run()

    def on_remodel(self):
        self.run_model_connect()
        self.scale_sum_back_diff_connect()

    def actionOpen_Cfl_connect(self):
        self.read_cfl()


    def scroll_all_v_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.verticalScrollBar().setValue(value)

            self.value_backup_v = value

    def scroll_all_h_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.horizontalScrollBar().setValue(value)

            self.value_backup_h = value

    def scale_all_views(self, scale_fact):
        self.real_time_zoom = True
        for gview in self.gview_list:
            gview.scale(1.0 + float(scale_fact) / 2500.0,
                        1.0 + float(scale_fact) / 2500.0)

        self.real_time_zoom = False

    def set_img_n(self, img_path, gscene_num):
        image1 = QImage(img_path)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.gscene_list[gscene_num].addPixmap(self.pixmap_1)

        self.num_to_move = gscene_num
        QTimer.singleShot(100, self.emit_move)


    def emit_move(self):
        self.pos_img_n(self.num_to_move)

    def pos_img_n(self, gview_num):
        self.gview_list[gview_num].horizontalScrollBar().setValue(self.value_backup_h)
        self.gview_list[gview_num].verticalScrollBar().setValue(self.value_backup_v)

    def setqimg(self, img_path, gscene_num):
        self.pixmap_1 = QPixmap.fromImage(img_path)
        self.gscene_list[gscene_num].addPixmap(self.pixmap_1)
        #self.bmp_heat = np2bmp_heat()
        self.num_to_move = gscene_num
        QTimer.singleShot(100, self.emit_move)

    def load_exp_img(self):
        print("self.btn_clk")
        self.info1 = QFileDialog.getOpenFileName(self, 'Open Image File', ".", "Files (*.*)")[0]
        ###############################################################
        img_in = fabio.open(self.info1).data.astype(np.float64)

        res_img = fabio.edfimage.edfimage()
        res_img.data = img_in
        res_img.write(str("img_64bit_xrd.edf"))

        ###############################################################
        self.path = "img_64bit_xrd.edf"
        img_arr = fabio.open(self.path).data.astype("float64")

        #img_arr.convert("edf").save("my_test.edf")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )


        self.setqimg(q_img, 0)


    def set_img3(self):
        self.set_img_n("../../lena_gray_inverted", 2)
        print ("run model")

    def set_img4(self):
        self.set_img_n("../../lena_gray_inverted.jpeg", 3)

    def remove_r(self):
        print("Row removed")
        self.window.tableWidget_ini.removeRow(self.window.tableWidget_ini.model().rowCount()-1)

    def add_r(self):
        print("Row Added")
        self.window.tableWidget_ini.insertRow(self.window.tableWidget_ini.model().rowCount())
        print(self.window.tableWidget_ini.model().rowCount())

    def read_cfl(self):
        self.info1 = QFileDialog.getOpenFileName(self, 'Open CFL File', ".", "Files (*.cfl)")[0]
        print("info from clf", self.info1)

        self.data = read_cfl_file(my_file_path = self.info1)

        self.window.title_edit.setText(str(self.data.title))
        self.window.spgr_edit.setText(str(self.data.spgr))
        self.window.a_edit.setText(str(self.data.a))
        self.window.b_edit.setText(str(self.data.b))
        self.window.c_edit.setText(str(self.data.c))
        self.window.a2_edit.setText(str(self.data.alpha))
        self.window.b2_edit.setText(str(self.data.beta))
        self.window.c2_edit.setText(str(self.data.gamma))
        self.window.sizelg_edit.setText(str(self.data.size_lg))
        self.window.ipfres_edit.setText(str(self.data.ipf_res))
        self.window.h_edit.setText(str(self.data.h))
        self.window.k_edit.setText(str(self.data.k))
        self.window.l_edit.setText(str(self.data.l))
        self.window.hklwh_edit.setText(str(self.data.hklwh))
        self.window.azmipf_edit.setText(str(self.data.azm_ipf))
        self.window.mask_treshold_edit.setText(str(self.data.mask_min))

        self.info2 = read_cfl_atoms(my_file_path = self.info1)
        self.window.tableWidget_ini.setRowCount(len(self.info2))
        for n,atm in enumerate(self.info2):

            self.window.tableWidget_ini.setItem(n,0,QTableWidgetItem(atm.Zn))
            self.window.tableWidget_ini.setItem(n,1,QTableWidgetItem(atm.Lbl))
            self.window.tableWidget_ini.setItem(n,2,QTableWidgetItem(atm.x))
            self.window.tableWidget_ini.setItem(n,3,QTableWidgetItem(atm.y))
            self.window.tableWidget_ini.setItem(n,4,QTableWidgetItem(atm.z))
            self.window.tableWidget_ini.setItem(n,5,QTableWidgetItem(atm.b))


    def savefile_cfl(self):

        self.cfl_file_name = QFileDialog.getSaveFileName(self, 'Save File', os.getenv('HOME'), filter='*.cfl')[0]
        print("save from cfl1=", self.cfl_file_name,"\n")
        self.write_out_cfl()

    def write_out_cfl(self):

        save_atm = []
        print("range of table=",range(self.window.tableWidget_ini.rowCount()),"\n")
        for d in range(self.window.tableWidget_ini.rowCount()):
            print("print d= ", d)
            atm = AtomData()
            atm.Zn = str(self.window.tableWidget_ini.item(d, 0).text())
            atm.Lbl = str(self.window.tableWidget_ini.item(d, 1).text())
            atm.x = float(self.window.tableWidget_ini.item(d,2).text())
            atm.y = float(self.window.tableWidget_ini.item(d,3).text())
            atm.z = float(self.window.tableWidget_ini.item(d,4).text())
            atm.b = float(self.window.tableWidget_ini.item(d,5).text())

            save_atm.append(atm)
        print("data save_atom",save_atm)

        data = CflData()
        data.title = self.window.title_edit.text()
        data.spgr = self.window.spgr_edit.text()
        data.a = self.window.a_edit.text()
        data.b = self.window.b_edit.text()
        data.c = self.window.c_edit.text()
        data.alpha = self.window.a2_edit.text()
        data.beta = self.window.b2_edit.text()
        data.gamma = self.window.c2_edit.text()
        data.size_lg = self.window.sizelg_edit.text()
        data.ipf_res = self.window.ipfres_edit.text()
        data.h = self.window.h_edit.text()
        data.k = self.window.k_edit.text()
        data.l = self.window.l_edit.text()
        data.hklwh = self.window.hklwh_edit.text()
        data.azm_ipf = self.window.azmipf_edit.text()
        data.mask_min = self.window.mask_treshold_edit.text()

        crystal = "Title  "
        crystal += str(data.title) + " \n"
        crystal += "!" + "        a" + "        b" + "        c" + \
                "     alpha" + "    beta" + "   gamma" + "\n"
        crystal += "Cell    " + str(data.a) + "     " + str(data.b) + "     " +\
                str(data.c) + "  " + str(data.alpha) + "  " + str(data.beta) + \
                "  " + str(data.gamma) + "\n"
        crystal += "!     " + "Space Group" + "\n"
        crystal += "Spgr  " + str(data.spgr) + "\n"
        crystal += "SIZE_LG " + str(data.size_lg) + "\n"
        crystal += "IPF_RES " + str(data.ipf_res) + "\n"
        crystal += "HKL_PREF " + str(data.h) + " " + str(data.k) + " " + str(data.l) + "\n"
        crystal += "HKL_PREF_WH " + str(data.hklwh) + "\n"
        crystal += "AZM_IPF " + str(data.azm_ipf) + "\n"
        crystal += "MASK_MIN " + str(data.mask_min) + "\n"
        crystal += "!" + "\n"

        cfl_data = str(crystal)

        self.info2 = open(self.cfl_file_name, 'w')
        for c in save_atm:
            print("data of table=",c.Zn,c.Lbl,c.x,c.y,c.z,c.b,"\n")
            var = "Atom "
            var += str(c.Zn) + "  "
            var += str(c.Lbl) + "  "
            var += str(c.x) + "  "
            var += str(c.y) + "  "
            var += str(c.z) + "  "
            var += str(c.b) + " \n"
            cfl_data += str(var)

        self.info2.write(cfl_data)
        self.info2.close()

    def read_dat(self):
        self.info1 = QFileDialog.getOpenFileName(self, 'Open DAT File', ".", "Files (*.dat)")[0]
        print("info from dat", self.info1)

        self.dt = read_dat_file(my_file_path = self.info1)

        self.window.x_beam_edit.setText(str(self.dt.x_res))
        self.window.y_beam_edit.setText(str(self.dt.y_res))
        self.window.x_max_edit.setText(str(self.dt.x_beam))
        self.window.y_max_edit.setText(str(self.dt.y_beam))
        self.window.wavelength_edit.setText(str(self.dt.lambd))
        self.window.sample_detector_distance_edit.setText(str(self.dt.dst_det))
        self.window.detector_diameter_edit.setText(str(self.dt.diam_det))
        self.window.upper_half_only_check_box.toggle()

    def savefile_dat(self):

        self.info_dat = QFileDialog.getSaveFileName(self, 'Save File', os.getenv('HOME'), filter='*.dat')[0]
        print("save from dat=", self.info_dat,"\n")
        self.write_new_params()
    
    def write_new_params(self):

        dt = InstrumentData()
        dt.x_res = self.window.x_beam_edit.text()
        dt.y_res = self.window.y_beam_edit.text()
        dt.x_beam = self.window.x_max_edit.text()
        dt.y_beam = self.window.y_max_edit.text()
        dt.lambd = self.window.wavelength_edit.text()
        dt.dst_det = self.window.sample_detector_distance_edit.text()
        dt.diam_det = self.window.detector_diameter_edit.text()

        crystal = "xres" + "\n" + str(dt.x_res) + " \n"
        crystal += "yres"  + "\n" + str(dt.y_res) + "\n"
        crystal += "xbeam" + "\n" + str(dt.x_beam) + "\n"
        crystal += "ybeam" + "\n" + str(dt.y_beam) + "\n"
        crystal += "lambda" + "\n" + str(dt.lambd) + "\n"
        crystal += "dst_det" + "\n" + str(dt.dst_det) + "\n"
        ##### Temporal
        crystal += "UPPER_HALF" + "\n"
        ##### Temporal
        crystal += "diam_det" + "\n" +str(dt.diam_det) + "\n"
        ##### Temporal
        crystal += "thet_min" + "\n"+ "0" + "\n"
        ##### Temporal
        crystal += "end"

        dat_data = str(crystal)
        self.info2 = open(self.info_dat, 'w')
        self.info2.write(dat_data)
        self.info2.close()



    #buttons test
    def on_run(self):
        self.cfl_file_name = "out_Cfl.cfl"
        self.write_out_cfl()

        self.info_dat = "new_params.dat"
        self.write_new_params()

        subprocess.call("anaelu_calc_xrd.exe new_params.dat out_Cfl.cfl \
                tmp_img_xrd.edf raw_img_file.raw")

        tmp_fix_edf("tmp_img_xrd.edf", "img_xrd.edf")

        img_arr = fabio.open("img_xrd.edf").data.astype("float64")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )

        self.setqimg(q_img, 2)

    def onmask_run(self):
        self.cfl_file_name = "out_Cfl.cfl"
        self.write_out_cfl()

        self.info_dat = "new_params.dat"
        self.write_new_params()

        subprocess.call("anaelu_calc_mask.exe new_params.dat out_Cfl.cfl \
            tmp_img_xrd.edf raw_img_file.raw")

        tmp_fix_edf("tmp_img_xrd.edf", "dummy.edf")


        img_arr = fabio.open("dummy.edf").data.astype("float64")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )


        self.setqimg(q_img, 1)

    def cut_mask_run(self):
        subprocess.call("anaelu_img_cut.exe img_64bit_xrd.edf dummy.edf tmp_img_xrd.edf")

        tmp_fix_edf("tmp_img_xrd.edf", "cuted.edf")

        img_arr = fabio.open("cuted.edf").data.astype("float64")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )

        self.setqimg(q_img, 3)

    def smooth_run(self):
        smooth_times = self.window.times_smooth_edit.text()

        smooth_func = "anaelu_img_smooth.exe " + "cuted.edf "
        smooth_func += "tmp_img_xrd.edf " + str(smooth_times)
        subprocess.call(smooth_func)

        tmp_fix_edf("tmp_img_xrd.edf", "img_smooth.edf")

        img_arr = fabio.open("img_smooth.edf").data.astype("float64")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )

        self.setqimg(q_img, 3)

    def scale_run(self):
        scale_times = self.window.scale_factor_edit.text()

        scale_func = "anaelu_img_scale.exe " + "img_xrd.edf "
        scale_func += "tmp_img_xrd.edf " + str(scale_times)
        subprocess.call(scale_func)

        tmp_fix_edf("tmp_img_xrd.edf", "scaled_img.edf")

        img_arr = fabio.open("scaled_img.edf").data.astype("float64")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )

        #self.setqimg(q_img, 1)

    def imgadd_run(self):
        subprocess.call("anaelu_img_add.exe scaled_img.edf img_smooth.edf tmp_img_xrd.edf")

        tmp_fix_edf("tmp_img_xrd.edf", "img_sum.edf")

        img_arr = fabio.open("img_sum.edf").data.astype("float64")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )

        self.setqimg(q_img, 2)

    def R_run(self):

        subprocess.call("anaelu_dif_res.exe img_sum.edf img_64bit_xrd.edf tmp_img_xrd.edf info_dif.dat")

        tmp_fix_edf("tmp_img_xrd.edf", "img_diff.edf")

        my_file_path = "info_dif.dat"
        print ("my_file_path =", my_file_path)

        myfile = open(my_file_path, "r")

        all_lines = myfile.readlines()
        myfile.close()

        r_found = None
        r_2_found = None

        for lin_char in all_lines:
            commented = False
            for delim in ',;=':
                lin_char = lin_char.replace(delim, ' ')

            lst_litle_blocks = lin_char.split()
            for pos, litle_block in enumerate(lst_litle_blocks):

                if( litle_block == "!" or litle_block == "#" ):
                    commented = True

                elif( litle_block.upper() == "R" ):
                    print ("found r")
                    r_found = float(lst_litle_blocks[pos+1])

                elif( litle_block.upper() == "R_2" ):
                    print ("found r_2")
                    r_2_found = float(lst_litle_blocks[pos+1])

        print ("r_found =", r_found)
        print ("r_2_found =", r_2_found)

        self.window.residual_edit.setText(str(r_found))

        img_arr = fabio.open("img_diff.edf").data.astype("float64")

        rgb_np = self.bmp_diff.img_2d_rgb(img_arr)

        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )

        self.setqimg(q_img, 1)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
