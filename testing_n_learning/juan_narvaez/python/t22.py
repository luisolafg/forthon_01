#Second rank tensors
import numpy as np
from numpy import sin,cos,pi,abs # makes the code more readable
from mayavi import mlab

#MPOD 360  LiCoPO4   Structure Point group mmm':
#d11=0;      d12=2;     d13=0
#d21=1;      d22=0;     d23=0
#d31=0;      d32=0;     d33=0

#Factor de Debye anisotropico Al2MgO4:
#d11=0.00462;      d12=-0.00014;     d13=-0.00014
#d21=-0.00014;      d22=0.00462;     d23=-0.00014
#d31=-0.00014;      d32=-0.00014;     d33=0.00462

#Factor de Debye anisotropico Fe2.565O4Si0.435 (ICSD 85698):
#d11=86;      d12=0;     d13=0
#d21=0;      d22=7.3;     d23=0
#d31=0;      d32=0;     d33=32.8

#Factor de Debye anisotropico O1 en yeso':
#d11=68;      d12=-8;     d13=31
#d21=-8;      d22=85;     d23=26
#d31=31;      d32=26;     d33=77

#Calcite thermal expansion:
d11=-6;  d12=0;  d13=0
d21=0;   d22=-6; d23=0
d31=0;   d32=0;  d33=25

phi, beta = np.mgrid[0:np.pi:180j,0:2*np.pi:360j]
x = sin(phi)*cos(beta)
y = sin(phi)*sin(beta)
z = cos(phi)

D=d11*(x**2) + d22*(y**2) + d33*(z**2) + (x*y)*(d12+d21) + (x*z)*(d13+d31) + (y*z)*(d23+d32)
#D=d11*(x**2) + d22*(y**2) + d33*(z**2) + np.abs(x*y)*(d12+d21) + np.abs(x*z)*(d13+d31) + np.abs(y*z)*(d23+d32)

x1 = D*sin(phi)*cos(beta)
y1 = D*sin(phi)*sin(beta)
z1 = D*cos(phi)
mlab.figure()

#mlab.mesh(x1,y1,z1,scalars=np.abs(D))
#mlab.mesh(x1,y1,z1)
mlab.mesh(x1,y1,z1, scalars=D)

mlab.scalarbar(orientation="vertical")
mlab.show()
