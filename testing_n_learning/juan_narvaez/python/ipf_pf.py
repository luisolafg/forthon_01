print("Modeling pole figure (PF) from given inverse pole figure (IPF)")
import numpy as np ; import math
import scipy.integrate as sq
import matplotlib.pyplot as plt
from numpy import sin,cos,pi,abs, arccos, exp, tan
from mpl_toolkits.mplot3d import Axes3D
from product_vectors_abel import Ortorrombic
from product_vectors import Trigonal
from Pandas import Arrays

#pzt
a = 3.98 ; alpha = 90
b = 3.98 ; beta = 90
c = 4.04 ; gamma = 90

#calling function
if a == b and b == c and c == a and alpha == beta and beta ==gamma and gamma == alpha \
or a == b and b < c and c > a and alpha == beta and beta ==gamma and gamma == alpha:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = \
        Ortorrombic(a,b,c,alpha,beta,gamma)
if a == b and b < c and c > a and alpha == beta and beta < gamma and gamma > alpha \
or b > c and c > a and alpha == gamma and beta > alpha and beta > gamma:
    a,b,c,alpha,beta,gamma,thetadeg,phideg,deltadeg,iotadeg,taudeg,gammadeg = \
        Trigonal(a,b,c,alpha,beta,gamma)

# Defining parameters:
rtg = 180.0 / pi ; widthdeg = 30.0
#thetadeg = 90.0; phideg = 0.00; 
#taudeg = 90.0 ; gammadeg = 0.0
if thetadeg == 0.0: thetadeg = 0.01
if taudeg == 0.0: taudeg = 0.01
theta = thetadeg/rtg; phi = phideg/rtg; distwidth = widthdeg/rtg
tau = [x/rtg for x in taudeg] ; gamma = [x/rtg for x in gammadeg] 
print (" Max of IPF at theta, phi (degrees) = ", thetadeg, phideg)
print (" Max of IPF at theta, phi (radians) = ", theta, phi)
print (" IPF width (degrees and radians) = ", widthdeg, distwidth)
print (" Pole figure tau, gamma (degrees) = ", taudeg, gammadeg)
print (" Pole figure tau, gamma (radians) = ", tau, gamma)

# Defining functions:
def R1(eta, chi):
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    if d >= 1.0: d = 1.0
    elif -1.0 < d < 1.0 : d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    elif d <= -1.0 : d = -1.0
    rho = arccos(d)
    return exp(-0.693*((rho/distwidth)**2))*sin(eta)
def Rnorma(eta, chi):
    return (4.0*pi/R0)*R1(eta, chi)
def Rvar(psi,fi,tau,gamma):
    A = cos(tau)*cos(fi) + sin(tau)*sin(fi)*cos(psi)
    eta = arccos(A)
    cteta = 1.0/tan(eta); cttau = 1.0/tan(tau)
    B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    if np.all(B >= 1.0): B = 1.0
    elif np.all(-1.0 < B < 1.0) : B = cos(fi)/(sin(eta)*sin(tau)) - cteta*cttau
    elif np.all(B <= -1.0) : B = -1.0
    if psi <= pi:
        chi = gamma - arccos(B)
    else:
        chi = gamma + arccos(B)
    d = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
    rho = arccos(d)
    return (4.0*pi/R0)*exp(-0.693*((rho/distwidth)**2))

def pfg(fi):
    pfgr = sq.quad(Rvar, 0, 2.0*pi)
    return pfgr[0]

# Calculating R surface integral:
R1suma = sq.dblquad(R1, 0, 2.0*pi, 0, pi)
R0 = R1suma[0]
print (" Integral of initial IPF, R0 = ", R0)

# Verifying normalization:
Rnormsum = sq.dblquad(Rnorma, 0, 2.0*pi, 0, pi)
Rnorsu = Rnormsum[0]
print (" Integral of the normalized IPF = ", Rnorsu)
print (" 4*pi = ", 4.0*pi)

# Maximum of normalized IPF, R:
print (" R_normalized(max) =", Rnorma(theta, phi)/sin(theta))
print ()

# Calculating the inverse pole figure surface:
eta, chi = np.meshgrid(np.linspace (0, pi, 36) , np.linspace(0, 2.0*pi,72))
#calculating R
p = cos(eta)*cos(theta)+sin(eta)*sin(theta)*cos(chi-phi)
p1 = arccos(p)
R = (4.0*pi/R0)*exp(-0.693*((p1/distwidth)**2))
#plotting inverse pole figure
fig= plt.figure()
ax = Axes3D(fig)
ax.plot_surface(eta*rtg, chi*rtg, R, cmap='viridis', linewidths=0.2)
ax.set_xlim(0, 360); ax.set_ylim(0, 360); ax.set_zlim(0, 11)
plt.show()

#Calculating and plotting the direct pole figure curve:
fidg = np.zeros(180); pfg = np.zeros(180)
fideg = 0.0; delfideg = 5
#tau = tau[2]; gamma = gamma[2]
print ("tau,gamma pf= ",tau,gamma)
Npeak = np.mgrid[1:12:12j]
print(Npeak,"Npeak")
while Npeak <= 12:
    tau[Npeak] = tau
    gamma[Npeak] = gamma
    while fideg <= 180.0:
        n = int(fideg/5)
        fidg[n] = fideg
        fi = fideg/rtg
        print ("fi values", fi)
        pf = sq.quad(Rvar, 0, 2.0*pi)
        pfg[n] = pf[0]/(2.0*pi)
        print (" n, fidg[n], Pole Figure[n] = ", n, fidg[n], pfg[n])
        fideg = fideg + delfideg

# fi= np.linspace(0,180,91)
# fi = [x/rtg for x in fi]
# tau = tau[4]
# tau = np.tile(tau,len(fi))
# gamma = gamma[4]
# gamma = np.tile(gamma,len(fi))
# print("new fi values for pf figures",fi, "\n")
# print("new tau values for pf figures",tau, "\n")
# print("new gammas values for pf figures",gamma, "\n")

# zip_test = zip(fi,tau,gamma)
# print("super list of list", list(zip_test))

# onedim_pf = []
# for i,j,k in zip(fi,tau,gamma):
#     oned_pf, error = sq.quad(Rvar,0, 2.0*pi, args=(i,j,k))
#     onedim_pf.append(oned_pf)
# onedim_pf = np.array(onedim_pf)
# onedim_pf1 = [x/(2.0*pi) for x in onedim_pf]
#onedim_pf1 = [(0.0) if math.isnan(i) else i for i in onedim_pf1]
# print("pf value for graphs =", onedim_pf1, "\n")

plt.plot(fidg, pfg)
#plt.plot(fi, onedim_pf1)
plt.show()

file = open("polefigure001.txt", "w")
for index in range(len(fi)):
    file.write(str(fi[index]) + " " + str(onedim_pf1[index]) + "\n")
file.close()

