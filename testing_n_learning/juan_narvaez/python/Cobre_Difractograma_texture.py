import numpy as np
from Angles import ph
def do_all():


    #Column 1 line
    #Line  = [1,2,3,4,5,6,7,8]

    #Column 2 planos
    h = [1,2,2,3,2,4,3,4]
    k = [1,0,2,1,2,0,3,2]
    l = [1,0,0,1,2,0,1,0]


    #Column 3
    plane = []
    for i in range(0, len(h)):
        plane.append(h[i]**2 + k[i]**2 + l[i]**2)
    print(plane)

    #Column 2 plane

    hkl = [[1,1,1],[2,0,0],[2,2,0],[3,1,1],[2,2,2],[4,0,0],[3,3,1],[4,2,0]]

    #Column 3
    squares = []

    for i in hkl:
        squares.append(float(i[0]**2.0 + i[1]**2.0 + i[2]**2.0))
    print("squares",squares,"\n")

    #column 4 sin**2(theta)

    wavelength = 1.542 #angstroms
    a = 4.3 #angstroms


    valsintheta = ( wavelength**2.0  )
    valsintheta2 = ( 4.0*(a**2.0) )
    valsintheta3 = valsintheta/valsintheta2

    sin2theta = [i * valsintheta3 for i in squares]

    print (valsintheta3)
    print ("sin2theta",sin2theta,"\n")

    #column 5 sintheta

    sintheta = [ i ** 0.5 for i in sin2theta]

    print ("sinthetha",sintheta,"\n")

    #column 6 theta

    import math

    theta = [math.asin(i) for i in sintheta]
    pi = 3.14159265358

    print ("theta",theta,"\n")

    todeg = (180.0/pi)
    angle = [ i * todeg for i in theta]

    print ("Angulos",angle,"\n")

    #column 7 sinthetawave

    sinthetawave = [ i * 1/wavelength for i in sintheta]

    print("sinthetawave",sinthetawave,"\n")

    #column 8
    #Crommer and Mann Coeficients

    a1 = 13.33800
    a2 = 7.167600
    a3 = 5.615800
    a4 = 1.673500
    c = 1.191000
    b1 = 3.582800
    b2 = 0.2470000
    b3 = 11.39660
    b4 = 64.81260

    #factor de dispersion
    fd = []
    for i in theta:

        fd.append((a1 * math.exp( -b1 * (math.sin(i) / wavelength) ** 2.0) ) \
                + (a2 * math.exp( -b2 * (math.sin(i) / wavelength) ** 2.0) ) \
                + (a3 * math.exp( -b3 * (math.sin(i) / wavelength) ** 2.0) ) \
                + (a4 * math.exp( -b4 * (math.sin(i) / wavelength) ** 2.0) ) )

    fd1 = []
    for i in fd:
        fd1.append(i+c)


    print ("dispersion factor",fd,"\n")

    print ("correction d. factor",fd1,"\n")


    #Column 9

    fe = [16.0*i**2.0 for i in fd1]



    print("fe",fe,"\n")


    #column 10 multiplcity
    mult = []
    for i in hkl:
        print("hkl=",i)
        if i[0] == i[1] and i[0] == i[2] and i[0] == i[2]:
            mult.append(8)
            print("multiplcity=8")

        if (i[0] == 0 and i[1] == 0 and i[2] > 0) or (i[0] > 0 and i[1] == 0 and i[2] == 0):
            mult.append(6)
            print("multiplcity=6")

        if (i[0] == 0 and i[1] > 0 and i[2] > 0 and i[1] == i[2]) or (i[0] > 0 and i[1] > 0 and i[2] == 0 and i[0] == i[1]):
            mult.append(12)
            print("multiplcity=12")

        if (i[0] > 1 and i[1] > 0 and i[2] > 0 and i[1] == i[2] or i[0] == i[1] and i[2] > 0 and i[0] > 2 and i[1] > 2 or i[0] > 2 and i[1] > 1 and i[2] == 0 or i[0] == 0 and i[1] > 1 and i[2]> 2):
            mult.append(24)
            print("multiplcity=24")

        if (i[0] > 0 & i[1] >0  & i[2] > 0) :
            mult.append(48)
            print("multiplcity=48")

    print("multiplcity",mult,"\n")


    #column 11
    hdst = []
    for i in theta:
        hds = (1.0 + math.cos(2.0*i)**2.0)
        hds1 =  math.sin(i)**2.0
        hds2 =  math.cos(i)

        print("hds",hds)
        print("hds1",hds1)
        print("hds2",hds2,"\n")
        hdst.append( hds / (hds1*hds2) )

    print("hdst",hdst,"\n")

        #hds.append(1.0+math.cos(2.0*i)**2.0)/((math.sin(i)**2.0)(math.cos(i)))

    #column 12 Intensity

    I = [fe[i] * mult[i] * hdst[i] for i in range(len(fe))]

    print("Intensity",I,"\n")

    #calling for graph angle vs Intensity
    import numpy as np
    ir = np.round(I,0)
    print("Intensity rounded: ",ir,"\n")

    twoangle = [2*i for i in angle]
    twoanglec = np.round(twoangle,0)
    print("angle rounded: ",twoanglec,"\n")

    difract = np.column_stack((ir, twoanglec))
    print("pair array for difract: ",difract,"\n")

    polef = [1.0 , 0 , 0 , 0 , 0 , 0 , 0, 0]
    list_polef = np.multiply(polef,ph())
    print("new texture", list_polef, "\n")

    ir_texture =np.multiply(polef,ir)
    print("texture array", ir_texture, "\n")

    ir_texture_test = np.multiply(list_polef,ir)

    difract_texture = np.column_stack((ir_texture_test, twoanglec))
    print("Array for texture:", difract_texture, "\n")

    return difract_texture


if __name__ == "__main__":
    result = do_all()

    #test






