import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *



class MyPopupDialog(QObject):
    def __init__(self, parent=None):
        super(MyPopupDialog, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("Dialog.ui") 
        self.window.show()
        self.window.update()

    def paintEvent(self,event):
            qp = self.window.QPainter(self.window.drawing_widget)
            print("hola=", qp)
            qp.begin(self.window.drawing_widget)
            qp.drawEllipse(40, 40, 400, 400)
            qp.end()


            
    

class Form(QObject):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("Main_Win.ui")
        self.window.push_Pop_Button.clicked.connect(self.popwindow)
        self.window.show()

    def popwindow(self):
        self.dialog = MyPopupDialog(self)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()

    sys.exit(app.exec_())
