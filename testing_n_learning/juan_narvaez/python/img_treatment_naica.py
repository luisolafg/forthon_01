import fabio
from matplotlib import pyplot 
import numpy as np


A1 = fabio.open("SWESH001_phi_1_to_3_90s_01_02_06111639.mar2300")
print ("Data array",A1.data, "\n")
print ("Header :",A1.header, "\n")

pyplot.imshow(A1.data)            
pyplot.show()    

#A1 = [(lambda i: 20000 if i > 48000 else i)(i) for i in A1.data]
#A1 = np.where(A1.data>48000, 48000, A1.data)
A1.data[A1.data > 48000] = 20000

pyplot.imshow(A1.data)            
pyplot.show()    

A1.write('treatment_naica20000.mar2300')