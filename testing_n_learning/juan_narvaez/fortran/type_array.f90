program hola
    
    type :: multi_var
        integer                              :: multi_size
        integer, allocatable , dimension(:)  :: array1 
    end type multi_var

    type(multi_var) :: multi_new
    multi_new%multi_size = 15
    allocate(multi_new%array1(multi_new%multi_size))
    multi_new%array1 = 3
    write(*,*) multi_new%array1

    
end program hola


