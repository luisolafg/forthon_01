echo
echo This Source Code Form is subject to the terms of the Mozilla Public
echo License, v. 2.0. If a copy of the MPL was not distributed with this
echo file, You can obtain one at http://mozilla.org/MPL/2.0/.
echo


set CRYSFML="..\..\..\..\miscellaneous\new_compiled_tools\crysfml_snapshot_nov_2017"

echo "anaelu_C_L_I  Program Compilation -static"

 gfortran -c -O2 -ffree-line-length-none -funroll-loops            input_args.f90

 gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC    cif2cfl.f90


echo " Linking"

 gfortran -o anaelu_cif2cfl.exe *.o  -L%CRYSFML%\GFortran\LibC -static -lcrysfml

del *.o *.mod

