#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#

rm *.o *.mod

export CRYSFML="../../../../miscellaneous/new_compiled_tools/crysfml_snapshot_n02_nov_2017"

# "anaelu_C_L_I  Program Compilation -static"

gfortran -c -O2 -ffree-line-length-none -funroll-loops -I$CRYSFML/GFortran64/LibC    Calc_Intens.f90

# " Linking"

gfortran -o calc_intens *.o  -L$CRYSFML/GFortran64/LibC -static -lcrysfml

