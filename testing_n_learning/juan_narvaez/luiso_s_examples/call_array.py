def callarray(name = "nobody"):
    var = "hello " + name
    return var

if __name__ == "__main__":
    vartoprint = callarray( "bart" )
    print(vartoprint)

array1 = [1,2,3,4,5]

array2 = [2*i for i in array1]

print (array2)
