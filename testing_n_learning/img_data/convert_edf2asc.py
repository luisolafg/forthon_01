import numpy as np
import fabio
import sys
#from matplotlib import pyplot as plt

#def plott_img(arr):
#    print ("Plotting arr")
#    plt.imshow(  arr , interpolation = "nearest", cmap="gist_stern" )
#    plt.show()

def wr_asc_file(f_name, img_arr):
    Nrows = np.size( img_arr[:, 0:1] )
    Ncols = np.size( img_arr[0:1, :] )

    print "Nrows =", Nrows
    print "Ncols =", Ncols
    print "writing file ..."

    myfile = open(f_name, "w")
    for row in xrange(Nrows):
        for col in xrange(Ncols):
            myfile.write(str(img_arr[row,col]) + "\n")

    myfile.close()
    print "... writing done"

if( __name__ == "__main__" ):
    path_to_img_1 = "APT73_d122_from_m02to02__01_41.mar2300-cut.edf"
    print "loading: ", path_to_img_1
    img_arr = fabio.open(path_to_img_1).data.astype(np.float64)
    #exp_img_arr[1020:1190,1125:1180] = -2

    print "loaded done"
    #plott_img(img_arr)

    wr_asc_file("cuted_img.asc", img_arr)
