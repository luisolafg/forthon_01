import numpy as np
import fabio
from matplotlib import pyplot as plt

img_arr = fabio.open("APT73_d122_from_m02to02__01_41.mar2300").data.astype(np.float64)
img_arr[950:1150,1125:1175] = 0.0

plt.imshow(img_arr, interpolation = "nearest" )
plt.show()

res_img = fabio.edfimage.edfimage()
res_img.data = img_arr
res_img.write("APT73_ddd_no_direct_beam.edf")
