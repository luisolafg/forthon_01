class kulungele(object):
    #defining class kulungele
    def __init__(self, first_value):
        #first function to be called
        self.my_num = 0
        print "Hello"
        print "self.my_num(before) = ", self.my_num
        self.my_num = first_value
        print "self.my_num(after) = ", self.my_num

    def cuad(self):
        return self.my_num ** 2


if( __name__ == "__main__" ):
    # object a which is an instance of kulungele
    a = kulungele(2)
    # object b which is an instance of kulungele
    b = kulungele(3)
    print "cuad =", a.cuad()
    print "cuad =", b.cuad()
