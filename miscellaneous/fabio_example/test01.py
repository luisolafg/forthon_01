import numpy as np
import fabio

if( __name__ == "__main__" ):

    path_to_img_1 = "../new_interface/code_sample_05/img_file.edf"

    obj_fabio = fabio.open(path_to_img_1)
    img_1_in = obj_fabio.data.astype(np.float64)
    xres, yres = np.shape(img_1_in)


    print("xres, yres =", xres, yres)

    ar_2d_img = np.zeros((xres,yres), np.float64)
    ar_2d_img[:,:] = img_1_in[:,:]
    ar_2d_img[5:1150,3:1130] = 100000.0

    # here, for Juan
    new_img = fabio.edfimage.edfimage()
    new_img.data = ar_2d_img
    new_img.write("file_out.edf")






