import numpy as np

class kulungele(object):
    #defining class kulungele
    def __init__(self, first_value):
        #first function to be called
        self.my_num = first_value
        print("self.my_num = ", self.my_num)

    def cuad(self):
        return self.my_num ** 2

    def loga(self):
        return np.log(self.my_num)


if( __name__ == "__main__" ):
    # object a which is an instance of kulungele
    a = kulungele(2)
    # object b which is an instance of kulungele
    b = kulungele(3)
    print("cuad(a) =", a.cuad())
    print("cuad(b) =", b.cuad())

    print("loga(a) =", a.loga())
    print("loga(b) =", b.loga())
