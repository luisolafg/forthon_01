import fabio
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


#-------------1
#Normal surface
z = np.loadtxt('observ.pat')
Z = z / 1000
X,Y = np.meshgrid(np.linspace(0,90,91),np.linspace(0,180,181))
print(Z)
'''
new_img = fabio.edfimage.edfimage()
new_img.data = Z
new_img.write("file_out.edf")
'''

#-------------2
#Cutted surface
z2 = np.loadtxt('cut.pat')
Z2 = z2 / 1000
X2,Y2 = np.meshgrid(np.linspace(0,90,91),np.linspace(0,180,181))
'''
new_img = fabio.edfimage.edfimage()
new_img.data = Z2
new_img.write("file_out.edf")
'''
#---------------3
#smoothed surface
z3 = np.loadtxt('smooth.pat')
Z3 = z3 / 1000
X3,Y3 = np.meshgrid(np.linspace(0,90,91),np.linspace(0,180,181))

new_img = fabio.edfimage.edfimage()
new_img.data = Z3
new_img.write("file_out.edf")



#coloring surfaces
