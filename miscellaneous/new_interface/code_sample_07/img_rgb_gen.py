import numpy as np
import time

class np2bmp_heat(object):
    def __init__(self):

        self.red_byte = np.empty( (255 * 3), 'int')
        self.green_byte = np.empty( (255 * 3), 'int')
        self.blue_byte = np.empty( (255 * 3), 'int')

        for i in range(255):
            self.red_byte[i] = i
            self.green_byte[i + 255] = i
            self.blue_byte[i + 255 * 2 ] = i

        self.red_byte[255:255 * 3] = 255
        self.green_byte[0:255] = 0
        self.green_byte[255 * 2 : 255 * 3] = 255
        self.blue_byte[0:255 * 2] = 0

        self.blue_byte[764] = 255
        self.red_byte[764] = 255
        self.green_byte[764] = 255


    def img_2d_rgb(self, data2d = None, invert = False,
                   sqrt_scale = False, i_min_max = [None, None]):

        data2d_min = data2d.min()
        data2d_max = data2d.max()

        self.local_min_max = [float(data2d_min), float(data2d_max)]
        if(i_min_max == [None, None]):
            print("no max and min provided")

        else:
            if(i_min_max[0] < data2d_min):
                data2d_min = i_min_max[0]

            if(i_min_max[1] > data2d_max):
                data2d_max = i_min_max[1]

        self.width = np.size( data2d[0:1, :] )
        self.height = np.size( data2d[:, 0:1] )

        data2d_pos = data2d[:,:] - data2d_min + 1.0
        data2d_pos_max = data2d_pos.max()

        calc_pos_max = data2d_max - data2d_min + 1.0
        if(calc_pos_max > data2d_pos_max):
            data2d_pos_max = calc_pos_max

        div_scale = 764.0 / data2d_pos_max

        #print "calc_pos_max =", calc_pos_max

        #print "data2d_pos_max =", data2d_pos_max, "\n"

        data2d_scale = np.multiply(data2d_pos, div_scale)

        if(sqrt_scale == True):
            for x in np.nditer(data2d_scale[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = np.sqrt(x[...])

            div_scale = 764.0 / data2d_scale.max()
            data2d_scale = np.multiply(data2d_scale, div_scale)

        if(invert == True):
            for x in np.nditer(data2d_scale[:,:], op_flags=['readwrite'], flags=['external_loop']):
                x[...] = 764.0 - x[...]

        #img_array = np.empty( (self.height ,self.width, 3),'uint8')
        img_array = np.zeros([self.width, self.height, 4], dtype=np.uint8)

        img_array_r = np.empty( (self.height, self.width), 'int')
        img_array_g = np.empty( (self.height, self.width), 'int')
        img_array_b = np.empty( (self.height, self.width), 'int')

        scaled_i = np.empty( (self.height, self.width), 'int')
        scaled_i[:,:] = data2d_scale[:,:]

        img_array_r[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_r[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.red_byte[x]

        img_array_g[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_g[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.green_byte[x]

        img_array_b[:,:] = scaled_i[:,:]
        for x in np.nditer(img_array_b[:,:], op_flags=['readwrite'], flags=['external_loop']):
            x[...] = self.blue_byte[x]

        img_array[:, :, 3] = 255
        img_array[:, :, 2] = img_array_r[:,:] #Blue
        img_array[:, :, 1] = img_array_g[:,:] #Green
        img_array[:, :, 0] = img_array_b[:,:] #Red


        return img_array
