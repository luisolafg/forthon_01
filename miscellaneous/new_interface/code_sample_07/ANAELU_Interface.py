import sys
import os
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *
import subprocess
import fabio
import numpy as np
from img_rgb_gen import np2bmp_heat
from grid_atoms_xyz import read_cfl_atoms,AtomData
from readcfl import read_cfl_file,CflData
import time


class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)

    def wheelEvent(self, event):
        int_delta = int(event.delta())
        self.m_scrolling.emit(int_delta)
        event.accept()


class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("Anaelu_gui.ui")
        self.window.setWindowTitle("ANAELU 2.0")
        self.window.setWindowIcon(QIcon("anaelu.png"))

        self.gscene_list = []

        for n in range(4):
            new_gsecene = MultipleImgSene()
            new_gsecene.m_scrolling.connect(self.scale_all_views)
            self.gscene_list.append(new_gsecene)

        self.gview_list = [self.window.graphicsView_Exp, self.window.graphicsView_Mask_n_Diff,
                           self.window.graphicsView_Modl, self.window.graphicsView_Bakgr]

        for n, gview in enumerate(self.gview_list):
            gview.setScene(self.gscene_list[n])
            gview.setDragMode(QGraphicsView.ScrollHandDrag)

        self.window.pushButton_load_exp.clicked.connect(self.set_img1)
        #self.window.pushButton_mask_model.clicked.connect(self.set_img2)
        self.window.run_model.clicked.connect(self.set_img3)
        self.window.background_smoothing.clicked.connect(self.set_img4)

        self.window.background_smoothing.clicked.connect(self.set_img4)
        self.window.background_smoothing.clicked.connect(self.set_img4)

        #open read cfl
        self.window.del_row_button.clicked.connect(self.remove_r)
        self.window.add_row_button.clicked.connect(self.add_r)
        self.window.actionOpen_Cfl.triggered.connect(self.read_r)
        self.window.actionSave_Cfl.triggered.connect(self.savefile)
        self.window.actionOpen_image.triggered.connect(self.set_img2)

        #colums table widget
        columns = ['z','Label','Xpos','Ypos','Zpos','DWF']
        self.window.tableWidget_ini.setColumnCount(len(columns))
        self.window.tableWidget_ini.setHorizontalHeaderLabels(columns)

        #Tooltip add
        self.window.pushButton_load_exp.setToolTip("Load exp tooltip")
        self.window.pushButton_mask_model.setToolTip("Draw mask tooltip")
        self.window.run_model.setToolTip("Run model tooltip")
        self.window.background_smoothing.setToolTip("Smooth tooltipgit")        

        for gview in self.gview_list:
            gview.verticalScrollBar().valueChanged.connect(self.scroll_all_v_bars)
            gview.horizontalScrollBar().valueChanged.connect(self.scroll_all_h_bars)

        self.real_time_zoom = False
        self.value_backup_h = 0
        self.value_backup_v = 0

        self.bmp_heat = np2bmp_heat()

        self.add = 1

        self.window.show()

    def scroll_all_v_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.verticalScrollBar().setValue(value)

            self.value_backup_v = value

    def scroll_all_h_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.horizontalScrollBar().setValue(value)

            self.value_backup_h = value

    def scale_all_views(self, scale_fact):
        self.real_time_zoom = True
        for gview in self.gview_list:
            gview.scale(1.0 + float(scale_fact) / 2500.0,
                        1.0 + float(scale_fact) / 2500.0)

        self.real_time_zoom = False

    def set_img_n(self, img_path, gscene_num):
        image1 = QImage(img_path)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.gscene_list[gscene_num].addPixmap(self.pixmap_1)

        self.num_to_move = gscene_num
        QTimer.singleShot(100, self.emit_move)


    def emit_move(self):
        self.pos_img_n(self.num_to_move)

    def pos_img_n(self, gview_num):
        self.gview_list[gview_num].horizontalScrollBar().setValue(self.value_backup_h)
        self.gview_list[gview_num].verticalScrollBar().setValue(self.value_backup_v)

    def set_img1(self):
        self.set_img_n("../../lena.jpeg", 0)
        subprocess.call("./anaelu_calc_mask my_params.dat My_Cfl.cfl img_mask.edf raw_img1.raw", shell=True)

    def setqimg(self, img_path, gscene_num):
        self.pixmap_1 = QPixmap.fromImage(img_path)
        self.gscene_list[gscene_num].addPixmap(self.pixmap_1)
        self.bmp_heat = np2bmp_heat()
        self.num_to_move = gscene_num
        QTimer.singleShot(100, self.emit_move)

    def set_img2(self):
        print("self.btn_clk")
        self.info1 = QFileDialog.getOpenFileName(self, 'Open File', ".", "Files (*.*)")[0]
        
        self.path = self.info1
        img_arr = fabio.open(self.path).data.astype("float64")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )


        self.setqimg(q_img, 1)

    def set_img3(self):
        self.set_img_n("../../lena_gray_inverted", 2)
        print ("run model")
                 

    def set_img4(self):
        self.set_img_n("../../lena_gray_inverted.jpeg", 3)

    def remove_r(self):
        print("Row removed")
        self.window.tableWidget_ini.removeRow(self.window.tableWidget_ini.model().rowCount()-1)

    def add_r(self):
        print("Row Added")
        self.window.tableWidget_ini.insertRow(self.window.tableWidget_ini.model().rowCount())
        print(self.window.tableWidget_ini.model().rowCount())

    def read_r(self):
        self.info1 = QFileDialog.getOpenFileName(self, 'Open File', ".", "Files (*.*)")[0]
        print("info from clf", self.info1)

        self.data = read_cfl_file(my_file_path = self.info1)
        
        self.window.title_edit.setText(str(self.data.title))
        self.window.spgr_edit.setText(str(self.data.spgr))
        self.window.a_edit.setText(str(self.data.a))
        self.window.b_edit.setText(str(self.data.b))
        self.window.c_edit.setText(str(self.data.c))
        self.window.a2_edit.setText(str(self.data.alpha))
        self.window.b2_edit.setText(str(self.data.beta))
        self.window.c2_edit.setText(str(self.data.gamma))
        self.window.sizelg_edit.setText(str(self.data.size_lg))
        self.window.ipfres_edit.setText(str(self.data.ipf_res))
        self.window.h_edit.setText(str(self.data.h))
        self.window.k_edit.setText(str(self.data.k))
        self.window.l_edit.setText(str(self.data.l))
        self.window.hklwh_edit.setText(str(self.data.hklwh))
        self.window.azmipf_edit.setText(str(self.data.azm_ipf))
        self.window.maskmin_edit.setText(str(self.data.mask_min))

        self.info2 = read_cfl_atoms(my_file_path = self.info1)
        self.window.tableWidget_ini.setRowCount(len(self.info2))
        for n,atm in enumerate(self.info2):

            self.window.tableWidget_ini.setItem(n,0,QTableWidgetItem(atm.Zn))
            self.window.tableWidget_ini.setItem(n,1,QTableWidgetItem(atm.Lbl))
            self.window.tableWidget_ini.setItem(n,2,QTableWidgetItem(atm.x))
            self.window.tableWidget_ini.setItem(n,3,QTableWidgetItem(atm.y))
            self.window.tableWidget_ini.setItem(n,4,QTableWidgetItem(atm.z))
            self.window.tableWidget_ini.setItem(n,5,QTableWidgetItem(atm.b))


    def savefile(self):

        self.info1 = QFileDialog.getSaveFileName(self, 'Save File', os.getenv('HOME'), filter='*.cfl')[0]
        print("save from cfl1=", self.info1,"\n")
        save_atm = []
        print("range of table=",range(self.window.tableWidget_ini.rowCount()),"\n")
        for d in range(self.window.tableWidget_ini.rowCount()):
            print("print d= ", d)
            atm = AtomData()
            atm.Zn = str(self.window.tableWidget_ini.item(d, 0).text())
            atm.Lbl = str(self.window.tableWidget_ini.item(d, 1).text())
            atm.x = float(self.window.tableWidget_ini.item(d,2).text())
            atm.y = float(self.window.tableWidget_ini.item(d,3).text())
            atm.z = float(self.window.tableWidget_ini.item(d,4).text())
            atm.b = float(self.window.tableWidget_ini.item(d,5).text())

            save_atm.append(atm)
 
        data = CflData()
        data.title = self.window.title_edit.text()
        data.spgr = self.window.spgr_edit.text()
        data.a = self.window.a_edit.text()
        data.b = self.window.b_edit.text()
        data.c = self.window.c_edit.text()
        data.alpha = self.window.a2_edit.text()
        data.beta = self.window.b2_edit.text()
        data.gamma = self.window.c2_edit.text()
        data.size_lg = self.window.sizelg_edit.text()
        data.ipf_res = self.window.ipfres_edit.text()
        data.h = self.window.h_edit.text()
        data.k = self.window.k_edit.text()
        data.l = self.window.l_edit.text()
        data.hklwh = self.window.hklwh_edit.text()
        data.azm_ipf = self.window.azmipf_edit.text()
        data.mask_min = self.window.maskmin_edit.text()

        crystal = "Title  "
        crystal += str(data.title) + " \n" 
        crystal += "!" + "        a" + "        b" + "        c" + \
                "     alpha" + "    beta" + "   gamma" + "\n"
        crystal += "Cell    " + str(data.a) + "     " + str(data.b) + "     " +\
                str(data.c) + "  " + str(data.alpha) + "  " + str(data.beta) + \
                "  " + str(data.gamma) + "\n"
        crystal += "!     " + "Space Group" + "\n"
        crystal += "Spgr  " + str(data.spgr) + "\n"
        crystal += "SIZE_LG " + str(data.size_lg) + "\n"
        crystal += "IPF_RES " + str(data.ipf_res) + "\n"
        crystal += "HKL_PREF " + str(data.h) + " " + str(data.k) + " " + str(data.l) + "\n"
        crystal += "HKL_PREF_WH " + str(data.hklwh) + "\n"
        crystal += "AZM_IPF " + str(data.azm_ipf) + "\n"
        crystal += "MASK_MIN " + str(data.mask_min) + "\n"
        crystal += "!" + "\n"
        
        cfl_data = str(crystal)

        self.info2 = open(self.info1, 'w')
        for c in save_atm:
            print("data of table=",c.Zn,c.Lbl,c.x,c.y,c.z,c.b,"\n")
            var = "Atom "
            var += str(c.Zn) + "  "
            var += str(c.Lbl) + "  "
            var += str(c.x) + "  "
            var += str(c.y) + "  "
            var += str(c.z) + "  "
            var += str(c.b) + " \n"
            cfl_data += str(var)
        
        self.info2.write(cfl_data)
        self.info2.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
