import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

from PySide2 import QtUiTools

class Form(QObject):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("tablewidget.ui")

        self.window.QTableWidget()
        self.window.tableWidget.setRowCount(4)
        self.window.tableWidget.setColumnCount(4)
        self.window.show()
