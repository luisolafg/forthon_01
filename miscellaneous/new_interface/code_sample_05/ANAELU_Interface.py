import sys
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *
from subprocess import *
import fabio
import numpy as np
from img_rgb_gen import np2bmp_heat
from readcfl import read_cfl_file






class MultipleImgSene(QGraphicsScene):
    m_scrolling = Signal(int)
    def __init__(self, parent = None):
        super(MultipleImgSene, self).__init__(parent)

    def wheelEvent(self, event):
        int_delta = int(event.delta())
        self.m_scrolling.emit(int_delta)
        event.accept()


class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)

        self.window = QtUiTools.QUiLoader().load("ANAELU_interface.ui")

        self.gscene_list = []

        for n in range(4):
            new_gsecene = MultipleImgSene()
            new_gsecene.m_scrolling.connect(self.scale_all_views)
            self.gscene_list.append(new_gsecene)

        self.gview_list = [self.window.graphicsView_Exp, self.window.graphicsView_Mask_n_Diff,
                           self.window.graphicsView_Modl, self.window.graphicsView_Bakgr]

        for n, gview in enumerate(self.gview_list):
            gview.setScene(self.gscene_list[n])
            gview.setDragMode(QGraphicsView.ScrollHandDrag)

        self.window.pushButton_load_exp.clicked.connect(self.set_img1)
        self.window.pushButton_mask_model.clicked.connect(self.set_img2)
        self.window.run_model.clicked.connect(self.set_img3)
        self.window.background_smoothing.clicked.connect(self.set_img4)

        for gview in self.gview_list:
            gview.verticalScrollBar().valueChanged.connect(self.scroll_all_v_bars)
            gview.horizontalScrollBar().valueChanged.connect(self.scroll_all_h_bars)

        self.real_time_zoom = False
        self.value_backup_h = 0
        self.value_backup_v = 0

        self.bmp_heat = np2bmp_heat()

        self.info = read_cfl_file(my_file_path = "My_Cfl.cfl")


        #assign text mannually
        print (self.info.a)


        self.window.title_edit.setText(self.info.new_inputtitle)

        self.window.a_edit.setText(self.info.a)
        self.window.b_edit.setText(self.info.b)
        self.window.c_edit.setText(self.info.c)

        self.window.a2_edit.setText(self.info.alpha)
        self.window.b2_edit.setText(self.info.beta)
        self.window.c2_edit.setText(self.info.gamma)

        self.window.space_group_edit.setText(self.info.spgr)
        self.window.average_crystal_edit.setText(self.info.lg_size)
        self.window.IPF_steps_edit.setText(self.info.new_avg_strain)
        self.window.h_edit.setText(self.info.h)
        self.window.k_edit.setText(self.info.k)
        self.window.l_edit.setText(self.info.l)
        self.window.IPF_FWHM_edit.setText(self.info.azm_ipf)
        self.window.IPF_steps_edit.setText(self.info.mask_min)

        #self.window.z_edit.setText()
        #self.window.label_edit.setText()
        #self.window.xpos_edit.setText()
        #self.window.ypos_edit.setText()
        #self.window.zpos_edit.setText()
        #self.window.dwf_edit.setText()

        self.window.show()

    def scroll_all_v_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.verticalScrollBar().setValue(value)

            self.value_backup_v = value

    def scroll_all_h_bars(self, value):
        if self.real_time_zoom == False:
            for other_gview in self.gview_list:
                other_gview.horizontalScrollBar().setValue(value)

            self.value_backup_h = value

    def scale_all_views(self, scale_fact):
        self.real_time_zoom = True
        for gview in self.gview_list:
            gview.scale(1.0 + float(scale_fact) / 2500.0,
                        1.0 + float(scale_fact) / 2500.0)

        self.real_time_zoom = False

    def set_img_n(self, img_path, gscene_num):
        image1 = QImage(img_path)
        self.pixmap_1 = QPixmap.fromImage(image1)
        self.gscene_list[gscene_num].addPixmap(self.pixmap_1)

        self.num_to_move = gscene_num
        QTimer.singleShot(100, self.emit_move)


    def emit_move(self):
        self.pos_img_n(self.num_to_move)

    def pos_img_n(self, gview_num):
        self.gview_list[gview_num].horizontalScrollBar().setValue(self.value_backup_h)
        self.gview_list[gview_num].verticalScrollBar().setValue(self.value_backup_v)

    def set_img1(self):
        self.set_img_n("../../lena.jpeg", 0)

    def setqimg(self, img_path, gscene_num):
        self.pixmap_1 = QPixmap.fromImage(img_path)
        self.gscene_list[gscene_num].addPixmap(self.pixmap_1)
        self.bmp_heat = np2bmp_heat()
        self.num_to_move = gscene_num
        QTimer.singleShot(100, self.emit_move)

    def set_img2(self):
        print("self.btn_clk")

        path = "../code_sample_04/img_file.edf"
        img_arr = fabio.open(path).data.astype("float64")

        rgb_np = self.bmp_heat.img_2d_rgb(img_arr)


        print(np.size(rgb_np[0:1, :, 0:1]))
        print(np.size(rgb_np[:, 0:1, 0:1]))

        q_img = QImage(
            rgb_np.data,
            np.size(rgb_np[0:1, :, 0:1]),
            np.size(rgb_np[:, 0:1, 0:1]),
            QImage.Format_ARGB32
        )


        self.setqimg(q_img, 1)

    def set_img3(self):
        self.set_img_n("../../lena_gray_inverted", 2)
        print ("run model")
        call("./anaelu_calc_xrd param_01.dat sepiolite.cfl img_run.edf raw_img2.raw", shell=True)
        print ("finish run model")

    def set_img4(self):
        self.set_img_n("../../lena_gray_inverted.jpeg", 3)

    #def read cfls



    #def Write cfls



if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
