import fabio
from matplotlib import pyplot

if __name__ == "__main__":
    with fabio.open_series(first_filename="img_file.edf") as series:
        for frame in series.frames():
            frame.data
            frame.header
            frame.index                    # frame index inside the file series
            frame.file_index               # frame index inside the edf file
            frame.file_container.filename

    img = fabio.open("img_file.edf")

    print (img.header)

    img2 = img.write('normed_0001.data')


    # Load matplotlib
    pyplot.imshow(img.data)
    pyplot.show()
