import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import uic

class Form(QObject):

    def __init__(self, parent=None):
        super(Form, self).__init__(parent)

        self.window = uic.loadUi("mainwindow.ui")

        fileName = "../../lena.jpeg"
        image = QImage(fileName)

        self.label = QLabel()
        self.label.setPixmap(QPixmap.fromImage(image))
        self.window.scrollArea.setWidget(self.label)

        image_2 = QImage(fileName)
        self.label_2 = QLabel()
        self.label_2.setPixmap(QPixmap.fromImage(image_2))
        self.window.scrollArea_2.setWidget(self.label_2)

        self.window.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())

