import sys
import os
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2 import QtUiTools
from PySide2.QtGui import *
from grid_atoms_xyz import read_cfl_atoms,AtomData
from readcfl import read_cfl_file,CflData



class Form(QDialog):
    def __init__(self, parent = None):
        super(Form, self).__init__(parent)
        self.window = QtUiTools.QUiLoader().load("test2.ui")

        self.window.delButtonY.clicked.connect(self.remove_r)
        self.window.addButtonY.clicked.connect(self.add_r)
        self.window.readButton.clicked.connect(self.read_r)
        self.window.writeButton.clicked.connect(self.savefile)

        columns = ['z','Label','Xpos','Ypos','Zpos','DWF']
        self.window.tableWidget_ini.setColumnCount(len(columns))
        self.window.tableWidget_ini.setHorizontalHeaderLabels(columns)



        self.window.show()

        self.info = read_cfl_file(my_file_path = "My_Cfl.cfl")
        #self.info2 = read_cfl_atoms(my_file_path = "cfl_out.cfl")



    def remove_r(self):
        print("Row removed")
        self.window.tableWidget_ini.removeRow(self.window.tableWidget_ini.model().rowCount()-1)

    def add_r(self):
        print("Row Added")
        self.window.tableWidget_ini.insertRow(self.window.tableWidget_ini.model().rowCount())
        print(self.window.tableWidget_ini.model().rowCount())

    def read_r(self):
        self.info1 = QFileDialog.getOpenFileName(self, 'Open File', ".", "Files (*.*)")[0]
        print("info from clf", self.info1)
        ####
        self.data = read_cfl_file(my_file_path = self.info1)
        
        self.window.title_edit.setText(str(self.data.title))
        self.window.spgr_edit.setText(str(self.data.spgr))
        self.window.a_edit.setText(str(self.data.a))
        self.window.b_edit.setText(str(self.data.b))
        self.window.c_edit.setText(str(self.data.c))
        self.window.a2_edit.setText(str(self.data.alpha))
        self.window.b2_edit.setText(str(self.data.beta))
        self.window.c2_edit.setText(str(self.data.gamma))
        self.window.sizelg_edit.setText(str(self.data.size_lg))
        self.window.ipfres_edit.setText(str(self.data.ipf_res))
        self.window.h_edit.setText(str(self.data.h))
        self.window.k_edit.setText(str(self.data.k))
        self.window.l_edit.setText(str(self.data.l))
        self.window.hklwh_edit.setText(str(self.data.hklwh))
        self.window.azmipf_edit.setText(str(self.data.azm_ipf))
        self.window.maskmin_edit.setText(str(self.data.mask_min))


        self.info2 = read_cfl_atoms(my_file_path = self.info1)
        self.window.tableWidget_ini.setRowCount(len(self.info2))
        for n,atm in enumerate(self.info2):

            self.window.tableWidget_ini.setItem(n,0,QTableWidgetItem(atm.Zn))
            self.window.tableWidget_ini.setItem(n,1,QTableWidgetItem(atm.Lbl))
            self.window.tableWidget_ini.setItem(n,2,QTableWidgetItem(atm.x))
            self.window.tableWidget_ini.setItem(n,3,QTableWidgetItem(atm.y))
            self.window.tableWidget_ini.setItem(n,4,QTableWidgetItem(atm.z))
            self.window.tableWidget_ini.setItem(n,5,QTableWidgetItem(atm.b))


    def savefile(self):

        self.info1 = QFileDialog.getSaveFileName(self, 'Save File', os.getenv('HOME'))[0]
        print("save from cfl1=", self.info1,"\n")
        save_atm = []
        print("range of table=",range(self.window.tableWidget_ini.rowCount()),"\n")
        for d in range(self.window.tableWidget_ini.rowCount()):
            print("print d= ", d)
            atm = AtomData()
            atm.Zn = str(self.window.tableWidget_ini.item(d, 0).text())
            atm.Lbl = str(self.window.tableWidget_ini.item(d, 1).text())
            atm.x = float(self.window.tableWidget_ini.item(d,2).text())
            atm.y = float(self.window.tableWidget_ini.item(d,3).text())
            atm.z = float(self.window.tableWidget_ini.item(d,4).text())
            atm.b = float(self.window.tableWidget_ini.item(d,5).text())

            save_atm.append(atm)
 
        data = CflData()
        data.title = self.window.title_edit.text()
        data.spgr = self.window.spgr_edit.text()
        data.a = self.window.a_edit.text()
        data.b = self.window.b_edit.text()
        data.c = self.window.c_edit.text()
        data.alpha = self.window.a2_edit.text()
        data.beta = self.window.b2_edit.text()
        data.gamma = self.window.c2_edit.text()
        data.size_lg = self.window.sizelg_edit.text()
        data.ipf_res = self.window.ipfres_edit.text()
        data.h = self.window.h_edit.text()
        data.k = self.window.k_edit.text()
        data.l = self.window.l_edit.text()
        data.hklwh = self.window.hklwh_edit.text()
        data.azm_ipf = self.window.azmipf_edit.text()
        data.mask_min = self.window.maskmin_edit.text()

        crystal = "Title  "
        crystal += str(data.title) + " \n" 
        crystal += "!" + "        a" + "        b" + "        c" + \
                "     alpha" + "    beta" + "   gamma" + "\n"
        crystal += "Cell    " + str(data.a) + "     " + str(data.b) + "     " +\
                str(data.c) + "  " + str(data.alpha) + "  " + str(data.beta) + \
                "  " + str(data.gamma) + "\n"
        crystal += "!     " + "Space Group" + "\n"
        crystal += "Spgr  " + str(data.spgr) + "\n"
        crystal += "SIZE_LG " + str(data.size_lg) + "\n"
        crystal += "IPF_RES " + str(data.ipf_res) + "\n"
        crystal += "HKL_PREF " + str(data.h) + " " + str(data.k) + " " + str(data.l) + "\n"
        crystal += "HKL_PREF_WH " + str(data.hklwh) + "\n"
        crystal += "AZM_IPF " + str(data.azm_ipf) + "\n"
        crystal += "MASK_MIN " + str(data.mask_min) + "\n"
        crystal += "!" + "\n"
        
        cfl_data = str(crystal)

        self.info2 = open(self.info1, 'w')
        for c in save_atm:
            print("data of table=",c.Zn,c.Lbl,c.x,c.y,c.z,c.b,"\n")
            var = "Atom "
            var += str(c.Zn) + "  "
            var += str(c.Lbl) + "  "
            var += str(c.x) + "  "
            var += str(c.y) + "  "
            var += str(c.z) + "  "
            var += str(c.b) + " \n"
            cfl_data += str(var)
        
        self.info2.write(cfl_data)
        self.info2.close()



if __name__ == '__main__':
    app = QApplication(sys.argv)
    form = Form()
    sys.exit(app.exec_())
    sys.getfilesystemencoding()

