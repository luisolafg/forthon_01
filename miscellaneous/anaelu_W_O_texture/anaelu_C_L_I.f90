! This file is part of the "Python-Fortran version of Anaelu" project,
! a tool for the treatment of 2D-XRD patterns of textured samples
!
! This Source Code Form is subject to the terms of the Mozilla Public
! License, v. 2.0. If a copy of the MPL was not distributed with this
! file, You can obtain one at http://mozilla.org/MPL/2.0/.

program anaelu_CLI

    use global_types_and_dependencies
    use CLI_sample_data,  only: my_kargs, dat_2d_det, opn_fil

    Use Calc_2D_pat,      only: ini_2d_det, ini_intens, ipf_ini, ipf_calc, &
                                calc_debye_prof, Calc_2D_img

    use mic_2D_tools,     only: wr_img_file, write_1d_info, write_debye_prof, write_1d_patern

    IMPLICIT NONE

    call my_kargs()
    call dat_2d_det()
    call ini_2d_det()
    call opn_fil()
    call ini_intens()
!    call ipf_ini()
!    call ipf_calc()
    call write_1d_info()

!    call calc_debye_prof()
!    call Calc_2D_img()

!    call write_debye_prof()
!    call wr_img_file()
!    call write_1d_patern()


end program anaelu_CLI
