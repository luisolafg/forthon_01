echo
echo This Source Code Form is subject to the terms of the Mozilla Public
echo License, v. 2.0. If a copy of the MPL was not distributed with this
echo file, You can obtain one at http://mozilla.org/MPL/2.0/.
echo

set CRYSFML="..\new_compiled_tools\crysfml_snapshot_n02_nov_2017"

echo "anaelu_C_L_I  Program Compilation -static"

 gfortran -c -O2 -ffree-line-length-none -funroll-loops       gfortran_pbar.f90
 gfortran -c -O2 -ffree-line-length-none -funroll-loops       input_args.f90
 gfortran -c -O2 -ffree-line-length-none -funroll-loops       rw_n_use_edf.f90

 gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC  global_types_n_deps.f90
 gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC            mic_tools.f90
 gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC              calc_2d.f90
 gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC      sample_CLI_data.f90

 gfortran -c -O2 -ffree-line-length-none -funroll-loops -I%CRYSFML%\GFortran\LibC         anaelu_C_L_I.f90

echo " Linking"

 gfortran -o anaelu_1d_powdr.exe *.o  -L%CRYSFML%\GFortran\LibC -static -lcrysfml

del *.o *.mod
