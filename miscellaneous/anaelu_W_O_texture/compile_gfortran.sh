#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
CRYSFML=../new_compiled_tools/crysfml_snapshot_n02_nov_2017
INC="-I$CRYSFML/GFortran64/LibC"
LIB="-L$CRYSFML/GFortran64/LibC"
LIBSTATIC=" -static -lcrysfml"

#OPT1="-c -g -debug full -CB -vec-report0"  # ifort ongly

OPT1="-c -O -ffree-line-length-none -funroll-loops" # same as CRYSFML

#OPT1="-c -g -fbacktrace -ffree-line-length-none" #debug mode

echo "anaelu_C_L_I >> Program Compilation -static"

 gfortran $OPT1                                   gfortran_pbar.f90
 gfortran $OPT1                                      input_args.f90

 gfortran $OPT1                                    rw_n_use_edf.f90

 gfortran $OPT1 $INC                        global_types_n_deps.f90
 gfortran $OPT1 $INC                                  mic_tools.f90
 gfortran $OPT1 $INC                                    calc_2d.f90
 gfortran $OPT1 $INC                            sample_CLI_data.f90

 gfortran $OPT1 $INC                               anaelu_C_L_I.f90

echo " Linking"

 gfortran -o anaelu_1d_xrd.exe *.o  $LIB $LIBSTATIC
rm *.o *.mod


