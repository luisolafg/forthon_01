#Download miniconda installer from:
https://conda.io/miniconda.html

#then type:
bash Miniconda2-latest-Linux-x86_64.sh

conda install numpy -y
conda install wxpython=3.0.0.0 -y
conda install -c fable fabio=0.3.0 -y
conda install lxml -y

# til here all works great with conda and anaelu
# now complementary conda packages:

#conda install -c fable pycifrw=4.1.1
#conda install lxml
#conda install matplotlib

#http://forge.epn-campus.eu/svn/crysfml for downloading with svn

'''
for emacs as default

as root:
 update-alternatives --install /usr/bin/editor editor /usr/bin/emacs 100
or
 update-alternatives --install /usr/bin/editor editor /usr/bin/emacs25-nox 100

as user:

 export export EDITOR=emacs
'''
