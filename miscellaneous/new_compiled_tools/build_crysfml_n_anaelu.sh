#  This file is part of the Anaelu Project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo " "
echo "Building Anaelu with its BIG dependency CRYSFML included"
echo " "
cd crysfml_snapshot_n02_nov_2017/
export CRYSFML=$(pwd)
cd Scripts/
./makecrys_lin gfortran     # regular mode
#./makecrys_lin gfortran deb # debug mode
echo " "
echo " "
echo "Building ANAELU inside anaelu_cli_tools"
echo " "
cd ../../anaelu_cli_tools_01/
echo " "
./make_anaelu_gfortran.sh
echo " "
./make_2d_mask_builder.sh                            #it works fine in my anaelu_test
echo " "
echo "done building Anaelu gfortran inside anaelu_cli_tools "
echo " "
