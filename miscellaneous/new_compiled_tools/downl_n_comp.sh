#!/bin/bash

rm -rf Miniconda3-latest-Linux-x86_64.sh
rm -rf conda_bundle/

TMP_ROOT=$(pwd)

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -p "$TMP_ROOT/conda_bundle" -b

source $TMP_ROOT/conda_bundle/bin/activate

conda init

conda install numpy -y
conda install -c conda-forge fabio -y
conda install -c anaconda git -y
conda install -c conda-forge pyside2 -y

source build_fcomp_link.sh

bash compile_with_conda.sh

#conda install -c anaconda gfortran_linux-64 -y
#conda install gfortran_linux-64 -y




