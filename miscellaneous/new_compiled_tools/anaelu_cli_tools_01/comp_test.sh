#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
#
echo "removing previous run files"
rm *.o *.mod *.r
COMP="-c -O2"
ROOT_DIR="/home/lui/f90lafg/f90lafg"
LIBC_DIR="code/crysfml_snapshot_n02_nov_2017/GFortran64/LibC"
PBAR_CODE="code/subset_sxtalsoft/Laue_Suite_Project/Laue_Modules/Src"
echo "compiling"
gfortran $COMP $ROOT_DIR-$PBAR_CODE/gfortran_specific.f90
gfortran $COMP -I$ROOT_DIR-$LIBC_DIR calc_2d.f90
gfortran $COMP -I$ROOT_DIR-$LIBC_DIR sample_CLI_data.f90
gfortran $COMP -I$ROOT_DIR-$LIBC_DIR anaelu_C_L_I.f90
echo "linking"
gfortran -o anaelu_calc_xrd *.o -L$ROOT_DIR-$LIBC_DIR -lcrysfml
echo "runnig"
./anaelu_calc_xrd
